/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;

/**
 *
 * @author shughes
 */
public class OfxTest {

	public static void main(String[] args) {
		try {
			StringBuffer ret = new StringBuffer();
			ret.append("<OFX>");
			
			ret.append("<SIGNONMSGSRQV1>");
			ret.append("<SONRQ>");
			ret.append("<DTCLIENT>20080623000000[-4:EDT]");
			ret.append("<USERID>257597207");
			ret.append("<USERPASS>hobson");
			ret.append("<LANGUAGE>ENG");
			ret.append("<FI>");
			ret.append("<ORG>Fifth Third Bank");
			ret.append("<FID>5829");
			ret.append("</FI>");
			ret.append("<APPID>QWIN");
			ret.append("<APPVER>1200");
			ret.append("</SONRQ>");
			ret.append("</SIGNONMSGSRQV1>");

			ret.append("<BANKMSGSRQV1>");
			ret.append("<STMTTRNRQ>");
			ret.append("<TRNUID>" + "0");
			ret.append("<STMTRQ>");
			ret.append("<BANKACCTFROM>");
			ret.append("<BANKID>" + "042000314");
			ret.append("<ACCTID>" + "7020658501");
			ret.append("<ACCTTYPE>" + "CHECKING");
			ret.append("</BANKACCTFROM>");
			ret.append("<INCTRAN>");
			ret.append("<DTSTART>" + "20080623000000[-4:EDT]");
			ret.append("<INCLUDE>Y");
			ret.append("</INCTRAN>");
			ret.append("</STMTRQ>");
			ret.append("</STMTTRNRQ>");
			ret.append("</BANKMSGSRQV1>");
			
			ret.append("</OFX>");

			String data = ret.toString();
			String host = "https://banking.53.com/ofx/OFXServlet";

			URL url = new URL(host);
			HttpsURLConnection conn;
			conn = (HttpsURLConnection) url.openConnection();

			//conn.setRequestProperty("REQUEST_METHOD", "POST");
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-ofx");
			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
			conn.setRequestProperty("Accept", "*/*, application/x-ofx");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(false);

			OutputStream os = conn.getOutputStream();
			os.write(data.getBytes());

			StringBuilder response = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			while (true) {
				int s = reader.read();
				if (s == -1) {
					break;
				}
				response.append((char) s);
			}

			System.out.println("response: " + response.toString());

			conn.disconnect();

		} catch (IOException ex) {
			Logger.getLogger(OfxTest.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
