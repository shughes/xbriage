/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import com.xbriage.config.Config;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author shughes
 */
public class XmlTest {

	private void testOne() {
		try {
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = builderFactory.newDocumentBuilder();
			InputStream input = Config.class.getResourceAsStream("AccountTypes.xml");
			Document doc = builder.parse(input);
			input.close();
			Element accountTypes = doc.getDocumentElement();
			System.out.println("accoun: " + accountTypes.getTagName());
			NodeList typeList = accountTypes.getElementsByTagName("type");
			for (int i = 0; i < typeList.getLength(); i++) {
				Node type = typeList.item(i);

			}

		} catch (SAXException ex) {
			Logger.getLogger(XmlTest.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(XmlTest.class.getName()).log(Level.SEVERE, null, ex);
		} catch (ParserConfigurationException ex) {
			Logger.getLogger(XmlTest.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

	public static void main(String[] args) {
		try {
			URL url = new URL("http://downloads.xbriage.com/Banks.xml");
			BufferedInputStream input = new BufferedInputStream(url.openStream());
			while(true) {
				int num = input.read();
				if(num == -1) {
					break;
				}
				char c = (char) num;
				System.out.println(c);
			}

		} catch (IOException ex) {
			Logger.getLogger(XmlTest.class.getName()).log(Level.SEVERE, null, ex);
		}

	}
}
