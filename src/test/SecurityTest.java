/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import com.xbriage.config.Config;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.DSAParams;
import java.security.interfaces.DSAPrivateKey;
import java.security.interfaces.DSAPublicKey;
import java.security.interfaces.DSAPublicKey;
import java.security.spec.DSAPrivateKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author shughes
 */
public class SecurityTest {

	private void testOne() {
		try {
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DSA");
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
			keyGen.initialize(1024, random);
			KeyPair pair = keyGen.generateKeyPair();

			Signature dsa = Signature.getInstance("SHA1withDSA");
			DSAPrivateKey privKey = (DSAPrivateKey) pair.getPrivate();
			dsa.initSign(privKey);
			byte[] data = "this is my hidden data".getBytes();
			dsa.update(data);
			byte[] sig = dsa.sign();

			DSAPublicKey pubKey = (DSAPublicKey) pair.getPublic();
			//dsa.initVerify(pubKey);
			//dsa.update(data);
			//boolean verifies = dsa.verify(sig);
			//System.out.println("signature verifies: " + verifies);

			Signature dsa2 = Signature.getInstance(pubKey.getAlgorithm());
			dsa2.initVerify(pubKey);
			dsa2.update(data, 0, data.length);
			System.out.println("sig verifies: " + dsa2.verify(sig));

		} catch (SignatureException ex) {
			Logger.getLogger(SecurityTest.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InvalidKeyException ex) {
			Logger.getLogger(SecurityTest.class.getName()).log(Level.SEVERE, null, ex);
		} catch (NoSuchProviderException ex) {
			Logger.getLogger(SecurityTest.class.getName()).log(Level.SEVERE, null, ex);
		} catch (NoSuchAlgorithmException ex) {
			Logger.getLogger(SecurityTest.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private String displayComponents() {
		try {
			String pass = "hobson";
			KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
			keyStore.load(new FileInputStream("/Users/shughes/.keystore"), pass.toCharArray());
			DSAPrivateKey privKey = (DSAPrivateKey) keyStore.getKey("shughes", pass.toCharArray());
			DSAParams params = privKey.getParams();
			System.out.println("x: " + privKey.getX());
			System.out.println("p: " + params.getP());
			System.out.println("q: " + params.getQ());
			System.out.println("g: " + params.getG());

		} catch (IOException ex) {
			Logger.getLogger(SecurityTest.class.getName()).log(Level.SEVERE, null, ex);
		} catch (CertificateException ex) {
			Logger.getLogger(SecurityTest.class.getName()).log(Level.SEVERE, null, ex);
		} catch (NoSuchAlgorithmException ex) {
			Logger.getLogger(SecurityTest.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnrecoverableKeyException ex) {
			Logger.getLogger(SecurityTest.class.getName()).log(Level.SEVERE, null, ex);
		} catch (KeyStoreException ex) {
			Logger.getLogger(SecurityTest.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	private void testTwo() throws FileNotFoundException, CertificateException, IOException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException, SignatureException, KeyStoreException, UnrecoverableKeyException, InvalidKeySpecException {
		BufferedInputStream input = new BufferedInputStream(new FileInputStream("/Users/shughes/certs/xbriage.cer"));
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		X509Certificate cert = null;

		while (input.available() > 0) {
			cert = (X509Certificate) cf.generateCertificate(input);
		}

		BigInteger y = new BigInteger("121499860039835410753693337134205252035892097442551199524589564689319515130484657714027260847866616805177810250410089591171388360595014035678602687563074137323206120773687914876045308200928703393146907193454289247557172516432781472844177103677929440727954210200923636582137029696513212739461359522268525015350");
		BigInteger x = new BigInteger("276092492381245231740517592307529906075485071818");
		BigInteger p = new BigInteger("178011905478542266528237562450159990145232156369120674273274450314442865788737020770612695252123463079567156784778466449970650770920727857050009668388144034129745221171818506047231150039301079959358067395348717066319802262019714966524135060945913707594956514672855690606794135837542707371727429551343320695239");
		BigInteger q = new BigInteger("864205495604807476120572616017955259175325408501");
		BigInteger g = new BigInteger("174068207532402095185811980123523436538604490794561350978495831040599953488455823147851597408940950725307797094915759492368300574252438761037084473467180148876118103083043754985190983472601550494691329488083395492313850000361646482644608492304078721818959999056496097769368017749273708962006689187956744210730");

		DSAPrivateKeySpec privKeySpec = new DSAPrivateKeySpec(x, p, q, g);
		KeyFactory keyFactory = KeyFactory.getInstance("DSA");
		PrivateKey privKey = keyFactory.generatePrivate(privKeySpec);

		Signature dsa = Signature.getInstance("SHA1withDSA");
		dsa.initSign(privKey);
		byte[] data = "this is my own data that should be signed".getBytes();
		dsa.update(data);
		byte[] sig = dsa.sign();

		dsa.initVerify(cert.getPublicKey());
		dsa.update(data);
		System.out.println("verifies: " + dsa.verify(sig));
	}

	public static void main(String[] args) throws FileNotFoundException, CertificateException, IOException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException, SignatureException, KeyStoreException, UnrecoverableKeyException, InvalidKeySpecException {
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		X509Certificate caCert = null;
		BufferedInputStream caInput = new BufferedInputStream(Config.class.getResourceAsStream("xbriage.cer"));
		while(caInput.available() > 0) {
			caCert = (X509Certificate) cf.generateCertificate(caInput);
		}
		
		BufferedInputStream input = new BufferedInputStream(new FileInputStream("/Users/shughes/certs/xbriage2.cer"));
		X509Certificate cert = null;		
		while (input.available() > 0) {
			cert = (X509Certificate) cf.generateCertificate(input);
		}
		input.close();

		cert.verify(caCert.getPublicKey());
		System.out.println("ca end: "+caCert.getNotAfter());
		System.out.println("end: " + cert.getNotAfter());
		cert.checkValidity();
		System.out.println("ca issuer: "+caCert.getIssuerX500Principal().getName());
		System.out.println("issuer: " + cert.getIssuerX500Principal().getName());
	}
}
