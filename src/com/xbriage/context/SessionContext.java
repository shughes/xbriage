/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.context;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import org.jdesktop.application.Application;

/**
 *
 * @author shughes
 */
public class SessionContext {

	private final static SessionContext instance = new SessionContext();

	private String sessionFile = "sessionState.xml";
	

	private SessionContext() {

	}
	
	public static SessionContext getInstance() {
		return instance;
	}
	
	
	public void startup(Application app, JFrame mainFrame) {
		try {
			app.getContext().getSessionStorage().restore(mainFrame, sessionFile);
			//mainFrame.setTitle(app.getContext().getResourceMap().getString("Application.title"));
			
		} catch (IOException ex) {
			Logger.getLogger(SessionContext.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	public void shutdown(Application app, JFrame mainFrame) {
		try {
			app.getContext().getSessionStorage().save(mainFrame, sessionFile);
		} catch (IOException ex) {
			Logger.getLogger(SessionContext.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
