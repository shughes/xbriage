/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.context;

import com.xbriage.services.Responder;
import com.xbriage.services.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author shughes
 */
public class ResponderContext {

	public static final String JOURNAL_PRESS_TAB = "Journal.pressTab";	
	public static final String ADJUST_ROW_BUTTONS = "Journal.adjustRowButtons";
	public static final String JOURNAL_UPDATE_CATEGORY_SELECTION = "Journal.updateCategories";
	public static final String ARRANGE_ENTRY_ROWS = "Journal.arrangeEntryRows";
	public static final String UPDATE_PAYEE_STRING_LIST = "Journal.updatePayeeStringList";
	public static final String UPDATE_CATEGORY_STRING_LIST = "Journal.updateCategoryStringList";
	public static final String SHOW_CATEGORIES_PANEL = "Journal.showCategoriesPanel";
	public static final String SHOW_PAYEES_PANEL = "Journal.showPayeesPanel";
	public static final String SHOW_CATEGORIES_LAST_ROW = "Journal.showCategoriesLastRow";
	public static final String HIDE_POPUP_PANEL = "Journal.hidePopupPanel";
	public static final String ADD_ENTRY_ROW = "JournalModule.addRow";
	public static final String JOURNAL_PRESS_ENTER = "Journal.pressEnter";
	public static final String SETUP_GAINED_FOCUSED_FIELD = "Journal.setupGainedFocusedField";
	public static final String SETUP_LOST_FOCUSED_FIELD = "Journal.setupLostFocusedField";
	public static final String UPDATE_ROW_BUFFER_DATA2 = "Journal2.updateRowBufferData";
	public static final String UPDATE_ROW_BUFFER_DATA = "Journal.updateRowBufferData";
	public static final String SHOW_CATEGORIES_PANEL2 = "Journal2.showCategoriesPanel";
	public static final String JOURNAL_ACCOUNT_CHANGED = "Journal.accountChanged";
	public static final String JOURNAL_REMOVE_ENTRY = "Journal.removeEntry";
	public static final String UPDATE_CATEGORY_ROWS = "Accounts.updateCategoryRows";
	public static final String ACCOUNTS_TREE_NODE_CHANGED = "Accounts.treeNodeChanged";
	public static final String ARRANGE_ACCOUNT_ROWS = "Accounts.arrangeAccountRows";
	public static final String ADD_ACCOUNT = "Accounts.addAccount";	
	public static final String UPDATE_WORKVIEW_TREE = "Workview.updateWorkviewTree";
	public static final String SELECT_WORKVIEW = "Workview.selectWorkview";
	public static final String SAVE_WORKVIEW = "Workview.saveView";
	public static final String DELETE_WORKVIEW = "Workview.deleteWorkview";
	public static final String JOURNAL2_ACCOUNT_CHANGED = "Journal2.accountChanged";
	

	public static void setup() {
		if(instance == null) {
			instance = new ResponderContext();
		}
		
		instance.init();
	}

	protected void init() {
		map = new HashMap<String, Responder>();
//		ServiceLoader<Responder> modules = ServiceLoader.load(Responder.class);
		List<Responder> modules = Service.loadAll(Responder.class);
		for (Responder module : modules) {
			map.put(module.getId(), module);
		}
	}

	public static <T> T get(String key) {
		if (instance.map == null) {
			throw new NullPointerException("moduleMap cannot be null");
		}
		return (T) instance.map.get(key);
	}
	
	private Map<String, Responder> map;
	private static ResponderContext instance = null;
}
