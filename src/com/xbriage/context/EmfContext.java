/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.context;

import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author shughes
 */
public class EmfContext {

	private String url = null;
	private EntityManagerFactory object = null;
	private static EmfContext instance = new EmfContext();

	protected EmfContext() {
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public static EmfContext getInstance() {
		return instance;
	}

	public static EntityManagerFactory get() {
		return instance.object;
	}

	public static EntityManager create() {
		if (instance.object == null) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("hibernate.connection.url", instance.url);
			instance.object = Persistence.createEntityManagerFactory("WorkbooksAppPU", map);
		}
		return instance.object.createEntityManager();
	}

	public static void close() {
		instance.object.close();
	}
}
