/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.context;

import com.xbriage.domain.Payee;
import com.xbriage.domain.Workview;
import com.xbriage.util.Parser;
import com.xbriage.app.WorkbooksApp;
import com.xbriage.app.WorkbooksView;
import com.xbriage.config.Config;
import com.xbriage.services.Settings;
import com.xbriage.services.Service;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.swing.JFrame;
import org.jdesktop.application.Application;
import org.jdesktop.application.LocalStorage;

/**
 *
 * @author shughes
 */
public class SetupContext {

	private Settings s = Service.load(Settings.class);
	private final static SetupContext instance = new SetupContext();
	private Map<String, String> settings;

	private SetupContext() {
		settings = s.get();
	}

	public static SetupContext getInstance() {
		return instance;
	}

	public Map<String, String> getSettings() {
		return settings;
	}

	private void setupArchives() {
		if (settings.get("Archives.folder") == null) {
			LocalStorage ls = WorkbooksApp.getApplication().getContext().getLocalStorage();
			String folder = ls.getDirectory().getAbsolutePath();
			settings.put("Archives.folder", folder);
		}
	}

	public void setup() {
		Application app = (Application) WorkbooksApp.getApplication();

		if (settings.get("Settings.defaultsExist") == null) {
			settings.put("Settings.defaultsExist", "true");
			setupDefaults();
		}

		if (settings.get("Settings.defaultWorkbook") == null) {
			settings.put("Settings.defaultWorkbook", "Home");
		}

		if (settings.get("Database.folder") == null) {
			LocalStorage ls = app.getContext().getLocalStorage();
			String file = ls.getDirectory().getPath() + File.separator + "db";
			settings.put("Database.folder", file);
			settings.put("Database.file", file + File.separator + "xbriage");
		}

		setupArchives();

	}

	public void save() {
		s.save();
	}

	public void shutdown() {
		saveView();
	}

	private void saveView() {
		if (settings.get("Window.width") != null) {
			WorkbooksView view = WorkbooksView.getInstance();
			view.getFrame().validate();
			int height = view.getFrame().getHeight();
			int width = view.getFrame().getWidth();
			int x = view.getFrame().getX();
			int y = view.getFrame().getY();
			int dividerX = view.getSplitPane().getDividerLocation();
			if (width != 0) {
				settings.put("Window.width", new Integer(width).toString());
				settings.put("Window.height", new Integer(height).toString());
				settings.put("Window.locationX", new Integer(x).toString());
				settings.put("Window.locationY", new Integer(y).toString());
				if (view.getFrame().getExtendedState() == JFrame.MAXIMIZED_BOTH) {
					settings.put("Window.maximized", "1");
				} else {
					settings.put("Window.maximized", "0");
				}
				settings.put("Window.dividerX", new Integer(dividerX).toString());
			}
		}
	}

	public void setupView() {
		WorkbooksView view = WorkbooksView.getInstance();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

		if (settings.get("Window.width") == null) {
			String width = new Integer(dim.width - 100).toString();
			settings.put("Window.width", width);
		}

		if (settings.get("Window.height") == null) {
			String height = new Integer(dim.height - 200).toString();
			settings.put("Window.height", height);
		}

		if (settings.get("Window.locationX") == null) {
			settings.put("Window.locationX", new Integer(50).toString());
		}

		if (settings.get("Window.locationY") == null) {
			settings.put("Window.locationY", new Integer(50).toString());
		}

		if (settings.get("Window.maximized") == null) {
			settings.put("Window.maximized", "0");
		}

		if (settings.get("Window.dividerX") == null) {
			Integer width = new Integer(settings.get("Window.width"));
			Integer x = width - 300;
			settings.put("Window.dividerX", x.toString());
		}

		Integer width = new Integer(settings.get("Window.width"));
		Integer height = new Integer(settings.get("Window.height"));
		Integer x = new Integer(settings.get("Window.locationX"));
		Integer y = new Integer(settings.get("Window.locationY"));
		view.getFrame().setSize(width, height);
		view.getFrame().setLocation(x, y);

		if (settings.get("Window.maximized").equals("1")) {
			view.getFrame().setExtendedState(view.getFrame().getExtendedState() | JFrame.MAXIMIZED_BOTH);
		}

		view.getFrame().validate();
		Integer dividerX = new Integer(settings.get("Window.dividerX"));
		view.getSplitPane().setDividerLocation(dividerX);
	}

	private void setupDefaults() {
		System.out.println("create defaults");
		EntityManager em = EmfContext.create();

		Payee defaultPayee = createDefaultPayee();
		if (defaultPayee != null) {
			em.persist(defaultPayee);
		}

		Workview rootWorkview = createRootWorkview();
		if (rootWorkview != null) {
			em.persist(rootWorkview);
		}

		em.getTransaction().begin();
		em.getTransaction().commit();
		em.close();

		// parser creates root if not exists
		setupDefaultAccounts(WorkbooksApp.getApplication());
	}

	private void setupDefaultAccounts(Application app) {
		Parser parser = new Parser();
		parser.parse(Config.class.getResourceAsStream("Accounts.xml"));
	}

	private Payee createDefaultPayee() {
		EntityManager em = EmfContext.create();
		List<Payee> result = em.createNamedQuery("Payee.findDefault").getResultList();
		Payee payee = null;
		if (result.size() == 0) {
			payee = new Payee();
			payee.setDefault(true);
			payee.setName("No Payee");
		}
		return payee;
	}

	private Workview createRootWorkview() {
		EntityManager em = EmfContext.create();
		List<Workview> result = em.createNamedQuery("Workview.findRoot").getResultList();
		Workview root = null;
		if (result.size() == 0) {
			root = new Workview();
			root.setIsRoot(1);
			root.setName("Root");
			root.setTemplate("empty");
		}
		return root;
	}
}
