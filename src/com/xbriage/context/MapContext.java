/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.context;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author shughes
 */
public class MapContext {

	private static MapContext instance = new MapContext();
	private Map<Object, Object> sessionMap = new HashMap<Object, Object>();

	public static Object get(String key) {
		return instance.sessionMap.get(key);
	}

	public static void put(Object key, Object value) {
		instance.sessionMap.put(key, value);
	}
	
}
