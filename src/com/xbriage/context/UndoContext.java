/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.context;

import javax.swing.undo.UndoManager;

/**
 *
 * @author shughes
 */
public class UndoContext {
	private final static UndoManager undoManager = new UndoManager();

	public static UndoManager getUndoManager() {
		return undoManager;
	}
	
}
