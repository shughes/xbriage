/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.context;

import com.xbriage.services.Template;
import com.xbriage.services.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author shughes
 */
public class TemplateContext {

	private static TemplateContext instance = null;
	private Map<String, Template> map;

	public static void setup() {
		if (instance == null) {
			instance = new TemplateContext();
		}

		instance.init();
	}

	protected void init() {
		map = new HashMap<String, Template>();
		List<Template> templates = Service.loadAll(Template.class);
		for (Template template : templates) {
			map.put(template.getId(), template);
		}
	}

	public static Template get(String key) {
		return instance.map.get(key);
	}
}
