/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.menus;

import com.xbriage.context.TemplateContext;
import com.xbriage.services.Template;

/**
 *
 * @author shughes
 */
public class MainMenuActions {

	private final static MainMenuActions instance = new MainMenuActions();
	private final static MainMenu mainMenu = MainMenu.getInstance(); //ModuleFactory.get("MainMenu.module");
	private Template template;
	
	private MainMenuActions() {

	}

	public static MainMenuActions getInstance() {
		return instance;
	}

	public void openLayoutOne() {
		Template t = TemplateContext.get("templateOne");
		mainMenu.setTemplate(t);
	}

	public void openLayoutTwo() {
		Template t = TemplateContext.get("templateTwo");
		mainMenu.setTemplate(t);
	}

	public void openLayoutThree() {
		Template t = TemplateContext.get("templateThree");
		mainMenu.setTemplate(t);
	}

	public void openLayout() {
		Template t = TemplateContext.get("");
	}

	public void openLayoutFour() {
		Template t = TemplateContext.get("templateFour");
		mainMenu.setTemplate(t);
	}
	
	public void openLayoutFive() {
		Template t = TemplateContext.get("templateFive");
		mainMenu.setTemplate(t);
	}
	
	public void openLayoutSix() {
		Template t = TemplateContext.get("templateSix");
		mainMenu.setTemplate(t);
	}
	
}
