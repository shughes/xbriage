/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.menus;

import com.xbriage.domain.Workview;
import com.xbriage.context.EmfContext;
import com.xbriage.util.IteratorEnumeration;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;
import javax.persistence.EntityManager;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;
import org.jdesktop.swingx.treetable.TreeTableNode;

/**
 *
 * @author shughes
 */
public class WorkviewTreeNode implements MutableTreeNode {
	private Workview workview;
	private List<TreeNode> children;
	private TreeNode parent;

	public WorkviewTreeNode(Workview workview, TreeNode parent) {
		this.workview = workview;
		this.parent = parent;
		children = new Vector<TreeNode>();
		for(Workview child: workview.getChildren()) {
			children.add(new WorkviewTreeNode(child, this));	
		}
	}

	public Enumeration<? extends TreeNode> children() {
		return new IteratorEnumeration(children.iterator());
	}

	public TreeNode getChildAt(int index) {
		return children.get(index);
	}

	public TreeNode getParent() {
		return parent;
	}
	
	@Override
	public String toString() {
		return workview.getName();
	}

	public int getChildCount() {
		return children.size();
	}

	public int getIndex(TreeNode node) {
		return children.indexOf(node);
	}

	public boolean getAllowsChildren() {
		return true;
	}

	public boolean isLeaf() {
		if(children.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

	public List<TreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<TreeNode> children) {
		this.children = children;
	}

	public void setParent(TreeTableNode parent) {
		this.parent = parent;
	}

	public Workview getWorkview() {
		return workview;
	}

	public void setWorkview(Workview workview) {
		this.workview = workview;
	}

	public void insert(MutableTreeNode arg0, int arg1) {
		
	}

	public void remove(int arg0) {
		
	}

	public void remove(MutableTreeNode arg0) {
		
	}

	public void setUserObject(Object object) {
		EntityManager em = EmfContext.create();
		workview = em.find(Workview.class, workview.getId());
		workview.setName(object.toString());
		em.getTransaction().begin();
		em.persist(workview);
		em.getTransaction().commit();
	}

	public void removeFromParent() {
		
	}

	public void setParent(MutableTreeNode arg0) {
		
	}
}
