/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.menus.responders;

import com.xbriage.services.Responder;
import com.xbriage.menus.WorkviewTreeModel;
import com.xbriage.menus.WorkviewsMenu;
import com.xbriage.domain.Workview;
import com.xbriage.context.EmfContext;
import com.xbriage.context.ResponderContext;
import com.xbriage.menus.WorkviewTreeNode;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.swing.JTree;

/**
 *
 * @author shughes
 */
public class UpdateWorkviewTree implements Responder {

	public void perform(Map params) {		
		EntityManager entityManager = EmfContext.get().createEntityManager();
		Query q = entityManager.createNamedQuery("Workview.findRoot");
		Workview parent = (Workview) q.getSingleResult();
		JTree tree = WorkviewsMenu.getInstance().getTree();
		WorkviewTreeNode rootNode = new WorkviewTreeNode(parent, null);
		tree.setModel(new WorkviewTreeModel(rootNode));
		//tree.expandAll();
	}

	public String getId() {
		return ResponderContext.UPDATE_WORKVIEW_TREE;
	}

}
