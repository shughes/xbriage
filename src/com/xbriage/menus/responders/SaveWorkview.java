/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.menus.responders;

import com.xbriage.services.Responder;
import com.xbriage.app.TemplateData;
import com.xbriage.app.WorkbooksApp;
import com.xbriage.app.WorkbooksView;
import com.xbriage.menus.MainMenu;
import com.xbriage.menus.WorkviewTreeNode;
import com.xbriage.menus.WorkviewsMenu;
import com.xbriage.domain.Book;
import com.xbriage.domain.Workview;
import com.xbriage.context.EmfContext;
import com.xbriage.context.MapContext;
import com.xbriage.context.ResponderContext;
import com.xbriage.services.Module;
import com.xbriage.services.Template;
import com.xbriage.templates.TemplatePanel;
import com.xbriage.util.GeneralUtil;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.swing.JPanel;
import javax.swing.tree.TreePath;
import org.jdesktop.application.Application;
import org.jdesktop.application.ResourceMap;

/**
 *
 * @author shughes
 */
public class SaveWorkview implements Responder {

	public void perform(Map params) {
		MainMenu mainMenu = MainMenu.getInstance(); //ModuleFactory.get("MainMenu.module");
		Template template = mainMenu.getTemplate();
		WorkviewsMenu workviewsMenu = WorkviewsMenu.getInstance();

		if (template != null) {
			EntityManager em = EmfContext.create();
			Workview parent = null;
			TreePath path = workviewsMenu.getTree().getSelectionPath();

			if (path != null) {
				WorkviewTreeNode node = (WorkviewTreeNode) path.getLastPathComponent();
				Workview workview = em.find(Workview.class, node.getWorkview().getId());
				parent = workview;
			}

			Map<String, JPanel> workviewMap = TemplateData.getInstance().getWorkviewMap(); //MapContext.getWorkviewMap();
			Set<String> keys = workviewMap.keySet();

			boolean showAlert = false;

			// determines whether or not view is fully filled.
			Map<String, Boolean> tracker = new HashMap<String, Boolean>();
			for (String key : keys) {
				TemplatePanel panel = (TemplatePanel) workviewMap.get(key);

				if (panel.getModule() == null) {
					showAlert = true;
				} else {
					if (tracker.get(panel.getModule().getId()) != null) {
						showAlert = true;
					}
					tracker.put(panel.getModule().getId(), true);
				}
			}

			if (showAlert) {
				GeneralUtil.showWarningAlert2(WorkbooksApp.getWarning("SaveWorkview.notFilled"));
			} else {

				Workview newView = new Workview();

				if (parent == null) {
					Query q = em.createNamedQuery("Workview.findRoot");
					parent = (Workview) q.getSingleResult();
				}

				em.getTransaction().begin();

				ResourceMap resourceMap = Application.getInstance(WorkbooksApp.class).getContext().getResourceMap(WorkviewsMenu.class);
				newView.setTemplate(TemplateData.getInstance().getCurrentTemplate());
				newView.setName(resourceMap.getString("viewName"));
				newView.setIsRoot(new Integer(0));
				newView.setParent(parent);

				List<Book> books = new Vector<Book>();
				for (String key : keys) {
					TemplatePanel panel = (TemplatePanel) workviewMap.get(key);

					Module module = panel.getModule();
					Book newBook = new Book();
					String[] vertices = key.split(":");
					Integer row = new Integer(vertices[0]);
					Integer col = new Integer(vertices[1]);
				
					//double width = middlePane.getSize().getWidth();
					//width = panel.getSize().getWidth() / width;
					double width = panel.getSize().getWidth();
				
					//double height = middlePane.getSize().getHeight();
					//height = panel.getSize().getHeight() / height;
					double height = panel.getSize().getHeight();
				
					newBook.setWidth(width);
					newBook.setHeight(height);
					newBook.setRow(row);
					newBook.setColumn(col);
					newBook.setModuleName(module.getId());
					newBook.setWorkview(newView);
					
					books.add(newBook);
					em.persist(newBook);
				}

				// save current windows width and height for scaling
				JPanel middlePane = WorkbooksView.getInstance().getMiddlePane();
				newView.setWidth(middlePane.getWidth());
				newView.setHeight(middlePane.getHeight());
				
				newView.setBooks(books);
				em.persist(newView);
				em.getTransaction().commit();
				em.close();

				Responder updateTree = ResponderContext.get(ResponderContext.UPDATE_WORKVIEW_TREE);
				updateTree.perform(null);

				WorkviewsMenu.getInstance().expandAll();
			}
		}
	}

	public String getId() {
		return ResponderContext.SAVE_WORKVIEW;
	}
}
