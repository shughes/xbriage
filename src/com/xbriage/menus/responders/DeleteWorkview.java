/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.menus.responders;

import com.xbriage.services.Responder;
import com.xbriage.menus.WorkviewTreeNode;
import com.xbriage.menus.WorkviewsMenu;
import com.xbriage.domain.Workview;
import com.xbriage.context.EmfContext;
import com.xbriage.context.ResponderContext;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.swing.tree.TreePath;

/**
 *
 * @author shughes
 */
public class DeleteWorkview implements Responder {

	public void perform(Map params) {
		WorkviewsMenu workviewsMenu = WorkviewsMenu.getInstance();
		TreePath path = workviewsMenu.getTree().getSelectionPath();
		WorkviewTreeNode node = (WorkviewTreeNode) path.getLastPathComponent();
		Workview workview = node.getWorkview();
		EntityManager em = EmfContext.create();
		workview = em.find(Workview.class, workview.getId());
		em.getTransaction().begin();
		em.remove(workview);
		em.getTransaction().commit();		
		Responder updateTree = ResponderContext.get(ResponderContext.UPDATE_WORKVIEW_TREE);
		updateTree.perform(null);
		em.close();
		
		WorkviewsMenu.getInstance().expandAll();
	}

	public String getId() {
		return ResponderContext.DELETE_WORKVIEW;
	}

}
