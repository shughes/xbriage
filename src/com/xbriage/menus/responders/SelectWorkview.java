/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.menus.responders;

import com.xbriage.services.Responder;
import com.xbriage.app.TemplateData;
import com.xbriage.menus.MainMenu;
import com.xbriage.menus.WorkviewTreeNode;
import com.xbriage.menus.WorkviewsMenu;
import com.xbriage.domain.Book;
import com.xbriage.domain.Workview;
import com.xbriage.services.Service;
import com.xbriage.context.ResponderContext;
import com.xbriage.context.TemplateContext;
import com.xbriage.services.Module;
import com.xbriage.app.WorkbooksView;
import com.xbriage.services.ModuleLoader;
import com.xbriage.services.Template;
import com.xbriage.templates.TemplatePanel;
import java.awt.Dimension;
import java.util.Map;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.tree.TreePath;

/**
 *
 * @author shughes
 */
public class SelectWorkview implements Responder {

	public void perform(Map params) {
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				WorkviewsMenu workviewsMenu = WorkviewsMenu.getInstance();
				TreePath path = workviewsMenu.getTree().getSelectionPath();

				if (path != null) {
					WorkviewTreeNode node = (WorkviewTreeNode) path.getLastPathComponent();
					Workview workview = node.getWorkview();

					if (workview.getChildren().size() == 0) { // is not a parent
						Map<String, JPanel> workviewMap = TemplateData.getInstance().getWorkviewMap(); //MapContext.getWorkviewMap();
						Template template = TemplateContext.get(workview.getTemplate());

						MainMenu menuModule = MainMenu.getInstance(); //Service.load(ModuleLoader.class).get("MainMenu.module");
						menuModule.setTemplate(template);

						double wFactor = workview.getWidth();
						wFactor = WorkbooksView.getInstance().getMiddlePane().getWidth() / wFactor;

						double hFactor = workview.getHeight();
						hFactor = WorkbooksView.getInstance().getMiddlePane().getHeight() / hFactor;

						for (Book book : workview.getBooks()) {
							Integer row = book.getRow();
							String key = row.toString();
							Integer col = book.getColumn();
							key = key + ":" + col.toString();

							String moduleName = book.getModuleName();
							final Module module = Service.load(ModuleLoader.class).get(moduleName);

							TemplatePanel templatePane = (TemplatePanel) workviewMap.get(key);
							templatePane.setModule(module);
							templatePane.removeAll();
							templatePane.add(module.getComponent(), 0);
							templatePane.validate();
							templatePane.repaint();

							// factors width based on width at time view was saved
							// with current width
							int w = (int) book.getWidth();
							double width = w;
							width = width * wFactor;
							w = (int) width;

							// same as width
							int h = (int) book.getHeight();
							double height = h;
							height = height * hFactor;
							h = (int) height;

							templatePane.setPreferredSize(new Dimension(w, h));

							if (templatePane.getParent() instanceof JSplitPane) {
								JSplitPane parent = (JSplitPane) templatePane.getParent();
								parent.resetToPreferredSizes();
							}

							SwingUtilities.invokeLater(new Runnable() {

								public void run() {
									module.afterDrop();
								}
							});
						}
					}
				}
			}
		});
	}

	public String getId() {
		return ResponderContext.SELECT_WORKVIEW;
	}
}
