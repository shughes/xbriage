/*
 * WorkbooksView.java
 */
package com.xbriage.app;

import com.xbriage.app.*;
import com.xbriage.app.WorkbooksAboutBox;
import com.xbriage.app.WorkbooksApp;
import com.xbriage.context.TemplateContext;
import com.xbriage.icons.Icons;
import com.xbriage.menus.MainMenu;
import com.xbriage.security.Register;
import com.xbriage.security.SecurityDialog;
import com.xbriage.services.Module;
import com.xbriage.services.ModuleLoader;
import com.xbriage.services.Service;
import com.xbriage.services.Template;
import com.xbriage.templates.TemplatePanel;
import com.xbriage.util.BindUtil;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.TaskMonitor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.Map;
import javax.swing.Timer;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import org.jdesktop.application.Action;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;

/**
 * The application's main frame.
 */
public class WorkbooksView extends FrameView {

	private javax.swing.JProgressBar progressBar = new JProgressBar();
	private javax.swing.JLabel statusAnimationLabel = new JLabel("");
	private javax.swing.JLabel statusMessageLabel = new JLabel("");
	private static WorkbooksView instance = new WorkbooksView(WorkbooksApp.getApplication());

	public static WorkbooksView getInstance() {
		return instance;
	}

	private WorkbooksView(SingleFrameApplication app) {
		super(WorkbooksApp.getInstance());


		initComponents();

		getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		MainMenu tmp = MainMenu.getInstance(); //ModuleFactory.get("MainMenu.module");
		menuPane.add(tmp, 0);

		// set window icon
		URL image = null;
		image = Icons.class.getResource("icon.png");
		getFrame().setIconImage(new ImageIcon(image).getImage());

		undoMenuItem.setEnabled(false);

		// status bar initialization - message timeout, idle icon and busy animation, etc
		ResourceMap resourceMap = getResourceMap();
		int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout");
		messageTimer = new Timer(messageTimeout, new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				statusMessageLabel.setText("");
			}
		});
		messageTimer.setRepeats(false);
		int busyAnimationRate = resourceMap.getInteger("StatusBar.busyAnimationRate");
		for (int i = 0; i < busyIcons.length; i++) {
			busyIcons[i] = resourceMap.getIcon("StatusBar.busyIcons[" + i + "]");
		}
		busyIconTimer = new Timer(busyAnimationRate, new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				busyIconIndex = (busyIconIndex + 1) % busyIcons.length;
				statusAnimationLabel.setIcon(busyIcons[busyIconIndex]);
			}
		});
		idleIcon = resourceMap.getIcon("StatusBar.idleIcon");
		statusAnimationLabel.setIcon(idleIcon);
		progressBar.setVisible(false);

		// connecting action tasks to status bar via TaskMonitor
		TaskMonitor taskMonitor = new TaskMonitor(getApplication().getContext());
		taskMonitor.addPropertyChangeListener(new java.beans.PropertyChangeListener() {

			public void propertyChange(java.beans.PropertyChangeEvent evt) {
				String propertyName = evt.getPropertyName();
				if ("started".equals(propertyName)) {
					if (!busyIconTimer.isRunning()) {
						statusAnimationLabel.setIcon(busyIcons[0]);
						busyIconIndex = 0;
						busyIconTimer.start();
					}
					progressBar.setVisible(true);
					progressBar.setIndeterminate(true);
				} else if ("done".equals(propertyName)) {
					busyIconTimer.stop();
					statusAnimationLabel.setIcon(idleIcon);
					progressBar.setVisible(false);
					progressBar.setValue(0);
				} else if ("message".equals(propertyName)) {
					String text = (String) (evt.getNewValue());
					statusMessageLabel.setText((text == null) ? "" : text);
					messageTimer.restart();
				} else if ("progress".equals(propertyName)) {
					int value = (Integer) (evt.getNewValue());
					progressBar.setVisible(true);
					progressBar.setIndeterminate(false);
					progressBar.setValue(value);
				}
			}
		});

		startup();
	}

	private void startup() {
		BindUtil.bind(UndoActions.getInstance(), "undo", getUndoMenuItem(), BindUtil.ACTION);
		BindUtil.bind(UndoActions.getInstance(), "redo", getRedoMenuItem(), BindUtil.ACTION);
		BindUtil.bind(this, "showHelp", helpMenuItem, BindUtil.ACTION);
	}

	public void showHelp() {
		Module module = Service.load(ModuleLoader.class).get("Help.module");
		Service.load(ModuleLoader.class).showModule(module);
	}

	@Action
	public void showAboutBox() {
		if (aboutBox == null) {
			JFrame mainFrame = WorkbooksApp.getApplication().getMainFrame();
			aboutBox = new WorkbooksAboutBox(mainFrame);
			aboutBox.setLocationRelativeTo(mainFrame);
		}
		WorkbooksApp.getApplication().show(aboutBox);
	}

	/** This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        splitPane = new javax.swing.JSplitPane();
        middlePane = new javax.swing.JPanel();
        menuPane = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        undoMenuItem = new javax.swing.JMenuItem();
        redoMenuItem = new javax.swing.JMenuItem();
        workbooksMenu = new javax.swing.JMenu();
        javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();
        helpMenuItem = new javax.swing.JMenuItem();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        addWorkviewItem = new javax.swing.JMenuItem();

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(com.xbriage.app.WorkbooksApp.class).getContext().getResourceMap(WorkbooksView.class);
        mainPanel.setBackground(resourceMap.getColor("Application.menuBackground")); // NOI18N
        mainPanel.setName("mainPanel"); // NOI18N
        mainPanel.setPreferredSize(new java.awt.Dimension(1100, 600));

        splitPane.setDividerLocation(800);
        splitPane.setResizeWeight(1.0);
        splitPane.setName("splitPane"); // NOI18N

        middlePane.setName("middlePane"); // NOI18N
        middlePane.setLayout(new javax.swing.BoxLayout(middlePane, javax.swing.BoxLayout.PAGE_AXIS));
        splitPane.setLeftComponent(middlePane);

        menuPane.setName("menuPane"); // NOI18N
        menuPane.setLayout(new javax.swing.BoxLayout(menuPane, javax.swing.BoxLayout.PAGE_AXIS));
        splitPane.setRightComponent(menuPane);

        jSeparator1.setName("jSeparator1"); // NOI18N

        jLabel1.setFont(resourceMap.getFont("jLabel1.font")); // NOI18N
        jLabel1.setForeground(resourceMap.getColor("jLabel1.foreground")); // NOI18N
        jLabel1.setText(resourceMap.getString("jLabel1.text")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(splitPane, javax.swing.GroupLayout.DEFAULT_SIZE, 769, Short.MAX_VALUE)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addComponent(jSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, 769, Short.MAX_VALUE)
                .addGap(0, 0, 0))
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(715, Short.MAX_VALUE))
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addComponent(splitPane, javax.swing.GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(jLabel1)
                .addContainerGap())
        );

        menuBar.setName("menuBar"); // NOI18N

        fileMenu.setText(resourceMap.getString("fileMenu.text")); // NOI18N
        fileMenu.setMnemonic('F');
        fileMenu.setName("fileMenu"); // NOI18N

        jMenuItem1.setText(resourceMap.getString("jMenuItem1.text")); // NOI18N
        jMenuItem1.setMnemonic('R');
        jMenuItem1.setName("jMenuItem1"); // NOI18N
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                register(evt);
            }
        });
        fileMenu.add(jMenuItem1);

        jMenuItem2.setText(resourceMap.getString("jMenuItem2.text")); // NOI18N
        jMenuItem2.setMnemonic('S');
        jMenuItem2.setName("jMenuItem2"); // NOI18N
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showSn(evt);
            }
        });
        fileMenu.add(jMenuItem2);

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(com.xbriage.app.WorkbooksApp.class).getContext().getActionMap(WorkbooksView.class, this);
        exitMenuItem.setAction(actionMap.get("quit")); // NOI18N
        exitMenuItem.setMnemonic('X');
        exitMenuItem.setName("exitMenuItem"); // NOI18N
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        jMenu1.setText(resourceMap.getString("jMenu1.text")); // NOI18N
        jMenu1.setMnemonic('E');
        jMenu1.setName("jMenu1"); // NOI18N

        undoMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.CTRL_MASK));
        undoMenuItem.setMnemonic('U');
        undoMenuItem.setText(resourceMap.getString("undoMenuItem.text")); // NOI18N
        undoMenuItem.setName("undoMenuItem"); // NOI18N
        jMenu1.add(undoMenuItem);

        redoMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        redoMenuItem.setMnemonic('R');
        redoMenuItem.setText(resourceMap.getString("redoMenuItem.text")); // NOI18N
        redoMenuItem.setEnabled(false);
        redoMenuItem.setName("redoMenuItem"); // NOI18N
        jMenu1.add(redoMenuItem);

        menuBar.add(jMenu1);

        workbooksMenu.setText(resourceMap.getString("workbooksMenu.text")); // NOI18N
        workbooksMenu.setMnemonic('W');
        workbooksMenu.setName("workbooksMenu"); // NOI18N
        menuBar.add(workbooksMenu);

        helpMenu.setText(resourceMap.getString("helpMenu.text")); // NOI18N
        helpMenu.setMnemonic('H');
        helpMenu.setName("helpMenu"); // NOI18N

        aboutMenuItem.setAction(actionMap.get("showAboutBox")); // NOI18N
        aboutMenuItem.setMnemonic('A');
        aboutMenuItem.setName("aboutMenuItem"); // NOI18N
        helpMenu.add(aboutMenuItem);

        helpMenuItem.setText(resourceMap.getString("helpMenuItem.text")); // NOI18N
        helpMenuItem.setMnemonic('H');
        helpMenuItem.setName("helpMenuItem"); // NOI18N
        helpMenu.add(helpMenuItem);

        menuBar.add(helpMenu);

        jPopupMenu1.setLabel(resourceMap.getString("jPopupMenu1.label")); // NOI18N
        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        addWorkviewItem.setAction(actionMap.get("addWorkview")); // NOI18N
        addWorkviewItem.setName("addWorkviewItem"); // NOI18N
        jPopupMenu1.add(addWorkviewItem);

        setComponent(mainPanel);
        setMenuBar(menuBar);
    }// </editor-fold>//GEN-END:initComponents
	private void register(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_register
		new Register();
	}//GEN-LAST:event_register

	private void showSn(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showSn
		SecurityDialog.showDialog();
	}//GEN-LAST:event_showSn

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem addWorkviewItem;
    private javax.swing.JMenuItem helpMenuItem;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JPanel menuPane;
    private javax.swing.JPanel middlePane;
    private javax.swing.JMenuItem redoMenuItem;
    private javax.swing.JSplitPane splitPane;
    private javax.swing.JMenuItem undoMenuItem;
    private javax.swing.JMenu workbooksMenu;
    // End of variables declaration//GEN-END:variables
	private final Timer messageTimer;
	private final Timer busyIconTimer;
	private final Icon idleIcon;
	private final Icon[] busyIcons = new Icon[15];
	private int busyIconIndex = 0;
	private JDialog aboutBox;

	public javax.swing.JPanel getMiddlePane() {
		return middlePane;
	}

	public void setMiddlePane(javax.swing.JPanel middlePane) {
		this.middlePane = middlePane;
	}

	public javax.swing.JMenuItem getUndoMenuItem() {
		return undoMenuItem;
	}

	public javax.swing.JPanel getMenuPane() {
		return menuPane;
	}

	public void setMenuPane(javax.swing.JPanel menuPane) {
		this.menuPane = menuPane;
	}

	public javax.swing.JSplitPane getSplitPane() {
		return splitPane;
	}

	public javax.swing.JMenuItem getRedoMenuItem() {
		return redoMenuItem;
	}

	public javax.swing.JMenu getWorkbooksMenu() {
		return workbooksMenu;
	}
}
