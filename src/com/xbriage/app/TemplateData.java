/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.app;

import com.xbriage.services.Module;
import com.xbriage.templates.TemplatePanel;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JPanel;

/**
 *
 * @author shughes
 */
public class TemplateData {
	private String currentTemplate;
	private static TemplateData instance = new TemplateData();
	private Map<String, JPanel> workviewMap = new HashMap<String, JPanel>();
	
	public static TemplateData getInstance() {
		return instance;
	}
	
	private TemplateData() {
	}

	public String getCurrentTemplate() {
		return currentTemplate;
	}

	public void setCurrentTemplate(String currentTemplate) {
		this.currentTemplate = currentTemplate;
	}
	
	public Map<String, JPanel> getWorkviewMap() {
		return workviewMap;
	}
	
	public boolean isDisplayed(Module module) {
		for(String key : workviewMap.keySet()) {
			TemplatePanel pane = (TemplatePanel) workviewMap.get(key);
			if(pane.getModule() != null) {
				String id = pane.getModule().getId();
				if(id.equals(module.getId())) {
					return true;
				}
			}
		}
		return false;
	}
}
