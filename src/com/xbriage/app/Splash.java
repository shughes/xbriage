/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.app;

import com.xbriage.icons.Icons;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;

/**
 *
 * @author shughes
 */
public class Splash extends JFrame {

	private static final Splash instance = new Splash();
	protected ImageIcon im;
	
	public static Splash getInstance() {
		return instance;
	}

	protected Splash() {
		super();

		String progName = "Xbriage";
		JFrame f = new JFrame();
		
		setUndecorated(true);
		URL image = null;
		image = Icons.class.getResource("icon.png");
		setIconImage(new ImageIcon(image).getImage());
		setTitle("Loading Xbriage...");

		// Can't use Swing border on JWindow: not a JComponent.
		// setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		URL url = Icons.class.getResource("splash.png");
		im = new ImageIcon(url);
		if (im.getImageLoadStatus() != MediaTracker.COMPLETE) {
			JOptionPane.showMessageDialog(f,
					"Warning: can't load image " + url.toString() + "\n" +
					"Please be sure you have installed " + progName + " correctly",
					"Warning",
					JOptionPane.WARNING_MESSAGE);
		}
		int w = im.getIconWidth(), h = im.getIconHeight();
		setSize(w, h);
		UtilGUI.center(this);
		addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {
				dispose();
			}
		});
		addKeyListener(new KeyAdapter() {

			public void keyTyped(KeyEvent e) {
				dispose();
			}
		});
	}

	public void paint(Graphics g) {
		im.paintIcon(this, g, 0, 0);
		//g.setColor(Color.green);
		g.drawRoundRect(0, 0, getWidth() - 1, getHeight() - 1, 7, 7);
	}
	
	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.pack();
		f.setVisible(true);
	}
}

class UtilGUI {

	/** Centre a Window, Frame, JFrame, Dialog, etc. */
	public static void centre(Window w) {
		// After packing a Frame or Dialog, centre it on the screen.
		Dimension us = w.getSize(), them = Toolkit.getDefaultToolkit().getScreenSize();
		int newX = (them.width - us.width) / 2;
		int newY = (them.height - us.height) / 2;
		w.setLocation(newX, newY);
	}

	/**
	 * Center a Window, Frame, JFrame, Dialog, etc., but do it the American
	 * Spelling Way :-)
	 */
	public static void center(Window w) {
		UtilGUI.centre(w);
	}

	/** Maximize a window, the hard way. */
	public static void maximize(Window w) {
		Dimension us = w.getSize(), them = Toolkit.getDefaultToolkit().getScreenSize();
		w.setBounds(0, 0, them.width, them.height);
	}
}
