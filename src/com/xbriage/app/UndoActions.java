/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.app;

import com.xbriage.app.WorkbooksView;
import com.xbriage.context.UndoContext;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.UndoManager;

/**
 *
 * @author shughes
 */
public class UndoActions {

	private final static UndoActions instance = new UndoActions();

	public static UndoActions getInstance() {
		return instance;
	}

	private UndoActions() {

	}

	public void undo() {
		UndoManager manager = UndoContext.getUndoManager();
		manager.undo();
		adjust();
	}

	public void redo() {
		UndoManager manager = UndoContext.getUndoManager();
		manager.redo();
		adjust();
	}

	public void discardEdits() {
		UndoManager manager = UndoContext.getUndoManager();
		manager.discardAllEdits();
		adjust();
	}

	public void addUndo(AbstractUndoableEdit undoableEdit) {
		UndoManager manager = UndoContext.getUndoManager();
		manager.addEdit(undoableEdit);
		adjust();
	}
	
	private void adjust() {
		UndoManager manager = UndoContext.getUndoManager();
		WorkbooksView.getInstance().getUndoMenuItem().setEnabled(manager.canUndo());
		WorkbooksView.getInstance().getRedoMenuItem().setEnabled(manager.canRedo());
		WorkbooksView.getInstance().getUndoMenuItem().setText(manager.getUndoPresentationName());
		WorkbooksView.getInstance().getRedoMenuItem().setText(manager.getRedoPresentationName());
	}

}
