/*
 * WorkbooksApp.java
 */
package com.xbriage.app;

import com.xbriage.context.ResponderContext;
import com.xbriage.services.DbServer;
import com.xbriage.context.SetupContext;
import com.xbriage.context.TemplateContext;
import com.xbriage.config.Config;
import com.xbriage.icons.Icons;
import com.xbriage.menus.MainMenu;
import com.xbriage.services.ModuleLoader;
import com.xbriage.services.OfxData;
import com.xbriage.services.Security;
import com.xbriage.services.Settings;
import com.xbriage.util.GeneralUtil;
import com.xbriage.services.Service;
import java.awt.AWTException;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.ColorUIResource;
import org.jdesktop.application.Application;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;

/**
 * The main class of the application.
 */
public class WorkbooksApp extends SingleFrameApplication {

	private DbServer server = Service.load(DbServer.class);
	private Security security = Service.load(Security.class);

	private void setupLogger() {
		try {
			String file = GeneralUtil.getLogFile();
			FileHandler fh = new FileHandler(file, true);
			fh.setFormatter(new SimpleFormatter());
			Logger logger = Logger.getLogger("");
			logger.addHandler(fh);

		} catch (IOException ex) {
			Logger.getLogger(WorkbooksApp.class.getName()).log(Level.SEVERE, null, ex);
		} catch (SecurityException ex) {
			Logger.getLogger(WorkbooksApp.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private void setupDefaults() {
		/*try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(WorkbooksApp.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			Logger.getLogger(WorkbooksApp.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(WorkbooksApp.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			Logger.getLogger(WorkbooksApp.class.getName()).log(Level.SEVERE, null, ex);
		}*/
		Color color1 = getContext().getResourceMap().getColor("Application.selectionColor");
		Color color2 = getContext().getResourceMap().getColor("Application.selectionForeground");
		UIDefaults uidefs = UIManager.getLookAndFeelDefaults();
		uidefs.put("SplitPane.darkShadow", new ColorUIResource(new Color(211, 211, 245)));
		uidefs.put("Tree.selectionBackground", new ColorUIResource(color1));
		uidefs.put("Tree.selectionForeground", new ColorUIResource(color2));
		uidefs.put("Table.selectionBackground", new ColorUIResource(color1));
		uidefs.put("Table.selectionForeground", new ColorUIResource(color2));
		/*for(Object key: uidefs.keySet()) {
			System.out.println(key+": "+uidefs.get(key));
		}*/
	}

	/**
	 * At setup create the main frame of the application.
	 */
	@Override
	protected void startup() {

		final Splash splash = Splash.getInstance();
		splash.setVisible(true);

		setupDefaults();

		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				server.start();

				setupLogger();

				final SetupContext setupContext = SetupContext.getInstance();
				setupContext.setup();

				security.verify();

				// to avoid mess from overlapping of security dialog and splash screen
				SwingUtilities.invokeLater(new Runnable() {

					public void run() {
						splash.setVisible(true);

						TemplateContext.setup();
						Service.load(ModuleLoader.class).setup();
						ResponderContext.setup();
						Service.load(ModuleLoader.class).fireStartup();
						MainMenu.getInstance().startup();

						WorkbooksView view = WorkbooksView.getInstance();
						splash.setVisible(false);
						show(view);

						setupContext.setupView();

						setupSystemTray();
					}
				});
			}
		});

	}

	public void softReset() {
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				Service.load(ModuleLoader.class).setup();
				Service.load(ModuleLoader.class).fireStartup();
				WorkbooksView view = WorkbooksView.getInstance();
				show(view);
			}
		});
	}

	public static String getWarning(String key) {
		String value = null;
		try {
			Properties prop = new Properties();
			InputStream input = Config.class.getResourceAsStream("Warnings.properties");
			prop.load(input);
			value = prop.getProperty(key);

		} catch (IOException ex) {
			Logger.getLogger(WorkbooksApp.class.getName()).log(Level.SEVERE, null, ex);
		}

		return value;
	}

	private void setupSystemTray() {
		TrayIcon trayIcon = null;
		if (SystemTray.isSupported()) {
			try {
				SystemTray tray = SystemTray.getSystemTray();

				URL url = Icons.class.getResource("tray.png");
				Image image = new ImageIcon(url).getImage();

				PopupMenu popup = new PopupMenu();
				MenuItem quitItem = new MenuItem("Quit");

				popup.add(quitItem);
				MenuItem openItem = new MenuItem("Open");

				popup.add(openItem);
				ActionListener quitListener = new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						exit();
					}
				};
				ActionListener openListener = new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						WorkbooksView.getInstance().getFrame().setState(Frame.NORMAL);
						WorkbooksView.getInstance().getFrame().setVisible(true);
					}
				};

				quitItem.addActionListener(quitListener);

				openItem.addActionListener(openListener);
				trayIcon = new TrayIcon(image, "Xbriage", popup);

				trayIcon.setImageAutoSize(true);

				trayIcon.addActionListener(openListener);

				tray.add(trayIcon);
			} catch (AWTException ex) {
				Logger.getLogger(WorkbooksApp.class.getName()).log(Level.SEVERE, null, ex);
			}
		}

	}

	@Override
	protected void ready() {

	}

	@Override
	protected void shutdown() {
		System.out.println("shutdown");

		Service.load(ModuleLoader.class).fireShutdown();
		SetupContext.getInstance().shutdown();
		Service.load(Settings.class).save();
		Service.load(OfxData.class).save();

		server.stop();
	}

	/**
	 * This method is to initialize the specified window by injecting resources.
	 * Windows shown in our application come fully initialized from the GUI
	 * builder, so this additional configuration is not needed.
	@Override
	protected void configureWindow(java.awt.Window root) {
	}*/
	/**
	 * A convenient static getter for the application instance.
	 * @return the instance of WorkbooksApp
	 */
	public static WorkbooksApp getApplication() {
		return Application.getInstance(WorkbooksApp.class);
	}

	public static String getResource(
			String key) {
		ResourceMap map = WorkbooksApp.getInstance().getContext().getResourceMap();
		return map.getString(key);
	}

	/**
	 * Main method launching the application.
	 */
	public static void main(String[] args) {
		WorkbooksApp.launch(WorkbooksApp.class, args);
	}
}
