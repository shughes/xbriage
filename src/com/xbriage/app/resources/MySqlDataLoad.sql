
drop table if exists Account;
create table if not exists Account (
	id bigint auto_increment primary key,
	name varchar(255) not null,
	typeName varchar(255) not null,
    isUserAccount int default 0 not null,
	memo text
);

drop table if exists JournalEntryRow;
create table if not exists JournalEntryRow (
	id bigint auto_increment primary key,
	accountId int not null,
	amount decimal(10,2) default 0.00 not null,
	journalEntryId int default -1,
    isDebitEntry int default 0 not null
);

drop table if exists JournalEntry;
create table if not exists JournalEntry (
	id bigint auto_increment primary key,
    payeeId bigint,
    _date timestamp default current_timestamp not null,
    entryType int default 0,
    memo text
);

drop table if exists Payee;
create table if not exists Payee (
    id bigint auto_increment primary key,
    name varchar(255) default "payee name" not null
);

drop table if exists Workview;
create table if not exists Workview (
	id bigint auto_increment primary key,
	name varchar(200) default "Name" not null,
	parentId bigint,
	isRoot int default 0 not null,
	template varchar(200) default "empty" not null
);

drop table if exists Book;
create table if not exists Book (
	id bigint auto_increment primary key,
	moduleName varchar(200) default "module" not null,
	_row int default 0 not null,
	_column int default 0 not null,
	workviewId bigint default 0
);

insert into Account(id, name, typename, memo, isUserAccount) values(1, 'Cash', 'Asset', 'memo', 1);
insert into Account(id, name, typename, memo, isUserAccount) values(2, 'Checking', 'Asset', 'memo', 1);
insert into Account(id, name, typename, memo, isUserAccount) values(3, 'Credit Card', 'Liability', 'memo', 1);
insert into Account(id, name, typename, memo) values(4, 'Food', 'Expense', 'memo');
insert into Account(id, name, typename, memo) values(5, 'Gas', 'Expense', 'memo');
insert into Account(id, name, typename, memo) values(6, 'No Category', 'Expense', 'memo');

insert into JournalEntry(id, payeeId, memo) values(1, 1, "the memo");
insert into JournalEntry(id, memo) values(2, "a memo");
insert into JournalEntry(id, memo) values(3, "a memo");

insert into JournalEntryRow(id, accountid, amount, journalentryid, isDebitEntry) values(1, 1, 100, 1, 0);
insert into JournalEntryRow(id, accountid, amount, journalentryid, isDebitEntry) values(2, 5, 100, 1, 1);

insert into JournalEntryRow(id, accountid, amount, journalentryid, isDebitEntry) values(3, 1, 55, 2, 1);
insert into JournalEntryRow(id, accountid, amount, journalentryid, isDebitEntry) values(4, 4, 55, 2, 0);

insert into JournalEntryRow(id, accountid, amount, journalentryid, isDebitEntry) values(5, 2, 120, 3, 1);
insert into JournalEntryRow(id, accountid, amount, journalentryid, isDebitEntry) values(6, 5, 120, 3, 0);

insert into Payee(id, name) values(1, "Walgreens");
insert into Payee(id, name) values(2, "Chevron");

insert into Workview(id, name, isRoot) values(1, "Root", 1);
insert into Workview(id, name, isRoot, parentId) values(2, "Home", 0, 1);
insert into Workview(id, name, isRoot, parentId, template) values(3, "View One", 0, 2, "templateOne");
insert into Workview(id, name, isRoot, parentId, template) values(4, "View Two", 0, 2, "templateThree");
insert into Workview(id, name, isRoot, parentId, template) values(5, "View Three", 0, 1, "templateTwo");
insert into Workview(id, name, isRoot, parentId, template) values(6, "View Four", 0, 1, "templateThree");

insert into Book(id, moduleName, _row, _column, workviewId) values(1, "journalModule", 0, 0, 3);
insert into Book(id, moduleName, _row, _column, workviewId) values(2, "accountsModule", 0, 1, 3);
insert into Book(id, moduleName, _row, _column, workviewId) values(3, "accountsModule", 0, 0, 4);
insert into Book(id, moduleName, _row, _column, workviewId) values(4, "journalModule", 0, 0, 5);
insert into Book(id, moduleName, _row, _column, workviewId) values(5, "accountsModule", 1, 0, 5);
insert into Book(id, moduleName, _row, _column, workviewId) values(6, "journalModule", 0, 0, 6);
