drop table if exists Map;
create table Map (
    id identity not null primary key,
    _key varchar not null,
    _value varchar not null
);

drop table if exists Item;
create table Item (
    id identity not null primary key,
    name varchar not null,
    parentId int,
    root boolean default false,
    accountId int 
);

drop table if exists Client;
create table Client (
    id identity not null primary key,
    name varchar(255) not null
);

drop table if exists Account;
create table Account (
	id identity not null primary key,
	name varchar(255) not null, 
    _default boolean default false,
	typeName varchar(255) not null,
    isUserAccount int default 0 not null,
    parentId int,
    isRoot int default 0 not null,
	memo varchar(255)
);

drop table if exists JournalEntryRow;
create table JournalEntryRow (
	id identity not null primary key,
	accountId int not null,
	amount decimal(10,2) default 0.00 not null,
	journalEntryId int default -1,
    isDebitEntry int default 0 not null,
    memo varchar
);

drop table if exists JournalEntry;
create table JournalEntry (
	id identity not null primary key,
    payeeId bigint default 1 not null,
    clientId bigint default 1 not null,
    _date timestamp default current_timestamp not null,
    entryType int default 0,
    memo varchar(255) default '(memo)'
);

drop table if exists Payee;
create table Payee (
    id identity not null primary key,
    _default boolean default false,
    name varchar(255) default 'payee name' not null
);

drop table if exists Workview;
create table Workview (
	id identity not null primary key,
	name varchar(200) default 'Name' not null,
	parentId bigint,
	isRoot int default 0 not null,
	template varchar(200) default 'empty' not null
);

drop table if exists Book;
create table Book (
	id identity not null primary key,
	moduleName varchar(200) default 'module' not null,
	_row int default 0 not null,
	_column int default 0 not null,
    width double default 0 not null,
    height double default 0 not null,
	workviewId bigint default 0
);

insert into Client(id, name) values (1, 'No Client');

insert into Item(id, name, root) values(1, 'Root', true);
insert into Item(id, name, accountId, parentId) values(2, 'Cut legs off', 10, 1);

insert into Account(id, isRoot, name, typename, memo) values(1, 1, 'Root', 'Expense', 'root node');
insert into Account(id, parentId, name, typename, memo, isUserAccount) values(2, 1, 'Checking', 'Asset', 'memo', 1);
insert into Account(id, parentId, name, typename, memo, isUserAccount) values(3, 1, 'Credit Card', 'Liability', 'memo', 1);
insert into Account(id, parentId, name, typename, memo) values(4, 1, 'Food', 'Expense', 'memo');
insert into Account(id, parentId, name, typename, memo) values(5, 8, 'Gas', 'Expense', 'memo');
insert into Account(id, parentId, name, typename, memo, _default) values(6, 1, 'No Category', 'Expense', 'memo', true);
insert into Account(id, parentId, name, typename, memo, isUserAccount) values(7, 1, 'Cash', 'Asset', 'memo', 1);
insert into Account(id, parentId, name, typename, memo) values(8, 1, 'Auto', 'Expense', 'memo');
insert into Account(id, parentId, name, typename, memo) values(9, 8, 'Oil Change', 'Expense', 'memo');
insert into Account(id, parentId, name, typename, memo) values(10, 1, 'Service', 'Expense', 'memo');

insert into JournalEntry(id, payeeId, memo) values(1, 1, 'the memo');
insert into JournalEntry(id, memo) values(2, 'a memo');
insert into JournalEntry(id, memo) values(3, 'a memo');

insert into JournalEntryRow(id, accountid, amount, journalentryid, isDebitEntry) values(1, 7, 100, 1, 0);
insert into JournalEntryRow(id, accountid, amount, journalentryid, isDebitEntry) values(2, 5, 100, 1, 1);

insert into JournalEntryRow(id, accountid, amount, journalentryid, isDebitEntry) values(3, 7, 55, 2, 1);
insert into JournalEntryRow(id, accountid, amount, journalentryid, isDebitEntry) values(4, 4, 55, 2, 0);

insert into JournalEntryRow(id, accountid, amount, journalentryid, isDebitEntry) values(5, 2, 120, 3, 1);
insert into JournalEntryRow(id, accountid, amount, journalentryid, isDebitEntry) values(6, 5, 120, 3, 0);

insert into Payee(id, name, _default) values(1, 'No Payee', true);
insert into Payee(id, name) values(2, 'Chevron');
insert into Payee(id, name) values(3, 'Walgreens');

insert into Workview(id, name, isRoot) values(1, 'Root', 1);
insert into Workview(id, name, isRoot, parentId) values(2, 'Home', 0, 1);
insert into Workview(id, name, isRoot, parentId, template) values(3, 'View One', 0, 2, 'templateOne');
insert into Workview(id, name, isRoot, parentId, template) values(4, 'View Two', 0, 2, 'templateThree');
insert into Workview(id, name, isRoot, parentId, template) values(5, 'View Three', 0, 1, 'templateTwo');
insert into Workview(id, name, isRoot, parentId, template) values(6, 'View Four', 0, 1, 'templateThree');

insert into Book(id, moduleName, _row, _column, workviewId, width, height) values(1, 'Journal.module', 0, 0, 3, .4, 1);
insert into Book(id, moduleName, _row, _column, workviewId, width, height) values(2, 'Categories.module', 0, 1, 3, .6, 1);

insert into Book(id, moduleName, _row, _column, workviewId) values(3, 'Categories.module', 0, 0, 4);

insert into Book(id, moduleName, _row, _column, workviewId, width, height) values(4, 'Journal.module', 0, 0, 5, 1, .6);
insert into Book(id, moduleName, _row, _column, workviewId, width, height) values(5, 'Categories.module', 1, 0, 5, 1, .4);

insert into Book(id, moduleName, _row, _column, workviewId) values(6, 'Journal.module', 0, 0, 6);
