/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.journal;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;

/**
 *
 * @author shughes
 */
public class AccountsTable extends JTable {

	private List<ActionListener> actionListeners = new ArrayList<ActionListener>();

	@Override
	public void changeSelection(int row, int col, boolean toggle, boolean extend) {
		super.changeSelection(row, col, toggle, extend);
		fireSelectionChanged();
	}

	protected void fireSelectionChanged() {
		ActionEvent event = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "selectionChanged");
		for (ActionListener listener : actionListeners) {
			listener.actionPerformed(event);
		}
	}

	public void addActionListener(ActionListener listener) {
		actionListeners.add(listener);
	}
}
