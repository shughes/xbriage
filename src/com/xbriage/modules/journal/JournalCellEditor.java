/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.journal;

import com.xbriage.modules.journal.proxy.EntryNode;
import com.xbriage.util.LookAheadTextField;
import com.xbriage.services.Responder;
import com.xbriage.services.AccountDao;
import com.xbriage.domain.JournalEntry;
import com.xbriage.domain.JournalEntryRow;
import com.xbriage.services.Service;
import com.xbriage.context.EmfContext;
import com.xbriage.services.Service;
import com.xbriage.modules.journal.models.JournalTableModel;
import com.xbriage.modules.journal.proxy.RowProxy;
import com.xbriage.services.ModuleLoader;
import com.xbriage.util.BindUtil;
import com.xbriage.util.GeneralUtil;
import java.awt.Component;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import javax.persistence.EntityManager;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.EventListenerList;
import javax.swing.table.TableCellEditor;

/**
 *
 * @author shughes
 */
public class JournalCellEditor extends JournalCellRenderer implements TableCellEditor {

	private EventListenerList listeners = new EventListenerList();
	private ChangeEvent event = new ChangeEvent(this);
	private EntryNode entryNode;
	private JournalModule module = Service.load(ModuleLoader.class).get("Journal.module");
	private JournalCell cell;
	private List<Responder> keyResponders;
	private List<Responder> categoriesButtonResponders;

	public void nothing() {
	// do nothing. this overrides other listeners
	}

	//private GlassPane glassPane = new GlassPane();
	public JournalCellEditor() {
		cell = getCell();
		cell.setSelected(true);
		cell.setFocusable(true);

		BindUtil.bind(this, "nothing", cell, BindUtil.MOUSE_PRESSED);

		LookAheadTextField numField = (LookAheadTextField) cell.getNumField();
		LookAheadTextField payeeField = (LookAheadTextField) cell.getPayeeField();
		LookAheadTextField categoryField = (LookAheadTextField) cell.getCategoryField();

		List<String> entryTypesArr = new ArrayList<String>();
		for (String str : GeneralUtil.getEntryTypes()) {
			entryTypesArr.add(str);
		}
		StringListLookAhead lookAhead = new StringListLookAhead();
		lookAhead.setValues(entryTypesArr);
		numField.setLookAhead(lookAhead);

		payeeField.setLookAhead(new PayeesLookAhead());
		categoryField.setLookAhead(new StringListLookAhead());

	}

	public JournalModule getModule() {
		return module;
	}

	public void setModule(JournalModule module) {
		this.module = module;
	}

	public List<Responder> getKeyResponders() {
		return keyResponders;
	}

	public void setKeyResponders(List<Responder> keyResponders) {
		this.keyResponders = keyResponders;
	}

	public List<Responder> getCategoriesButtonResponders() {
		return categoriesButtonResponders;
	}

	public void setCategoriesButtonResponders(List<Responder> categoriesButtonResponders) {
		this.categoriesButtonResponders = categoriesButtonResponders;
	}

	/**
	 * JournalTableModel.setSelectedEntry is set here.
	 * 
	 * @param table
	 * @param value
	 * @param isSelected
	 * @param row
	 * @param column
	 * @return
	 */
	@Override
	public Component getTableCellEditorComponent(JTable table, Object value,
			boolean isSelected, int row, int column) {

		entryNode = (EntryNode) value;

		setupCell(cell, entryNode, isSelected, true);

		cell.getShowCategoriesButton().setVisible(false);
		cell.getShowPayeesButton().setVisible(false);
		cell.getClearSplitButton().setVisible(false);

		if (cell.getCategoryField().getText().equals("-- split --")) {
			cell.getShowCategoriesButton().setVisible(true);
			cell.getClearSplitButton().setVisible(true);
		}

		JournalEntry entry = entryNode.getEntry();
		EntityManager em = EmfContext.create();
		entry = em.find(JournalEntry.class, entry.getId());
		JournalTableModel.setSelectedEntry(entry);

		// setup splitRows
		AccountDao dao = Service.load(AccountDao.class);
		JournalEntryRow defaultRow = entryNode.getIndex().get(JournalTableModel.getSelectedAccount());
		List<RowProxy> splitRows = new ArrayList<RowProxy>();
		for (JournalEntryRow entryRow : entry.getRows()) {
			if (!entryRow.equals(defaultRow)) {
				RowProxy proxy = new RowProxy();
				proxy.setAccount(dao.concatName(entryRow.getAccount()));
				proxy.setMemo(entryRow.getMemo());
				
				BigDecimal amount = entryRow.getAmount();
				if(entryRow.isDebitEntry().compareTo(defaultRow.isDebitEntry()) == 0) {
					amount = amount.multiply(new BigDecimal(-1));
				}
				proxy.setAmount(GeneralUtil.formatStringNumber(amount.toString()));
				splitRows.add(proxy);
			}
		}
		entryNode.setSplitRows(splitRows);

		// enabling below.

		cell.setSelected(true);

		cell.getDateField().setEnabled(true);
		cell.getNumField().setEnabled(true);
		cell.getPayeeField().setEnabled(true);
		cell.getMemoField().setEnabled(true);
		cell.getDepositField().setEnabled(true);
		cell.getPaymentField().setEnabled(true);
		cell.getCategoryField().setEnabled(true);

		if (cell.getCategoryField().getText().equals("-- split --")) {
			cell.getClearSplitButton().setEnabled(true);
			cell.getShowCategoriesButton().setEnabled(false);
			cell.getCategoryField().setEnabled(false);
		} else {
			cell.getClearSplitButton().setEnabled(false);
			cell.getShowCategoriesButton().setEnabled(true);
		}

		return cell;
	}

	public Object getCellEditorValue() {
		return getEntryNode();
	}

	public boolean isCellEditable(EventObject anEvent) {
		return true;
	}

	public boolean shouldSelectCell(EventObject anEvent) {
		return true;
	}

	/**
	 * Hides JournalCellEditor's panel, so what is seen is JournalCellRender's panel,
	 * which looks the same.
	 * 
	 * @return
	 */
	public boolean stopCellEditing() {
		fireEditingStopped();
		return true;
	}

	public void cancelCellEditing() {
		fireEditingCanceled();
	}

	public void addCellEditorListener(CellEditorListener l) {
		listeners.add(CellEditorListener.class, l);
	}

	public void removeCellEditorListener(CellEditorListener l) {
		listeners.remove(CellEditorListener.class, l);
	}

	public void fireEditingStopped() {
		Object[] l = listeners.getListenerList();
		for (int i = l.length - 2; i >= 0; i -= 2) {
			((CellEditorListener) l[i + 1]).editingStopped(event);
		}

	}

	public void fireEditingCanceled() {
		Object[] l = listeners.getListenerList();
		for (int i = l.length - 2; i >= 0; i -= 2) {
			((CellEditorListener) l[i + 1]).editingCanceled(event);
		}
	}

	public EntryNode getEntryNode() {
		return entryNode;
	}

	public void setEntryNode(EntryNode entryRow) {
		this.entryNode = entryRow;
	}
}
