/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.journal;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author shughes
 */
public class JournalEntriesLayout implements LayoutManager {

	private int minWidth = 0,  minHeight = 0;
	private JournalModule module;

	public JournalEntriesLayout(JournalModule mod) {
		module = mod;
	}

	public void addLayoutComponent(String name, Component comp) {

	}

	public void removeLayoutComponent(Component comp) {

	}

	public Dimension preferredLayoutSize(Container parent) {
		return parent.getSize();
	}

	public Dimension minimumLayoutSize(Container parent) {
		return new Dimension(0, 0);
	}

	private Point getRowButtonsPoint() {
		int y;
		JTable table = module.getEntriesTable();
		JournalCellEditor cellEditor = (JournalCellEditor) module.getEntriesTable().getColumnModel().getColumn(0).getCellEditor();
		Rectangle bounds = cellEditor.getCell().getBounds();
		Rectangle scrollBounds = module.getJScrollPane3().getBounds();
		Rectangle tableBounds = table.getBounds();
		y = bounds.y;
		int x = table.getWidth() - JournalController.ROW_WIDTH;
		y = y + table.getTableHeader().getHeaderRect(0).height;
		y = y + table.getRowHeight();
		y = y + tableBounds.y;

		RowButtons buttons = RowButtons.getInstance();
		
		if(y < HeaderRenderer.HEIGHT) {
			buttons.setVisible(false);
		} 
		
		int extra = 1;
		
		if (y + JournalController.ROW_HEIGHT > scrollBounds.height) { // && !splitPanel.isVisible()) {
			buttons.setOnTop(true);
			buttons.setButtonsY(2);
			buttons.repaint();
			y = (y - table.getRowHeight() - JournalController.ROW_HEIGHT) + extra;
		} else {
			buttons.setOnTop(false);
			buttons.setButtonsY(1);
		}
		
		Point p = new Point();
		p.x = x;
		p.y = y;
		return p;
	}

	public void layoutContainer(Container parent) {
		int count = parent.getComponentCount();
		for (int i = 0; i < count; i++) {
			if (parent.getComponent(i) instanceof RowButtons) { // rowButtons
				if (module != null) {
					Point p = this.getRowButtonsPoint();
					parent.getComponent(i).setBounds(p.x, p.y, JournalController.ROW_WIDTH, JournalController.ROW_HEIGHT);
				}
			} else if (parent.getComponent(i) instanceof JScrollPane) { // entries table?
				Component comp = parent.getComponent(i);
				comp.setBounds(0, 0, parent.getWidth(), parent.getHeight());
			} 
		}
	}
}
