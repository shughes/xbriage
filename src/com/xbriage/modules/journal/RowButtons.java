/*
 * RowButtons.java
 *
 * Created on April 23, 2008, 11:27 PM
 */
package com.xbriage.modules.journal;

import com.xbriage.util.BindUtil;
import java.awt.BasicStroke;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.geom.GeneralPath;

/**
 *
 * @author  shughes
 */
public class RowButtons extends javax.swing.JPanel {


	private static RowButtons instance = new RowButtons();
	private boolean onTop = false;
	private boolean split = false;

	public static RowButtons getInstance() {
		return instance;
	}
	
	public void nothing() {
		
	}

	/** Creates new form RowButtons */
	public RowButtons() {		
		initComponents();
		
		BindUtil.bind(this, "nothing", this, BindUtil.MOUSE_PRESSED);
		
		setOpaque(false);
		setVisible(false);
		int h = 16;		
		removeButton.setSize(removeButton.getSize().width, h);
		splitButton.setSize(splitButton.getSize().width, h);
		enterButton.setSize(enterButton.getSize().width, h);
		int gap = 5;
		int width = removeButton.getWidth() + gap + splitButton.getWidth() + gap + enterButton.getWidth();
		int left = (JournalController.ROW_WIDTH - width) / 2;
		int y = 1;
		enterButton.setBounds(left, y, enterButton.getWidth(), h);
		left = left + enterButton.getWidth() + gap;
		removeButton.setBounds(left, y, removeButton.getWidth(), h);
		left = left + removeButton.getWidth() + gap;
		splitButton.setBounds(left, y, splitButton.getWidth(), h);		
	}
	
	public void setButtonsY(int value) {
		Rectangle bounds = removeButton.getBounds();
		bounds.y = value;
		removeButton.setBounds(bounds);
		bounds = splitButton.getBounds();
		bounds.y = value;
		splitButton.setBounds(bounds);
		bounds = enterButton.getBounds();
		bounds.y = value;
		enterButton.setBounds(bounds);
	}

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		Dimension d = getSize();
		GeneralPath line = new GeneralPath(GeneralPath.WIND_EVEN_ODD, 4);
		Stroke s = new BasicStroke(2);
		int x1 = 8;
		if (isOnTop()) {
			g2d.setPaint(JournalCell.SELECTED_COLOR);
			g2d.fillRect(0, d.height - 2, d.width, 2);
			int width = d.width;
			int height = d.height;
			line.moveTo(0, height);
			line.lineTo(x1, 0);
			line.lineTo(width - x1, 0);
			line.lineTo(width, height);
			g2d.fill(line);
			g2d.setStroke(s);
			g2d.setColor(JournalCell.SELECTED_LINES_COLOR);
			g2d.draw(line);
			g2d.drawLine(x1, 1, width-x1, 1);
		} else {
			g2d.setPaint(JournalCell.SELECTED_COLOR);
			int width = d.width;
			int height = d.height;
			line.moveTo(0, 0);
			line.lineTo(x1, height);
			line.lineTo(width - x1, height);
			line.lineTo(width, 0);
			g2d.fill(line);
			g2d.setPaint(JournalCell.SELECTED_LINES_COLOR);
			g2d.setStroke(s);
			g2d.draw(line);
			g2d.drawLine(x1, height-1, width-x1, height-1);
		}
	}

	/** This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        enterButton = new javax.swing.JButton();
        removeButton = new javax.swing.JButton();
        splitButton = new javax.swing.JButton();

        setName("Form"); // NOI18N
        setOpaque(false);
        setLayout(null);

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(com.xbriage.app.WorkbooksApp.class).getContext().getResourceMap(RowButtons.class);
        enterButton.setFont(resourceMap.getFont("enterButton.font")); // NOI18N
        enterButton.setText(resourceMap.getString("enterButton.text")); // NOI18N
        enterButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        enterButton.setName("enterButton"); // NOI18N
        add(enterButton);
        enterButton.setBounds(10, 0, 40, 20);

        removeButton.setFont(resourceMap.getFont("removeButton.font")); // NOI18N
        removeButton.setText(resourceMap.getString("removeButton.text")); // NOI18N
        removeButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        removeButton.setName("removeButton"); // NOI18N
        add(removeButton);
        removeButton.setBounds(60, 0, 50, 20);

        splitButton.setFont(resourceMap.getFont("splitButton.font")); // NOI18N
        splitButton.setText(resourceMap.getString("splitButton.text")); // NOI18N
        splitButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        splitButton.setName("splitButton"); // NOI18N
        add(splitButton);
        splitButton.setBounds(120, 0, 40, 20);
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton enterButton;
    private javax.swing.JButton removeButton;
    private javax.swing.JButton splitButton;
    // End of variables declaration//GEN-END:variables
	public javax.swing.JButton getEnterButton() {
		return enterButton;
	}

	public void setEnterButton(javax.swing.JButton enterButton) {
		this.enterButton = enterButton;
	}

	public javax.swing.JButton getRemoveButton() {
		return removeButton;
	}

	public void setRemoveButton(javax.swing.JButton removeButton) {
		this.removeButton = removeButton;
	}


	public javax.swing.JButton getSplitButton() {
		return splitButton;
	}

	public void setSplitButton(javax.swing.JButton splitButton) {
		this.splitButton = splitButton;
	}

	public boolean isOnTop() {
		return onTop;
	}

	public void setOnTop(boolean onTop) {
		this.onTop = onTop;
	}

	public boolean isSplit() {
		return split;
	}

	public void setSplit(boolean split) {
		this.split = split;
	}
}
