/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.journal;

import com.xbriage.util.LookAheadTextField;
import com.xbriage.domain.Payee;
import com.xbriage.context.EmfContext;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author shughes
 */
public class PayeesLookAhead implements LookAheadTextField.TextLookAhead {

	private EntityManagerFactory emf = EmfContext.get();

	public String doLookAhead(String key) {
		key = key.toLowerCase();
		
		EntityManager em = emf.createEntityManager();
		Query q = em.createQuery("select a from Payee a where lower(a.name) like :name");
		String newKey = "%" + key + "%";
		q.setParameter("name", newKey);
		List<Payee> result = q.getResultList();
		String name = null;
		
		for (Payee payee : result) {
			String tmp = payee.getName().toLowerCase();
			if (tmp.startsWith(key)) {
				name = payee.getName();
			}
		}

		if (name == null) {
			return key;
		} else {
			return name;
		}
	}
}
