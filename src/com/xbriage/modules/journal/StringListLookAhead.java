/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.journal;

import com.xbriage.util.LookAheadTextField;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author shughes
 */
public class StringListLookAhead implements LookAheadTextField.TextLookAhead {

	public StringListLookAhead() {
		values = new ArrayList<String>();
	}

	public StringListLookAhead(List<String> values) {
		this.values = values;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}

	public List<String> getValues() {
		return values;
	}

	public String doLookAhead(String key) {
		key = key.toLowerCase();

		// Look for a string that starts with the key
		for (String value: values) {
			String tmp = value.toLowerCase();
			if (tmp.startsWith(key) == true) {
				return value;
			}	
		}

		// No match found - return null
		return null;
	}
	protected List<String> values;
}

