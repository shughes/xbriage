/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.journal.actions;

import com.xbriage.app.UndoActions;
import com.xbriage.modules.journal.proxy.ProxyContext;
import com.xbriage.modules.journal.proxy.EntryNode;
import com.xbriage.app.WorkbooksApp;
import com.xbriage.modules.journal.*;
import com.xbriage.modules.journal.models.JournalTableModel;
import com.xbriage.domain.Account;
import com.xbriage.domain.JournalEntry;
import com.xbriage.domain.JournalEntryRow;
import com.xbriage.context.EmfContext;
import com.xbriage.services.Service;
import com.xbriage.modules.journal.proxy.EntryProxy;
import com.xbriage.services.ModuleLoader;
import com.xbriage.services.Refresh;
import com.xbriage.util.GeneralUtil;
import com.xbriage.services.Service;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import javax.swing.SwingWorker;

/**
 *
 * @author shughes
 */
public class RowActions {

	private JournalModule journalModule = null;
	private JournalCellEditor journalCellEditor;
	private JournalTableModel journalTableModel;
	private final static RowActions instance = new RowActions();
	private boolean shouldSave = true;

	public void setShouldSave(boolean s) {
		shouldSave = s;
	}

	public boolean shouldSave() {
		return shouldSave;
	}

	public static RowActions getInstance() {
		return instance;
	}

	private RowActions() {
		journalModule = Service.load(ModuleLoader.class).get("Journal.module");
		journalCellEditor = (JournalCellEditor) journalModule.getCellEditor();
		journalTableModel = (JournalTableModel) journalModule.getEntriesTable().getModel();
	}

	private class RefreshNode {

		private int row;
		private String balance;

		RefreshNode(int row, String balance) {
			this.row = row;
			this.balance = balance;
		}

		public int getRow() {
			return row;
		}

		public String getBalance() {
			return balance;
		}
	}

	private class RefreshBalance extends SwingWorker<String, RefreshNode> {

		private int index = 0;

		public RefreshBalance() {
		}

		public RefreshBalance(int index) {
			this.index = index;
		}

		@Override
		protected String doInBackground() throws Exception {
			JournalTableModel model = (JournalTableModel) journalModule.getEntriesTable().getModel();
			EntityManager em = EmfContext.create();
			//EntryNode p = model.getRowData().get(index);
			BigDecimal balance = null;
			if(index == 0) {
				balance = new BigDecimal(0);
			} else {
				balance = new BigDecimal(model.getRowData().get(index-1).getBalance());
			}
			
			System.out.println("beg bal: "+balance.toString());

			for (int i = index; i < model.getRowData().size(); i++) {

				EntryNode entryNode = model.getRowData().get(i);

				JournalEntry entry = entryNode.getEntry();
				entry = em.find(JournalEntry.class, entry.getId());
				entryNode.setEntry(entry);

				Account account = JournalTableModel.getSelectedAccount();
				JournalEntryRow row = null;

				row = entryNode.getIndex().get(account);
				row = em.find(JournalEntryRow.class, row.getId());

				if (row.isDebitEntry() == 1) {
					balance = balance.add(row.getAmount());
				} else {
					balance = balance.subtract(row.getAmount());
				}

				String sBal = GeneralUtil.formatStringNumber(balance.toString());
//				String sBal = GeneralUtil.formatStringNumberWithCommas(balance.toString());
				entryNode.setBalance(sBal);
			}

			return GeneralUtil.formatStringNumber(balance.toString());
		}

		@Override
		protected void process(List<RefreshNode> nodes) {

			for (RefreshNode node : nodes) {
				JournalTableModel model = (JournalTableModel) journalModule.getEntriesTable().getModel();

				EntryNode entryNode = model.getRowData().get(node.getRow());

				entryNode.setBalance(node.getBalance());
				model.getRowData().set(node.getRow(), entryNode);
				model.fireTableRowsUpdated(node.getRow(), node.getRow());

				if (!journalCellEditor.getCell().getCategoryField().getText().equals("-- split --")) {
					journalCellEditor.getCell().getCategoryField().setEnabled(true);
				}
			}
		}

		@Override
		protected void done() {
			//String sBal = get();
			JournalTableModel model = (JournalTableModel) journalModule.getEntriesTable().getModel();

			int row = journalModule.getEntriesTable().getSelectedRow();
			if (row != -1) {
				journalCellEditor.getCell().getBalanceLabel().setText(((EntryNode) model.getValueAt(row, 0)).getBalance());
			}

			model.fireTableRowsUpdated(0, model.getRowCount());

			if (model.getRowCount() > 0) {
				String sBal = model.getRowData().get(model.getRowCount() - 1).getBalance();
				BigDecimal balance = new BigDecimal(sBal);
				if (balance.compareTo(new BigDecimal(0)) == -1) {
					sBal = GeneralUtil.formatStringNumberWithCommas(sBal);
					sBal = GeneralUtil.formatRedString(sBal);
				} else {
					sBal = GeneralUtil.formatStringNumberWithCommas(sBal);
				}
				journalModule.getBalanceLabel().setText(sBal);

				if (!journalCellEditor.getCell().getCategoryField().getText().equals("-- split --")) {
					journalCellEditor.getCell().getCategoryField().setEnabled(true);
				}
			}

		}
	}

	public void refreshBalance(int index) {
		new RefreshBalance(index).execute();
	}

	public void refreshBalance() {
		new RefreshBalance().execute();
	}

	private void add() {
		Account account = JournalTableModel.getSelectedAccount();
		ProxyContext.getInstance().addEntry(account);
	}

	public void enter() {
		journalModule.getCategoriesMenu().setVisible(false);
		journalModule.getPayeesMenu().setVisible(false);

		JournalTableModel model = (JournalTableModel) journalModule.getEntriesTable().getModel();
		model.setConfirmed(true);
		int row = journalModule.getEntriesTable().getSelectedRow();

		// will cause row to be saved
		journalCellEditor.stopCellEditing();

		if (row == (journalModule.getEntriesTable().getRowCount() - 1)) {
			add();
		}

		GeneralUtil.moveRow(journalModule.getEntriesTable(), ++row);
		journalCellEditor.getCell().getDateField().requestFocusInWindow();

		refreshBalance(row-1);
	}

	public boolean save() {
		JournalCell cell = journalCellEditor.getCell();
		JournalTableModel model = (JournalTableModel) journalModule.getEntriesTable().getModel();
		int row = journalModule.getEntriesTable().getSelectedRow();
		EntryNode node = model.getRowData().get(row);

		EntryProxy rowProxy = new EntryProxy();
		rowProxy.setDate(cell.getDateField().getText());
		rowProxy.setNum(cell.getNumField().getText());
		rowProxy.setPayee(cell.getPayeeField().getText());
		rowProxy.setMemo(cell.getMemoField().getText());
		rowProxy.setPayment(cell.getPaymentField().getText());
		rowProxy.setDeposit(cell.getDepositField().getText());
		rowProxy.setCategory(cell.getCategoryField().getText());

		ProxyContext.getInstance().saveEntry(rowProxy, node, JournalTableModel.getSelectedAccount());

		return true;
	}

	public void remove() {
		boolean yes = GeneralUtil.showWarningAlert(WorkbooksApp.getWarning("RowActions.shouldRemove"));

		if (yes) {
			shouldSave = false;
			int row = journalModule.getEntriesTable().getSelectedRow();
			EntryNode proxy = (EntryNode) journalModule.getEntriesTable().getModel().getValueAt(row, 0);

			UndoActions.getInstance().addUndo(new UndoableJournalEntry(UndoableJournalEntry.REMOVE, proxy));

			ProxyContext.getInstance().removeEntry(proxy);
			
			Service.load(Refresh.class).refresh("RowActions.remove");

			int rows = journalModule.getEntriesTable().getRowCount();
			if (row < rows) {
				GeneralUtil.moveRow(journalModule.getEntriesTable(), row);
			}
		}
		
		journalCellEditor.getCell().getDateField().requestFocusInWindow();
	}

	public JournalModule getJournalModule() {
		return journalModule;
	}

	public void setJournalModule(JournalModule journalModule) {
		this.journalModule = journalModule;
	}

	public JournalCellEditor getJournalCellEditor() {
		return journalCellEditor;
	}

	public void setJournalCellEditor(JournalCellEditor journalCellEditor) {
		this.journalCellEditor = journalCellEditor;
	}

	public JournalTableModel getJournalTableModel() {
		return journalTableModel;
	}

	public void setJournalTableModel(JournalTableModel journalTableModel) {
		this.journalTableModel = journalTableModel;
	}
}
