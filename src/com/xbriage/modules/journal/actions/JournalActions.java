/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.journal.actions;

import com.xbriage.app.UndoActions;
import com.xbriage.modules.journal.proxy.ProxyContext;
import com.xbriage.modules.journal.proxy.EntryNode;
import com.xbriage.modules.journal.models.JournalTableModel;
import com.xbriage.modules.journal.models.AccountsModel;
import com.xbriage.modules.journal.models.PayeesModel;
import com.xbriage.modules.journal.*;
import com.xbriage.util.LookAheadTextField;
import com.xbriage.app.TemplateData;
import com.xbriage.app.WorkbooksApp;
import com.xbriage.services.AccountDao;
import com.xbriage.services.AccountDao;
import com.xbriage.domain.Account;
import com.xbriage.domain.JournalEntry;
import com.xbriage.domain.JournalEntryRow;
import com.xbriage.domain.Payee;
import com.xbriage.context.EmfContext;
import com.xbriage.context.MapContext;
import com.xbriage.services.Service;
import com.xbriage.util.AccountComparator;
import com.xbriage.modules.categories.CategoryActions;
import com.xbriage.modules.journal.models.CategoriesModel;
import com.xbriage.services.ModuleLoader;
import com.xbriage.services.Refresh;
import com.xbriage.util.GeneralUtil;
import com.xbriage.util.GeneralUtil;
import com.xbriage.services.Service;
import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 *
 * @author shughes
 */
public class JournalActions {

	private JTable splitEntryTable = SplitPanel.getInstance().getTable();
	private final RowActions rowActions = RowActions.getInstance();
	private final JournalModule journalModule = Service.load(ModuleLoader.class).get("Journal.module");
	private JournalCellEditor journalCellEditor = (JournalCellEditor) journalModule.getCellEditor();
	private final PayeesComboBox payeesComboBox = PayeesComboBox.getInstance();
	private final CategoriesComboBox categoriesComboBox = CategoriesComboBox.getInstance();
	private final static JournalActions instance = new JournalActions();
	private boolean triggerPopup = true;
	private List<EntryNode> tmpList;
	private ProxyContext proxyContext = ProxyContext.getInstance();
	private int oldAccountsTableIndex = -1; //
	private boolean forceBalance = false;

	public static JournalActions getInstance() {
		return instance;
	}

	protected JournalActions() {

	}

	public void setForceBalance(boolean forceBalance) {
		this.forceBalance = forceBalance;
	}

	public void updatePayeeTextField() {
		JTextField textField = journalCellEditor.getCell().getPayeeField();
		JTable table = payeesComboBox.getTable();
		int row = table.getSelectedRow();
		String payee = (String) table.getModel().getValueAt(row, 0);
		textField.setText(payee);
	}

	public void hideCategories() {
		journalModule.getCategoriesMenu().setVisible(false);
		triggerPopup = false;
		journalCellEditor.getCell().getCategoryField().requestFocusInWindow();
	}

	public void hidePayees() {
		journalModule.getPayeesMenu().setVisible(false);
		triggerPopup = false;
		journalCellEditor.getCell().getPayeeField().requestFocusInWindow();
	}

	public void updateCategorySelection() {
		JournalCell cell = journalCellEditor.getCell();
		String value = cell.getCategoryField().getText();
		AccountDao dao = Service.load(AccountDao.class);
		CategoriesModel model = (CategoriesModel) categoriesComboBox.getTable().getModel();

		for (int i = 0; i < model.getCategories().size(); i++) {
			value = value.toLowerCase();

			Account account = model.getCategories().get(i);
			String fullName = dao.concatName(account);
			fullName = fullName.toLowerCase();

			if (value.equals("")) {

			} else {

				if (fullName.equals(value)) {
					JTable table = categoriesComboBox.getTable();
					table.getSelectionModel().setSelectionInterval(i, i);
					table.scrollRectToVisible(table.getCellRect(i, 0, true));
				}
			}
		}

	}

	public void updatePayeeSelection() {
		JournalCell cell = journalCellEditor.getCell();
		String value = cell.getPayeeField().getText();
		PayeesModel model = (PayeesModel) payeesComboBox.getTable().getModel();

		for (int i = 0; i < model.getRowCount(); i++) {
			String name = (String) model.getValueAt(i, 0);
			if (name.equals(value)) {
				JTable table = payeesComboBox.getTable();
				table.getSelectionModel().setSelectionInterval(i, i);
				table.scrollRectToVisible(table.getCellRect(i, 0, true));
			}
		}

	}

	public void updateCategoryTextField() {
		JTable table = categoriesComboBox.getTable();
		int row = table.getSelectedRow();
		if (row != -1) {
			CategoriesModel model = (CategoriesModel) table.getModel();
			Account account = model.getCategories().get(row);
			AccountDao dao = Service.load(AccountDao.class);
			String name = dao.concatName(account);
			JTextField textField = journalCellEditor.getCell().getCategoryField();
			textField.setText(name);
		}
	}

	public void showPopup() {
		Map params = (Map) MapContext.get(getClass().getName() + ".showPopup");
		MouseEvent evt = (MouseEvent) params.get(MouseEvent.class);
		int row = journalModule.getAccountsTable().rowAtPoint(evt.getPoint());
		if (evt.isPopupTrigger()) {
			if (row != -1) {
				GeneralUtil.moveRow(journalModule.getAccountsTable(), row);
			}
			journalModule.getPopupMenu().show(evt.getComponent(), evt.getX(), evt.getY());
		}
	}

	public void adjustRowButtons() {

		boolean validate = false;
		if (journalModule.getEntriesTable().getSelectedRow() == -1) {
			validate = false;
		} else {
			RowButtons.getInstance().setVisible(true);
			validate = true;
		}

		if (validate) {
			journalModule.getTablePane().validate();
		}
	}

	public void adjustRowButtons(PropertyChangeEvent evt) {
		adjustRowButtons();
	}

	public void adjustForAccountSelectionChange() {

		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				int index = journalModule.getAccountsTable().getSelectedRow();
				if (index != oldAccountsTableIndex) {
					UndoActions.getInstance().discardEdits();
				}
				oldAccountsTableIndex = index;

				AccountsModel model = (AccountsModel) journalModule.getAccountsTable().getModel();
				if (index != -1) {
					if (index < model.getAccounts().size()) {
						int id = model.getAccounts().get(index).getId();
						EntityManager em = EmfContext.create();
						Account account = em.find(Account.class, id);

						int rowSize = getEntryRowSize(account);
						if (rowSize < 1 && rowSize != -1) {
							ProxyContext.getInstance().addEntry(account);
							rowSize++;
						}

						refresh();

						GeneralUtil.moveRow(journalModule.getEntriesTable(), rowSize - 1);
						journalModule.getEntriesTable().scrollRectToVisible(journalModule.getEntriesTable().getCellRect(rowSize - 1, 0, true));
						journalCellEditor.getCell().getDateField().requestFocusInWindow();
						journalModule.getSearchField().setText("");
						refreshCategoriesComboBox();
					}
				}

			}
		});
	}
	
	private int getEntryRowSize(Account account) {
		EntityManager em = EmfContext.create();
		Query q = em.createQuery("select entry from JournalEntry entry, JournalEntryRow row where row.account = :account and row.journalEntry = entry");
		q.setParameter("account", account);
		List<JournalEntry> result = q.getResultList();
		Map<JournalEntry, Boolean> entryManager = new HashMap<JournalEntry, Boolean>();
		for (JournalEntry entry : result) {
			entryManager.put(entry, true);
		}
		return entryManager.size();
	}

	public void enableRemoveButton(PropertyChangeEvent evt) {
		RowButtons rowButtons = RowButtons.getInstance();
		if (journalModule.getEntriesTable().getModel().getRowCount() == 1) {
			rowButtons.getRemoveButton().setEnabled(false);
		} else {
			rowButtons.getRemoveButton().setEnabled(true);
		}
	}

	public void clearAccountSelection() {
		JournalTableModel model = (JournalTableModel) journalModule.getEntriesTable().getModel();
		journalModule.getAccountsTable().clearSelection();
		RowButtons.getInstance().setVisible(false);
		List<EntryNode> emptyRows = new ArrayList<EntryNode>();
		model.setRowData(emptyRows);
	}

	public void clear() {
		journalModule.getSearchField().setText("");
		ProxyContext.getInstance().refreshProxies(JournalTableModel.getSelectedAccount());
		int row = journalModule.getEntriesTable().getRowCount() - 1;
		if (row != -1) {
			GeneralUtil.moveRow(journalModule.getEntriesTable(), row);
		}
		RowActions.getInstance().refreshBalance();
	}

	public void search() {
		String query = journalModule.getSearchField().getText().trim();
		JournalTableModel model = (JournalTableModel) journalModule.getEntriesTable().getModel();
		if (!query.equals("")) {
			List<EntryNode> nodes = ProxyContext.getInstance().filterProxies(JournalTableModel.getSelectedAccount(), query);
			model.setRowData(nodes);
			int row = nodes.size() - 1;
			if (row < journalModule.getEntriesTable().getRowCount() && row != -1) {
				GeneralUtil.moveRow(journalModule.getEntriesTable(), row);
			} else {
				RowButtons.getInstance().setVisible(false);
			}
		} else {
			ProxyContext.getInstance().refreshProxies(JournalTableModel.getSelectedAccount());
			if (model.getRowCount() > 0) {
				GeneralUtil.moveRow(journalModule.getEntriesTable(), model.getRowCount() - 1);
			}
		}
		RowActions.getInstance().refreshBalance();
	}

	public void refreshAndSetRow() {
		int row = journalModule.getEntriesTable().getSelectedRow();
		refresh();
		if (row != -1) {
			GeneralUtil.moveRow(journalModule.getEntriesTable(), row);
		}
	}

	public void refresh() {
		_refresh(true);
	}

	public void refreshWithoutBalanceAndSetRow() {
		int row = journalModule.getEntriesTable().getSelectedRow();
		if (forceBalance) {
			_refresh(true);
		} else {
			_refresh(false);
		}
		if (row != -1) {
			GeneralUtil.moveRow(journalModule.getEntriesTable(), row);
		}
	}

	private void _refresh(boolean withBalance) {
		if (TemplateData.getInstance().isDisplayed(journalModule)) {
			int index = journalModule.getAccountsTable().getSelectedRow();
			AccountsModel accountsModel = (AccountsModel) journalModule.getAccountsTable().getModel();

			if (index != -1) {
				if (index < accountsModel.getAccounts().size()) {
					Account account = accountsModel.getAccounts().get(index);
					EntityManager em = EmfContext.create();
					account = em.find(Account.class, account.getId());

					if (account.getTypeName().equals("Credit Card")) {
						HeaderRenderer header = (HeaderRenderer) journalModule.getEntriesTable().getColumnModel().getColumn(0).getHeaderRenderer();
						header.getCell().getPaymentField().setText("Charge");
						header.getCell().getDepositField().setText("Payment");
					} else {
						HeaderRenderer header = (HeaderRenderer) journalModule.getEntriesTable().getColumnModel().getColumn(0).getHeaderRenderer();
						header.getCell().getPaymentField().setText("Payment");
						header.getCell().getDepositField().setText("Deposit");
					}

					JournalTableModel.setSelectedAccount(account);
					proxyContext.refreshProxies(account);
					if (withBalance) {
						System.out.println("JournalActions.refresh");
						RowActions.getInstance().refreshBalance();
					} else {
						System.out.println("JournalActions.refreshWithoutBalanceAndSetRow");
					}
				}
			}

			forceBalance = false;
		}
	}

	public void refreshCategoriesComboBox() {

		EntityManager em = EmfContext.create();
		int index = journalModule.getAccountsTable().getSelectedRow();
		if (index != -1) {
			CategoriesModel model = (CategoriesModel) categoriesComboBox.getTable().getModel();
			AccountsModel accountsModel = (AccountsModel) journalModule.getAccountsTable().getModel();
			if (accountsModel.getRowCount() > 0) {
				Account selectedAccount = accountsModel.getAccounts().get(index);
				Query q = em.createNamedQuery("Account.findAllButRootAndAccount").setParameter("account", selectedAccount);
				List<Account> accounts = q.getResultList();
				Collections.sort(accounts, new AccountComparator());
				model.setCategories(accounts);
			}
		}
	}

	public void showCategoriesComboBox() {
		refreshCategoriesComboBox();

		JPopupMenu menu = journalModule.getCategoriesMenu();
		CategoriesComboBox comboBox = CategoriesComboBox.getInstance();
		menu.removeAll();
		comboBox.setVisible(true);
		menu.add(comboBox);
		JournalCell cell = journalCellEditor.getCell();
		int width = cell.getCategoryField().getX();
		width = cell.getPaymentField().getX() - width;
		width = width - 3;
		comboBox.setPreferredSize(new Dimension(width, 100));

		int x = cell.getNumField().getX() + cell.getNumField().getWidth();
		int y = cell.getLocation().y + cell.getHeight() - 1;

		updateCategorySelection();
		menu.show(journalModule.getEntriesTable(), x, y);

		journalCellEditor.getCell().getCategoryField().requestFocusInWindow();
	}

	public void showPayeesComboBox() {
		refreshPayeeComboBox();

		JPopupMenu menu = journalModule.getPayeesMenu();
		PayeesComboBox comboBox = PayeesComboBox.getInstance();
		menu.removeAll();
		comboBox.setVisible(true);
		menu.add(comboBox);
		JournalCell cell = journalCellEditor.getCell();
		int width = cell.getPayeeField().getX();
		width = cell.getPaymentField().getX() - width;
		width = width - 3;
		comboBox.setPreferredSize(new Dimension(width, 100));

		updatePayeeSelection();
		menu.show(journalModule.getEntriesTable(), cell.getNumField().getX() + cell.getNumField().getWidth(), cell.getY() + (cell.getHeight() / 2));
		journalCellEditor.getCell().getPayeeField().requestFocusInWindow();

	}

	public void refreshPayeeComboBox() {
		PayeesModel payeesModel = (PayeesModel) payeesComboBox.getTable().getModel();
		EntityManager em = EmfContext.get().createEntityManager();
		List<Payee> payeeList = em.createNamedQuery("Payee.findAll").getResultList();
		payeesModel.setPayees(payeeList);
	}

	public void adjustForLostFocus() {
		Map params = (Map) MapContext.get(JournalActions.class.getName() + ".adjustForLostFocus");
		FocusEvent e = (FocusEvent) params.get(FocusEvent.class);
		if (e.getSource() instanceof JTextField) {
			JTextField textField = (JTextField) e.getSource();

			JournalCell cell = journalCellEditor.getCell();

			if (textField.getName().equals("dateField")) {
				String text = textField.getText();
				boolean showMessage = verifyDateFormat(text);
				if (showMessage) {
					GeneralUtil.showWarningAlert2(WorkbooksApp.getWarning("JournalActions.badDateFormat"));
					String sDate = GeneralUtil.dateToString(journalCellEditor.getEntryNode().getEntry().getDate());
					textField.setText(sDate);
				}
			}

			if (textField.getName().equals("categoryField")) {
				String text = textField.getText();
				if (!text.equals("-- split --")) {
					AccountDao dao = Service.load(AccountDao.class);
					Account account = dao.breakName(text);
					if (account == null) {
						GeneralUtil.showWarningAlert2(WorkbooksApp.getWarning("JournalActions.categoryNotExist"));
						EntityManager em = EmfContext.create();
						Account defaultAccount = (Account) em.createNamedQuery("Account.findDefault").getSingleResult();
						textField.setText(defaultAccount.getName());
					}
				}
			}
			
			if(textField.getName().equals("numField")) {
				String text = textField.getText();
				if(!isNumber(text)) {
					String[] types = GeneralUtil.getEntryTypes();
					boolean exists = false;
					for(String type : types) {
						if(text.equals(type)) {
							exists = true;
						}
					}
					if(!exists) {
						GeneralUtil.showWarningAlert2(WorkbooksApp.getWarning("JournalActions.typeNotExist"));
						int type = journalCellEditor.getEntryNode().getEntry().getType();
						String sType = GeneralUtil.getType(type);
						textField.setText(sType);
					}
				}
			}

			cell.setFocusedTextField(null);
			textField.repaint();
		}
	}

	public void autoSelectCategory(String text) {
		JournalCell cell = journalCellEditor.getCell();
		String cat = cell.getCategoryField().getText();

		if (!text.equals("No Payee")) {

			EntityManager em = EmfContext.create();
			Account defaultAccount = (Account) em.createNamedQuery("Account.findDefault").getSingleResult();

			Query q = em.createQuery("select row from JournalEntry entry, JournalEntryRow row where row.journalEntry = entry and row.account != :account and entry.payee.name = :name order by entry.date, entry.id");
			q.setParameter("name", text);
			q.setParameter("account", JournalTableModel.getSelectedAccount());
			//q.setParameter("account2", defaultAccount);

			List<JournalEntryRow> rows = q.getResultList();

			String payment = cell.getPaymentField().getText();
			String deposit = cell.getDepositField().getText();

			AccountDao dao = Service.load(AccountDao.class);

			boolean setDefault = false;
			for (JournalEntryRow row : rows) {
				if (row.getJournalEntry().getRows().size() > 2) {
					setDefault = false;
				} else {
					setDefault = true;
				}

				if (setDefault) {
					cat = dao.concatName(row.getAccount());
					if (row.isDebitEntry() == 0) {
						deposit = GeneralUtil.formatStringNumber(row.getAmount().toString());
						payment = "";
					} else {
						payment = GeneralUtil.formatStringNumber(row.getAmount().toString());
						deposit = "";
					}
				}
			}

			setDefault = true;

			JournalEntry entry = JournalTableModel.getSelectedEntry(em);
			if (entry.getRows().size() > 2) {
				setDefault = false;
			}

			if (setDefault) {
				cell.getCategoryField().setText(cat);
				cell.getPaymentField().setText(payment);
				cell.getDepositField().setText(deposit);
			}
		}
	}

	private boolean verifyDateFormat(String text) {
		boolean showMessage = false;

		String[] arr = text.split("-");

		if (arr.length != 3) {
			showMessage = true;
		} else {
			if (arr[0].length() != 4) {
				showMessage = true;
			}

			if (arr[1].length() != 2 && arr[1].length() != 1) {
				showMessage = true;
			}

			if (arr[2].length() != 2 && arr[2].length() != 1) {
				showMessage = true;
			}

			Pattern p = Pattern.compile("\\d+");
			Matcher m = p.matcher(arr[1]);
			if (!m.matches()) {
				System.out.println("one");
				showMessage =
						true;
			}

			m = p.matcher(arr[2]);
			if (!m.matches()) {
				System.out.println("two");
				showMessage =
						true;
			}

			if (!showMessage) {
				Integer month = new Integer(arr[1]);
				Integer day = new Integer(arr[2]);

				if (month > 12 || month == 0) {
					showMessage = true;
				}

				if (day > 31 || day == 0) {
					showMessage = true;
				}

			}
		}

		return showMessage;
	}

	public void adjustForGainedFocus() {
		Map params = (Map) MapContext.get(JournalActions.class.getName() + ".adjustForGainedFocus");
		FocusEvent e = (FocusEvent) params.get(FocusEvent.class);

		if (e.getSource() instanceof JTextField) {
			JTextField textField = (JTextField) e.getSource();
			JournalCell cell = journalCellEditor.getCell();

			if (textField.getName().equals("payeeField")) {
				cell.getShowPayeesButton().setVisible(true);

			} else {
				cell.getShowPayeesButton().setVisible(false);
			}

			if (textField.getName().equals("categoryField") || cell.getCategoryField().getText().equals("-- split --")) {
				cell.getShowCategoriesButton().setVisible(true);
				cell.getClearSplitButton().setVisible(true);
			} else {
				cell.getShowCategoriesButton().setVisible(false);
				cell.getClearSplitButton().setVisible(false);
			}

			if (textField.getName().equals("paymentField")) {
				journalModule.getPayeesMenu().setVisible(false);
			}

			if (textField.getName().equals("memoField")) {
				journalModule.getCategoriesMenu().setVisible(false);
			}


			if (textField.getName().equals("payeeField")) {
				if (journalModule.getEntriesTable().getSelectedRow() == journalModule.getEntriesTable().getRowCount() - 1 && triggerPopup) {
					showPayeesComboBox();
				}
			}

			if (textField.getName().equals("categoryField")) {
				if (journalModule.getEntriesTable().getSelectedRow() == journalModule.getEntriesTable().getRowCount() - 1 && triggerPopup) {
					showCategoriesComboBox();
				}
			}

			triggerPopup = true;

			textField.selectAll();

			cell.setFocusedTextField(textField);
			cell.setSelected(true);
			cell.repaint();

		}
	}

	public void updatePayeeStringList() {
		LookAheadTextField textField = (LookAheadTextField) journalCellEditor.getCell().getCategoryField();
		StringListLookAhead lookAhead = (StringListLookAhead) textField.getLookAhead();
		EntityManagerFactory emf = EmfContext.get();
		EntityManager em = emf.createEntityManager();
		List<Payee> payees = em.createQuery("select a from Payee a").getResultList();
		List<String> names = new ArrayList<String>();
		for (Payee payee : payees) {
			names.add(payee.getName());
		}

		lookAhead.setValues(names);
	}

	public void updateCategoryStringList() {
		LookAheadTextField textField = (LookAheadTextField) journalCellEditor.getCell().getCategoryField();
		StringListLookAhead lookAhead = (StringListLookAhead) textField.getLookAhead();
		EntityManagerFactory emf = EmfContext.get();
		EntityManager em = emf.createEntityManager();
		Query q = em.createQuery("select a from Account a where a != :account");
		q.setParameter("account", JournalTableModel.getSelectedAccount());
		List<Account> accounts = q.getResultList();
		List<String> names = new ArrayList<String>();
		for (Account account : accounts) {
			AccountDao dao = Service.load(AccountDao.class);
			String name = dao.concatName(account);
			names.add(name);
		}

		lookAhead.setValues(names);
	}

	public void arrowKeyAction(JTable table, int direction) {
		//SplitActions.getInstance().close();

		int row = table.getSelectedRow();
		JournalModule module = Service.load(ModuleLoader.class).get("Journal.module");
		JournalCellEditor cellEditor = (JournalCellEditor) module.getCellEditor();
		boolean move = true;
		row = row + direction;

		if (direction == 1 && row == table.getRowCount()) {
			move = false;
		} else if (direction == -1 && row == -1) {
			move = false;
		}

		if (journalModule.getCategoriesMenu().isVisible()) {
			move = false;
			table = categoriesComboBox.getTable();
			table.requestFocusInWindow();
			int row2 = table.getSelectedRow();
			if (row2 == table.getRowCount() - 1 && direction == 1) {
				row2 = -1;
			}
			GeneralUtil.moveRow(table, row2 + direction);
			updateCategoryTextField();
			journalCellEditor.getCell().getCategoryField().requestFocusInWindow();
		}

		if (journalModule.getPayeesMenu().isVisible()) {
			move = false;
			table = payeesComboBox.getTable();
			table.requestFocusInWindow();
			int row2 = table.getSelectedRow();
			if (row2 == table.getRowCount() - 1 && direction == 1) {
				row2 = -1;
			}
			GeneralUtil.moveRow(table, row2 + direction);
			updatePayeeTextField();
			journalCellEditor.getCell().getPayeeField().requestFocusInWindow();
		}

		if (move) {
			cellEditor.fireEditingStopped();
			GeneralUtil.moveRow(table, row);
			journalCellEditor.getCell().getDateField().requestFocusInWindow();
		}

	}

	/**
	 * called by JournalTableModel.setValueAt
	 * 
	 * @param rowProxy
	 * @param rowIndex
	 * @return
	 */
	public boolean saveTableRow(EntryNode entryNode, int rowIndex) {
		boolean confirmWarning = false;
		JournalTableModel model = (JournalTableModel) journalModule.getEntriesTable().getModel();
		boolean updated = false;

		// shouldSave set in RowActions.remove()
		if (RowActions.getInstance().shouldSave()) {
			boolean changed = compareEntry();

			if (changed && !model.isConfirmed()) { // show warning alert
				confirmWarning = GeneralUtil.showWarningAlert(WorkbooksApp.getWarning("JournalActions.confirmChanges"));
			}

			// make sure entry is balanced.
			boolean offBalance = false;
			if (confirmWarning || model.isConfirmed()) {
				if (journalCellEditor.getCell().getCategoryField().getText().equals("-- split --")) {
					SplitActions.getInstance().setupSplitEntries();
					SplitActions.getInstance().updateSplitStats();

					if (!SplitPanel.getInstance().getRemainderLabel().getText().equals("0.00")) {
						GeneralUtil.showWarningAlert2(WorkbooksApp.getWarning("JournalActions.offBalance"));
						offBalance = true;
					}
				}
			}


			if ((confirmWarning || model.isConfirmed()) && !offBalance) { // update data.
				if (changed) {
					UndoActions.getInstance().addUndo(new UndoableJournalEntry(UndoableJournalEntry.EDIT, journalCellEditor.getEntryNode()));
				}

				model.setConfirmed(false);
				model.getRowData().set(rowIndex, entryNode);

				List<JournalEntryRow> oldRows = entryNode.getEntry().getRows();

				rowActions.save();

				// refresh just the involved categories.
				EntityManager em = EmfContext.create();
				JournalEntry newEntry = em.find(JournalEntry.class, entryNode.getEntry().getId());
				List<JournalEntryRow> newRows = newEntry.getRows();
				Map<Account, Boolean> tracker = new HashMap<Account, Boolean>();
				oldRows.addAll(newRows);
				for (JournalEntryRow row : oldRows) {
					if (tracker.get(row.getAccount()) == null) {
						CategoryActions.getInstance().refreshCategory(row.getAccount());
						tracker.put(row.getAccount(), true);
					}
				}


				updated = true; // so journaltablemodel knows to fire table data changed.

				Service.load(Refresh.class).refresh("JournalActions.saveTableRow");

				// if enter is not pressed, then refreshes here.
				// if enter is pressed, then in RowActions.enter(), it uses 
				// RowActions.refreshBalance(), which is a separate thread.
				if (confirmWarning) {
					RowActions.getInstance().refreshBalance(rowIndex);
				}

			} else {
				if (changed) { // reset row
					//SplitActions.setChanged(false);
					ProxyContext.getInstance().setSplitAltered(false);
					model.fireTableRowsUpdated(rowIndex, rowIndex);
				}
			}

			// keeps it from selecting after row change.
			journalCellEditor.getCell().getMemoField().setEnabled(false);
			journalCellEditor.getCell().getCategoryField().setEnabled(false);
		}

		// only false when removing a row
		RowActions.getInstance().setShouldSave(true);

		return updated;
	}

	private boolean isNumber(String sNum) {
		Pattern p = Pattern.compile("\\d+");
		Matcher m = p.matcher(sNum);
		return m.matches();
	}

	public boolean compareEntry() {
		EntryNode rowProxy = journalCellEditor.getEntryNode();
		JournalCell cell = journalCellEditor.getCell();
		EntityManager em = EmfContext.create();
		JournalEntry entry = rowProxy.getEntry();
		entry = em.find(JournalEntry.class, entry.getId());

		JournalEntryRow entryRow = null;
		for (JournalEntryRow row : entry.getRows()) {
			if (row.getAccount().equals(JournalTableModel.getSelectedAccount())) {
				entryRow = row;
				break;
			}
		}
		entryRow = em.find(JournalEntryRow.class, entryRow.getId());

		String sDate = GeneralUtil.dateToString(entry.getDate());
		if (!sDate.equals(cell.getDateField().getText())) {
			System.out.println("1");
			return true;
		}

		String sType = GeneralUtil.getType(entry.getType());
		boolean isCheck = isNumber(cell.getNumField().getText());
		if (isCheck) {
			String sCheck = new Integer(entry.getCheckNo()).toString();
			if (!sCheck.equals(cell.getNumField().getText())) {
				System.out.println("1.5");
				return true;
			}
		} else {
			if (!sType.equals(cell.getNumField().getText())) {
				System.out.println("2");
				return true;
			}
		}

		String sPayee = entry.getPayee().getName();
		if (!sPayee.equals(cell.getPayeeField().getText())) {
			System.out.println("3");
			return true;
		}

		String sCategory = "";
		if (entry.getRows().size() > 2) {
			sCategory = "-- split --";
		} else {
			JournalEntryRow row2 = null;
			for (JournalEntryRow row : entry.getRows()) {
				if (!row.equals(entryRow)) {
					row2 = row;
					break;
				}
			}
			AccountDao dao = Service.load(AccountDao.class);
			sCategory = dao.concatName(row2.getAccount());
		}

		if (!sCategory.equals(cell.getCategoryField().getText())) {
			System.out.println("4");
			return true;
		}

		String memo = entry.getMemo();
		if (memo == null) {
			memo = "";
		}
		if (!memo.equals(cell.getMemoField().getText())) {
			System.out.println("5");
			return true;
		}


		String sPayment = "";
		String sDeposit = "";
		if (entryRow.isDebitEntry() == 0) {
			sPayment = GeneralUtil.formatStringNumber(entryRow.getAmount().toString());
			sDeposit = "";
		} else {
			sDeposit = GeneralUtil.formatStringNumber(entryRow.getAmount().toString());
			sPayment = "";
		}

		if (!sPayment.equals(cell.getPaymentField().getText())) {
			System.out.println("6");
			return true;
		}

		if (!sDeposit.equals(cell.getDepositField().getText())) {
			System.out.println("7");
			return true;
		}

		if (ProxyContext.getInstance().isSplitAltered()) {
			//SplitActions.setChanged(false);
			return true;
		}

		return false;
	}

	public JournalCellEditor getJournalCellEditor() {
		return journalCellEditor;
	}

	public void setJournalCellEditor(JournalCellEditor journalCellEditor) {
		this.journalCellEditor = journalCellEditor;
	}

	public JTable getSplitEntryTable() {
		return splitEntryTable;
	}

	public void setSplitEntryTable(JTable splitEntryTable) {
		this.splitEntryTable = splitEntryTable;
	}
}

