/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.journal.actions;

import com.xbriage.modules.journal.*;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

/**
 *
 * @author shughes
 */
public class PressEnterAction extends AbstractAction {

	public void actionPerformed(ActionEvent e) {
		RowActions.getInstance().enter();
	}

}
