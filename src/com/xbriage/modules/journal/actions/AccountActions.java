/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.journal.actions;

import com.xbriage.app.WorkbooksApp;
import com.xbriage.modules.journal.*;
import com.xbriage.domain.Account;
import com.xbriage.domain.JournalEntryRow;
import com.xbriage.context.EmfContext;
import com.xbriage.context.MapContext;
import com.xbriage.services.Service;
import com.xbriage.app.WorkbooksView;
import com.xbriage.modules.categories.CategoryActions;
import com.xbriage.modules.journal.dialogs.RemoveAccountDialog;
import com.xbriage.modules.journal.models.AccountsModel;
import com.xbriage.modules.journal.proxy.EntryNode;
import com.xbriage.modules.journal.proxy.ProxyContext;
import com.xbriage.services.ModuleLoader;
import com.xbriage.services.Refresh;
import com.xbriage.util.GeneralUtil;
import com.xbriage.services.Service;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.swing.JTable;

/**
 *
 * @author shughes
 */
public class AccountActions {

	private static AccountActions instance = new AccountActions();
	private final JournalModule module = Service.load(ModuleLoader.class).get("Journal.module");
	private final JTable table = module.getAccountsTable();
	private boolean editCanceled = false;

	public static AccountActions getInstance() {
		return instance;
	}

	private AccountActions() {

	}

	public void edit() {
		Map params = (Map) MapContext.get(AccountActions.class.getName() + ".edit");
		MouseEvent evt = (MouseEvent) params.get(MouseEvent.class);
		int row = -1;
		int count = 0;

		if (evt != null) {
			count = evt.getClickCount();
		}

		row = table.getSelectedRow();

		if (params.get(ActionEvent.class) != null) {
			count = 2;
		}

		if (row != -1) {
			if (count == 2) {
				AccountsModel model = (AccountsModel) table.getModel();
				CategoryActions.getInstance().edit(model.getAccounts().get(row));
				if (!editCanceled) {
					JournalActions.getInstance().clearAccountSelection();
				}
				editCanceled = false;
			}
		}

	}

	public void add() {
		EntityManager em = EmfContext.create();
		Account parent = (Account) em.createNamedQuery("Account.findRoot").getSingleResult();
		Account account = new Account("New Account", "Asset", 1, 0, parent);
		em.getTransaction().begin();
		em.persist(account);
		em.getTransaction().commit();
		AccountsModel model = (AccountsModel) table.getModel();
		model.getAccounts().add(account);
		em.close();
		Service.load(Refresh.class).refresh("AccountActions.add");
	}

	public void refresh() {
		AccountsModel model = (AccountsModel) table.getModel();
		int index = table.getSelectedRow();
		EntityManager em = EmfContext.create();
		List<Account> accounts = em.createNamedQuery("Account.findUserAccounts").getResultList();
		model.setAccounts(accounts);
		table.getSelectionModel().setSelectionInterval(index, index);
	}

	public void remove() {
		RemoveAccountDialog d = new RemoveAccountDialog(WorkbooksView.getInstance().getFrame(), true);
		d.setVisible(true);
		int status = d.getReturnStatus();
		if (status == RemoveAccountDialog.RET_OK) {
			int index = table.getSelectedRow();
			AccountsModel model = (AccountsModel) table.getModel();
			Account account = model.getAccounts().get(index);

			List<Account> accounts = model.getAccounts();
			accounts.remove(account);
			EntityManager em = EmfContext.create();
			account = em.find(Account.class, account.getId());

			List<EntryNode> nodes = ProxyContext.getInstance().getProxies(account);
			List<EntryNode> entryNodes = ProxyContext.getInstance().getEntryNodes();
			for (EntryNode node : nodes) {
				entryNodes.remove(node);
			}

			List<JournalEntryRow> rows = account.getJournalEntryRows();
			for (JournalEntryRow row : rows) {
				em.remove(row.getJournalEntry());
			}

			em.getTransaction().begin();
			em.remove(account);
			em.getTransaction().commit();
			em.close();

			model.setAccounts(accounts);

			Service.load(Refresh.class).refresh("AccountActions.remove");

			GeneralUtil.showWarningAlert2(WorkbooksApp.getWarning("AccountActions.remindArchive"));
		}

	}

	public boolean isEditCanceled() {
		return editCanceled;
	}

	public void setEditCanceled(boolean editCanceled) {
		this.editCanceled = editCanceled;
	}
}
