/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.journal.actions;

import com.xbriage.modules.journal.JournalCell;
import com.xbriage.modules.journal.JournalCellEditor;
import com.xbriage.services.Service;
import com.xbriage.modules.journal.JournalModule;
import com.xbriage.services.ModuleLoader;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

/**
 *
 * @author shughes
 */
public class PressTabAction extends AbstractAction {

	public void actionPerformed(ActionEvent e) {
		JournalModule module = Service.load(ModuleLoader.class).get("Journal.module");
		JournalCellEditor cellEditor = (JournalCellEditor) module.getCellEditor();
		JournalCell cell = cellEditor.getCell();

		boolean doAuto = false;
		if (cell.getFocusedTextField() != null) {
			if (cell.getFocusedTextField().getName().equals("payeeField")) {
				int row = module.getEntriesTable().getSelectedRow();
				if (row == module.getEntriesTable().getRowCount() - 1) {
					doAuto = true;
				}
			}
		}

		if (module.getCategoriesMenu().isVisible()) {
			JournalActions.getInstance().hideCategories();
			cell.getMemoField().requestFocusInWindow();

		} else if (module.getPayeesMenu().isVisible()) {
			JournalActions.getInstance().hidePayees();
			cell.getPaymentField().requestFocusInWindow();

		} else {
			KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
			manager.focusNextComponent();
		}

		if (doAuto) {
			JournalActions.getInstance().autoSelectCategory(cell.getPayeeField().getText());
		}
	}
}
