/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.journal.actions;

import com.xbriage.modules.journal.dialogs.SplitDialog;
import com.xbriage.app.WorkbooksApp;
import com.xbriage.modules.journal.*;
import com.xbriage.services.AccountDao;
import com.xbriage.domain.Account;
import com.xbriage.domain.JournalEntry;
import com.xbriage.domain.JournalEntryRow;
import com.xbriage.services.Service;
import com.xbriage.context.EmfContext;
import com.xbriage.services.Service;
import com.xbriage.app.WorkbooksView;
import com.xbriage.modules.journal.models.JournalTableModel;
import com.xbriage.modules.journal.models.SplitModel;
import com.xbriage.modules.journal.proxy.EntryNode;
import com.xbriage.modules.journal.proxy.ProxyContext;
import com.xbriage.modules.journal.proxy.RowProxy;
import com.xbriage.services.ModuleLoader;
import com.xbriage.util.GeneralUtil;
import java.awt.Rectangle;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.List;
import javax.persistence.EntityManager;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;

/**
 *
 * @author shughes
 */
public class SplitActions {

	private final static SplitActions instance = new SplitActions();
	private JournalModule journalModule = null;
	private JournalCellEditor journalCellEditor = null;
	private JournalTableModel journalTableModel = null;
	private SplitModel splitEntryModel;
	private boolean balanced = false;

	private SplitActions() {
		journalModule = Service.load(ModuleLoader.class).get("Journal.module");
		journalCellEditor = (JournalCellEditor) journalModule.getCellEditor();
		journalTableModel = (JournalTableModel) journalModule.getEntriesTable().getModel();
		splitEntryModel = (SplitModel) SplitPanel.getInstance().getTable().getModel();
	}

	public static SplitActions getInstance() {
		return instance;
	}

	public void setAccountComboBox() {
		AccountDao dao = Service.load(AccountDao.class);
		EntityManager em = EmfContext.create();
		List<Account> accounts = em.createNamedQuery("Account.findAllButRoot").getResultList();
		List<String> names = new ArrayList<String>();
		for (Account account : accounts) {
			if (!account.equals(JournalTableModel.getSelectedAccount())) {
				String name = dao.concatName(account);
				names.add(name);
			}
		}
		Collections.sort(names);
		DefaultComboBoxModel model = new DefaultComboBoxModel(names.toArray());
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(model);
		SplitPanel splitPanel = SplitPanel.getInstance();
		splitPanel.getTable().getColumnModel().getColumn(0).setCellEditor(new DefaultCellEditor(comboBox));
	}

	public JournalEntryRow getSelectedAccountEntry(JournalEntry entry) {
		JournalEntryRow theRow = null;
		for (JournalEntryRow row : entry.getRows()) {
			if (row.getAccount().equals(JournalTableModel.getSelectedAccount())) {
				theRow = row;
				break;
			}
		}
		return theRow;
	}

	/**
	 * shows stats related to split entries in the splitpanel
	 * 
	 */
	public void updateSplitStats() {
		SplitPanel panel = SplitPanel.getInstance();
		JLabel splitTotalLabel = panel.getSplitTotalLabel();
		JLabel remainderLabel = panel.getRemainderLabel();
		JLabel totalLabel = panel.getTotalLabel();
		String total = "";

		if (!journalCellEditor.getCell().getPaymentField().getText().equals("")) {
			total = journalCellEditor.getCell().getPaymentField().getText();
		} else {
			total = journalCellEditor.getCell().getDepositField().getText();
		}

		BigDecimal splitTotal = new BigDecimal(0);
		for (int i = 0; i < splitEntryModel.getRowCount(); i++) {
			RowProxy rowProxy = splitEntryModel.getSplitRows().get(i);
			if (rowProxy.getAmount() != null && !rowProxy.getAmount().trim().equals("")) {
				splitTotal = splitTotal.add(new BigDecimal(rowProxy.getAmount()));
			}
		}

		BigDecimal tmpTotal = new BigDecimal(total);
		BigDecimal remainder = tmpTotal.subtract(splitTotal);

		remainderLabel.setText(GeneralUtil.formatStringNumber(remainder.toString()));

		if (remainder.compareTo(new BigDecimal(0)) != 0) {
			balanced = false;
		} else {
			balanced = true;
		}

		splitTotalLabel.setText(GeneralUtil.formatStringNumber(splitTotal.toString()));
		totalLabel.setText(GeneralUtil.formatStringNumber(total));
	}

	public void setupSplitEntries() {
		SplitPanel panel = SplitPanel.getInstance();
		EntryNode node = journalCellEditor.getEntryNode();

		List<RowProxy> splitRows = node.getSplitRows();
		List<RowProxy> newRows = new ArrayList<RowProxy>();
		for (RowProxy proxy : splitRows) {
			RowProxy newProxy = new RowProxy(proxy);
			newRows.add(newProxy);
		}

		for (int i = 0; i < 20; i++) {
			RowProxy tmp = new RowProxy();
			tmp.setMemo(null);
			tmp.setAmount(null);
			tmp.setAccount(null);
			newRows.add(tmp);
		}

		SplitModel model = (SplitModel) panel.getTable().getModel();
		model.setSplitRows(newRows);
	}

	public void open() {
		SplitDialog dialog = new SplitDialog(WorkbooksView.getInstance().getFrame(), true);
		Rectangle rect = WorkbooksView.getInstance().getFrame().getBounds();
		dialog.setBounds(rect.x + 100, rect.y + 100, dialog.getWidth(), dialog.getHeight());
		SplitPanel panel = SplitPanel.getInstance();

		EntryNode node = journalCellEditor.getEntryNode();

		setupSplitEntries();

		SplitModel model = (SplitModel) panel.getTable().getModel();
		//model.setSplitRows(newRows);

		setAccountComboBox();

		panel.setVisible(true);
		updateSplitStats();

		dialog.getPanel().add(panel, 0);
		dialog.setVisible(true);


		int status = dialog.getReturnStatus();

		// after ok/cancel

		panel.getTable().getColumnModel().getColumn(0).getCellEditor().stopCellEditing();
		panel.getTable().getColumnModel().getColumn(1).getCellEditor().stopCellEditing();
		panel.getTable().getColumnModel().getColumn(2).getCellEditor().stopCellEditing();

		if (status == SplitDialog.RET_OK) {

			// changed here, and also JournalActions.saveTableRow, and ProxyContext.saveEntry
			ProxyContext.getInstance().setSplitAltered(true);

			List<RowProxy> rows = model.getSplitRows();
			List<RowProxy> newRows2 = new ArrayList<RowProxy>();
			for (RowProxy proxy : rows) {
				if (proxy.getAmount() != null && !proxy.getAmount().equals("")) {

					// if no account selected, defaults to 'No Category'
					if (proxy.getAccount() == null) {
						EntityManager em = EmfContext.create();
						Account account = (Account) em.createNamedQuery("Account.findDefault").getSingleResult();
						AccountDao dao = Service.load(AccountDao.class);
						String sAccount = dao.concatName(account);
						proxy.setAccount(sAccount);
					}

					newRows2.add(proxy);
				}
			}

			node.setSplitRows(newRows2);

			//List<RowProxy> rows = node.getSplitRows();
			if (newRows2.size() > 1) {
				journalCellEditor.getCell().getCategoryField().setText("-- split --");
				journalCellEditor.getCell().getCategoryField().setEnabled(false);
				journalCellEditor.getCell().getShowCategoriesButton().setEnabled(false);
				journalCellEditor.getCell().getClearSplitButton().setEnabled(true);
			} else {
				for (RowProxy row : newRows2) {
					journalCellEditor.getCell().getCategoryField().setText(row.getAccount());
					journalCellEditor.getCell().getCategoryField().setEnabled(true);
					journalCellEditor.getCell().getShowCategoriesButton().setEnabled(true);
					journalCellEditor.getCell().getClearSplitButton().setEnabled(false);
				}
			}
		}

		if (!panel.getRemainderLabel().getText().equals("0.00") && status != SplitDialog.RET_CANCEL) {
			GeneralUtil.showWarningAlert2(WorkbooksApp.getWarning("SplitActions.adjustSplitEntry"));
			open();
		}
		
		journalCellEditor.getCell().getDateField().requestFocusInWindow();
	}

	public void clear() {
		boolean yes = GeneralUtil.showWarningAlert(WorkbooksApp.getWarning("SplitActions.clearSplit"));

		if (yes) {
			EntryNode node = journalCellEditor.getEntryNode();
			EntityManager em = EmfContext.create();
			Account defaultAccount = (Account) em.createNamedQuery("Account.findDefault").getSingleResult();
			String sDep = journalCellEditor.getCell().getDepositField().getText();
			String sPay = journalCellEditor.getCell().getPaymentField().getText();

			RowProxy row = new RowProxy();
			row.setAccount(defaultAccount.getName());
			if (sDep.trim().equals("")) {
				row.setAmount(sPay.trim());
			} else {
				row.setAmount(sDep.trim());
			}

			List<RowProxy> rows = new ArrayList<RowProxy>();
			rows.add(row);
			node.setSplitRows(rows);

			journalCellEditor.getCell().getCategoryField().setText(defaultAccount.getName());
			journalCellEditor.getCell().getCategoryField().setEnabled(true);

			ProxyContext.getInstance().setSplitAltered(true);
		}
	}
}
