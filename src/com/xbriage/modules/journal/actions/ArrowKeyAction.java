/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.journal.actions;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JTable;

/**
 *
 * @author shughes
 */
public class ArrowKeyAction extends AbstractAction {

	public static int UP = -1;
	public static int DOWN = 1;
	private JTable table;
	private int direction = UP;

	public ArrowKeyAction(int direction, JTable table) {
		this.direction = direction;
		this.table = table;
	}

	public void actionPerformed(ActionEvent e) {
		JournalActions.getInstance().arrowKeyAction(table, direction);
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}
}
