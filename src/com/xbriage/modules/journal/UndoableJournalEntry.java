/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.journal;

import com.xbriage.context.EmfContext;
import com.xbriage.services.Service;
import com.xbriage.domain.JournalEntry;
import com.xbriage.modules.journal.proxy.EntryNode;
import com.xbriage.modules.journal.proxy.ProxyContext;
import com.xbriage.services.ModuleLoader;
import com.xbriage.services.Refresh;
import com.xbriage.util.GeneralUtil;
import com.xbriage.services.Service;
import javax.persistence.EntityManager;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;

/**
 *
 * @author shughes
 */
public class UndoableJournalEntry extends AbstractUndoableEdit {

	public static final int EDIT = 0;
	public static final int REMOVE = 1;
	private JournalEntry oldEntry;
	private JournalEntry redoEntry;
	private EntryNode node;
	private int type;

	public UndoableJournalEntry(int type, EntryNode node) {
		this.oldEntry = new JournalEntry(node.getEntry());
		this.node = node;
		this.type = type;
	}

	@Override
	public String getUndoPresentationName() {
		if (type == REMOVE) {
			return "Undo removed entry";
		} else {
			return "Undo entry edit";
		}
	}

	@Override
	public String getRedoPresentationName() {
		switch (type) {
			case REMOVE:
				return "Redo removed entry";

			case EDIT:
				return "Redo entry edit";
		}
		return null;
	}

	@Override
	public void undo() throws CannotUndoException {
		super.undo();
		switch (type) {
			case EDIT:
				undoEdit();
				break;
			case REMOVE:
				undoRemove();
				break;
		}
		refresh();
	}

	private void refresh() {
		Service.load(Refresh.class).refresh("UndoableJournalEntry.refresh");
	}

	private void undoRemove() {
		EntityManager em = EmfContext.create();
		oldEntry = em.merge(oldEntry);
		em.getTransaction().begin();
		em.getTransaction().commit();
		node = ProxyContext.getInstance().addEntry(oldEntry);
	}

	private void undoEdit() {
		EntityManager em = EmfContext.create();
		JournalEntry entry = em.find(JournalEntry.class, node.getEntry().getId());
		redoEntry = new JournalEntry(entry);
		em.remove(entry);
		oldEntry = em.merge(oldEntry);
		em.getTransaction().begin();
		em.getTransaction().commit();
		node.setEntry(oldEntry);
		ProxyContext.getInstance().updateIndex(node);
	}

	@Override
	public void redo() throws CannotRedoException {
		super.redo();
		switch (type) {
			case EDIT:
				EntityManager em = EmfContext.create();
				oldEntry = em.find(JournalEntry.class, oldEntry.getId());
				em.remove(oldEntry);
				redoEntry = em.merge(redoEntry);
				em.getTransaction().begin();
				em.getTransaction().commit();
				node.setEntry(redoEntry);
				ProxyContext.getInstance().updateIndex(node);
				break;

			case REMOVE:
				JournalModule mod = Service.load(ModuleLoader.class).get("Journal.module");
				JournalCellEditor cellEditor = (JournalCellEditor) mod.getCellEditor();
				if (cellEditor.getEntryNode().getEntry().equals(node.getEntry())) {
					int row = mod.getEntriesTable().getSelectedRow();
					GeneralUtil.moveRow(mod.getEntriesTable(), --row);
				}
				ProxyContext.getInstance().removeEntry(node);
				break;
		}

		refresh();
	}
}
