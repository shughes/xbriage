/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.journal;

import com.xbriage.modules.journal.actions.RowActions;
import com.xbriage.modules.journal.actions.SplitActions;
import com.xbriage.modules.journal.actions.JournalActions;
import com.xbriage.modules.journal.actions.AccountActions;
import com.xbriage.modules.journal.actions.PressEnterAction;
import com.xbriage.modules.journal.actions.ArrowKeyAction;
import com.xbriage.modules.journal.actions.PressTabAction;
import com.xbriage.util.BindUtil;
import com.xbriage.services.Service;
import com.xbriage.modules.journal.models.SplitModel;
import com.xbriage.services.ModuleLoader;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import javax.swing.InputMap;
import javax.swing.JTable;
import javax.swing.KeyStroke;

/**
 *
 * @author shughes
 */
public class JournalController {

	public final static int ROW_HEIGHT = 20;
	public final static int ROW_WIDTH = 200;
	public final static int SPLIT_ENTRY_HEIGHT = 200;
	public final static int COMBOBOX_HEIGHT = 100;
	private JournalModule module = null;
	private final CategoriesComboBox catPane = CategoriesComboBox.getInstance();
	private JournalCellEditor cellEditor = null;
	private final PayeesComboBox payeesPane = PayeesComboBox.getInstance();
	private JTable entriesTable = null;
	private final PressEnterAction pressEnterAction = new PressEnterAction();
	private final PressTabAction pressTabAction = new PressTabAction();
	private final SplitModel splitEntryModel = (SplitModel) SplitPanel.getInstance().getTable().getModel();

	public void startup() {
		module = Service.load(ModuleLoader.class).get("Journal.module");
		cellEditor = (JournalCellEditor) module.getCellEditor();
		entriesTable = module.getEntriesTable();
		
		InputMap tableInputMap = entriesTable.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		tableInputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), "pressUp");
		tableInputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), "pressDown");
		tableInputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0), "pressTab");
		tableInputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "pressEnter");
		tableInputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_DOWN_MASK), "pressUp");
		tableInputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK), "pressDown");
		
		SplitActions.getInstance().setAccountComboBox();
		
		setupActions();
	}

	private void setupActions() {
		setupAccountActions();
		setupSplitActions();
		setupRowActions();
		setupCellFocusActions();
		setupJournalActions();
	}

	private void setupAccountActions() {
		AccountActions accountActions = AccountActions.getInstance();
		BindUtil.bind(accountActions, "add", module.getAddButton(), BindUtil.ACTION);
		BindUtil.bind(accountActions, "add", module.getAddMenuItem(), BindUtil.ACTION);
		BindUtil.bind(accountActions, "remove", module.getRemoveButton(), BindUtil.ACTION);
		BindUtil.bind(accountActions, "remove", module.getRemoveMenuItem(), BindUtil.ACTION);
		BindUtil.bind(accountActions, "edit", module.getAccountsTable(), BindUtil.MOUSE_PRESSED);
		BindUtil.bind(accountActions, "edit", module.getEditMenuItem(), BindUtil.ACTION);
		BindUtil.bind(accountActions, "edit", module.getEditButton(), BindUtil.ACTION);
	}
	

	private void setupJournalActions() {
		JournalActions journalActions = JournalActions.getInstance();
		BindUtil.bind(journalActions, "search", module.getSearchField(), BindUtil.ACTION);
		BindUtil.bind(journalActions, "search", module.getSearchButton(), BindUtil.ACTION);
		BindUtil.bind(journalActions, "clear", module.getClearButton(), BindUtil.ACTION);
		BindUtil.selectAllBind(module.getSearchField());
		
		BindUtil.bind(journalActions, "showPopup", module.getAccountsTable(), BindUtil.MOUSE_PRESSED);
		BindUtil.bind(journalActions, "showPopup", module.getAccountsTable(), BindUtil.MOUSE_RELEASED);
		
		BindUtil.bind(journalActions, "adjustForAccountSelectionChange", module.getAccountsTable(), BindUtil.ACTION);
				
		BindUtil.propertyBind(journalActions, "enableRemoveButton", module.getEntriesTable(), "tableCellEditor");
		
		// refer to InputMap in JournalController.init()
		ArrowKeyAction pressUpAction = new ArrowKeyAction(ArrowKeyAction.UP, entriesTable);
		entriesTable.getActionMap().put("pressUp", pressUpAction);
		ArrowKeyAction pressDownAction = new ArrowKeyAction(ArrowKeyAction.DOWN, entriesTable);
		entriesTable.getActionMap().put("pressDown", pressDownAction);
		entriesTable.getActionMap().put("pressTab", pressTabAction);
		entriesTable.getActionMap().put("pressEnter", pressEnterAction);

		// categories popup
		BindUtil.bind(journalActions, "showCategoriesComboBox", cellEditor.getCell().getShowCategoriesButton(), BindUtil.ACTION);
		BindUtil.bind(journalActions, new String[]{"updateCategoryTextField", "hideCategories"}, catPane.getTable(), BindUtil.MOUSE_RELEASED);
		BindUtil.bind(journalActions, "updateCategoryTextField", catPane.getTable(), BindUtil.MOUSE_PRESSED);
		BindUtil.bind(journalActions, "updateCategorySelection", cellEditor.getCell().getCategoryField(), BindUtil.KEY_RELEASE);
		BindUtil.bind(journalActions, "updateCategoryTextField", catPane.getTable(), BindUtil.KEY_RELEASE);
		BindUtil.bind(journalActions, "updateCategoryTextField", catPane.getTable(), BindUtil.KEY_PRESSED);
		
		// payees popup
		BindUtil.bind(journalActions, "showPayeesComboBox", cellEditor.getCell().getShowPayeesButton(), BindUtil.ACTION);
		BindUtil.bind(journalActions, "updatePayeeSelection", cellEditor.getCell().getPayeeField(), BindUtil.KEY_RELEASE);
		BindUtil.bind(journalActions, new String[]{"updatePayeeTextField", "hidePayees"}, payeesPane.getTable(), BindUtil.MOUSE_RELEASED);
		BindUtil.bind(journalActions, "updatePayeeTextField", payeesPane.getTable(), BindUtil.MOUSE_PRESSED);
		BindUtil.bind(journalActions, "updatePayeeTextField", payeesPane.getTable(), BindUtil.KEY_RELEASE);
		BindUtil.bind(journalActions, "updatePayeeTextField", payeesPane.getTable(), BindUtil.KEY_PRESSED);
		
		BindUtil.bind(journalActions, "adjustRowButtons", entriesTable, BindUtil.COMPONENT_RESIZED);
		BindUtil.bind(journalActions, "adjustRowButtons", module.getJScrollPane3(), BindUtil.MOUSE_WHEEL);
		BindUtil.bind(journalActions, "adjustRowButtons", entriesTable, BindUtil.COMPONENT_MOVED);
		
	}

	private void setupRowActions() {	
		RowActions rowActions = RowActions.getInstance();
		BindUtil.bind(rowActions, "remove", RowButtons.getInstance().getRemoveButton(), BindUtil.ACTION);
		BindUtil.bind(rowActions, "enter", RowButtons.getInstance().getEnterButton(), BindUtil.ACTION);
	}

	private void setupSplitActions() {
		SplitActions splitActions = SplitActions.getInstance();
		BindUtil.bind(splitActions, "updateSplitStats", splitEntryModel, BindUtil.TABLE_MODEL_CHANGED);
		BindUtil.bind(splitActions, "clear", cellEditor.getCell().getClearSplitButton(), BindUtil.ACTION);		
		BindUtil.bind(splitActions, "open", RowButtons.getInstance().getSplitButton(), BindUtil.ACTION);
	}

	private void setupCellFocusActions() {
		JournalActions journalActions = JournalActions.getInstance();
		BindUtil.bind(journalActions, "adjustForGainedFocus",
				cellEditor.getCell().getDateField(), BindUtil.FOCUS_GAINED);
		
		BindUtil.bind(journalActions, "adjustForLostFocus",
				cellEditor.getCell().getDateField(), BindUtil.FOCUS_LOST);
		
		BindUtil.bind(journalActions, "adjustForGainedFocus",
				cellEditor.getCell().getNumField(), BindUtil.FOCUS_GAINED);
		
		BindUtil.bind(journalActions, "adjustForLostFocus",
				cellEditor.getCell().getNumField(), BindUtil.FOCUS_LOST);
		
		BindUtil.bind(journalActions, new String[]{"adjustForGainedFocus", "updateCategoryStringList"},
				cellEditor.getCell().getCategoryField(), BindUtil.FOCUS_GAINED);
		
		BindUtil.bind(journalActions, "adjustForLostFocus",
				cellEditor.getCell().getCategoryField(), BindUtil.FOCUS_LOST);
		
		BindUtil.bind(journalActions, "adjustForGainedFocus",
				cellEditor.getCell().getMemoField(), BindUtil.FOCUS_GAINED);
		
		BindUtil.bind(journalActions, "adjustForLostFocus",
				cellEditor.getCell().getMemoField(), BindUtil.FOCUS_LOST);
		
		BindUtil.bind(journalActions, new String[]{"adjustForGainedFocus", "updatePayeeStringList"},
				cellEditor.getCell().getPayeeField(), BindUtil.FOCUS_GAINED);
		
		BindUtil.bind(journalActions, "adjustForLostFocus",
				cellEditor.getCell().getPayeeField(), BindUtil.FOCUS_LOST);
		
		BindUtil.bind(journalActions, "adjustForGainedFocus",
				cellEditor.getCell().getPaymentField(), BindUtil.FOCUS_GAINED);
		
		BindUtil.bind(journalActions, "adjustForLostFocus",
				cellEditor.getCell().getPaymentField(), BindUtil.FOCUS_LOST);
		
		BindUtil.bind(journalActions, "adjustForGainedFocus",
				cellEditor.getCell().getDepositField(), BindUtil.FOCUS_GAINED);
		
		BindUtil.bind(journalActions, "adjustForLostFocus",
				cellEditor.getCell().getDepositField(), BindUtil.FOCUS_LOST);
	}

}
