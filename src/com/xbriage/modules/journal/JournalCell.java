/*
 * JournalCell.java
 *
 * Created on April 10, 2008, 5:00 PM
 */
package com.xbriage.modules.journal;

import com.xbriage.modules.journal.actions.JournalActions;
import com.xbriage.util.LookAheadTextField;
import com.xbriage.app.WorkbooksApp;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;
import javax.swing.JTextField;

/**
 *
 * @author  shughes
 */
public class JournalCell extends javax.swing.JPanel {

	public static final Color GRID_COLOR = WorkbooksApp.getInstance().getContext().getResourceMap().getColor("Application.gridColor");
	public static final Color SELECTED_COLOR = WorkbooksApp.getInstance().getContext().getResourceMap().getColor("Application.journalSelectionColor");
	public static final Color SELECTED_LINES_COLOR = new Color(255, 217, 89);
	public static final Color OTHER_COLOR = WorkbooksApp.getApplication().getContext().getResourceMap().getColor("Application.otherColor"); //new Color(237, 240, 252); //new Color(204, 204, 255, 255);
	public static final Color DARK_GRAY = new Color(204, 204, 204);
	public static final Color LIGHT_GRAY = new Color(225, 225, 225);
	public static final Color DARK_LINE = new Color(102, 102, 102);
	private boolean selected = false;
	private boolean header = false;
	private JTextField focusedTextField;
	private List<String> categoryList;
	private boolean expanded = false;
	private PropertyChangeSupport changes = new PropertyChangeSupport(this);

	@Override
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		changes.addPropertyChangeListener(listener);
	}

	/** Creates new form JournalCell */
	public JournalCell() {

		initComponents();
		setOpaque(false);
		showCategoriesButton.setVisible(true);
		clearSplitButton.setVisible(false);
		dateField.setOpaque(false);
		numField.setOpaque(false);
		payeeField.setOpaque(false);
		paymentField.setOpaque(false);
		depositField.setOpaque(false);
		categoryField.setOpaque(false);
		memoField.setOpaque(false);

		getPaymentField().addKeyListener(new KeyListener() {

			public void keyTyped(KeyEvent e) {
			}

			public void keyPressed(KeyEvent e) {
				String mod = KeyEvent.getKeyModifiersText(e.getModifiers());
				String keyText = KeyEvent.getKeyText(e.getKeyCode());
				if (!isSpecialKey(keyText, mod)) {
					getDepositField().setText("");
				}
			}

			public void keyReleased(KeyEvent e) {
			}
		});

		getDepositField().addKeyListener(new KeyListener() {

			public void keyTyped(KeyEvent e) {
			}

			public void keyPressed(KeyEvent e) {
				String mod = KeyEvent.getKeyModifiersText(e.getModifiers());
				String keyText = KeyEvent.getKeyText(e.getKeyCode());
				if (!isSpecialKey(keyText, mod)) {
					getPaymentField().setText("");
				}
			}

			public void keyReleased(KeyEvent e) {
			}
		});
	}

	private boolean isSpecialKey(String keyText, String mod) {
		boolean special = true;
		if (!mod.equals("Ctrl") && !keyText.equals("Shift") && !keyText.equals("Tab") && !keyText.equals("Enter") && !keyText.equals("Up") && !keyText.equals("Down") && !keyText.equals("Left") && !keyText.equals("Right")) {
			special = false;
		}
		return special;
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
	}

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		Dimension d = getSize();
		Color c;
		Paint p = g2d.getPaint();
		if (selected) {
			JournalActions.getInstance().adjustRowButtons();

			c = SELECTED_COLOR;
			g2d.setPaint(c);
			g2d.fillRect(0, 0, d.width, d.height);

			c = SELECTED_LINES_COLOR;
			g2d.setPaint(c);
			g2d.fillRect(0, d.height / 2, d.width, 2);
			g2d.fillRect(0, 0, d.width, 2);
			g2d.fillRect(0, d.height - 2, d.width, 2);
			g2d.fillRect(d.width - 2, 0, 2, d.height);
			g2d.fillRect(0, 0, 2, d.height);

			if (focusedTextField != null) {
				Point point = focusedTextField.getLocation();
				Dimension d2 = focusedTextField.getSize();
				g2d.setPaint(Color.WHITE);
				int y = point.y;
				y = (y == 0) ? y + 2 : y + 1;
				int h = d2.height;
				h = (y == 2) ? h : h - 2;
				g2d.fillRect(point.x - 4, y, d2.width + 4, h);
			}

			Point p2 = memoField.getLocation();
			g2d.setPaint(Color.LIGHT_GRAY);
			g2d.fillRect(p2.x - 4, d.height / 2 + 2, 1, d.height / 2 - 3);

		} else if (isHeader()) {
			g2d.setPaint(DARK_GRAY);
			g2d.fillRect(0, 0, d.width, d.height / 2);
			g2d.setPaint(LIGHT_GRAY);
			g2d.fillRect(0, d.height / 2, d.width, d.height / 2);
			g2d.setPaint(DARK_LINE);
			g2d.fillRect(0, d.height - 1, d.width, 1);
			g2d.fillRect(0, 0, d.width, 1);
			g2d.fillRect(d.width - 1, 0, 1, d.height);
			g2d.fillRect(0, d.height / 2, d.width, 1);
			g2d.setPaint(Color.LIGHT_GRAY);
			g2d.fillRect(memoField.getX() - 4, d.height / 2 + 2, 1, d.height / 2 - 3);
		} else {
			c = OTHER_COLOR;
			g2d.setPaint(c);
			g2d.fillRect(0, 0, d.width, d.height / 2);
			c = Color.WHITE;
			g2d.setPaint(c);
			g2d.fillRect(0, d.height / 2, d.width, d.height);
		}

		g2d.setPaint(Color.LIGHT_GRAY);
		g2d.fillRect(getDateField().getX() + getDateField().getWidth(), 0, 1, d.height);
		g2d.fillRect(getNumField().getX() + getNumField().getWidth(), 0, 1, d.height);

		/*if(getShowTypesButton().isVisible()) {
		g2d.fillRect(getShowTypesButton().getX() + getShowTypesButton().getWidth(), 0, 1, d.height);
		} else {
		g2d.fillRect(getNumField().getX() + getNumField().getWidth() + 10, 0, 1, d.height);
		}*/

		if (isHeader()) {
			g2d.fillRect(getPaymentField().getX() - 5, 0, 1, d.height);
			g2d.fillRect(getPaymentField().getX() + getPaymentField().getWidth() - 1, 0, 1, d.height);
			g2d.fillRect(getDepositField().getX() + getDepositField().getWidth() - 1, 0, 1, d.height);
		} else {
			g2d.fillRect(getPaymentField().getX() - 4, 0, 1, d.height);
			g2d.fillRect(getPaymentField().getX() + getPaymentField().getWidth(), 0, 1, d.height);
			g2d.fillRect(getDepositField().getX() + getDepositField().getWidth(), 0, 1, d.height);
		}

		g2d.setPaint(p);
		super.paintComponent(g);
	}

	/** This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        numField = getNumField();
        payeeField = getPayeeField();
        memoField = new javax.swing.JTextField();
        dateField = getDateField();
        showCategoriesButton = new javax.swing.JButton();
        categoryField = getCategoryField();
        paymentField = new javax.swing.JTextField();
        depositField = new javax.swing.JTextField();
        balanceLabel = new javax.swing.JLabel();
        showPayeesButton = new javax.swing.JButton();
        clearSplitButton = new javax.swing.JButton();

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(com.xbriage.app.WorkbooksApp.class).getContext().getResourceMap(JournalCell.class);
        setBackground(resourceMap.getColor("Form.background")); // NOI18N
        setName("Form"); // NOI18N

        numField.setText(resourceMap.getString("numField.text")); // NOI18N
        numField.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        numField.setName("numField"); // NOI18N
        numField.setNextFocusableComponent(payeeField);

        payeeField.setText(resourceMap.getString("payeeField.text")); // NOI18N
        payeeField.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        payeeField.setName("payeeField"); // NOI18N
        payeeField.setNextFocusableComponent(paymentField);

        memoField.setText(resourceMap.getString("memoField.text")); // NOI18N
        memoField.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        memoField.setName("memoField"); // NOI18N
        memoField.setNextFocusableComponent(dateField);

        dateField.setText(resourceMap.getString("dateField.text")); // NOI18N
        dateField.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        dateField.setName("dateField"); // NOI18N
        dateField.setNextFocusableComponent(numField);

        showCategoriesButton.setIcon(resourceMap.getIcon("showCategoriesButton.icon")); // NOI18N
        showCategoriesButton.setText(resourceMap.getString("showCategoriesButton.text")); // NOI18N
        showCategoriesButton.setToolTipText(resourceMap.getString("showCategoriesButton.toolTipText")); // NOI18N
        showCategoriesButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        showCategoriesButton.setBorderPainted(false);
        showCategoriesButton.setContentAreaFilled(false);
        showCategoriesButton.setName("showCategoriesButton"); // NOI18N

        categoryField.setText(resourceMap.getString("categoryField.text")); // NOI18N
        categoryField.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        categoryField.setName("categoryField"); // NOI18N
        categoryField.setNextFocusableComponent(memoField);

        paymentField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        paymentField.setText(resourceMap.getString("paymentField.text")); // NOI18N
        paymentField.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        paymentField.setName("paymentField"); // NOI18N
        paymentField.setNextFocusableComponent(depositField);

        depositField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        depositField.setText(resourceMap.getString("depositField.text")); // NOI18N
        depositField.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        depositField.setName("depositField"); // NOI18N
        depositField.setNextFocusableComponent(categoryField);

        balanceLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        balanceLabel.setText(resourceMap.getString("balanceLabel.text")); // NOI18N
        balanceLabel.setName("balanceLabel"); // NOI18N

        showPayeesButton.setIcon(resourceMap.getIcon("showPayeesButton.icon")); // NOI18N
        showPayeesButton.setToolTipText(resourceMap.getString("showPayeesButton.toolTipText")); // NOI18N
        showPayeesButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        showPayeesButton.setBorderPainted(false);
        showPayeesButton.setContentAreaFilled(false);
        showPayeesButton.setName("showPayeesButton"); // NOI18N

        clearSplitButton.setIcon(resourceMap.getIcon("clearSplitButton.icon")); // NOI18N
        clearSplitButton.setToolTipText(resourceMap.getString("clearSplitButton.toolTipText")); // NOI18N
        clearSplitButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        clearSplitButton.setBorderPainted(false);
        clearSplitButton.setContentAreaFilled(false);
        clearSplitButton.setName("clearSplitButton"); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(dateField, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(numField, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(categoryField, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(showCategoriesButton, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(clearSplitButton, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(memoField, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(payeeField, javax.swing.GroupLayout.DEFAULT_SIZE, 355, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(showPayeesButton, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(paymentField, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(depositField, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(balanceLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(numField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(dateField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(paymentField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(depositField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(balanceLabel)
                        .addComponent(payeeField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(showPayeesButton, javax.swing.GroupLayout.DEFAULT_SIZE, 16, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(categoryField)
                    .addComponent(clearSplitButton, javax.swing.GroupLayout.DEFAULT_SIZE, 16, Short.MAX_VALUE)
                    .addComponent(memoField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(showCategoriesButton, javax.swing.GroupLayout.DEFAULT_SIZE, 16, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel balanceLabel;
    private javax.swing.JTextField categoryField;
    private javax.swing.JButton clearSplitButton;
    private javax.swing.JTextField dateField;
    private javax.swing.JTextField depositField;
    private javax.swing.JTextField memoField;
    private javax.swing.JTextField numField;
    private javax.swing.JTextField payeeField;
    private javax.swing.JTextField paymentField;
    private javax.swing.JButton showCategoriesButton;
    private javax.swing.JButton showPayeesButton;
    // End of variables declaration//GEN-END:variables
	public JTextField getCategoryField() {
		if (categoryField == null) {
			categoryField = new LookAheadTextField();
		}
		return categoryField;
	}

	public void setCategoryField(JTextField categoryField) {
		this.categoryField = categoryField;
	}

	public javax.swing.JTextField getDateField() {
		if (dateField == null) {
			dateField = new DateTextField();
		}
		return dateField;
	}

	public void setDateField(javax.swing.JTextField dateField) {
		this.dateField = dateField;
	}

	public javax.swing.JTextField getDepositField() {
		return depositField;
	}

	public void setDepositField(javax.swing.JTextField depositField) {
		this.depositField = depositField;
	}

	public javax.swing.JTextField getMemoField() {
		return memoField;
	}

	public void setMemoField(javax.swing.JTextField memoField) {
		this.memoField = memoField;
	}

	public javax.swing.JTextField getNumField() {
		if (numField == null) {
			numField = new LookAheadTextField();
		}
		return numField;
	}

	public void setNumField(javax.swing.JTextField numField) {
		this.numField = numField;
	}

	public javax.swing.JTextField getPaymentField() {
		return paymentField;
	}

	public void setPaymentField(javax.swing.JTextField paymentField) {
		this.paymentField = paymentField;
	}

	public javax.swing.JTextField getPayeeField() {
		if (payeeField == null) {
			payeeField = new LookAheadTextField();
		}
		return payeeField;
	}

	public void setPayeeField(javax.swing.JTextField payeeField) {
		this.payeeField = payeeField;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public javax.swing.JLabel getBalanceLabel() {
		return balanceLabel;
	}

	public void setBalanceLabel(javax.swing.JLabel balanceLabel) {
		this.balanceLabel = balanceLabel;
	}

	public javax.swing.JButton getShowCategoriesButton() {
		return showCategoriesButton;
	}

	public void setShowCategoriesButton(javax.swing.JButton showCategoriesButton) {
		this.showCategoriesButton = showCategoriesButton;
	}

	public JTextField getFocusedTextField() {
		return focusedTextField;
	}

	public void setFocusedTextField(JTextField focusedTextField) {
		this.focusedTextField = focusedTextField;
	}

	public List<String> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<String> categoryList) {
		this.categoryList = categoryList;
	}

	public boolean isExpanded() {
		return expanded;
	}

	public void setExpanded(boolean expanded) {
		changes.firePropertyChange("expanded", this.expanded, expanded);
		this.expanded = expanded;
	}

	public javax.swing.JButton getShowPayeesButton() {
		return showPayeesButton;
	}

	public void setShowPayeesButton(javax.swing.JButton showPayeesButton) {
		this.showPayeesButton = showPayeesButton;
	}

	public boolean isHeader() {
		return header;
	}

	public void setHeader(boolean header) {
		this.header = header;
	}

	public javax.swing.JButton getClearSplitButton() {
		return clearSplitButton;
	}
}
