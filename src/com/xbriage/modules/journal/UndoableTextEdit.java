/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.modules.journal;

import javax.swing.JTextField;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotUndoException;

/**
 *
 * @author shughes
 */
public class UndoableTextEdit extends AbstractUndoableEdit {
	
	private JTextField textField;
	private String originalText;
	
	public UndoableTextEdit(JTextField textField) {
		this.textField = textField;
		originalText = textField.getText();
	}

	@Override
	public void undo() throws CannotUndoException {
		System.out.println("original text: "+originalText);
		textField.setText(originalText);
	}
	
}
