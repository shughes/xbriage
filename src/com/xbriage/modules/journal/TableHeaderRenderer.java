/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.modules.journal;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author shughes
 */
public class TableHeaderRenderer extends JPanel implements TableCellRenderer {
	private JLabel title = new JLabel();
	
	public TableHeaderRenderer() {
		setPreferredSize(new Dimension(getWidth(), 18));
		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		add(title, 0);
	}
	
	public TableHeaderRenderer(String title) {
		this();
		getTitle().setText(title);
	}

	@Override
	public void paintComponent(Graphics g) {
		g.setColor(JournalCell.DARK_GRAY);
		g.fillRect(0, 0, getWidth(), getHeight());
		g.setColor(JournalCell.DARK_LINE);
		g.fillRect(0, getHeight()-1, getWidth(), 1);
		g.fillRect(getWidth()-1, 0, 1, getHeight());
	}

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		return this;
	}

	public JLabel getTitle() {
		return title;
	}

}
