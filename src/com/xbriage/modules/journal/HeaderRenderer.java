/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.journal;

import java.awt.Component;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.OverlayLayout;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author shughes
 */
public class HeaderRenderer implements TableCellRenderer {

	public static final int HEIGHT = 40;
	
	private JournalCell cell = new JournalCell();
	private JPanel panel = new JPanel();

	public HeaderRenderer() {
		cell.setHeader(true);
		cell.getShowCategoriesButton().setVisible(false);
		cell.getShowPayeesButton().setVisible(false);
		cell.setPreferredSize(new Dimension(cell.getWidth(), HEIGHT));
		panel.setLayout(new OverlayLayout(panel));
	}

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		return cell;
	}
	
	public JournalCell getCell() {
		return cell;
	}
}
