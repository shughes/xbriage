/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.journal;

import com.xbriage.modules.journal.proxy.EntryNode;
import com.xbriage.services.AccountDao;
import com.xbriage.domain.Account;
import com.xbriage.domain.JournalEntry;
import com.xbriage.domain.JournalEntryRow;
import com.xbriage.services.Service;
import com.xbriage.context.EmfContext;
import com.xbriage.modules.journal.models.JournalTableModel;
import com.xbriage.util.GeneralUtil;
import java.awt.Component;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * 
 * @author shughes
 */
public class JournalCellRenderer implements TableCellRenderer {

	private JLabel text = new JLabel("Hey");
	private JournalCell cell = new JournalCell();
	private boolean focus = false;

	public JournalCellRenderer() {
		cell.getShowCategoriesButton().setVisible(false);
		cell.getShowPayeesButton().setVisible(false);
		cell.getClearSplitButton().setVisible(false);
	}

	public void setupCell(JournalCell c, EntryNode rowProxy, boolean isSelected, boolean hasFocus) {
		if (isSelected) {
			c.setSelected(true);
			setFocus(hasFocus);
		} else {
			c.setSelected(false);
		}

		EntityManager em = EmfContext.create();

		JournalEntry entry = rowProxy.getEntry();
		entry = em.find(JournalEntry.class, entry.getId());

		Account selectedAccount = JournalTableModel.getSelectedAccount();
		JournalEntryRow entryRow = null;
		JournalEntryRow otherEntryRow = null;
		boolean isSplit = (entry.getRows().size() > 2) ? true : false;

		for (JournalEntryRow entryRow2 : entry.getRows()) {
			if (!entryRow2.getAccount().equals(selectedAccount)) {
				otherEntryRow = entryRow2;
			} else if (entryRow2.getAccount().equals(selectedAccount)) {
				entryRow = entryRow2;
				if(isSplit) {
					break;
				}
			}

			if (otherEntryRow != null && entryRow != null) {
				break;
			}
		}

		entryRow = em.find(JournalEntryRow.class, entryRow.getId());

		//rowProxy.setCell(c);
		AccountDao accountDao = Service.load(AccountDao.class);

		if (entryRow.isDebitEntry().equals(1)) {
			String amt = GeneralUtil.formatStringNumber(entryRow.getAmount().toString());
			c.getPaymentField().setText("");
			c.getDepositField().setText(amt);
		} else {
			String amt = GeneralUtil.formatStringNumber(entryRow.getAmount().toString());
			c.getDepositField().setText("");
			c.getPaymentField().setText(amt);
		}

		c.getDateField().setEnabled(false);
		c.getNumField().setEnabled(false);
		c.getPayeeField().setEnabled(false);
		c.getMemoField().setEnabled(false);
		c.getDepositField().setEnabled(false);
		c.getPaymentField().setEnabled(false);
		c.getCategoryField().setEnabled(false);

		if (isSplit) {
			c.getCategoryField().setText("-- split --");

		} else {
			c.getCategoryField().setText(accountDao.concatName(otherEntryRow.getAccount()));
		}

		if (entry.getCheckNo() == 0) {
			c.getNumField().setText(GeneralUtil.getType(entry.getType()));
		} else {
			c.getNumField().setText(new Integer(entry.getCheckNo()).toString());
		}

		String balance = rowProxy.getBalance();
		c.getBalanceLabel().setText(GeneralUtil.formatStringNumber(balance));
		Date date = entry.getDate();
		String sDate = GeneralUtil.dateToString(date);
		c.getDateField().setText(sDate);

		if (entry.getMemo() != null) {
			c.getMemoField().setText(entry.getMemo());
		} else {
			c.getMemoField().setText("");
		}

		if (entry.getPayee() != null) {
			c.getPayeeField().setText(entry.getPayee().getName());
		} else {
			c.getPayeeField().setText("");
		}
	}

	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		setupCell(cell, (EntryNode) value, isSelected, hasFocus);
		return cell;
	}

	public JournalCell getCell() {
		return cell;
	}

	public void setCell(JournalCell cell) {
		this.cell = cell;
	}

	public boolean isFocus() {
		return focus;
	}

	public void setFocus(boolean hasFocus) {
		this.focus = hasFocus;
	}
}
