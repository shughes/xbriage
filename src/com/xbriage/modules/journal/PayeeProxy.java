/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.journal;

/**
 *
 * @author shughes
 */
public class PayeeProxy {

	private String payee;
	private String paymentAmount;
	private String debitAmount;
	private String category;
	
	public PayeeProxy(String payee, String category, String paymentAmount, String debitAmount) {
		this.payee = payee;
		this.category = category;
		this.paymentAmount = paymentAmount;
		this.debitAmount = debitAmount;
	}

	public String getPayee() {
		return payee;
	}

	public void setPayee(String payee) {
		this.payee = payee;
	}

	public String getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(String amount) {
		this.paymentAmount = amount;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(String debitAmount) {
		this.debitAmount = debitAmount;
	}
	}
