/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.journal;

import com.xbriage.util.GeneralUtil;
import java.awt.event.KeyEvent;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JTextField;

/**
 *
 * @author shughes
 */
public class DateTextField extends JTextField {

	private boolean lastWasAction = false;

	@Override
	public void processKeyEvent(KeyEvent ev) {


		switch (ev.getKeyCode()) {
			case KeyEvent.VK_U:
				if (ev.getID() == KeyEvent.KEY_PRESSED) {
					changeDate(1);
					lastWasAction = true;
				}
				break;

			case KeyEvent.VK_D:
				if (ev.getID() == KeyEvent.KEY_PRESSED) {
					changeDate(0);
					lastWasAction = true;
				}
				break;

			default:
				if (!lastWasAction) {
					super.processKeyEvent(ev);
				} else {
					lastWasAction = false;
				}
		}
	}

	private void changeDate(int direction) {
		Date date = GeneralUtil.stringToDate(getText());
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		if (direction == 0) {
			cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) - 1);
		} else {
			cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + 1);

		}
		date = cal.getTime();

		String sDate = GeneralUtil.dateToString(date);
		setText(sDate);
	}
}
