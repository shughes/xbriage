/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.modules.journal;

import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
import java.util.List;

/**
 *
 * @author shughes
 */
public class CellFocusTraversalPolicy extends FocusTraversalPolicy {
	private List<Component> order;
	
	public CellFocusTraversalPolicy(List<Component> order) {
		this.order = order;
	}

	@Override
	public Component getComponentAfter(Container aContainer, Component aComponent) {
		int index = order.indexOf(aComponent) + 1;
		if(index == order.size()) {
			index = 0;
		}
		return order.get(index);
	}

	@Override
	public Component getComponentBefore(Container aContainer, Component aComponent) {
		int index = order.indexOf(aComponent) - 1;
		if(index < 0) {
			index = order.size()-1;
		}
		return order.get(index);
	}

	@Override
	public Component getFirstComponent(Container aContainer) {
		return order.get(0);
	}

	@Override
	public Component getLastComponent(Container aContainer) {
		return order.get(order.size()-1);
	}

	@Override
	public Component getDefaultComponent(Container aContainer) {
		return order.get(0);
	}

}
