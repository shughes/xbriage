/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.modules.journal.models;

import com.xbriage.services.AccountDao;
import com.xbriage.domain.Account;
import com.xbriage.services.Service;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author shughes
 */
public class CategoriesModel extends AbstractTableModel {
	
	private List<Account> categories;
	private List<String> names;

	public int getRowCount() {
		return categories.size();
		//return names.size();
	}

	public int getColumnCount() {
		return 1;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		Account tmp = categories.get(rowIndex);
		//String tmp = names.get(rowIndex);
		if(tmp != null) {
			AccountDao dao = Service.load(AccountDao.class);
			String name = dao.concatName(tmp);
			return name;
			//return tmp;
		} else {
			return "";
		}		
	}

	public List<Account> getCategories() {
		return categories;
	}

	public void setCategories(List<Account> categories) {
		this.categories = categories;
		fireTableDataChanged();
	}
	
	public void setNames(List<String> names) {
		this.names = names;
		fireTableDataChanged();
	}
	
	public List<String> getNames() {
		return names;
	}

}
