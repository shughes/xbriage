/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.journal.models;

import com.xbriage.modules.journal.proxy.SplitRowProxy;
import com.xbriage.modules.journal.*;
import com.xbriage.services.AccountDao;
import com.xbriage.domain.Account;
import com.xbriage.services.Service;
import com.xbriage.util.GeneralUtil;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author shughes
 */
public class SplitEntryModel extends AbstractTableModel {

	private static Account selectedAccount;
	private List<SplitRowProxy> rowData = new ArrayList<SplitRowProxy>();

	public int getRowCount() {
		return rowData.size();
	}

	public int getColumnCount() {
		return 3;
	}

	public static void setSelectedAccount(Account account) {
		selectedAccount = account;
	}

	public static Account getSelectedAccount() {
		return selectedAccount;
	}

	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		AccountDao dao = Service.load(AccountDao.class);
		
		switch (columnIndex) {
			case 0: // account
				Account account = null;
				if (value != null) {
					account = dao.breakName((String) value);
				}
				
				if (rowIndex >= rowData.size()) {
					SplitRowProxy rowProxy = new SplitRowProxy();
					rowProxy.setAccount(account);
					rowData.add(rowProxy);
				} else {
					SplitRowProxy rowProxy = getRowData().get(rowIndex);
					rowProxy.setAccount(account);
				}
				
				break;
				
			case 1: // memo				
				if(value != null) {
					rowData.get(rowIndex).setMemo(value.toString());
				} else {
					rowData.get(rowIndex).setMemo(null);
				}
				break;
				
			case 2: // amount
				BigDecimal amount = null;
				if (value != null && !value.equals("")) {
					amount = new BigDecimal(GeneralUtil.formatStringNumber((String) value));
				}
				if (rowIndex >= rowData.size()) {
					SplitRowProxy rowProxy = new SplitRowProxy();
					rowProxy.setAmount(amount);
					rowData.add(rowProxy);
				} else {
					SplitRowProxy rowProxy = getRowData().get(rowIndex);
					rowProxy.setAmount(amount);
				}
				break;
		}
		
		fireTableRowsUpdated(rowIndex, rowIndex);
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		return true;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		AccountDao dao = Service.load(AccountDao.class);
		switch (columnIndex) {
			case 0:
				Account account = rowData.get(rowIndex).getAccount();
				if (account != null) {
					return dao.concatName(account);
				} else {
					return null;
				}
				
			case 1:
				return rowData.get(rowIndex).getMemo();
			
			case 2:
				if (rowData.get(rowIndex).getAmount() != null) {
					return rowData.get(rowIndex).getAmount().toString();
				} else {
					return null;
				}
			
			default:
				return null;
		}
	}

	public List<SplitRowProxy> getRowData() {
		return rowData;
	}

	public void setRowData(List<SplitRowProxy> rowData) {
		this.rowData = rowData;
		this.fireTableDataChanged();
	}
}
