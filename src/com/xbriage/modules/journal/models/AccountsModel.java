/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.modules.journal.models;

import com.xbriage.domain.Account;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author shughes
 */
public class AccountsModel extends AbstractTableModel {
	private List<Account> accounts;

	public int getRowCount() {
		return accounts.size();
	}

	public int getColumnCount() {
		return 1;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
			case 0:
				return accounts.get(rowIndex).getName();
				
			default: 
				return null;
		}
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	public List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
		fireTableDataChanged();
	}

}
