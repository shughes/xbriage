/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.modules.journal.models;

import com.xbriage.modules.journal.proxy.RowProxy;
import com.xbriage.util.GeneralUtil;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author shughes
 */
public class SplitModel extends AbstractTableModel {
	private List<RowProxy> splitRows;

	public int getRowCount() {
		return splitRows.size();
	}

	public int getColumnCount() {
		return 3;
	}
	
	@Override
	public boolean isCellEditable(int row, int col) {
		return true;
	}
	
	@Override
	public void setValueAt(Object value, int row, int col) {
		RowProxy proxy = splitRows.get(row);
		String v = (String) value;
		
		switch(col) {
			case 0:
				proxy.setAccount(v);
				break;
				
			case 1:
				proxy.setMemo(v);
				break;
				
			case 2:
				proxy.setAmount(v);
				break;
		}
		
		fireTableRowsUpdated(row, row);
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
			case 0:
				return splitRows.get(rowIndex).getAccount();
				
			case 1:
				return splitRows.get(rowIndex).getMemo();
				
			case 2:
				String sAmt = splitRows.get(rowIndex).getAmount();
				if(sAmt == null) {
					sAmt = "";
				} else if(sAmt.trim().equals("")) {
					sAmt = "";
				} else {
					sAmt = GeneralUtil.formatStringNumber(sAmt);
				}
				return sAmt;
				
			default:
				return "";
		}
	}

	public List<RowProxy> getSplitRows() {
		return splitRows;
	}

	public void setSplitRows(List<RowProxy> splitRows) {
		this.splitRows = splitRows;
		fireTableDataChanged();
	}

	
}
