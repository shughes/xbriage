/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.journal.models;

import com.xbriage.modules.journal.proxy.EntryNode;
import com.xbriage.modules.journal.*;
import com.xbriage.modules.journal.actions.JournalActions;
import com.xbriage.domain.Account;
import com.xbriage.domain.JournalEntry;
import com.xbriage.modules.journal.proxy.ProxyModel;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author shughes
 */
public class JournalTableModel extends AbstractTableModel implements ProxyModel {

	private String[] entryTypes;
	private static Account selectedAccount;
	private static JournalEntry selectedEntry;
	private List<EntryNode> rowData = new ArrayList<EntryNode>();
	private boolean confirmed;
		
	public static void setSelectedEntry(JournalEntry aSelectedEntry) {
		selectedEntry = aSelectedEntry;
	}
	
	public static JournalEntry getSelectedEntry(EntityManager em) {
		selectedEntry = em.find(JournalEntry.class, selectedEntry.getId());
		return selectedEntry;
	}
	
	public static JournalEntry getSelectedEntry() {
		return selectedEntry;
	}

	public JournalTableModel() {
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		return true;
	}

	public int getRowCount() {
		return getRowData().size();
	}

	public int getColumnCount() {
		return 1;
	}
	
	@Override
	public void setValueAt(Object value, int rowIndex, int colIndex) {
		
		boolean updated = JournalActions.getInstance().saveTableRow((EntryNode) value, rowIndex);

		if (updated) {
			fireTableRowsUpdated(rowIndex, rowIndex);
		}
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		EntryNode proxy = getRowData().get(rowIndex);
		return proxy;
	}

	@Override
	public String getColumnName(int col) {
		return "";
	}

	public static Account getSelectedAccount() {
		return selectedAccount;
	}

	public static void setSelectedAccount(Account account) {
		selectedAccount = account;
	}

	public List<EntryNode> getRowData() {
		return rowData;
	}

	public void setRowData(List<EntryNode> rows) {
		this.rowData = rows;
		this.fireTableDataChanged();
	}

	public String[] getEntryTypes() {
		return entryTypes;
	}

	public void setEntryTypes(String[] entryTypes) {
		this.entryTypes = entryTypes;
	}

	public boolean isConfirmed() {
		return confirmed;
	}

	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}
}
