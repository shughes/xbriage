/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.journal.models;

import com.xbriage.domain.Payee;
import com.xbriage.context.EmfContext;
import com.xbriage.modules.journal.PayeeProxy;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author shughes
 */
public class PayeesModel extends AbstractTableModel {

	List<Payee> payees;
	private List<PayeeProxy> proxies;

	public PayeesModel() {
		EntityManager em = EmfContext.get().createEntityManager();
		List<Payee> payeeList = em.createQuery("select p from Payee p").getResultList();
		setPayees(payeeList);
	}

	public int getRowCount() {
		return payees.size();
		//return proxies.size();
	}

	public int getColumnCount() {
		return 1;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		Payee payee = payees.get(rowIndex);
		/*
		PayeeProxy proxy = proxies.get(rowIndex);
		
		switch(columnIndex) {
			case 0:
				return proxy.getPayee();
				
			case 1:
				return proxy.getCategory();
				
			case 2:
				String p = proxy.getPaymentAmount();
				String d = proxy.getDebitAmount();
				
				if(!p.equals("")) {
					BigDecimal dp = new BigDecimal(p);
					dp = dp.multiply(new BigDecimal(-1));
					return dp.toString();
				}
				
				if(!d.equals("")) {
					return d;
				}
				
				return "0.00";
		}
		
		return null;
		*/
		return payee.getName();
	}

	public void setPayees(List<Payee> payees) {
		this.payees = payees;
		fireTableDataChanged();
	}

	public List<PayeeProxy> getProxies() {
		return proxies;
	}

	public void setProxies(List<PayeeProxy> proxies) {
		this.proxies = proxies;
		fireTableDataChanged();
	}

	
}
