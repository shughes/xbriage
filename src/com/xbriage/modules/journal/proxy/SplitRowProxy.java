/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.modules.journal.proxy;

import com.xbriage.domain.Account;
import java.math.BigDecimal;

/**
 *
 * @author shughes
 */
public class SplitRowProxy {
	private Account account;
	private BigDecimal amount;
	private String memo;
	
	public SplitRowProxy() {
		
	}

	public SplitRowProxy(Account account, BigDecimal amount) {
		this.account = account;
		this.amount = amount;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}
	 
}
