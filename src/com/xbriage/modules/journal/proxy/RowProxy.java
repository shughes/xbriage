/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.modules.journal.proxy;

/**
 *
 * @author shughes
 */
public class RowProxy {
	private String amount;
	private String memo;
	private String account;
	private EntryProxy entry;
	private boolean debit;
	
	public RowProxy() {
		
	}
	
	public RowProxy(RowProxy proxy) {
		amount = proxy.getAmount();
		memo = proxy.getMemo();
		account = proxy.getAccount();
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public EntryProxy getEntry() {
		return entry;
	}

	public void setEntry(EntryProxy entry) {
		this.entry = entry;
	}

	public boolean isDebit() {
		return debit;
	}

	public void setDebit(boolean debit) {
		this.debit = debit;
	}
	
}
