/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.modules.journal.proxy;

import java.util.List;

/**
 *
 * @author shughes
 */
public interface ProxyModel {
	
	public List<EntryNode> getRowData();
	public void setRowData(List<EntryNode> rowData);

}
