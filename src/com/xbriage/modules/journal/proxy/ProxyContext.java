/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.journal.proxy;

import com.xbriage.app.WorkbooksApp;
import com.xbriage.services.AccountDao;
import com.xbriage.modules.journal.*;
import com.xbriage.domain.Account;
import com.xbriage.domain.JournalEntry;
import com.xbriage.domain.JournalEntryRow;
import com.xbriage.domain.Payee;
import com.xbriage.services.Service;
import com.xbriage.context.EmfContext;
import com.xbriage.services.Service;
import com.xbriage.modules.journal.actions.JournalActions;
import com.xbriage.modules.journal.models.JournalTableModel;
import com.xbriage.services.ModuleLoader;
import com.xbriage.util.GeneralUtil;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author shughes
 */
public class ProxyContext {

	private static final ProxyContext instance = new ProxyContext();
	private Map<Account, List<EntryNode>> map = new HashMap<Account, List<EntryNode>>();
	private JournalModule module = Service.load(ModuleLoader.class).get("Journal.module");
	private JournalTableModel model = (JournalTableModel) module.getEntriesTable().getModel();
	private List<EntryNode> entryNodes = null;
	private boolean splitAltered = false;

	public static ProxyContext getInstance() {
		return instance;
	}

	public void saveEntry(EntryProxy proxy, EntryNode node, Account selectedAccount) {
		EntityManager em = EmfContext.create();
		JournalEntry entry = node.getEntry();
		entry = em.find(JournalEntry.class, entry.getId());

		// date
		String sDate = proxy.getDate();
		Date date = GeneralUtil.stringToDate(sDate);
		entry.setDate(date);

		// num
		String sNum = proxy.getNum();
		Pattern p = Pattern.compile("\\d+");
		Matcher m = p.matcher(sNum);
		if (m.matches()) {
			Integer num = new Integer(sNum);
			entry.setCheckNo(num);
		} else {
			int num = GeneralUtil.getType(sNum);
			entry.setType(num);
			entry.setCheckNo(0);
		}

		// payee
		String sPayee = proxy.getPayee();
		Payee payee = null;
		Payee defaultPayee = (Payee) em.createNamedQuery("Payee.findDefault").getSingleResult();
		if (sPayee == null || sPayee.equals("")) {
			payee = defaultPayee;
		} else {
			Query q = em.createQuery("select p from Payee p where p.name = :name");
			q.setParameter("name", sPayee);
			List<Payee> result = q.getResultList();
			if (result.size() > 0) {
				payee = result.get(0);
			} else {
				boolean yes = GeneralUtil.showWarningAlert(WorkbooksApp.getWarning("RowActions.payeeNotExist"));
				if (yes) {
					payee = new Payee(sPayee, false);
					em.persist(payee);
				} else {
					payee = defaultPayee;
				}
			}
		}
		entry.setPayee(payee);

		// category
		String sCategory = proxy.getCategory();
		Account category = null;
		AccountDao accountDao = Service.load(AccountDao.class);
		if (!sCategory.equals("-- split --")) {
			category = accountDao.breakName(sCategory);
		}

		// memo
		String sMemo = proxy.getMemo();
		entry.setMemo(sMemo);

		// payment
		int isDebit = 0;
		BigDecimal amount = null;
		String sPayment = proxy.getPayment();
		if (!sPayment.equals("") && sPayment != null) {
			sPayment = GeneralUtil.formatStringNumber(sPayment);
			amount = new BigDecimal(sPayment);
		}

		// deposit
		String sDeposit = proxy.getDeposit();
		if (!sDeposit.equals("") && sDeposit != null) {
			sDeposit = GeneralUtil.formatStringNumber(sDeposit);
			amount = new BigDecimal(sDeposit);
			isDebit = 1;
		}

		// set row 
		boolean changed = false;
		for (JournalEntryRow row : entry.getRows()) {
			if (row.getAccount().equals(selectedAccount)) {
				if (row.isDebitEntry().compareTo(isDebit) != 0) {
					changed = true;
					break;
				}
			}
		}

		for (JournalEntryRow row : entry.getRows()) {

			if (!row.getAccount().equals(selectedAccount) && !sCategory.equals("-- split --")) {

				row.setAmount(amount);

				if (category == null) {
					category = (Account) em.createNamedQuery("Account.findDefault").getSingleResult();
				}

				if (!category.equals(selectedAccount)) {
					row.setAccount(category);
				} else {
					GeneralUtil.showWarningAlert2(WorkbooksApp.getWarning("RowActions.categoryNotAccount"));
				}

			} else if (row.getAccount().equals(selectedAccount)) {
				row.setDebitEntry(isDebit);
				row.setAmount(amount);
			}

			if (!row.getAccount().equals(selectedAccount)) {
				int otherIsDebit = 0;
				int oldIsDebit = row.isDebitEntry();
				if (changed) {
					otherIsDebit = (oldIsDebit == 1) ? 0 : 1;
				} else {
					otherIsDebit = oldIsDebit;
				}
				row.setDebitEntry(otherIsDebit);
			}
		}

		// split entry
		if (node.getSplitRows().size() > 1 || splitAltered) {
			System.out.println("is a split");

			JournalEntryRow currentRow = node.getIndex().get(selectedAccount);
			AccountDao dao = Service.load(AccountDao.class);
			int currentIsDebit = 0;
			if (!proxy.getDeposit().equals("")) {
				currentIsDebit = 1;
				currentRow.setAmount(new BigDecimal(proxy.getDeposit()));
			} else {
				currentRow.setAmount(new BigDecimal(proxy.getPayment()));
			}

			currentRow.setDebitEntry(currentIsDebit);

			// remove all but current row
			for (JournalEntryRow row : entry.getRows()) {
				if (!row.equals(currentRow)) {
					row = em.find(JournalEntryRow.class, row.getId());
					em.remove(row);
				}
			}

			List<JournalEntryRow> rows = new ArrayList<JournalEntryRow>();
			rows.add(currentRow);
			for (RowProxy r : node.getSplitRows()) {
				JournalEntryRow row = new JournalEntryRow();

				// if only one split row, category field overrides whatever the one account is 
				// in splitRows.
				if (node.getSplitRows().size() == 1) {
					r.setAccount(proxy.getCategory());
				}

				Account account = dao.breakName(r.getAccount());
				BigDecimal amt = new BigDecimal(r.getAmount());
				int isDebit2 = 0;
				int sign = amt.compareTo(new BigDecimal(0));
				if (sign == -1) {
					isDebit2 = currentIsDebit;
				} else {
					isDebit2 = (currentIsDebit == 0) ? 1 : 0;
				}
				String memo = r.getMemo();
				amt = amt.abs();

				row.setAccount(account);
				row.setMemo(memo);
				row.setDebitEntry(isDebit2);
				row.setJournalEntry(entry);
				row.setAmount(amt);

				row = em.merge(row);
				rows.add(row);
			}

			entry.setRows(rows);
			splitAltered = false;
		}

		// update transaction
		em.getTransaction().begin();
		entry = em.merge(entry);
		//em.persist(entry);
		em.getTransaction().commit();
		em.close();

		updateIndex(node);

	}

	public void updateIndex(EntryNode node) {
		EntityManager em = EmfContext.create();
		JournalEntry entry = node.getEntry();
		entry = em.find(JournalEntry.class, entry.getId());

		Map<Account, JournalEntryRow> index = new HashMap<Account, JournalEntryRow>();
		for (JournalEntryRow row : entry.getRows()) {
			if (row.getAccount().isUserAccount() == 1) {
				index.put(row.getAccount(), row);
			}
		}

		node.setIndex(index);
	}

	public void removeEntry(EntryNode proxy) {
		JournalEntry entry = proxy.getEntry();

		// remove entry node from entryNodes and proxies
		for (JournalEntryRow row : entry.getRows()) {
			Account account2 = row.getAccount();
			List<EntryNode> proxies = getProxies(account2);

			if (proxies != null) {
				for (EntryNode proxy2 : proxies) {
					if (proxy2.getEntry().equals(entry)) {
						proxies.remove(proxy2);
						entryNodes.remove(proxy2);
						break;
					}
				}
			}
		}

		EntityManager em = EmfContext.create();
		entry = em.find(JournalEntry.class, entry.getId());
		em.remove(entry);
		em.getTransaction().begin();
		em.getTransaction().commit();
		em.close();

		JournalActions.getInstance().refresh();
		int rowSize = module.getEntriesTable().getRowCount();

		if (module.getEntriesTable().getSelectedRow() == -1) {
			module.getEntriesTable().changeSelection(rowSize - 1, 0, false, false);
			module.getEntriesTable().editCellAt(rowSize - 1, 0);
		}
	}

	public void refreshProxies(Account account) {
		List<EntryNode> newRows = getProxies(account);
		model.setRowData(newRows);
		module.getAccountLabel().setText(account.getName());
		EntryNode tmp = newRows.get(newRows.size() - 1);
		String sBal = tmp.getBalance();
		BigDecimal bBal = new BigDecimal(sBal);
		sBal = GeneralUtil.formatStringNumberWithCommas(sBal);
		if (bBal.compareTo(new BigDecimal(0)) == -1) {
			sBal = GeneralUtil.formatRedString(sBal);
		}

		module.getBalanceLabel().setText(sBal); // here
	}

	public List<EntryNode> filterProxies(Account account, String query) {
		List<EntryNode> nodes = new ArrayList<EntryNode>();

		String[] arr = query.split(" ");

		for (EntryNode node : entryNodes) {
			JournalEntry entry = node.getEntry();
			String payee = entry.getPayee().getName().toLowerCase();
			String checkNo = new Integer(entry.getCheckNo()).toString();
			String type = GeneralUtil.getType(entry.getType()).toLowerCase();
			String date = GeneralUtil.dateToString(entry.getDate());
			String memo = entry.getMemo();
			if (memo == null) {
				memo = "";
			}
			memo = memo.toLowerCase();

			boolean check = false;
			for (JournalEntryRow row : entry.getRows()) {
				if (row.getAccount().equals(account)) {
					check = true;
				}
			}

			if (check) {
				Map<EntryNode, Boolean> tracker = new HashMap<EntryNode, Boolean>();
				boolean add = false;
				for (String q : arr) {
					if (tracker.get(node) == null) {
						q = q.toLowerCase();

						if (payee.contains(q)) {
							add = true;
						}
						if (checkNo.contains(q)) {
							add = true;
						}
						if (type.contains(q)) {
							add = true;
						}
						if (date.contains(q)) {
							add = true;
						}
						if (memo.contains(q)) {
							add = true;
						}

						for (JournalEntryRow row : entry.getRows()) {
							AccountDao dao = Service.load(AccountDao.class);
							String amt = GeneralUtil.formatStringNumber(row.getAmount().toString());
							String acct = dao.concatName(row.getAccount()).toLowerCase();
							String m = "";
							if (row.getMemo() != null) {
								m = row.getMemo().toLowerCase().trim();
							}

							if (amt.contains(q)) {
								add = true;
							}
							if (acct.contains(q)) {
								add = true;
							}
							if (m.contains(q)) {
								add = true;
							}
						}

						if (add) {
							nodes.add(node);
							tracker.put(node, true);
						}
					}
				}
			}
		}

		return nodes;
	}

	public EntryNode addEntry(JournalEntry entry) {
		EntryNode proxy = new EntryNode();
		proxy.setEntry(entry);
		proxy.setBalance("0.00");
		updateIndex(proxy);
		entryNodes.add(proxy);
		return proxy;
	}

	public void addEntry(Account selectedAccount) {
		JournalEntry entry = createEntry(selectedAccount);
		EntryNode proxy = addEntry(entry);
		List<EntryNode> rowData = model.getRowData();
		rowData.add(proxy);
		model.setRowData(rowData);
	}
	
	public List<EntryNode> getEntryNodes() {
		return entryNodes;
	}

	public List<EntryNode> getProxies(Account account) {
		List<EntryNode> proxies = new ArrayList<EntryNode>(); //map.get(account);
		for (EntryNode node : entryNodes) {
			if (node.getIndex().get(account) != null) {
				proxies.add(node);
			}
		}
		Collections.sort(proxies, new DateComparator(account));
		return proxies;
	}

	private class DateComparator implements Comparator {

		private Account account;

		public DateComparator(Account account) {
			this.account = account;
		}

		public int compare(Object obj1, Object obj2) {
			EntryNode node1 = (EntryNode) obj1;
			EntryNode node2 = (EntryNode) obj2;

			JournalEntryRow row1 = node1.getIndex().get(account);
			JournalEntryRow row2 = node2.getIndex().get(account);

			Date date1 = row1.getJournalEntry().getDate();
			Date date2 = row2.getJournalEntry().getDate();

			return date1.compareTo(date2);
		}
	}

	public void setup() {
		EntityManager em = EmfContext.create();
		List<JournalEntry> entries = em.createNamedQuery("JournalEntry.findAll").getResultList();
		List<EntryNode> nodes = new ArrayList<EntryNode>();

		for (JournalEntry entry : entries) {
			EntryNode node = new EntryNode();
			node.setBalance("0.00");
			node.setEntry(entry);

			updateIndex(node);

			nodes.add(node);
		}

		entryNodes = nodes;
	}

	protected ProxyContext() {
		setup();
	}

	private JournalEntry createEntry(Account selectedAccount) {
		EntityManager em = EmfContext.get().createEntityManager();
		Payee defaultPayee = (Payee) em.createNamedQuery("Payee.findDefault").getSingleResult();

		em.getTransaction().begin();

		JournalEntryRow rowOne = new JournalEntryRow();
		JournalEntryRow rowTwo = new JournalEntryRow();
		JournalEntry entry = new JournalEntry();

		List<JournalEntryRow> rows = new ArrayList<JournalEntryRow>();
		rows.add(rowOne);
		rows.add(rowTwo);
		entry.setRows(rows);

		rowOne.setAmount(new BigDecimal(0));
		rowTwo.setAmount(new BigDecimal(0));

		Account otherAccount = (Account) em.createNamedQuery("Account.findDefault").getSingleResult();

		rowOne.setAccount(selectedAccount);
		rowOne.setDebitEntry(0);
		rowOne.setJournalEntry(entry);

		rowTwo.setDebitEntry(1);
		rowTwo.setAccount(otherAccount);
		rowTwo.setJournalEntry(entry);

		entry.setDate(new Date());
		entry.setPayee(defaultPayee);
		entry.setType(GeneralUtil.getType("ATM"));

		entry = em.merge(entry);
		em.getTransaction().commit();
		em.close();

		return entry;
	}

	public boolean isSplitAltered() {
		return splitAltered;
	}

	public void setSplitAltered(boolean splitAltered) {
		this.splitAltered = splitAltered;
	}
}
