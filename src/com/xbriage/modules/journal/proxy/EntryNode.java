/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.modules.journal.proxy;

import com.xbriage.domain.Account;
import com.xbriage.modules.journal.*;
import com.xbriage.domain.JournalEntry;
import com.xbriage.domain.JournalEntryRow;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 *
 * @author shughes
 */
public class EntryNode {
	private JournalEntry entry;
	private String balance;
	private Map<Account, JournalEntryRow> index;
	private List<RowProxy> splitRows;
	
	public EntryNode() {

	}
	
	public EntryNode(JournalEntry entry, String balance) {
		this.entry = entry;
		this.balance = balance;
	}
	
	public JournalEntry getEntry() {
		return entry;
	}

	public void setEntry(JournalEntry entry) {
		this.entry = entry;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public Map<Account, JournalEntryRow> getIndex() {
		return index;
	}

	public void setIndex(Map<Account, JournalEntryRow> index) {
		this.index = index;
	}

	public List<RowProxy> getSplitRows() {
		return splitRows;
	}

	public void setSplitRows(List<RowProxy> splitRows) {
		this.splitRows = splitRows;
	}
}
