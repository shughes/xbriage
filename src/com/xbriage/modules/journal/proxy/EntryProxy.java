/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.modules.journal.proxy;

import java.util.List;

/**
 *
 * @author shughes
 */
public class EntryProxy {	
	private String category;
	private String date;
	private String num;
	private String payee;
	private String memo;
	private String payment;
	private String deposit;
	
	private List<RowProxy> rows;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getPayee() {
		return payee;
	}

	public void setPayee(String payee) {
		this.payee = payee;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getDeposit() {
		return deposit;
	}

	public void setDeposit(String deposit) {
		this.deposit = deposit;
	}

	public List<RowProxy> getRows() {
		return rows;
	}

	public void setRows(List<RowProxy> rows) {
		this.rows = rows;
	}
}
