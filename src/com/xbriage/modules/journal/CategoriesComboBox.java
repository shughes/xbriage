/*
 * CategoriesComboBox.java
 *
 * Created on May 10, 2008, 1:24 AM
 */
package com.xbriage.modules.journal;

import com.xbriage.modules.journal.models.CategoriesModel;
import javax.swing.ListSelectionModel;

/**
 *
 * @author  shughes
 */
public class CategoriesComboBox extends javax.swing.JPanel {

	private static CategoriesComboBox instance = new CategoriesComboBox();

	public static CategoriesComboBox getInstance() {
		return instance;
	}

	/** Creates new form CategoriesComboBox */
	private CategoriesComboBox() {
		initComponents();
		
		CategoriesModel model = new CategoriesModel();
		getTable().setModel(model);
		
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}

	/*
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.darkGray);
		g.fillRect(0, 0, 1, this.getHeight());
		g.fillRect(0, this.getHeight() - 1, this.getWidth(), 1);
		g.fillRect(this.getWidth() - 1, 0, 1, this.getHeight());
	}
	*/

	/** This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(com.xbriage.app.WorkbooksApp.class).getContext().getResourceMap(CategoriesComboBox.class);
        setBackground(resourceMap.getColor("Application.defaultBackground")); // NOI18N
        setName("Form"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        table.setName("table"); // NOI18N
        table.setShowHorizontalLines(false);
        table.setShowVerticalLines(false);
        table.setTableHeader(null);
        jScrollPane1.setViewportView(table);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 289, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable table;
    // End of variables declaration//GEN-END:variables


	public javax.swing.JTable getTable() {
		return table;
	}
}
