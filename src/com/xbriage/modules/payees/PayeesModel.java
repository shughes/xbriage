/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.payees;

import com.xbriage.app.WorkbooksApp;
import com.xbriage.domain.Payee;
import com.xbriage.context.EmfContext;
import com.xbriage.services.Service;
import com.xbriage.modules.journal.JournalCellEditor;
import com.xbriage.modules.journal.JournalModule;
import com.xbriage.services.ModuleLoader;
import com.xbriage.services.Refresh;
import com.xbriage.util.GeneralUtil;
import com.xbriage.services.Service;
import java.util.List;
import javax.persistence.EntityManager;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author shughes
 */
public class PayeesModel extends AbstractTableModel {

	private List<Payee> payees;

	public int getRowCount() {
		return payees.size();
	}

	public int getColumnCount() {
		return 1;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
			case 0:
				return payees.get(rowIndex).getName();
		}
		return null;
	}

	@Override
	public void setValueAt(Object value, int rowIndex, int colIndex) {
		if (payees.size() > rowIndex) {
			Payee payee = payees.get(rowIndex);
			EntityManager em = EmfContext.create();
			payee = em.find(Payee.class, payee.getId());
			String oldName = payee.getName();
			Payee defaultPayee = (Payee) em.createNamedQuery("Payee.findDefault").getSingleResult();

			switch (colIndex) {
				case 0:
					if (value != null) {
						if (!payee.equals(defaultPayee)) {
							payee.setName(value.toString());
						} else {
							GeneralUtil.showWarningAlert2(WorkbooksApp.getWarning("PayeesModel.cannotEditDefault"));
						}
					}
					break;
			}

			em.getTransaction().begin();
			em.persist(payee);
			em.getTransaction().commit();
			em.close();
			payees.set(rowIndex, payee);
			fireTableRowsUpdated(rowIndex, rowIndex);
			
			JournalModule journalModule = Service.load(ModuleLoader.class).get("Journal.module");
			Service.load(Refresh.class).refresh("PayeesModel.setValueAt");
			JournalCellEditor cellEditor = (JournalCellEditor) journalModule.getCellEditor();
			if (cellEditor.getCell().getPayeeField().getText().equals(oldName)) {
				cellEditor.getCell().getPayeeField().setText(payee.getName());
			}

		}
	}

	public List<Payee> getPayees() {
		return payees;
	}

	public void setPayees(List<Payee> payees) {
		this.payees = payees;
		this.fireTableDataChanged();
	}
}
