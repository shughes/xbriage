/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.payees;

import com.xbriage.context.EmfContext;
import com.xbriage.services.Service;
import com.xbriage.domain.JournalEntry;
import com.xbriage.domain.Payee;
import com.xbriage.modules.journal.JournalCellEditor;
import com.xbriage.modules.journal.JournalModule;
import com.xbriage.modules.journal.actions.JournalActions;
import com.xbriage.services.ModuleLoader;
import javax.persistence.EntityManager;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotUndoException;

/**
 *
 * @author shughes
 */
public class UndoablePayee extends AbstractUndoableEdit {

	private Payee payee = null;
	private String oldName;

	public UndoablePayee(Payee payee) {
		oldName = payee.getName();
		this.payee = payee;
	}

	@Override
	public String getUndoPresentationName() {
		return "Undo payee name";
	}

	@Override
	public void undo() throws CannotUndoException {
		EntityManager em = EmfContext.create();
		payee.setName(oldName);
		em.merge(payee);
		em.getTransaction().begin();
		em.getTransaction().commit();
		refresh();
	}
	
	protected void refresh() {
		PayeeActions.getInstance().refresh();
		JournalModule journalModule = Service.load(ModuleLoader.class).get("Journal.module");
		JournalActions.getInstance().refresh();
		JournalCellEditor cellEditor = (JournalCellEditor) journalModule.getCellEditor();
		
		JournalEntry entry = cellEditor.getEntryNode().getEntry();
		EntityManager em = EmfContext.create();
		entry = em.find(JournalEntry.class, entry.getId());
		
		if(entry.getPayee().getName().equals(payee.getName())) {
			cellEditor.getCell().getPayeeField().setText(payee.getName());
		}
	}
}
