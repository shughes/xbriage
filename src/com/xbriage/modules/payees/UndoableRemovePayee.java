/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.modules.payees;

import com.xbriage.context.EmfContext;
import com.xbriage.domain.JournalEntry;
import com.xbriage.domain.Payee;
import javax.persistence.EntityManager;
import javax.swing.undo.CannotUndoException;

/**
 *
 * @author shughes
 */
public class UndoableRemovePayee extends UndoablePayee {
	
	private Payee oldPayee;
	
	public UndoableRemovePayee(Payee oldPayee) {
		super(oldPayee);
		this.oldPayee = new Payee(oldPayee);
	}
	
	@Override
	public String getUndoPresentationName() {
		return "Undo removed payee";
	}
	
	@Override
	public void undo() throws CannotUndoException {
		EntityManager em = EmfContext.create();
		em.getTransaction().begin();
		oldPayee = em.merge(oldPayee);
		for(JournalEntry entry : oldPayee.getJournalEntries()) {
			entry = em.find(JournalEntry.class, entry.getId());
			entry.setPayee(oldPayee);
			em.persist(entry);
		}
		em.merge(oldPayee);
		em.getTransaction().commit();
		refresh();
	}

}
