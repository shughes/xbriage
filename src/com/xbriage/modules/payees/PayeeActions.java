/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.payees;

import com.xbriage.app.TemplateData;
import com.xbriage.app.WorkbooksApp;
import com.xbriage.domain.JournalEntry;
import com.xbriage.domain.Payee;
import com.xbriage.context.EmfContext;
import com.xbriage.context.MapContext;
import com.xbriage.services.Service;
import com.xbriage.modules.journal.JournalCellEditor;
import com.xbriage.modules.journal.JournalModule;
import com.xbriage.modules.journal.actions.JournalActions;
import com.xbriage.services.ModuleLoader;
import com.xbriage.util.GeneralUtil;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author shughes
 */
public class PayeeActions {

	private final static PayeeActions instance = new PayeeActions();
	private final PayeesModule module = Service.load(ModuleLoader.class).get("Payees.module");

	public static PayeeActions getInstance() {
		return instance;
	}

	private PayeeActions() {

	}

	public void showPopup() {
		Map params = (Map) MapContext.get(getClass().getName() + ".showPopup");
		MouseEvent evt = (MouseEvent) params.get(MouseEvent.class);
		int row = module.getTable().rowAtPoint(evt.getPoint());
		if (evt.isPopupTrigger()) {
			module.getTable().getSelectionModel().setSelectionInterval(row, row);
			module.getPopupMenu().show(evt.getComponent(), evt.getX(), evt.getY());
		}
	}

	public void refresh() {
		if (TemplateData.getInstance().isDisplayed(module)) {
			System.out.println("PayeeActions.refresh");
			EntityManager em = EmfContext.create();
			List<Payee> payees = em.createNamedQuery("Payee.findAll").getResultList();
			PayeesModel model = (PayeesModel) module.getTable().getModel();
			model.setPayees(payees);
		}
	}

	public void add() {
		Payee payee = new Payee("New Payee");
		EntityManager em = EmfContext.create();
		em.getTransaction().begin();
		em.persist(payee);
		em.getTransaction().commit();
		em.close();
		PayeesModel model = (PayeesModel) module.getTable().getModel();
		List<Payee> payees = model.getPayees();
		payees.add(payee);
		model.setPayees(payees);
		module.getTable().getSelectionModel().setSelectionInterval(payees.size() - 1, payees.size() - 1);
		module.getTable().scrollRectToVisible(module.getTable().getCellRect(payees.size() -1 , 0, true));
	}

	public void remove() {
		PayeesModel model = (PayeesModel) module.getTable().getModel();
		int index = module.getTable().getSelectedRow();

		if (index != -1) {
			boolean yes = GeneralUtil.showWarningAlert(WorkbooksApp.getWarning("PayeeActions.confirmDelete"));

			if (yes) {
				Payee payee = model.getPayees().get(index);
				removePayee(payee);
			}
		}
	}

	public void removePayee(Payee payee) {
		EntityManager em = EmfContext.create();
		payee = em.find(Payee.class, payee.getId());
		String oldName = payee.getName();

		Payee defaultPayee = (Payee) em.createNamedQuery("Payee.findDefault").getSingleResult();

		if (!payee.equals(defaultPayee)) {

			Query q = em.createQuery("select j from JournalEntry j where j.payee = :payee");
			q.setParameter("payee", payee);
			List<JournalEntry> entries = q.getResultList();
			for (JournalEntry entry : entries) {
				entry.setPayee(defaultPayee);
				em.persist(entry);
			}

			em.getTransaction().begin();
			em.remove(payee);
			em.getTransaction().commit();
			em.close();

			PayeesModel model = (PayeesModel) module.getTable().getModel();
			List<Payee> payees = model.getPayees();
			payees.remove(payee);
			model.setPayees(payees);

			JournalModule journalModule = Service.load(ModuleLoader.class).get("Journal.module");
			JournalActions.getInstance().refreshAndSetRow();
			JournalCellEditor cellEditor = (JournalCellEditor) journalModule.getCellEditor();
			if (cellEditor.getCell().getPayeeField().getText().equals(oldName)) {
				cellEditor.getCell().getPayeeField().setText(defaultPayee.getName());
			}
		} else {
			GeneralUtil.showWarningAlert2(WorkbooksApp.getWarning("PayeeActions.cannotRemoveDefault"));
		}
	}

}
