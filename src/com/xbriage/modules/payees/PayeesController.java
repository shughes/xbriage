/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.payees;

import com.xbriage.domain.Payee;
import com.xbriage.context.EmfContext;
import com.xbriage.services.Service;
import com.xbriage.modules.journal.TableHeaderRenderer;
import com.xbriage.services.ModuleLoader;
import com.xbriage.util.BindUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

/**
 *
 * @author shughes
 */
public class PayeesController {

	private final PayeeActions payeeActions = PayeeActions.getInstance();
	private final PayeesModule module = Service.load(ModuleLoader.class).get("Payees.module");
	private final PayeesModel model = new PayeesModel();
	private final TableHeaderRenderer headerRenderer = new TableHeaderRenderer();
	private final JTextField cellField = new JTextField();

	public PayeesController() {
		EntityManager em = EmfContext.create();
		List<Payee> payees = em.createNamedQuery("Payee.findAll").getResultList();
		model.setPayees(payees);
		module.getTable().setModel(model);
		module.getTable().setFillsViewportHeight(true);
		headerRenderer.getTitle().setText("Name");
		module.getTable().getColumnModel().getColumn(0).setHeaderRenderer(headerRenderer);
		module.getTable().getColumnModel().getColumn(0).setCellEditor(new DefaultCellEditor(cellField));
		
		module.getAddMenuItem().setText("Add Payee");
		module.getRemoveMenuItem().setText("Remove Payee");
		
		module.getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}

	public void startup() {
		BindUtil.bind(payeeActions, "add", module.getAddButton(), BindUtil.ACTION);
		BindUtil.bind(payeeActions, "add", module.getAddMenuItem(), BindUtil.ACTION);
		BindUtil.bind(payeeActions, "remove", module.getRemoveButton(), BindUtil.ACTION);
		BindUtil.bind(payeeActions, "remove", module.getRemoveMenuItem(), BindUtil.ACTION);

		BindUtil.bind(payeeActions, "showPopup", module.getTable(), BindUtil.MOUSE_PRESSED);
		BindUtil.bind(payeeActions, "showPopup", module.getTable(), BindUtil.MOUSE_RELEASED);

		BindUtil.selectAllBind(cellField);

	}
}
