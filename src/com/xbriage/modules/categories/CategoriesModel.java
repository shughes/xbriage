/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.modules.categories;

import com.xbriage.services.AccountDao;
import com.xbriage.domain.Account;
import com.xbriage.services.Service;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author shughes
 */
public class CategoriesModel extends AbstractTableModel {
	private List<Account> categories = new ArrayList<Account>();
	private List<String> balances = new ArrayList<String>();
	
	public CategoriesModel() {
		
	}

	public int getRowCount() {
		return categories.size();
	}

	public int getColumnCount() {
		return 3;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		Account account = categories.get(rowIndex);
		AccountDao dao = Service.load(AccountDao.class);
		switch(columnIndex) {
			case 0:
				String name = dao.concatName(account);
				return name;
			case 1:
				return account.getTypeName();
			case 2:
				return balances.get(rowIndex);
			default:
				return "empty";
		}
	}
	
	public List<Account> getCategories() {
		return categories;
	}

	public void setCategories(List<Account> categories) {
		this.categories = categories;
//		fireTableRowsUpdated(0, categories.size());
		fireTableDataChanged();
	}

	public List<String> getBalances() {
		return balances;
	}

	public void setBalances(List<String> balances) {
		this.balances = balances;
	}

}
