/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.categories;

import com.xbriage.domain.Account;
import com.xbriage.domain.JournalEntry;
import com.xbriage.domain.JournalEntryRow;
import com.xbriage.util.GeneralUtil;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author shughes
 */
public class EntriesModel extends AbstractTableModel {

	private List<JournalEntryRow> entries = new ArrayList<JournalEntryRow>();
	private Account account;

	public int getRowCount() {
		return entries.size();
	}

	public int getColumnCount() {
		return 3;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		JournalEntryRow row = entries.get(rowIndex);
		JournalEntry entry = row.getJournalEntry();
		JournalEntryRow acctRow = null;
		
		for (JournalEntryRow row2 : entry.getRows()) {
			if(account.isUserAccount() == 1) {
				if(row2.getAccount().equals(account)) {
					acctRow = row2;
					break;
				}
			} else if (row2.getAccount().isUserAccount() == 1) {
				acctRow = row2;
				break;
			}
		}

		switch (columnIndex) {
			case 0:
				if (acctRow != null) {
					return acctRow.getAccount().getName();
				}

			case 1:
				if (acctRow != null) {
					BigDecimal amt = acctRow.getAmount();
					if (acctRow.isDebitEntry() == 0) {
						amt = amt.multiply(new BigDecimal(-1));
					}
					return GeneralUtil.formatStringNumberWithCommas(amt.toString());
				}

			case 2:
				if (row.getAccount().isUserAccount() != 1) {
					BigDecimal amt = row.getAmount();
					if (row.getAccount().getTypeName().equals("Income")) {
						if(row.isDebitEntry() == 1) {
							amt = amt.multiply(new BigDecimal(-1));
						}
					} else {
						if (row.isDebitEntry() == 0) {
							amt = amt.multiply(new BigDecimal(-1));
						}
					}
					return GeneralUtil.formatStringNumberWithCommas(amt.toString());
				}
		}
		return null;
	}

	public List<JournalEntryRow> getEntries() {
		return entries;
	}

	public void setEntries(List<JournalEntryRow> entries) {
		this.entries = entries;
		fireTableDataChanged();
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
}
