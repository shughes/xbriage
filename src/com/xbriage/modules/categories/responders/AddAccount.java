/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.categories.responders;

import com.xbriage.services.Responder;
import com.xbriage.context.ResponderContext;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;

/**
 *
 * @author shughes
 */
public class AddAccount extends AbstractAction implements Responder {

	private boolean category = false;

	public void perform(java.util.Map params) {
		/*
		Account account = new Account();
		if (isCategory()) {
			account.setUserAccount(0);
			account.setTypeName("Expense");
			account.setName("New Category");			
		} else {
			account.setUserAccount(1);
			account.setName("New Account");
			account.setTypeName("Checking");
		}
		account.setRoot(0);
		EntityManager em = EmfFactory.get().createEntityManager();
		
		AccountTreeNode node = new AccountTreeNode(account.getName());
		node.setAccountId(account.getId());
		AccountsModule module = ModuleFactory.get(ModuleFactory.ACCOUNTS_MODULE);
		
		AccountTreeNode parentNode = null;
		if(tree.getSelectionPath() == null) {
			parentNode = (AccountTreeNode) getTreeModel().getRoot();
		} else {
			parentNode = (AccountTreeNode) tree.getSelectionPath().getLastPathComponent();
		}
		
		if((parentNode.getChildCount() == 0) && parentNode.isRoot() == false) {			
			parentNode = (AccountTreeNode) parentNode.getParent();
		} 
		
		Account parent = (Account) em.createNamedQuery("Account.findById")
				.setParameter("id", parentNode.getAccountId()).getSingleResult();
		
		account.setParent(parent);
		List<Account> children = parent.getChildren();
		children.add(account);
		parent.setChildren(children);

		em.getTransaction().begin();
		em.persist(parent);
		em.persist(account);
		em.getTransaction().commit();
		
		node.setAccountId(account.getId());
		getTreeModel().insertNodeInto(node, parentNode, parentNode.getChildCount());
		*/
	}

	public String getId() {
		return ResponderContext.ADD_ACCOUNT;
	}

	public boolean isCategory() {
		return category;
	}

	public void setCategory(boolean isCategory) {
		this.category = isCategory;
	}

	public void actionPerformed(ActionEvent e) {
		perform(null);
	}

	public JTree getTree() {
		return tree;
	}

	public void setTree(JTree tree) {
		this.tree = tree;
	}
	public DefaultTreeModel getTreeModel() {
		return treeModel;
	}

	public void setTreeModel(DefaultTreeModel treeModel) {
		this.treeModel = treeModel;
	}
	
	private JTree tree;	
	private DefaultTreeModel treeModel;

}
