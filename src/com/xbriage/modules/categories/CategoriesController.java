/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.categories;

import com.xbriage.app.WorkbooksView;
import com.xbriage.services.ModuleLoader;
import com.xbriage.services.Service;
import com.xbriage.util.BindUtil;

/**
 *
 * @author shughes
 */
public class CategoriesController {

	private CategoriesModule module = Service.load(ModuleLoader.class).get("Categories.module");
	private CategoryActions categoryActions = CategoryActions.getInstance();
	private final CategoryDialog categoryDialog = new CategoryDialog(WorkbooksView.getInstance().getFrame(), true);

	public CategoriesController() {
		categoryActions.setCategoryDialog(categoryDialog);
		categoryActions.startupRefresh();
	}

	public void startup() {

		BindUtil.bind(categoryActions, "resizeTable", module.getTable(), BindUtil.COMPONENT_RESIZED);

		BindUtil.bind(categoryActions, "startupRefresh", module.getStart(), BindUtil.ACTION);
		BindUtil.bind(categoryActions, "startupRefresh", module.getEnd(), BindUtil.ACTION);

		BindUtil.bind(categoryActions, "dialogCancel", categoryDialog.getCancelButton(), BindUtil.ACTION);
		BindUtil.bind(categoryActions, "dialogOk", categoryDialog.getOkButton(), BindUtil.ACTION);
		BindUtil.bind(categoryActions, "edit", module.getEditButton(), BindUtil.ACTION);
		BindUtil.bind(categoryActions, "edit", module.getEditMenuItem(), BindUtil.ACTION);
		BindUtil.bind(categoryActions, "remove", module.getRemoveButton(), BindUtil.ACTION);
		BindUtil.bind(categoryActions, "remove", module.getRemoveMenuItem(), BindUtil.ACTION);
		BindUtil.bind(categoryActions, "add", module.getAddButton(), BindUtil.ACTION);
		BindUtil.bind(categoryActions, "add", module.getAddMenuItem(), BindUtil.ACTION);

		BindUtil.bind(categoryActions, "doubleClickEdit", module.getTable(), BindUtil.MOUSE_PRESSED);
		BindUtil.bind(categoryActions, "showPopup", module.getTable(), BindUtil.MOUSE_PRESSED);
		BindUtil.bind(categoryActions, "showPopup", module.getTable(), BindUtil.MOUSE_RELEASED);

		BindUtil.selectEmptyRow(module.getTable());
		BindUtil.selectAllBind(categoryDialog.getNameField());
	}

}
