/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.categories;

import com.xbriage.services.AccountDao;
import com.xbriage.domain.Account;
import com.xbriage.services.Service;
import java.util.Comparator;

/**
 *
 * @author shughes
 */
public class AccountComparator implements Comparator {

	public int compare(Object o1, Object o2) {
		Account a1 = (Account) o1;
		Account a2 = (Account) o2;
		AccountDao dao = Service.load(AccountDao.class);
		String n1 = dao.concatName(a1);
		String n2 = dao.concatName(a2);
		return n1.compareTo(n2);
	}
}
