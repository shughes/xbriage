/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.categories;

import com.xbriage.services.AccountDao;
import com.xbriage.domain.Account;
import com.xbriage.services.Service;
import com.xbriage.context.EmfContext;
import java.util.List;
import javax.persistence.EntityManager;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author shughes
 */
public class ParentComboBoxModel extends DefaultComboBoxModel {

	public final static String NO_PARENT = "<< No parent >>";
	private List<Account> accounts;

	public ParentComboBoxModel() {
		refresh();
	}

	public void refresh() {
		EntityManager em = EmfContext.create();
		List<Account> tmp = em.createNamedQuery("Account.findAllButRoot").getResultList();
		setAccounts(tmp);
	}

	@Override
	public int getSize() {
		return getAccounts().size() + 1;
	}

	@Override
	public Object getElementAt(int index) {
		String name = "";
		if (index == 0) {
			name = NO_PARENT;
		} else {
			Account account = getAccounts().get(index-1);
			AccountDao dao = Service.load(AccountDao.class);
			name = dao.concatName(account);
		}
		return name;
	}

	public List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
		this.fireContentsChanged(this, 0, accounts.size() - 1);
	}
}
