/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.categories;

import com.xbriage.services.AccountDao;
import com.xbriage.util.GeneralUtil;
import com.xbriage.modules.journal.*;
import com.xbriage.domain.Account;
import com.xbriage.services.Service;
import com.xbriage.context.EmfContext;
import com.xbriage.util.GeneralUtil;
import com.xbriage.util.GeneralUtil;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author shughes
 */
public class AccountsModel extends AbstractTableModel {

	List<Account> accounts;

	public AccountsModel() {
		EntityManager em = EmfContext.get().createEntityManager();
		Query q = em.createNamedQuery(Account.FIND_ALL);
		List<Account> accountList = q.getResultList();
		this.setAccounts(accountList);
	}

	public int getRowCount() {
		return accounts.size();
	}

	public int getColumnCount() {
		return 2;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		Account account = accounts.get(rowIndex);
		AccountDao dao = Service.load(AccountDao.class);
		if (columnIndex == 1) {
			BigDecimal balance = dao.getBalance(account);
			String sBalance = GeneralUtil.formatStringNumber(balance.toString());
			return sBalance;
		} else {
			return dao.concatName(account);
		}
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
		fireTableDataChanged();
	}
}
