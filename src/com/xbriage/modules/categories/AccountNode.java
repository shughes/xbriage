/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.categories;

import com.xbriage.util.IteratorEnumeration;
import com.xbriage.domain.Account;
import com.xbriage.context.EmfContext;
import com.xbriage.modules.journal.models.JournalTableModel;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.persistence.EntityManager;
import javax.swing.tree.TreeNode;
import org.jdesktop.swingx.treetable.TreeTableNode;

public class AccountNode implements TreeTableNode {

	public static final int NAME = 0;
	public static final int BALANCE = 2;
	private Account account;
	private TreeTableNode parent;
	private List<TreeTableNode> children;
	private Object userObject;

	public AccountNode(Account account, TreeTableNode parent, boolean showSelectedAccount) {
		EntityManager em = EmfContext.create();
		account = em.find(Account.class, account.getId());
		this.parent = parent;
		this.account = account;
		children = new ArrayList<TreeTableNode>();
		Account selectedAccount = JournalTableModel.getSelectedAccount();
		for (Account a : account.getChildren()) {
			if (!a.equals(selectedAccount) || showSelectedAccount) {
				children.add(new AccountNode(a, this, showSelectedAccount));
			}
		}
	}

	public AccountNode(Account account, TreeTableNode parent) {
		this(account, parent, false);
	}
		
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public TreeTableNode getChildAt(int childIndex) {
		return children.get(childIndex);
	}

	public int getChildCount() {
		return children.size();
	}

	public TreeTableNode getParent() {
		return parent;
	}

	public int getIndex(TreeNode node) {
		return children.indexOf(node);
	}

	public boolean getAllowsChildren() {
		return true;
	}

	public boolean isLeaf() {
		return (children.size() > 0) ? false : true;
	}

	public Enumeration children() {
		return new IteratorEnumeration(children.iterator());
	}

	@Override
	public String toString() {
		return account.getName();
	}

	public Object getValueAt(int arg0) {
		return children.get(arg0);
	}

	public int getColumnCount() {
		return 3;
	}

	public boolean isEditable(int arg0) {
		return true;
	}

	public void setValueAt(Object value, int col) {
		switch (col) {
			case NAME:
				EntityManager em = EmfContext.get().createEntityManager();
				account = em.find(Account.class, account.getId());
				account.setName((String) value);
				em.getTransaction().begin();
				em.persist(account);
				em.getTransaction().commit();
				em.close();
				break;
		}
	}

	public Object getUserObject() {
		return userObject;
	}

	public void setUserObject(Object arg0) {
		userObject = arg0;
	}
}
