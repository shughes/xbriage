/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.categories;

import com.xbriage.util.AccountComparator;
import com.xbriage.app.TemplateData;
import com.xbriage.app.WorkbooksApp;
import com.xbriage.app.WorkbooksView;
import com.xbriage.services.AccountDao;
import com.xbriage.domain.Account;
import com.xbriage.domain.JournalEntryRow;

import com.xbriage.context.EmfContext;
import com.xbriage.context.MapContext;
import com.xbriage.services.Service;
import com.xbriage.modules.journal.JournalCellEditor;
import com.xbriage.modules.journal.JournalModule;
import com.xbriage.modules.journal.actions.AccountActions;
import com.xbriage.services.ModuleLoader;
import com.xbriage.services.Refresh;
import com.xbriage.util.GeneralUtil;
import com.xbriage.services.Service;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JViewport;
import javax.swing.SwingWorker;

/**
 *
 * @author shughes
 */
public class CategoryActions {

	private final static CategoryActions instance = new CategoryActions();
	private final CategoriesModule module = Service.load(ModuleLoader.class).get("Categories.module");
	private CategoryDialog categoryDialog;
	private boolean needsRefresh = false;

	public static CategoryActions getInstance() {
		return instance;
	}

	protected CategoryActions() {
	}

	public void setNeedsRefresh(boolean needsRefresh) {
		this.needsRefresh = needsRefresh;
	}

	public boolean needsRefresh() {
		return needsRefresh;
	}

	public void dialogCancel() {
		AccountActions.getInstance().setEditCanceled(true);
		categoryDialog.setVisible(false);
	}

	public void resizeTable() {
		module.getTable().getColumnModel().getColumn(0).setPreferredWidth(module.getTable().getWidth() / 2);
	}

	public void dialogOk() {
		categoryDialog.setVisible(false);
		EntityManager em = EmfContext.create();
		String newName = categoryDialog.getNameField().getText();
		Account account = categoryDialog.getAccount();
		String oldName = account.getName();
		account = em.find(Account.class, account.getId());
		account.setName(newName);
		account.setTypeName((String) categoryDialog.getTypeComboBox().getSelectedItem());
		String sParent = (String) categoryDialog.getParentComboBox().getSelectedItem();
		if (sParent == null) {
			sParent = "Root";
		}
		if (sParent.equals("")) {
			sParent = "Root";
		}
		if (sParent.equals(ParentComboBoxModel.NO_PARENT)) {
			sParent = "Root";
		}

		AccountDao dao = Service.load(AccountDao.class);
		Account parent = dao.breakName(sParent);
		Account defaultA = (Account) em.createNamedQuery("Account.findDefault").getSingleResult();
		Account root = (Account) em.createNamedQuery("Account.findRoot").getSingleResult();

		if (parent == null) {
			parent = root;
		}
		if (parent.equals(defaultA)) {
			// parent cannot be 'No Category'
			GeneralUtil.showWarningAlert2(WorkbooksApp.getWarning("CategoryActions.parentNotDefault"));
			parent = root;
		}

		account.setParent(parent);
		em.getTransaction().begin();
		em.persist(account);
		em.getTransaction().commit();
		em.close();

		Service.load(Refresh.class).refresh("CategoryActions.dialogOk");

		JournalModule journalModule = Service.load(ModuleLoader.class).get("Journal.module");

		// adjust account label in journal module		
		if (journalModule.getAccountLabel().getText().equals(oldName)) {
			journalModule.getAccountLabel().setText(account.getName());
		}

		// adjust category field
		JournalCellEditor cellEditor = (JournalCellEditor) journalModule.getCellEditor();
		if (cellEditor.getCell().getCategoryField().getText().equals(oldName)) {
			cellEditor.getCell().getCategoryField().setText(account.getName());
		}
	}

	public void refreshAccounts() {
		CategoriesModel model = (CategoriesModel) module.getTable().getModel();
		EntityManager em = EmfContext.create();
		List<Account> accounts = em.createNamedQuery("Account.findAllButRoot").getResultList();
		Collections.sort(accounts, new AccountComparator());
		model.setCategories(accounts);
	}

	public void doubleClickEdit() {
		Map params = (Map) MapContext.get(CategoryActions.class.getName() + ".doubleClickEdit");
		MouseEvent evt = (MouseEvent) params.get(MouseEvent.class);
		int count = evt.getClickCount();
		if (count == 2) {
			edit();
		}
	}

	public void edit() {
		int index = module.getTable().getSelectedRow();
		CategoriesModel model = (CategoriesModel) module.getTable().getModel();
		if (index != -1) {
			Account account = model.getCategories().get(index);
			edit(account);
		}
	}

	public void edit(Account account) {
		EntityManager em = EmfContext.create();
		Account acct = (Account) em.createNamedQuery("Account.findDefault").getSingleResult();
		if (account.equals(acct)) {
			GeneralUtil.showWarningAlert2(WorkbooksApp.getWarning("CategoryActions.noEditDefault"));
		} else {
			Account parent = account.getParent();
			AccountDao dao = Service.load(AccountDao.class);
			String sParent = dao.concatName(parent);
			if (sParent.equals("Root")) {
				sParent = ParentComboBoxModel.NO_PARENT;
			}

			if (account.isUserAccount().equals(1)) {
				categoryDialog.getParentComboBox().setEnabled(false);
				String[] acctTypes = GeneralUtil.getAccountTypes();
				List<String> newTypes = new ArrayList<String>();
				for (String type : acctTypes) {
					if (!type.equals("Expense") && !type.equals("Income")) {
						newTypes.add(type);
					}
				}
				categoryDialog.getTypeComboBox().setModel(new DefaultComboBoxModel(newTypes.toArray()));
			} else {
				categoryDialog.getParentComboBox().setEnabled(true);
				categoryDialog.getTypeComboBox().setModel(new DefaultComboBoxModel(new String[]{"Income", "Expense"}));
			}

			categoryDialog.getParentComboBox().setSelectedItem(sParent);

			refreshParentComboBox(account);

			categoryDialog.setAccount(account);
			categoryDialog.getNameField().setText(account.getName());
			categoryDialog.getTypeComboBox().setSelectedItem(account.getTypeName());

			Rectangle mainBounds = WorkbooksView.getInstance().getFrame().getBounds();
			// set dialog's bounds.
			int height = mainBounds.height;
			int width = mainBounds.width;
			int x = mainBounds.x + (width / 2) - categoryDialog.getWidth();
			int y = mainBounds.y + (height / 2) - categoryDialog.getHeight();
			categoryDialog.setBounds(x, y, categoryDialog.getWidth(), categoryDialog.getHeight());

			// show dialog
			categoryDialog.getNameField().requestFocusInWindow();
			categoryDialog.getRootPane().setDefaultButton(categoryDialog.getOkButton());
			categoryDialog.setVisible(true);
		}
	}

	public void remove() {
		int index = module.getTable().getSelectedRow();
		CategoriesModel model = (CategoriesModel) module.getTable().getModel();
		if (index != -1) {
			Account account = model.getCategories().get(index);
			warnAndRemove(account);
		}
	}

	private void warnAndRemove(Account account) {
		boolean yes = GeneralUtil.showWarningAlert(WorkbooksApp.getWarning("CategoryActions.confirmDelete"));
		if (yes) {
			removeCategory(account);
		}
	}

	public void showPopup() {
		Map params = (Map) MapContext.get(getClass().getName() + ".showPopup");
		MouseEvent evt = (MouseEvent) params.get(MouseEvent.class);
		int row = module.getTable().rowAtPoint(evt.getPoint());
		if (evt.isPopupTrigger()) {
			module.getTable().getSelectionModel().setSelectionInterval(row, row);
			module.getPopupMenu().show(evt.getComponent(), evt.getX(), evt.getY());
		}
	}

	public void removeCategory(Account category) {
		List<Account> accounts = category.getChildren();
		accounts.add(category);
		EntityManager em = EmfContext.create();
		for (Account account : accounts) {
			if (!account.isUserAccount().equals(1)) {
				Account defaultAccount = (Account) em.createNamedQuery("Account.findDefault").getSingleResult();
				if (account.equals(defaultAccount)) {
					GeneralUtil.showWarningAlert2(WorkbooksApp.getWarning("CategoryActions.noDeleteDefault"));
				} else {
					em.getTransaction().begin();

					Query q = em.createQuery("select row from JournalEntryRow row where row.account = :account");
					q.setParameter("account", account);
					List<JournalEntryRow> rows = q.getResultList();
					for (JournalEntryRow row : rows) {
						row.setAccount(defaultAccount);
						em.persist(row);
					}

					em.getTransaction().commit();
					em.getTransaction().begin();
					account = em.find(Account.class, account.getId());
					em.remove(account);
					em.getTransaction().commit();
				}
			} else {
				GeneralUtil.showWarningAlert2(WorkbooksApp.getWarning("CategoryActions.clarifyDelete"));
			}
		}
		em.close();

		Service.load(Refresh.class).refresh("CategoryActions.removeCategory");
	}

	public void add() {
		EntityManager em = EmfContext.create();
		Account parent = (Account) em.createNamedQuery("Account.findRoot").getSingleResult();
		Account account = createCategory("New Category", "Expense", em, parent);
		refreshAfterAdd(account);
		edit(account); // refreshed in dialogOk()
	}

	private Account createCategory(String name, String type, EntityManager em, Account parent) {
		Account account = new Account(name, type, 0, 0, parent);
		em.getTransaction().begin();
		em.persist(account);
		em.getTransaction().commit();
		return account;
	}

	public void refreshIfNeeded() {
		if (TemplateData.getInstance().isDisplayed(module)) {
			if (needsRefresh) {
				System.out.println("CategoryActions.refresh");
				startupRefresh();
				needsRefresh = false;
			}
		}
	}
	
	private void refreshAfterAdd(Account account) {
		new RefreshBalance(account).execute();
	}

	public void refresh() {
		needsRefresh = true;
		refreshIfNeeded();
	}

	public void startupRefresh() {
		new RefreshBalance().execute();
	}

	private Date[] getDates() {
		String sStart = (String) module.getStart().getSelectedItem();
		String sEnd = (String) module.getEnd().getSelectedItem();
		Date start = GeneralUtil.stringToDate(sStart);
		Date end = GeneralUtil.stringToDate(sEnd);
		return new Date[]{start, end};
	}

	private class Node {
		public int row = -1;
		public List<Account> accounts;
		public List<String> balances;
	}

	private class RefreshBalance extends SwingWorker<Node, Object> {

		private CategoriesModel model = null;
		private Account selectedAccount = null;
		
		public RefreshBalance() {
			
		}
		
		public RefreshBalance(Account account) {
			selectedAccount = account;
		}

		@Override
		protected Node doInBackground() throws Exception {
			EntityManager em = EmfContext.create();
			List<Account> accounts = em.createNamedQuery("Account.findAllButRoot").getResultList();
			Collections.sort(accounts, new AccountComparator());
			if (model == null) {
				model = (CategoriesModel) module.getTable().getModel();
			}

			List<String> balances = new ArrayList<String>();
			AccountDao dao = Service.load(AccountDao.class);
			
			int i = 0;
			int row = -1;
			
			for (Account account : accounts) {
				boolean debitAdds = true;

				if (account.getTypeName().equals("Income")) {
					debitAdds = false;
				}
				
				if(selectedAccount != null) {
					if(account.equals(selectedAccount)) {
						row = i;
					}
				}

				Date[] dates = getDates();
				BigDecimal balance = dao.getBalance(account, dates[0], dates[1], debitAdds);
				String sBal = formatBalanceString(balance);
				balances.add(sBal);
				
				i++;
			}

			Node node = new Node();
			node.balances = balances;
			node.accounts = accounts;
			node.row = row;

			return node;
		}

		@Override
		protected void done() {
			try {
				Node node = get();
				model.setBalances(node.balances);
				model.setCategories(node.accounts);
				
				if(node.row != -1) {
					module.getTable().getSelectionModel().setSelectionInterval(node.row, node.row);
					Rectangle bounds = module.getTable().getCellRect(node.row, 0, true);
					JViewport sp = (JViewport) module.getTable().getParent();
					sp.setViewPosition(bounds.getLocation());
				}

			} catch (InterruptedException ex) {
				Logger.getLogger(CategoryActions.class.getName()).log(Level.SEVERE, null, ex);
			} catch (ExecutionException ex) {
				Logger.getLogger(CategoryActions.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	private String formatBalanceString(BigDecimal balance) {
		String sBal = GeneralUtil.formatStringNumberWithCommas(balance.toString());
		int compareTo = balance.compareTo(new BigDecimal(0));
		if (compareTo == -1) {
			sBal = GeneralUtil.formatRedString(sBal);
		} else if (compareTo == 1) {
			sBal = GeneralUtil.formatGreenString(sBal);
		} else {
//			sBal = GeneralUtil.formatGrayString(sBal);
		}
		return sBal;
	}

	public void refreshParentComboBox(Account account) {
		EntityManager em = EmfContext.create();
		List<Account> accounts = em.createNamedQuery("Account.findAllButRootAndAccount").setParameter("account", account).getResultList();
		Collections.sort(accounts, new AccountComparator());
		ParentComboBoxModel model = (ParentComboBoxModel) categoryDialog.getParentComboBox().getModel();
		model.setAccounts(accounts);
	}

	public void refreshCategory(final Account cat) {
		needsRefresh = true;
		if (TemplateData.getInstance().isDisplayed(module)) {
			System.out.println("refreshing: " + cat.getName());
			SwingWorker<String, Object> worker = new SwingWorker<String, Object>() {

				private CategoriesModel model = null;
				private int row = -1;

				@Override
				protected String doInBackground() throws Exception {
					if (model == null) {
						model = (CategoriesModel) module.getTable().getModel();
					}
					int i = 0;
					for (Account acct : model.getCategories()) {
						if (acct.equals(cat)) {
							row = i;
							break;
						}
						i++;
					}

					AccountDao dao = Service.load(AccountDao.class);

					boolean debitAdds = true;
					if (cat.getTypeName().equals("Income")) {
						debitAdds = false;
					}

					Date[] dates = getDates();

					BigDecimal bal = dao.getBalance(cat, dates[0], dates[1], debitAdds);
					String sBal = formatBalanceString(bal);
					return sBal;
				}

				@Override
				protected void done() {
					try {
						model.getBalances().set(row, get());
						model.fireTableRowsUpdated(row, row);
					} catch (InterruptedException ex) {
						Logger.getLogger(CategoryActions.class.getName()).log(Level.SEVERE, null, ex);
					} catch (ExecutionException ex) {
						Logger.getLogger(CategoryActions.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			};
			worker.execute();
			needsRefresh = false;
		}
	}

	public CategoryDialog getCategoryDialog() {
		return categoryDialog;
	}

	public void setCategoryDialog(CategoryDialog categoryDialog) {
		this.categoryDialog = categoryDialog;
	}
}
