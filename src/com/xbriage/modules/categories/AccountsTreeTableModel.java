/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.categories;

import com.xbriage.app.UndoActions;
import com.xbriage.services.AccountDao;
import com.xbriage.util.GeneralUtil;
import com.xbriage.domain.Account;
import com.xbriage.services.Service;
import com.xbriage.context.EmfContext;
import com.xbriage.util.GeneralUtil;
import javax.persistence.EntityManager;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;

/**
 *
 * @author shughes
 */
public class AccountsTreeTableModel extends DefaultTreeTableModel {

	private AccountNode rootNode;
	private Account account;

	public AccountsTreeTableModel(Account account) {
		rootNode = new AccountNode(account, null, true);
		this.account = account;
		this.setRoot(rootNode);
	}

	public Account getAccount() {
		return account;
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public String getColumnName(int column) {
		switch (column) {
			case 0:
				return "Name";
			case 1:
				return "Type";
			default:
				return "Balance";
		}
	}

	@Override
	public Object getValueAt(Object aObject, int col) {
		AccountNode node = (AccountNode) aObject;
		AccountDao dao = Service.load(AccountDao.class);
		Account acct = node.getAccount();
		EntityManager em = EmfContext.create();
		acct = em.find(Account.class, acct.getId());
		switch (col) {
			case 1:
				return acct.getTypeName();
			case 2:
				String sBalance = dao.getBalance(node.getAccount()).toString();
				return GeneralUtil.formatStringNumber(sBalance);
			default:				
				return acct.getName();
		}
	}

	@Override
	public void setValueAt(Object value, Object n, int column) {
		AccountNode node = (AccountNode) n;
		EntityManager em = EmfContext.create();
		Account acct = node.getAccount();
		acct = em.find(Account.class, acct.getId());
		AccountDao dao = Service.load(AccountDao.class);
		String message = "Cannot edit default category";
		Account defaultAccount = (Account) em.createNamedQuery("Account.findDefault").getSingleResult();
		switch (column) {
			case 0:
				if (!acct.equals(defaultAccount)) {
					if (acct.isUserAccount().equals(1)) {
						GeneralUtil.showWarningAlert2("Cannot edit a user account here. Edit user accounts in Journal workbooks.");
					} else {
						if (value != null) {
							acct.setName(value.toString());
						}
					}
				} else {
					GeneralUtil.showWarningAlert2(message);
				}
				break;
			case 1:
				if (!acct.equals(defaultAccount)) {
					if (acct.isUserAccount().equals(1)) {
						GeneralUtil.showWarningAlert2("Cannot edit a user account here. Edit user accounts in Journal workbook.");
					} else {
						if (value != null) {
							acct.setTypeName(value.toString());
						}
					}
				} else {
					GeneralUtil.showWarningAlert2(message);
				}
				break;
		}
		em.getTransaction().begin();
		em.persist(acct);
		em.getTransaction().commit();
		em.close();
		node.setAccount(acct);
	}
}
