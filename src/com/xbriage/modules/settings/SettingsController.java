/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.settings;

import com.xbriage.services.Service;
import com.xbriage.context.SetupContext;
import com.xbriage.context.TemplateContext;
import com.xbriage.services.Module;
import com.xbriage.menus.MainMenu;
import com.xbriage.services.ModuleLoader;
import com.xbriage.templates.TemplateThree;
import com.xbriage.util.BindUtil;
import com.xbriage.util.GeneralUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author shughes
 */
public class SettingsController {

	private SettingsModule module = Service.load(ModuleLoader.class).get("Settings.module");
	private SettingsActions actions = SettingsActions.getInstance();
	private Map<String, String> settings = SetupContext.getInstance().getSettings();

	public SettingsController() {
		// uses single panel template (TemplateThree) for default workbook.
//		TemplateThree template = (TemplateThree) TemplateContext.get("templateThree");
//		MainMenu mainMenu = MainMenu.getInstance(); 
//		mainMenu.setTemplate(template);

		Map<String, Module> modules = Service.load(ModuleLoader.class).getMap();
		List<String> labels = new ArrayList<String>();
		for (String key : modules.keySet()) {
			Module m = modules.get(key);
			labels.add(m.getLabel());

			if (m.getLabel().equals(settings.get("Settings.defaultWorkbook"))) {
				Service.load(ModuleLoader.class).showModule(m);
			}
		}
		Collections.sort(labels);
		module.getDefaultWorkbook().setModel(new DefaultComboBoxModel(labels.toArray()));
		module.getDefaultWorkbook().setSelectedItem(settings.get("Settings.defaultWorkbook"));

		actions.refreshUserAccounts();
		
		module.getLogFileLabel().setText(GeneralUtil.getLogFile());
	}

	
	public void startup() {
//		BindUtil.bind(actions, "saveChanges", module.getSaveButton(), BindUtil.ACTION);
//		BindUtil.bind(actions, "resetChanges", module.getResetButton(), BindUtil.ACTION);

		BindUtil.bind(actions, "setDefaultWorkbook", module.getDefaultWorkbook(), BindUtil.ACTION);
		BindUtil.bind(actions, "setArchivesFolder", module.getArchivesField(), BindUtil.KEY_RELEASE);

		BindUtil.bind(actions, "setBankLogin", module.getBankLoginField(), BindUtil.KEY_RELEASE);
		BindUtil.bind(actions, "setBankPassword", module.getBankPasswordField(), BindUtil.KEY_RELEASE);
		BindUtil.bind(actions, "setBank", module.getBankComboBox(), BindUtil.ACTION);
//		BindUtil.bind(actions, "setRoutingNumber", module.getRoutingNumberField(), BindUtil.KEY_RELEASE);
		BindUtil.bind(actions, "searchAccounts", module.getSearchButton(), BindUtil.ACTION);

		BindUtil.bind(actions, "browse", module.getBrowseButton(), BindUtil.ACTION);

//		BindUtil.bind(actions, "addBankAccount", module.getAddButton(), BindUtil.ACTION);
		BindUtil.bind(actions, "removeBankAccount", module.getRemoveButton(), BindUtil.ACTION);
		BindUtil.bind(actions, "refreshOfx", module.getBankAccounts().getModel(), BindUtil.TABLE_MODEL_CHANGED);

//		BindUtil.selectAllBind(module.getRoutingNumberField());
		BindUtil.selectAllBind(module.getArchivesField());
		BindUtil.selectAllBind(module.getBankLoginField());
		BindUtil.selectAllBind(module.getBankPasswordField());

	}
}
