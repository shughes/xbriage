/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.settings;

import com.xbriage.context.EmfContext;
import com.xbriage.domain.Account;
import com.xbriage.domain.BankAccount;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author shughes
 */
public class BankAccountsModel extends AbstractTableModel {

	private List<BankAccount> bankAccounts;

	public int getRowCount() {
		return bankAccounts.size();
	}

	public int getColumnCount() {
		return 3;
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		switch(col) {
			case 2:
				return true;
		}
		return false;
	}

	@Override
	public void setValueAt(Object value, int row, int col) {
		EntityManager em = EmfContext.create();
		BankAccount account;

		if (row >= bankAccounts.size()) {
			account = new BankAccount("", "", null);
		} else {
			account = bankAccounts.get(row);
			account = em.find(BankAccount.class, account.getId());
		}

		switch (col) {
			case 0:
				account.setBankId(value.toString());
				break;
			case 1:
				account.setType(value.toString());
				break;
			case 2:
				Account acct = null;
				if (value != null) {
					if (!value.equals("")) {
						Query q = em.createNamedQuery("Account.findUserAccountByName").setParameter("name", value.toString());
						List<Account> result = q.getResultList();
						if (result.size() > 0) {
							acct = result.get(0);
						}
					}
					account.setUserAccount(acct);
				}
				break;
		}

		em.persist(account);

		em.getTransaction().begin();
		em.getTransaction().commit();

		if (row >= bankAccounts.size()) {
			bankAccounts.add(account);
		}

		em.close();
		fireTableRowsUpdated(row, row);
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		if (rowIndex < bankAccounts.size()) {
			BankAccount bankAccount = bankAccounts.get(rowIndex);
			EntityManager em = EmfContext.create();
			bankAccount = em.find(BankAccount.class, bankAccount.getId());

			switch (columnIndex) {
				case 0:
					return bankAccount.getBankId();

				case 1:
					return bankAccount.getType();

				case 2:
					Account acct = bankAccount.getUserAccount();
					String name = "";
					if (acct != null) {
						name = acct.getName();
					}
					return name;
			}
		}

		return "";
	}

	public List<BankAccount> getBankAccounts() {
		return bankAccounts;
	}

	public void refresh() {
		EntityManager em = EmfContext.create();
		bankAccounts = em.createNamedQuery("BankAccount.findAll").getResultList();
		fireTableDataChanged();
	}
}
