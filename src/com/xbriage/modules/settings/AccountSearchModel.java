/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.settings;

import com.xbriage.util.OfxAccountInfo;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author shughes
 */
public class AccountSearchModel extends AbstractTableModel {

	private List<OfxAccountInfo> rowData = new ArrayList<OfxAccountInfo>();

	public int getRowCount() {
		return rowData.size();
	}

	public int getColumnCount() {
		return 3;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		OfxAccountInfo acctInfo = rowData.get(rowIndex);
		
		switch(columnIndex) {
			case 0:
				if(acctInfo != null) {
					return acctInfo.getName();
				}
				break;
				
			case 1:
				if(acctInfo != null) {
					return acctInfo.getAccountId();
				}
				break;
				
			case 2:
				if(acctInfo != null) {
					return acctInfo.getAccountType();
				}
		}
		return "";
	}

	public List<OfxAccountInfo> getRowData() {
		return rowData;
	}

	public void setRowData(List<OfxAccountInfo> rowData) {
		this.rowData = rowData;
		fireTableDataChanged();
	}
}
