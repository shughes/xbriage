/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.settings;

import com.xbriage.app.WorkbooksView;
import com.xbriage.context.EmfContext;
import com.xbriage.context.MapContext;
import com.xbriage.services.Service;
import com.xbriage.context.SetupContext;
import com.xbriage.domain.Account;
import com.xbriage.domain.BankAccount;
import com.xbriage.modules.ofx.OfxActions;
import com.xbriage.modules.settings.AccountSearchDialog;
import com.xbriage.services.ModuleLoader;
import java.awt.event.FocusEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import com.xbriage.util.DesEncrypter;

/**
 *
 * @author shughes
 */
public class SettingsActions {

	private final static SettingsActions instance = new SettingsActions();
	private SettingsModule module = Service.load(ModuleLoader.class).get("Settings.module");
	private Map<String, String> settings = SetupContext.getInstance().getSettings(); //DaoFactory.getMapDao();
	private Map<String, String> changes = new HashMap<String, String>();
	private DesEncrypter encrypter = DesEncrypter.getInstance();

	public static SettingsActions getInstance() {
		return instance;
	}

	public void searchAccounts() {
		AccountSearchDialog dialog = new AccountSearchDialog(WorkbooksView.getInstance().getFrame(), true);
		dialog.setVisible(true);
	}

	public void setArchivesFolder() {
		saveChanges("Archives.folder", module.getArchivesField().getText());
	}

	public void setBankLogin() {
		saveChanges("Bank.login", module.getBankLoginField().getText());
	}

	public void setBankPassword() {
		String pass = module.getBankPasswordField().getText();
		String encrypted = encrypter.encrypt(pass);
		saveChanges("Bank.password", encrypted);
	}

	public void setBank() {
		if (module.getBankComboBox().getSelectedItem() != null) {
			String key = "Bank.organization";
			String value = module.getBankComboBox().getSelectedItem().toString();
			saveChanges(key, value);
		}
	}

	public void saveChanges(String key, String value) {
		settings.put(key, value);
	}

	public void refreshUserAccounts() {
		JComboBox comboBox = new JComboBox();
		EntityManager em = EmfContext.create();
		List<Account> accounts = em.createNamedQuery("Account.findUserAccounts").getResultList();
		List<String> sAccounts = new ArrayList<String>();
		for (Account account : accounts) {
			sAccounts.add(account.getName());
		}
		comboBox.setModel(new DefaultComboBoxModel(sAccounts.toArray()));
		module.getBankAccounts().getColumnModel().getColumn(2).setCellEditor(new DefaultCellEditor(comboBox));

		BankAccountsModel model = (BankAccountsModel) module.getBankAccounts().getModel();
		model.refresh();
	}

	public void browse() {
		String folder = settings.get("Archives.folder");
		File file = new File(folder);
		JFileChooser chooser = new JFileChooser(file);
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		int returnValue = chooser.showOpenDialog(module.getComponent());
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			String path = chooser.getSelectedFile().getAbsolutePath();
			module.getArchivesField().setText(path);
			setArchivesFolder();
		}
	}

	public void focusGained() {
		Map params = (Map) MapContext.get(SettingsActions.class.getName() + ".focusGained");
		FocusEvent event = (FocusEvent) params.get(FocusEvent.class);
		JTextField field = (JTextField) event.getSource();
		field.selectAll();
	}

	public void setDefaultWorkbook() {
		JComboBox comboBox = module.getDefaultWorkbook();
		String text = comboBox.getSelectedItem().toString();
		String key = "Settings.defaultWorkbook";
		saveChanges(key, text);
	}

	public void addBankAccount() {
		EntityManager em = EmfContext.create();
		BankAccount account = new BankAccount("", "", null);
		em.persist(account);
		em.getTransaction().begin();
		em.getTransaction().commit();
		em.close();
		refreshUserAccounts();
	}

	public void refreshBank() {
		if (settings.get("Bank.organization") != null) {
			module.getBankComboBox().setSelectedItem(settings.get("Bank.organization"));
		}

		if (settings.get("Bank.login") != null) {
			module.getBankLoginField().setText(settings.get("Bank.login"));
		}

		if (settings.get("Bank.password") != null) {
			String pass = DesEncrypter.getInstance().decrypt(settings.get("Bank.password"));
			module.getBankPasswordField().setText(pass);
		}
	}

	public void removeBankAccount() {
		EntityManager em = EmfContext.create();
		BankAccountsModel model = (BankAccountsModel) module.getBankAccounts().getModel();
		int row = module.getBankAccounts().getSelectedRow();
		BankAccount account = null;
		if (row != -1) {
			account = model.getBankAccounts().get(row);
			account = em.find(BankAccount.class, account.getId());
		}
		if (account != null) {
			em.remove(account);
		}
		em.getTransaction().begin();
		em.getTransaction().commit();
		em.close();
		refreshUserAccounts();
		refreshOfx();
	}

	public void refreshOfx() {
		OfxActions.getInstance().refreshAccounts();
		OfxActions.getInstance().refreshNodes();
	}
}
