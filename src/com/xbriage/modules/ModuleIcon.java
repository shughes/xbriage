/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules;

import com.xbriage.services.Module;
import java.awt.Insets;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.Serializable;
import javax.activation.DataHandler;
import javax.swing.JButton;

/**
 *
 * @author shughes
 */
public class ModuleIcon extends JButton implements DragGestureListener, DragSourceListener, Serializable, MouseListener {

	public static String MIME_TYPE_ONE = "application/x-java-serialized-object; class="+Module.class.getName();
	
	private DragSource dragSource = DragSource.getDefaultDragSource();
	private Module module;

	public ModuleIcon(Module module) {
		this.module = module;
		dragSource.createDefaultDragGestureRecognizer(this, TOP, this);
		
		setBorderPainted(false);
		setMargin(new Insets(0, 0, 0, 0));
		setFocusPainted(false);
		setContentAreaFilled(false);
		
		addMouseListener(this);
	}

	public void dragGestureRecognized(DragGestureEvent arg0) {
		//Module mod = Service.load(ModuleLoader.class).get(this.getActionCommand());
		dragSource.startDrag(arg0, null, new DataHandler(module, ModuleIcon.MIME_TYPE_ONE), this);
	}

	public void dragEnter(DragSourceDragEvent arg0) {
	}

	public void dragOver(DragSourceDragEvent arg0) {
	}

	public void dropActionChanged(DragSourceDragEvent arg0) {
	}

	public void dragExit(DragSourceEvent arg0) {
	}

	public void dragDropEnd(DragSourceDropEvent arg0) {
		setFocusPainted(false);
		setContentAreaFilled(false);
	}

	public void mouseClicked(MouseEvent e) {

	}

	public void mousePressed(MouseEvent e) {

	}

	public void mouseReleased(MouseEvent e) {

	}

	public void mouseEntered(MouseEvent e) {
		setFocusPainted(true);
		setContentAreaFilled(true);
	}

	public void mouseExited(MouseEvent e) {
		setFocusPainted(false);
		setContentAreaFilled(false);
	}
}
