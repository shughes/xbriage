/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.ofx;

import com.xbriage.app.TemplateData;
import com.xbriage.app.WorkbooksApp;

import com.xbriage.context.EmfContext;
import com.xbriage.context.MapContext;
import com.xbriage.services.Service;
import com.xbriage.context.SetupContext;
import com.xbriage.services.AccountDao;
import com.xbriage.domain.Account;
import com.xbriage.domain.BankAccount;
import com.xbriage.domain.BankAccount;
import com.xbriage.domain.JournalEntry;
import com.xbriage.domain.JournalEntryRow;
import com.xbriage.domain.Payee;
import com.xbriage.modules.journal.proxy.ProxyContext;
import com.xbriage.services.ModuleLoader;
import com.xbriage.services.OfxData;
import com.xbriage.services.Refresh;
import com.xbriage.util.GeneralUtil;
import com.xbriage.util.Ofx2Xml;
import com.xbriage.util.OfxClient;
import com.xbriage.util.OfxParser;
import com.xbriage.util.OfxTrnNode;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.SwingUtilities;
import org.jdesktop.application.LocalStorage;
import com.xbriage.util.DesEncrypter;
import com.xbriage.services.Service;
import java.awt.event.MouseEvent;
import javax.swing.JFileChooser;

/**
 *
 * @author shughes
 */
public class OfxActions {

	private static final OfxActions instance = new OfxActions();
	private OfxModule module = Service.load(ModuleLoader.class).get("Ofx.module");
	private Map<String, String> settings = SetupContext.getInstance().getSettings();
	private boolean success = true;

	public static OfxActions getInstance() {
		return instance;
	}

	public void showPopup() {
		Map params = (Map) MapContext.get(getClass().getName() + ".showPopup");
		MouseEvent evt = (MouseEvent) params.get(MouseEvent.class);
		int row = module.getBufferTable().rowAtPoint(evt.getPoint());
		if (evt.isPopupTrigger()) {
			module.getBufferTable().getSelectionModel().setSelectionInterval(row, row);
			module.getPopupMenu().show(evt.getComponent(), evt.getX(), evt.getY());
		}
	}

	public void remove() {
		int row = module.getBufferTable().getSelectedRow();
		if (row != -1) {
			remove(row);
		}
	}

	private void remove(int row) {
		OfxBufferModel model = (OfxBufferModel) module.getBufferTable().getModel();
		OfxTrnNode node = model.getNodes().get(row);
//		OfxContext.getTransactions().remove(node);
		Service.load(OfxData.class).getTransactions().remove(node);
		model.refreshNodes();
		if (model.getRowCount() > row) {
			GeneralUtil.moveRow(module.getBufferTable(), row);
		} else if (model.getRowCount() > 0) {
			GeneralUtil.moveRow(module.getBufferTable(), 0);
		}
	}

	public void confirmAll() {
		boolean yes = GeneralUtil.showWarningAlert(WorkbooksApp.getWarning("OfxActions.confirmAll"));
		if (yes) {
			EntityManager em = EmfContext.create();
//			List<OfxTrnNode> trns = OfxContext.getTransactions();
			List<OfxTrnNode> trns = Service.load(OfxData.class).getTransactions();
			List<JournalEntry> entries = new ArrayList<JournalEntry>();

			for (int i = 0; i < trns.size(); i++) {
				JournalEntry entry = confirm(em, i);
				entries.add(entry);
			}

			em.getTransaction().begin();
			em.getTransaction().commit();

			for (JournalEntry entry : entries) {
				entry = em.find(JournalEntry.class, entry.getId());
				ProxyContext.getInstance().addEntry(entry);
			}

			trns.clear();
			OfxBufferModel model = (OfxBufferModel) module.getBufferTable().getModel();
			model.refreshNodes();

			afterConfirm();
			em.close();

			GeneralUtil.showWarningAlert2(WorkbooksApp.getWarning("OfxActions.finishedImport"));
		}
	}

	public void confirm() {
		System.out.println("confirm");
		int row = module.getBufferTable().getSelectedRow();
		EntityManager em = EmfContext.create();
		JournalEntry entry = confirm(em, row);
		if (row != -1) {
			remove();
		}

		if (entry != null) {
			em.getTransaction().begin();
			em.getTransaction().commit();

			entry = em.find(JournalEntry.class, entry.getId());
			ProxyContext.getInstance().addEntry(entry);

			afterConfirm();
			em.close();
		}
	}

	private void afterConfirm() {
		Service.load(Refresh.class).refresh("OfxActions.afterConfirm");
	}

	private JournalEntry confirm(EntityManager em, int row) {
		JournalEntry entry = null;
		if (row != -1) {
			OfxBufferModel model = (OfxBufferModel) module.getBufferTable().getModel();
			OfxTrnNode node = model.getNodes().get(row);

			Account account = null;
			if (node.getAccountId() != null) {
				account = em.find(Account.class, node.getAccountId());
			}

			String fitId = node.getFitId();
			Query q = em.createQuery("select j from JournalEntry j where j.fitId = :fitId").setParameter("fitId", fitId);
			List<JournalEntry> result1 = q.getResultList();
			// if no duplicates exist first
			if (result1.size() == 0) {

				if (account != null) {
					entry = new JournalEntry();
					JournalEntryRow row1 = new JournalEntryRow();
					JournalEntryRow row2 = new JournalEntryRow();
					List<JournalEntryRow> rows = new ArrayList<JournalEntryRow>();
					rows.add(row1);
					rows.add(row2);
					entry.setRows(rows);

					// date
					Date date = GeneralUtil.ofxDatetoDate(node.getDate());
					entry.setDate(date);

					// check
					if (node.getCheckNo() != null) {
						entry.setCheckNo(new Integer(node.getCheckNo()));
					}

					// payee
					String sPayee = node.getName();
					Payee payee = null;
					if (sPayee != null) {
						List<Payee> result = em.createNamedQuery("Payee.findByName").setParameter("name", sPayee).getResultList();
						System.out.println("result: "+result);
						if (result.size() == 0) {
							EntityManager em2 = EmfContext.create();
							em2.getTransaction().begin();
							payee = new Payee(sPayee, false);
							em2.persist(payee);
							em2.getTransaction().commit();
						} else {
							payee = result.get(0);
						}
					} else {
						payee = (Payee) em.createNamedQuery("Payee.findDefault").getSingleResult();
					}
					entry.setPayee(payee);

					// rows
					AccountDao dao = Service.load(AccountDao.class);
					Account otherAccount = dao.breakName(node.getCategory());
					if (otherAccount.equals(account)) {
						GeneralUtil.showWarningAlert2(WorkbooksApp.getWarning("OfxActions.cannotBeSameAccount"));
						otherAccount = null;
					}
					if (otherAccount == null) {
						otherAccount = (Account) em.createNamedQuery("Account.findDefault").getSingleResult();
					}
					row2.setAccount(otherAccount);
					row1.setAccount(account);
					BigDecimal amt = new BigDecimal(node.getAmount());
					int row1IsDebit = 0;
					if (amt.compareTo(new BigDecimal(0)) == 1) {
						row1IsDebit = 1;
					}
					amt = amt.abs();
					int row2IsDebit = (row1IsDebit == 0) ? 1 : 0;
					row2.setDebitEntry(row2IsDebit);
					row2.setAmount(amt);
					row2.setJournalEntry(entry);
					row1.setDebitEntry(row1IsDebit);
					row1.setAmount(amt);
					row1.setJournalEntry(entry);

					// memo
					String memo = node.getMemo();
					entry.setMemo(memo);

					// type
					int type = GeneralUtil.getType(node.getType());
					entry.setType(type);
					entry.setFitId(node.getFitId());

					em.persist(entry);
				}
			} else {
				System.out.println("duplicate exists");
			}
		}
		return entry;
	}

	private Account autoSelectAccount(Account selectedAccount, Payee payee) {
		EntityManager em = EmfContext.create();
		Query q = em.createQuery("select row from JournalEntryRow row where row.account=:account and row.journalEntry.payee=:payee order by row.journalEntry.date");
		q.setParameter("payee", payee);
		q.setParameter("account", selectedAccount);
		List<JournalEntryRow> rows = q.getResultList();

		Account account = null;

		if (rows.size() > 0) {
			for (JournalEntryRow row : rows) {
				JournalEntry entry = row.getJournalEntry();
				if (entry.getRows().size() == 2) {
					for (JournalEntryRow row2 : entry.getRows()) {
						if (!row2.getAccount().equals(selectedAccount)) {
							account = row2.getAccount();
						}
					}
				}
			}
		}

		return account;
	}
	
	private List<String> getAllConcattedAccountNames() {
		EntityManager em = EmfContext.create();
		Query q = em.createQuery("select a from Account a where a.root != 1");
		List<Account> accounts = q.getResultList();
		List<String> names = new ArrayList<String>();
		for (Account account : accounts) {
			AccountDao dao = Service.load(AccountDao.class);
			String concacted = dao.concatName(account);
			names.add(concacted);
		}
		Collections.sort(names);
		return names;
	}

	public void startupRefreshCategoriesComboBox() {
		List<String> names = getAllConcattedAccountNames();
		JComboBox comboBox = new JComboBox(new DefaultComboBoxModel(names.toArray()));
		module.getBufferTable().getColumnModel().getColumn(5).setCellEditor(new DefaultCellEditor(comboBox));

		EntityManager em = EmfContext.create();
		List<Account> accounts = em.createNamedQuery("Account.findUserAccounts").getResultList();
		List<String> acctNames = new ArrayList<String>();
		for (Account account : accounts) {
			acctNames.add(account.getName());
		}
		Collections.sort(acctNames);
		module.getAccount2ComboBox().setModel(new DefaultComboBoxModel(acctNames.toArray()));

	}

	public void refreshCategoriesComboBox() {
		if (TemplateData.getInstance().isDisplayed(module)) {
			System.out.println("OfxAction.refreshCategoriesComboBox");
			startupRefreshCategoriesComboBox();
		}
	}

	public void refreshAccounts() {
		EntityManager em = EmfContext.create();
		List<BankAccount> accounts = em.createNamedQuery("BankAccount.findAll").getResultList();
		List<String> keys = new ArrayList<String>();
		for (BankAccount account : accounts) {
			String key = account.getType() + ":" + account.getBankId();
			keys.add(key);
		}
		JComboBox comboBox = module.getAccountComboBox();
		if (keys.size() > 0) {
			comboBox.setModel(new DefaultComboBoxModel(keys.toArray()));
			module.getImportButton().setEnabled(true);
		} else {
			comboBox.setModel(new DefaultComboBoxModel());
			module.getImportButton().setEnabled(false);
		}
	}

	public void refreshNodes() {
		OfxBufferModel model = (OfxBufferModel) module.getBufferTable().getModel();
		model.refreshNodes();
	}

	private void setupTransactions(String xmlPath, String sAccount) {
		List<OfxTrnNode> trns = null;

		EntityManager em = EmfContext.create();
		//BankAccount bankAccount = null;
		Account account = null;
		if (sAccount == null) {
			// determine bankAccount
			String key = module.getAccountComboBox().getSelectedItem().toString();
			String arr[] = key.split(":");
			Query q = em.createQuery("select a from BankAccount a where a.type = :type and a.bankId = :bankId");
			q.setParameter("type", arr[0]);
			q.setParameter("bankId", arr[1]);
			List<BankAccount> result = q.getResultList();
			if (result.size() > 0) {
				//bankAccount = result.get(0);
				account = result.get(0).getUserAccount();
			}
		} else {
			Query q = em.createNamedQuery("Account.findUserAccountByName");
			q.setParameter("name", sAccount);
			List<Account> result = q.getResultList();
			if (result.size() > 0) {
				account = result.get(0);
			}
		}

		OfxParser parser = new OfxParser(xmlPath);
		try {
			if (parser.getAccountType().equals(OfxParser.CHECKING) || parser.getAccountType().equals(OfxParser.SAVINGS)) {
				trns = parser.readBankTransactions();
			} else if (parser.getAccountType().equals(OfxParser.CREDITCARD)) {
				trns = parser.readCCTransactions();
			}
		} catch (Exception ex) {
			Logger.getLogger(OfxActions.class.getName()).log(Level.SEVERE, null, ex);
			success = false;
			showWarning(ex.getMessage());
		}

		Account defaultAccount = (Account) em.createNamedQuery("Account.findDefault").getSingleResult();
		AccountDao dao = Service.load(AccountDao.class);
		String sDefaultAccount = dao.concatName(defaultAccount);

//		List<OfxTrnNode> transactions = OfxContext.getTransactions();
		List<OfxTrnNode> transactions = Service.load(OfxData.class).getTransactions();

		if (trns != null) {
			for (OfxTrnNode newNode : trns) {
				boolean add = true;
				for (OfxTrnNode node : transactions) {
					if (newNode.getFitId().equals(node.getFitId())) {
						add = false;
						break;
					}
				}

				// if entry already exists among journalentries, don't add.
				Query q2 = em.createQuery("select j from JournalEntry j where j.fitId = :fitId");
				List<JournalEntry> result2 = q2.setParameter("fitId", newNode.getFitId()).getResultList();
				if (result2.size() > 0) {
					add = false;
				}

				if (add) {

					// guess correct category using payee.
//					Query q3 = em.createNamedQuery("Payee.findByName").setParameter("name", newNode.getName());
//					List<Payee> result3 = q3.getResultList();
//					if(result3.size() > 0) {
//						payee = result3.get(0);
//						defaultAccount = autoSelectAccount(bankAccount.getUserAccount(), payee);
//						sDefaultAccount = dao.concatName(defaultAccount);
//					}
//					newNode.setCategory(sDefaultAccount);

					newNode.setType("ATM");
//					if(bankAccount != null) {
//						newNode.setBankAccountId(bankAccount.getId());
//					}
					if (account != null) {
						newNode.setAccountId(account.getId());
					}
					transactions.add(newNode);
				}
			}
		}

//		OfxContext.setTransactions(transactions);
		Service.load(OfxData.class).setTransactions(transactions);
		refreshNodes();
	}

	public void showWarning(String message) {
		GeneralUtil.showWarningAlert2("<html>Bank returned the following message: \"" + message +
				".\"<br/>Verify the bank account has been set properly in Settings Workbook.</html>");
	}

	public void readOfx(String file, String sAccount) {
		Reader reader = null;
		try {
			LocalStorage ls = WorkbooksApp.getInstance().getContext().getLocalStorage();
			File dir = ls.getDirectory();
			reader = new FileReader(file);
			Ofx2Xml ofx2Xml = new Ofx2Xml(reader);
			String xmlString = ofx2Xml.getXmlString();
			String xmlPath = dir.getAbsolutePath() + File.separator + "temp.xml";
			BufferedWriter writer = new BufferedWriter(new FileWriter(xmlPath));
			writer.write(xmlString);
			writer.close();

			setupTransactions(xmlPath, sAccount);

			if (success) {
				GeneralUtil.showWarningAlert2(WorkbooksApp.getWarning("OfxActions.finishedImport"));
			}
			success = true;
		} catch (Exception ex) {
			Logger.getLogger(OfxActions.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			try {
				reader.close();
			} catch (IOException ex) {
				Logger.getLogger(OfxActions.class.getName()).log(Level.SEVERE, null, ex);
			}

		}
	}

	public void importFile() {
		String path = module.getOfxField().getText();
		if (path != null) {
			if (!path.trim().equals("")) {
				String account = (String) module.getAccount2ComboBox().getSelectedItem();
				readOfx(path, account);
				module.getOfxField().setText("");
			}
		}
	}

	public void doImport() {
		System.out.println("import");
		module.getWaitLabel().setText("Downloading transactions...");
		module.getWaitLabel().setVisible(true);

		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				String item = (String) module.getAccountComboBox().getSelectedItem();
				if (item != null) {
					String[] arr = item.split(":");
					if (arr.length == 2) {
						String type = arr[0];
						String num = arr[1];
						String org = settings.get("Bank.organization");
						String login = settings.get("Bank.login");
						String password = DesEncrypter.getInstance().decrypt(settings.get("Bank.password"));
						String routingNumber = settings.get("Bank.routingNumber");

						String begDate = module.getDateComboBox().getSelectedItem().toString();
						String[] dateArr = begDate.split("-");
						String ofxDate = GeneralUtil.formatOfxDate(dateArr[0], dateArr[1], dateArr[2]);

						OfxClient client = new OfxClient(org, login, password, routingNumber);

						String query = "";
						if (type.equals(OfxParser.CHECKING)) {
							query = client.baQuery(num, ofxDate, "CHECKING");
						}
						if (type.equals(OfxParser.SAVINGS)) {
							query = client.baQuery(num, ofxDate, "SAVINGS");
						}
						if (type.equals(OfxParser.CREDITCARD)) {
							query = client.ccQuery(num, ofxDate);
						}

						if (!query.equals("")) {
							try {
								LocalStorage ls = WorkbooksApp.getInstance().getContext().getLocalStorage();
								File dir = ls.getDirectory();
								String path = dir.getAbsolutePath();
								path = path + File.separator + "temp.ofx";
								client.doQuery(query, path);

								readOfx(path, null);

							} catch (Exception ex) {
								GeneralUtil.showWarningAlert2(ex.getMessage());
								success = false;
								Logger.getLogger(OfxActions.class.getName()).log(Level.SEVERE, null, ex);
							}
						}
					}
				}
				module.getWaitLabel().setVisible(false);
			}
		});

	}

	public void browse() {
		String file = settings.get("Ofx.lastFolder");
		JFileChooser chooser = null;
		if (file != null) {
			chooser = new JFileChooser(new File(file));
		} else {
			chooser = new JFileChooser();
		}
		int returnValue = chooser.showOpenDialog(module);
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			String path = chooser.getSelectedFile().getAbsolutePath();
			module.getOfxField().setText(path);
			settings.put("Ofx.lastFolder", path);
		}
	}
}
