/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.modules.ofx;

import com.xbriage.services.ModuleLoader;
import com.xbriage.services.Service;
import com.xbriage.util.BindUtil;

/**
 *
 * @author shughes
 */
public class OfxController {
	private OfxActions ofxActions = OfxActions.getInstance();
	private OfxModule module = Service.load(ModuleLoader.class).get("Ofx.module");

	public OfxController() {
		ofxActions.refreshAccounts();
		ofxActions.startupRefreshCategoriesComboBox();
		ofxActions.refreshNodes();
	}

	public void startup() {
		BindUtil.bind(ofxActions, "browse", module.getBrowseButton(), BindUtil.ACTION);
		BindUtil.bind(ofxActions, "importFile", module.getImport2Button(), BindUtil.ACTION);
		BindUtil.bind(ofxActions, "doImport", module.getImportButton(), BindUtil.ACTION);
		BindUtil.bind(ofxActions, "confirm", module.getConfirmButton(), BindUtil.ACTION);
		BindUtil.bind(ofxActions, "confirmAll", module.getConfirmAllButton(), BindUtil.ACTION);
		BindUtil.bind(ofxActions, "remove", module.getRemoveButton(), BindUtil.ACTION);
		
		BindUtil.bind(ofxActions, "showPopup", module.getBufferTable(), BindUtil.MOUSE_PRESSED);
		BindUtil.bind(ofxActions, "showPopup", module.getBufferTable(), BindUtil.MOUSE_RELEASED);
		BindUtil.bind(ofxActions, "confirm", module.getConfirmItem(), BindUtil.ACTION);
		BindUtil.bind(ofxActions, "remove", module.getRemoveItem(), BindUtil.ACTION);
	}

}
