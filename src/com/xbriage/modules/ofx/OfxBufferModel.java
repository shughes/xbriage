/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.modules.ofx;


import com.xbriage.context.EmfContext;
import com.xbriage.services.AccountDao;
import com.xbriage.domain.Account;
import com.xbriage.services.OfxData;
import com.xbriage.services.Service;
import com.xbriage.util.GeneralUtil;
import com.xbriage.util.OfxTrnNode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author shughes
 */
public class OfxBufferModel extends AbstractTableModel {

	private List<OfxTrnNode> nodes = new ArrayList<OfxTrnNode>();
	private Map<Integer, Account> accounts = new HashMap<Integer, Account>();

	public int getRowCount() {
		if (nodes != null) {
			return nodes.size();
		} else {
			return 0;
		}
	}

	public int getColumnCount() {
		return 8;
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		switch (col) {
			case 2:
				return true;
			case 5:
				return true;
		}
		return false;
	}

	@Override
	public void setValueAt(Object value, int row, int col) {
		OfxTrnNode node = nodes.get(row);
		AccountDao dao = Service.load(AccountDao.class);
		switch (col) {
			case 2:
				node.setType(value.toString());
				break;
			case 5:
				node.setCategory(value.toString());
				Account cat = dao.breakName(value.toString());
				node.setCategoryId(cat.getId());
				break;
		}
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		OfxTrnNode node = nodes.get(rowIndex);
		Account account = accounts.get(rowIndex);
		
		switch (columnIndex) {
			case 0:
				if(account != null) {
					return account.getName();
				}
				break;
			case 1:
				String sDate = node.getDate();
				String year = sDate.substring(0, 4);
				String month = sDate.substring(4, 6);
				String day = sDate.substring(6, 8);
				sDate = year + "-" + month + "-" + day;
				return sDate;
			case 2:
				return node.getType();
			case 3:
				return node.getCheckNo();
			case 4:
				return node.getName();
			case 5:
				return node.getCategory();
			case 6:
				return node.getMemo();
			case 7:
				return GeneralUtil.formatStringNumber(node.getAmount());
		}

		return "";
	}

	public List<OfxTrnNode> getNodes() {
		return nodes;
	}

	/**
	 * If bankaccount no longer exists in the OfxTrnNode, OfxTrnNode will be removed
	 * from the transactions.
	 */
	public void refreshNodes() {
//		nodes = OfxContext.getTransactions();
		nodes = Service.load(OfxData.class).getTransactions();
		if (nodes != null) {
			Collections.sort(nodes, new DateComparator());
			accounts = new HashMap<Integer, Account>();
			List<OfxTrnNode> tmpNodes = new ArrayList<OfxTrnNode>(nodes);
			EntityManager em = EmfContext.create();
			Account defaultAccount = (Account) em.createNamedQuery("Account.findDefault").getSingleResult();
			int index = 0;
			for (OfxTrnNode node : tmpNodes) {
				
				if(node.getAccountId() != null) {
					Account account = em.find(Account.class, node.getAccountId());
					if(account == null) {
						nodes.remove(node);
					} else {
						accounts.put(index, account);
						index++;
					}
				}

				AccountDao dao = Service.load(AccountDao.class);
				Account cat = null;
				if (node.getCategoryId() != null) {
					cat = em.find(Account.class, node.getCategoryId()); //dao.breakName(node.getCategory());
				}
				cat = (cat == null) ? defaultAccount : cat;
				node.setCategory(dao.concatName(cat));
				node.setCategoryId(cat.getId());
			}
			fireTableDataChanged();
		}
	}

	private class DateComparator implements Comparator {

		public int compare(Object obj1, Object obj2) {
			OfxTrnNode node1 = (OfxTrnNode) obj1;
			OfxTrnNode node2 = (OfxTrnNode) obj2;
			String sDate1 = node1.getDate();
			String sDate2 = node2.getDate();
			Date date1 = GeneralUtil.ofxDatetoDate(sDate1);
			Date date2 = GeneralUtil.ofxDatetoDate(sDate2);
			return date1.compareTo(date2);
		}
	}
}
