/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.util;

import com.xbriage.domain.Account;

/**
 *
 * @author shughes
 */
public interface AccountsListener {
	public static final int UPDATE = 0;
	public static final int REMOVE = 1;
	
	public void updateAccounts(String oldName, String newName, int action);
}
