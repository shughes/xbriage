/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.util;

import com.xbriage.modules.journal.AccountsTable;

/**
 *
 * @author shughes
 */
public class AlternateTable extends AccountsTable {
	public AlternateTable() {
		super();
		setDefaultRenderer(Object.class, new AlternateCellRenderer());
	}
}
