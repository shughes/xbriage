/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.util;

import com.xbriage.services.OfxBanks;
import com.xbriage.services.Service;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;

/**
 *
 * @author shughes
 */
public class OfxClient {

	private static List<OfxBank> siteList = null;
	private static Map<String, OfxBank> sites = null;
	private String password;
	private String user;
	private OfxBank config;
	private String configKey;
	private int cookie;

	private static OfxBank getSite(String key) {
		if (sites == null) {
			sites = new HashMap<String, OfxBank>();
			List<OfxBank> siteList2 = retrieveBanks();
			for (OfxBank site : siteList2) {
				sites.put(site.getName(), site);
			}
		}
		return sites.get(key);
	}

	public OfxClient(String configKey, String user, String password) {
		this.configKey = configKey;
		config = getSite(configKey);
		this.user = user;
		this.password = password;
		this.cookie = 3;
		config.setUser(user);
		config.setPassword(password);
		if(config.getAppId() == null) {
			// QWIN, 1200
			config.setAppId("Money");
			config.setVerId("1600");
		}
	}

	public OfxClient(String configKey, String user, String password, String bankId) {
		this(configKey, user, password);
		config.setBankId(bankId);
	}

	private static List<OfxBank> retrieveBanks() {
		if (siteList == null) {
			OfxBanks ofxBanks = Service.load(OfxBanks.class);
			List<OfxBank> bankList = ofxBanks.getBanks();
			siteList = new ArrayList<OfxBank>(bankList);
		}
		return siteList;
	}

	private String genuuid() {
		return UUID.randomUUID().toString().trim().toUpperCase();
	}

	private String field(String tag, String value) {
		return "<" + tag + ">" + value;
	}

	private String tag(String tag, List<String> contents) {
		StringBuffer ret = new StringBuffer();
		ret.append("<" + tag + ">\r\n");
		for (String line : contents) {
			ret.append(line + "\r\n");
		}
		ret.append("</" + tag + ">\r\n");
		return ret.toString();
	}

	private String formatNum(int num) {
		String sNum = new Integer(num).toString();
		sNum = (num < 10) ? "0" + sNum : sNum;
		return sNum;
	}

	private String date() {
		Date d = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		return date(cal.get(Calendar.MONTH));
	}

	public String date(int month) {
		Date d = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		cal.set(Calendar.MONTH, month);
		int year = cal.get(Calendar.YEAR);
		String sDate = date(month, year);
		return sDate;
	}

	public String date(int month, int year) {
		Date d = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.YEAR, year);
		String sDate = "";
		String sYear = new Integer(cal.get(Calendar.YEAR)).toString();
		String sMonth = formatNum(cal.get(Calendar.MONTH) + 1);
		String sDay = formatNum(cal.get(Calendar.DAY_OF_MONTH));
		String sHour = formatNum(cal.get(Calendar.HOUR_OF_DAY));
		String sMin = formatNum(cal.get(Calendar.MINUTE));
		String sSec = formatNum(cal.get(Calendar.SECOND));
		sDate = sYear + sMonth + sDay + sHour + sMin + sSec;
		return sDate;
	}

	private String cookie() {
		return new Integer(++cookie).toString();
	}

	private String signOn() {
		List<String> fiData = new ArrayList<String>();
		fiData.add(field("ORG", config.getFiorg()));
		if(config.getFid() != null) {
			fiData.add(field("FID", config.getFid()));
		}

		List<String> list2 = new ArrayList<String>();
		list2.add(field("DTCLIENT", date()));
		list2.add(field("USERID", config.getUser()));
		list2.add(field("USERPASS", config.getPassword()));
		list2.add(field("LANGUAGE", "ENG"));
		list2.add(tag("FI", fiData));
		list2.add(field("APPID", config.getAppId()));
		list2.add(field("APPVER", config.getVerId()));

		List<String> list = new ArrayList<String>();
		list.add(tag("SONRQ", list2));
		String tag = tag("SIGNONMSGSRQV1", list);

		return tag;
	}

	private String acctReq(String dtStart) {
		List<String> list = new ArrayList<String>();
		list.add(field("DTACCTUP", dtStart));
		String req = tag("ACCTINFORQ", list);
		return message("SIGNUP", "ACCTINFO", req);
	}

	private String baReq(String acctid, String dtStart, String acctType) {
		List<String> list = new ArrayList<String>();
		List<String> list2 = new ArrayList<String>();
		list2.add(field("BANKID", getSite(configKey).getBankId()));
		list2.add(field("ACCTID", acctid));
		list2.add(field("ACCTTYPE", acctType));
		list.add(tag("BANKACCTFROM", list2));
		List<String> list3 = new ArrayList<String>();
		list3.add(field("DTSTART", dtStart));
		list3.add(field("INCLUDE", "Y"));
		list.add(tag("INCTRAN", list3));
		String req = tag("STMTRQ", list);
		return message("BANK", "STMT", req);
	}

	private String ccReq(String acctId, String dtStart) {
		List<String> list = new ArrayList<String>();
		List<String> list2 = new ArrayList<String>();
		list2.add(field("ACCTID", acctId));
		list.add(tag("CCACCTFROM", list2));
		List<String> list3 = new ArrayList<String>();
		list3.add(field("DTSTART", dtStart));
		list3.add(field("INCLUDE", "Y"));
		list.add(tag("INCTRAN", list3));
		String req = tag("CCSTMTRQ", list);
		return message("CREDITCARD", "CCSTMT", req);
	}

	public String invstReq(String brokerId, String acctId, String dtStart) {
		String dtnow = date();
		List<String> list = new ArrayList<String>();
		List<String> list2 = new ArrayList<String>();
		list2.add(field("BROKERID", brokerId));
		list2.add(field("ACCTID", acctId));
		list.add(tag("INVACCTFROM", list2));
		List<String> list3 = new ArrayList<String>();
		list3.add(field("DTSTART", dtStart));
		list3.add(field("INCLUDE", "Y"));
		list.add(tag("INCTRAN", list3));
		list.add(field("INCOO", "Y"));
		List<String> list4 = new ArrayList<String>();
		list4.add(field("DTASOF", dtnow));
		list4.add(field("INCLUDE", "Y"));
		list.add(tag("INCPOS", list4));
		list.add(field("INCBAL", "Y"));
		String req = tag("INVSTMTRQ", list);
		return message("INVSTMT", "INVSTMT", req);
	}

	private String message(String msgType, String trnType, String request) {
		List<String> list = new ArrayList<String>();
		List<String> list2 = new ArrayList<String>();
		list2.add(field("TRNUID", genuuid()));
		list2.add(field("CLTCOOKIE", cookie()));
		list2.add(request);
		list.add(tag(trnType + "TRNRQ", list2));
		return tag(msgType + "MSGSRQV1", list);
	}

	private String header() {
		String header = "OFXHEADER:100\r\n" +
				"DATA:OFXSGML\r\n" +
				"VERSION:102\r\n" +
				"SECURITY:NONE\r\n" +
				"ENCODING:USASCII\r\n" +
				"CHARSET:1252\r\n" +
				"COMPRESSION:NONE\r\n" +
				"OLDFILEUID:NONE\r\n" +
				"NEWFILEUID:" + genuuid() + "\r\n";
		return header;
	}

	public String baQuery(String acctId, String dtStart, String acctType) {
		List<String> list = new ArrayList<String>();
		list.add(signOn());
		list.add(baReq(acctId, dtStart, acctType));
		String query = header() + "\r\n" +
				tag("OFX", list) + "\r\n";
		return query;
	}

	public String ccQuery(String acctId, String dtStart) {
		List<String> list = new ArrayList<String>();
		list.add(signOn());
		list.add(ccReq(acctId, dtStart));
		String query = header() + "\r\n" +
				tag("OFX", list) + "\r\n";
		return query;
	}

	public String acctQuery(String dtStart) {
		List<String> list = new ArrayList<String>();
		list.add(signOn());
		list.add(acctReq(dtStart));
		String query = header() + "\r\n" +
				tag("OFX", list) + "\r\n";
		return query;
	}

	public String invstQuery(String brokerId, String acctId, String dtStart) {
		List<String> list = new ArrayList<String>();
		list.add(signOn());
		list.add(invstReq(brokerId, acctId, dtStart));
		String query = header() + "\r\n" +
				tag("OFX", list) + "\r\n";
		return query;
	}

	public void doQuery(String query, String name) throws IOException {
		BufferedWriter writer = null;
		URL url = new URL(config.getUrl());
		HttpsURLConnection conn = null;
		conn = (HttpsURLConnection) url.openConnection();
		conn.setRequestProperty("Content-type", "application/x-ofx");
		conn.setRequestProperty("Content-length", new Integer(query.length()).toString());
		conn.setRequestProperty("Accept", "*/*, application/x-ofx");
		conn.setRequestMethod("POST");
		conn.setDoInput(true);
		conn.setDoOutput(true);

		OutputStream os = conn.getOutputStream();
		os.write(query.getBytes());

		StringBuilder response = new StringBuilder();
		BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

		while (true) {
			int s = reader.read();
			if (s == -1) {
				break;
			}
			response.append((char) s);
		}

		writer = new BufferedWriter(new FileWriter(name));
		writer.write(response.toString());

		conn.disconnect();
		writer.close();
	}

	public static void main(String[] args) {
		//OfxClient client = new OfxClient("FifthThird", "257597207", "hobson");
		//client.retrieveBanks();

//	List<Map<String, Object>> list = OfxClient.retrieveBanks();
//	for(Map<String, Object> site : list) {
//		System.out.println("site: "+site.get("id"));
//	}

		BufferedWriter writer = null;
		try {
			//OfxClient client = new OfxClient("Wachovia", "", "", "");
			//OfxClient client = new OfxClient("Fifth Third", "", "");
			//String query = client.baQuery("7020658501", client.date(4), "CHECKING");
			//String query = client.baQuery("1010134954969", client.date(4), "CHECKING");
			String query = client.baQuery("3000079156173", client.date(4), "SAVINGS");
			//String query = client.ccQuery("5444000195614212", client.date(4));
			System.out.println("date: " + client.date(1, 2007));
			//String query = client.acctQuery(client.date(1, 2000));

			System.out.println(query);
			String ofxFile = "/Users/shughes/Desktop/ofx.txt";
			client.doQuery(query, ofxFile);

			FileReader reader = new FileReader(ofxFile);
			Ofx2Xml ofx2Xml = new Ofx2Xml(reader);
			String xml = ofx2Xml.getXmlString();

			writer = new BufferedWriter(new FileWriter("/Users/shughes/Desktop/test.xml"));
			writer.write(xml);

		} catch (Exception ex) {
			Logger.getLogger(OfxClient.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			try {
				writer.close();
			} catch (IOException ex) {
				Logger.getLogger(OfxClient.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}
}
