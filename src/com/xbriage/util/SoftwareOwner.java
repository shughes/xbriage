/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.util;

import com.xbriage.app.WorkbooksApp;
import com.xbriage.context.SetupContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.SocketException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingWorker;
import org.apache.commons.net.ftp.FTPClient;
import org.jdesktop.application.LocalStorage;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

/**
 *
 * @author shughes
 */
public class SoftwareOwner extends SwingWorker<Boolean, Object> {

	public SoftwareOwner() {
		
	}

	@Override
	protected Boolean doInBackground() throws Exception {
		boolean result = false;
		
		try {
			Map<String, String> settings = SetupContext.getInstance().getSettings();
			if (settings.get("Settings.ownerId") == null) {
				
				URL url = new URL("http://config.xbriage.com/Owners.xml");
				InputStream input = url.openStream();
				System.out.println("input: "+input);

				SAXBuilder sax = new SAXBuilder();
				Document doc = sax.build(input);
				input.close();
				Element owners = doc.getRootElement();
				List<Element> ownerList = owners.getChildren("Owner");
				int maxId = 0;
				for (Element owner : ownerList) {
					int id = owner.getAttribute("id").getIntValue();
					maxId = (id > maxId) ? id : maxId;
				}
				Element owner = new Element("Owner");
				owner.setAttribute("id", new Integer(++maxId).toString());
				owners.addContent(owner);

				settings.put("Settings.ownerId", new Integer(maxId).toString());

				XMLOutputter serializer = new XMLOutputter();
				serializer.setFormat(Format.getPrettyFormat());
				LocalStorage ls = WorkbooksApp.getApplication().getContext().getLocalStorage();
				String file = ls.getDirectory().toString() + File.separator + "Owners.xml";
				PrintWriter out = new PrintWriter(new FileWriter(file));
				serializer.output(doc, out);
				out.close();
				
				FTPClient client = new FTPClient();
				client.connect("ftp.samueljennings.com");
				client.login("config@samueljennings.com", "hobson");
				InputStream input2 = new FileInputStream(file);
				client.storeFile("Owners.xml", input2);
				client.logout();
				client.disconnect();
				
				result = true;
			}

		} catch (JDOMException ex) {
			Logger.getLogger(SoftwareOwner.class.getName()).log(Level.SEVERE, null, ex);
		} catch (SocketException ex) {
			Logger.getLogger(SoftwareOwner.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(SoftwareOwner.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		return result;
	}
}
