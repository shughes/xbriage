/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.util;

import com.xbriage.context.MapContext;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author shughes
 */
public class MethodResponderImpl implements MethodResponder {

	private String id;
	private Map args;
	private Class[] argTypes;
	private String methodName;
	private Object object;

	public void perform(Map params) {
		try {
			Class clazz = object.getClass();
			String key = clazz.getName() + "." + methodName;
			MapContext.put(key, params);
			Method m = clazz.getMethod(methodName, new Class[]{});
			m.invoke(object, new Object[]{});
		} catch (IllegalAccessException ex) {
			Logger.getLogger(MethodResponderImpl.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalArgumentException ex) {
			Logger.getLogger(MethodResponderImpl.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InvocationTargetException ex) {
			Logger.getLogger(MethodResponderImpl.class.getName()).log(Level.SEVERE, null, ex);
		} catch (NoSuchMethodException ex) {
			Logger.getLogger(MethodResponderImpl.class.getName()).log(Level.SEVERE, null, ex);
		} catch (SecurityException ex) {
			Logger.getLogger(MethodResponderImpl.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setArgs(Map args) {
		this.args = args;
	}

	public Map getArgs() {
		return args;
	}

	public void setMethodName(String name) {
		methodName = name;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setArgTypes(Class[] types) {
		argTypes = types;
	}

	public Class[] getArgTypes() {
		return argTypes;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}
}
