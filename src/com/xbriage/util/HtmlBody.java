/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.util;

/**
 *
 * @author shughes
 */
public class HtmlBody {
	StringBuffer html = new StringBuffer();
	private String stylesheet = "";
	private String content;

	public HtmlBody() {
		
	}
	
	@Override
	public String toString() {
		html = html.append("<html>");
		html = html.append("<head>");
		html = html.append("<link href=\""+getStylesheet()+"\" rel=\"stylesheet\" type=\"text/css\"/>");
		html = html.append("</head>");
		html = html.append("<body>");
		html = html.append(content);
		html = html.append("</body>");
		html = html.append("</html>");
		return html.toString();
	}
	
	public String getStylesheet() {
		return stylesheet;
	}

	public void setStylesheet(String stylesheet) {
		this.stylesheet = stylesheet;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
