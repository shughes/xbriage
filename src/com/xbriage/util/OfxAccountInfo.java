/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.util;

/**
 *
 * @author shughes
 */
public class OfxAccountInfo {
	private String name;
	private String bankId;
	private String accountId;
	private String accountType;
	
	public OfxAccountInfo() {
		
	}
	
	public OfxAccountInfo(String name, String bankId, String accountId, String accountType) {
		this.name = name;
		this.bankId = bankId;
		this.accountId = accountId;
		this.accountType = accountType;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accounttype) {
		this.accountType = accounttype;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
