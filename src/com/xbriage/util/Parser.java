/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.util;

import com.xbriage.services.AccountDao;
import com.xbriage.domain.Account;
import com.xbriage.domain.JournalEntry;
import com.xbriage.domain.JournalEntryRow;
import com.xbriage.domain.Payee;
import com.xbriage.services.Service;
import com.xbriage.context.EmfContext;
import com.xbriage.context.SetupContext;
import com.xbriage.domain.BankAccount;
import com.xbriage.domain.Budget;
import com.xbriage.security.GeneralSecurity;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

/**
 *
 * @author shughes
 */
public class Parser {

	public static final String NAMESPACE = "http://xml.netbeans.org/schema/xbriage";
	private Namespace namespace = Namespace.getNamespace(NAMESPACE);
	private Namespace xsiNamespace = Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
	private Attribute schemaAttribute = new Attribute("schemaLocation", "http://xml.netbeans.org/schema/xbriage xbriage.xsd", xsiNamespace);

	public Parser() {

	}

	public void removeAll() {
		EntityManager em = EmfContext.create();
		em.getTransaction().begin();

		List<JournalEntry> entries = em.createNamedQuery("JournalEntry.findAll").getResultList();
		for (JournalEntry entry : entries) {
			em.remove(entry);
		}

		List<Payee> payees = em.createNamedQuery("Payee.findAll").getResultList();
		for (Payee payee : payees) {
			if (!payee.isDefault()) {
				em.remove(payee);
			}
		}

		List<Account> accounts = em.createNamedQuery("Account.findAll").getResultList();
		for (Account account : accounts) {
			if (!account.isDefault() && !account.isRoot().equals(1)) {
				em.remove(account);
			}
		}

		List<Budget> budgets = em.createNamedQuery("Budget.findAll").getResultList();
		for (Budget budget : budgets) {
			em.remove(budget);
		}

		em.getTransaction().commit();
		em.close();
	}

	public void removeAccount(Account account) {
		EntityManager em = EmfContext.create();
		em.getTransaction().begin();

		account = em.find(Account.class, account.getId());

		List<JournalEntryRow> rows = em.createQuery("select row from JournalEntryRow row where row.account = :account").setParameter("account", account).getResultList();
		Map<Integer, Boolean> tracker = new HashMap<Integer, Boolean>();
		for (JournalEntryRow row : rows) {
			if (row.getJournalEntry() != null) {
				if (tracker.get(row.getJournalEntry().getId()) == null) {
					tracker.put(row.getJournalEntry().getId(), true);
					em.remove(row.getJournalEntry());
				}
			}
		}

		em.remove(account);
		em.getTransaction().commit();
		em.close();
	}

	private void finishParse(BufferedReader reader) {
		try {
			SAXBuilder parser = new SAXBuilder();
			Document response = parser.build(reader);

			Element root = response.getRootElement();
			List<Element> accounts = root.getChildren("Account", namespace);
			EntityManager em = EmfContext.create();
			
			em.getTransaction().begin();
			int i = 0;
			for (Element account : accounts) {
				if ((i + 1) % 100 == 0) {
					em.flush();
					em.clear();
				}
				parseAccount(em, account);
				i++;
			}
			em.getTransaction().commit();

			List<Element> entries = root.getChildren("JournalEntry", namespace);
			em.getTransaction().begin();
			i = 0;
			for (Element entry : entries) {
				if ((i + 1) % 25 == 0) {
					em.flush();
					em.clear();
				}
				parseJournalEntry(em, entry, null);
				i++;
			}
			em.getTransaction().commit();

			parseBankProperties(root);

			List<Element> bankAccounts = root.getChildren("BankAccount", namespace);
			em.getTransaction().begin();
			i = 0;
			for (Element bankAccount : bankAccounts) {
				if ((i + 1) % 100 == 0) {
					em.flush();
					em.clear();
				}
				parseBankAccount(em, bankAccount);
				i++;
			}
			em.getTransaction().commit();

			List<Element> budgets = root.getChildren("Budget", namespace);
			em.getTransaction().begin();
			i = 0;
			for (Element budget : budgets) {
				if ((i + 1) % 100 == 0) {
					em.flush();
					em.clear();
				}
				parseBudget(em, budget);
				i++;
			}
			em.getTransaction().commit();
			
			em.close();

		} catch (JDOMException ex) {
			Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			try {
				reader.close();
			} catch (IOException ex) {
				Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	public void parse(InputStream input) {
		BufferedReader reader = null;
		InputStreamReader isr = new InputStreamReader(input);
		reader = new BufferedReader(isr);
		finishParse(reader);
	}

	public void parse(String file) {
		try {
			BufferedReader reader = null;
			reader = new BufferedReader(new FileReader(file));
			finishParse(reader);
		} catch (FileNotFoundException ex) {
			Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private void parseBankProperties(Element root) {
		Map<String, String> settings = SetupContext.getInstance().getSettings();
		String sDate = "";

		Element bankProperties = root.getChild("BankProperties", namespace);
		if (bankProperties != null) {
			Element organization = bankProperties.getChild("organization", namespace);
			if (organization != null) {
				String org = organization.getTextTrim();
				settings.put("Bank.organization", org);
			}

			Element routingNumber = bankProperties.getChild("routingNumber", namespace);
			if (routingNumber != null) {
				String routing = routingNumber.getTextTrim();
				settings.put("Bank.routingNumber", routing);
			}

			Element login = bankProperties.getChild("login", namespace);
			if (login != null) {
				String sLogin = login.getTextTrim();
				settings.put("Bank.login", sLogin);
			}

			Element password = bankProperties.getChild("password", namespace);
			if (password != null) {
				String sPass = password.getTextTrim();
				settings.put("Bank.password", sPass);
			}

			Element date = bankProperties.getChild("date", namespace);
			if (date != null) {
				sDate = date.getTextTrim();
				settings.put("Security.expireDate", sDate);
			}
		}

		// if someone tampers with expire date, it's reset. 
		// won't affect xbriage when serial number and email are correct.
		if (GeneralSecurity.dateExpired(sDate)) {
			GeneralSecurity.resetToday();
		}
	}

	private void parseBudget(EntityManager em, Element budget) {
		Budget b = new Budget();
		AccountDao dao = Service.load(AccountDao.class);

		Element account = budget.getChild("account", namespace);
		if (account != null) {
			Account acct = dao.breakName(account.getTextTrim());
			b.setAccount(acct);
		}

		Element amount = budget.getChild("amount", namespace);
		if (amount != null) {
			String amt = amount.getTextTrim();
			b.setAmount(new BigDecimal(amt));
		}

		Element start = budget.getChild("start", namespace);
		if (start != null) {
			String sStart = start.getTextTrim();
			b.setStart(GeneralUtil.stringToDate(sStart));
		}

		Element end = budget.getChild("end", namespace);
		if (end != null) {
			String sEnd = end.getTextTrim();
			b.setEnd(GeneralUtil.stringToDate(sEnd));
		}

		em.merge(b);
	}

	private void parseBankAccount(EntityManager em, Element bankAccount) {
		Account account = null;
		String sBankId = null;
		String sType = null;

		Element userAccount = bankAccount.getChild("userAccount", namespace);
		if (userAccount != null) {
			AccountDao dao = Service.load(AccountDao.class);
			account = dao.breakName(userAccount.getTextTrim());
		}

		Element bankId = bankAccount.getChild("bankId", namespace);
		if (bankId != null) {
			sBankId = bankId.getTextTrim();
		}

		Element type = bankAccount.getChild("type", namespace);
		if (type != null) {
			sType = type.getTextTrim();
		}

		BankAccount bAccount = new BankAccount(sBankId, sType, account);
		em.merge(bAccount);
	}

	public void parseJournalEntry(EntityManager em, Element journalEntry, List<Element> entryRows) {

		//List<Element> rows = new ArrayList<Element>();
		Element rows = journalEntry.getChild("rows", namespace);
		List<Element> rowsList = rows.getChildren("JournalEntryRow", namespace);

		JournalEntry entry = new JournalEntry();

		// fitId
		Element fitId = journalEntry.getChild("fitId", namespace);
		if (fitId != null) {
			String sFitId = fitId.getTextTrim();
			entry.setFitId(sFitId);
		}

		// date
		Element date = journalEntry.getChild("date", namespace);
		String sDate = "";
		Date dDate = new Date();
		if (date != null) {
			sDate = date.getTextTrim();
			dDate = GeneralUtil.stringToDate(sDate);
		}
		entry.setDate(dDate);

		// memo
		Element memo = journalEntry.getChild("memo", namespace);
		String sMemo = "";
		if (memo != null) {
			sMemo = memo.getTextTrim();
		}
		entry.setMemo(sMemo);

		// payee
		Element payee = journalEntry.getChild("payee", namespace);
		Payee defaultPayee = (Payee) em.createNamedQuery("Payee.findDefault").getSingleResult();
		if (payee == null) {
			entry.setPayee(defaultPayee);
		} else {
			String sPayee = payee.getTextTrim();
			List<Payee> result = em.createNamedQuery("Payee.findByName").setParameter("name", sPayee).getResultList();
			if (result.size() != 0) {
				entry.setPayee(result.get(0));
			} else { // if payee doesn't exist, create it.
				Payee newPayee = new Payee(sPayee, false);
				em.persist(newPayee);
				entry.setPayee(newPayee);
			}
		}

		// type
		Element type = journalEntry.getChild("type", namespace);
		String sType = "Deposit";
		if (type != null) {
			sType = type.getTextTrim();
		}
		int iType = GeneralUtil.getType(sType);
		entry.setType(iType);

		// checkNo
		Element checkNo = journalEntry.getChild("checkNo", namespace);
		String sCheckNo = "0";
		if (checkNo != null) {
			sCheckNo = checkNo.getTextTrim();
		}
		entry.setCheckNo(new Integer(sCheckNo));

		entry = em.merge(entry);

		// journal entry rows
		for (Element row : rowsList) {
			parseJournalEntryRow(em, row, entry);
		}

		em.merge(entry);
	}

	public void parseJournalEntryRow(EntityManager em, Element row, JournalEntry entry) {
		JournalEntryRow entryRow = new JournalEntryRow();

		// amount
		Element amount = row.getChild("amount", namespace);
		String sAmount = "";
		if (amount != null) {
			sAmount = amount.getTextTrim();
		}
		BigDecimal bAmount = new BigDecimal(sAmount);
		entryRow.setAmount(bAmount);

		// entry
		entryRow.setJournalEntry(entry);

		// debitEntry
		Element debitEntry = row.getChild("debitEntry", namespace);
		if (debitEntry != null) {
			String sDebitEntry = debitEntry.getTextTrim();
			int iDebitEntry = (sDebitEntry.equals("true")) ? 1 : 0;
			entryRow.setDebitEntry(iDebitEntry);
		}

		// account
		Element account = row.getChild("account", namespace);
		String sAccount = "";
		if (account != null) {
			sAccount = account.getTextTrim();
		}
		AccountDao dao = Service.load(AccountDao.class);
		Account aAccount = dao.breakName(sAccount);

		// if account relating to this journal entry still does not exist, it'll be created here.
		if (aAccount == null) {
			Account root = (Account) em.createNamedQuery("Account.findRoot").getSingleResult();

			int userAccount = 0;
			if (sAccount.charAt(0) == '[') {
				sAccount = sAccount.replace('[', ' ');
				sAccount = sAccount.replace(']', ' ');
				sAccount = sAccount.trim();
				userAccount = 1;
			}

			createAccount(em, root, sAccount.split(":"), 0, userAccount, "Asset", false);

			sAccount = (userAccount == 1) ? "[" + sAccount + "]" : sAccount;
			aAccount = dao.breakName(sAccount);
		}

		entryRow.setAccount(aAccount);

		// memo
		Element memo = row.getChild("memo", namespace);
		String sMemo = "";
		if (memo != null) {
			sMemo = memo.getText();
		}
		entryRow.setMemo(sMemo);

		// merge		
		em.persist(entryRow);
	}

	public void parseAccount(EntityManager em, Element account) {
		Element name = account.getChild("name", Namespace.getNamespace(NAMESPACE));
		String nameStack = "";
		if (name != null) {
			nameStack = name.getTextTrim();
		}
		if (nameStack.charAt(0) == '[') {
			nameStack = nameStack.replace('[', ' ');
			nameStack = nameStack.replace(']', ' ');
			nameStack = nameStack.trim();
		}
		String names[] = nameStack.split(":");

		List<Account> result = em.createNamedQuery("Account.findRoot").getResultList();
		Account root = null;
		if (result.size() == 0) {
			root = createRoot(em);
		} else {
			root = result.get(0);
		}

		Element typeName = account.getChild("typeName", Namespace.getNamespace(NAMESPACE));
		String sTypeName = "Expense";
		if (typeName != null) {
			sTypeName = typeName.getTextTrim();
		}

		Element userAccount = account.getChild("userAccount", Namespace.getNamespace(NAMESPACE));
		int iUserAccount = 0;
		if (userAccount != null) {
			iUserAccount = (userAccount.getTextTrim().equals("true")) ? 1 : 0;
		}

		Element _default = account.getChild("default", Namespace.getNamespace(NAMESPACE));
		boolean bDefault = false;
		if (_default != null) {
			bDefault = (_default.getTextTrim().equals("false")) ? false : true;
		}

		createAccount(em, root, names, 0, iUserAccount, sTypeName, bDefault);
	}
	
	private Account createRoot(EntityManager em) {
		List<Account> result = em.createNamedQuery("Account.findRoot").getResultList();
		Account root = new Account();

		if (result.size() != 0) {
			System.out.println("Root already exists");
		} else {
			root.setName("Root");
			root.setRoot(1);
			root.setDefault(false);
			root.setTypeName("Expense");
			root.setUserAccount(0);
			em.persist(root);
		}

		return root;
	}

	private void createAccount(EntityManager em, Account parent, String[] names, int i, int userAccount, String type, boolean _default) {
		if (i < names.length) {
			String name = names[i];
			String sQuery = "select a from Account a where a.name = :name and a.parent = :parent";
			Query query = em.createQuery(sQuery);
			query.setParameter("name", name);
			query.setParameter("parent", parent);

			List<Account> result = query.getResultList();
			if (result.size() == 0) {
				Account account = new Account();
				account.setName(name);
				account.setTypeName(type);
				account.setParent(parent);
				account.setUserAccount(userAccount);
				account.setRoot(0);

				if (_default) {
					List<Account> defaults = em.createNamedQuery("Account.findDefault").getResultList();
					if (defaults.size() != 0) {
						Account d = defaults.get(0);
						d.setDefault(false);
						em.persist(d);
					}
				}
				account.setDefault(_default);

				account = em.merge(account);
				createAccount(em, account, names, ++i, userAccount, type, _default);
			} else {
				createAccount(em, result.get(0), names, ++i, userAccount, type, _default);
			}
		}
	}

	public void backupAccount(String file, Account account) {
		PrintWriter out = null;
		try {
			XMLOutputter serializer = setupOutputter();
			Element xbriage = setupElement();
			out = new PrintWriter(new FileWriter(file));

			EntityManager em = EmfContext.create();
			account = em.find(Account.class, account.getId());
			Query q = em.createQuery("select row from JournalEntryRow row where row.account = :account").setParameter("account", account);

			List<JournalEntryRow> rows = q.getResultList();
			Map<Integer, Boolean> tracker = new HashMap<Integer, Boolean>();
			int i = 0;
			for (JournalEntryRow row : rows) {
				if (tracker.get(row.getJournalEntry().getId()) == null) {
					tracker.put(row.getJournalEntry().getId(), true);
					xbriage.addContent(createJournalEntryElement(row.getJournalEntry()));
				}
			}

			Document request = new Document(xbriage);
			serializer.output(request, out);
		} catch (IOException ex) {
			Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			out.close();
		}
	}

	private Element setupElement() {
		Element xbriage = new Element("xbriage", namespace);
		xbriage.addNamespaceDeclaration(xsiNamespace);
		xbriage.setAttribute(schemaAttribute);
		return xbriage;
	}

	private XMLOutputter setupOutputter() {
		XMLOutputter serializer = new XMLOutputter();
		Format format = Format.getPrettyFormat();
		serializer.setFormat(format);
		return serializer;
	}

	public void backup(String file) {

		PrintWriter out = null;
		try {
			XMLOutputter serializer = setupOutputter();
			Element xbriage = setupElement();
			out = new PrintWriter(new FileWriter(file));

			EntityManager em = EmfContext.create();

			List<Account> accounts = em.createNamedQuery("Account.findAllButRoot").getResultList();
			for (Account aAccount : accounts) {
				xbriage.addContent(createAccountElement(aAccount));
			}

			List<JournalEntry> entries = em.createNamedQuery("JournalEntry.findAll").getResultList();
			for (JournalEntry entry : entries) {
				xbriage.addContent(createJournalEntryElement(entry));
			}

			createBankProperties(xbriage);

			List<BankAccount> bankAccounts = em.createNamedQuery("BankAccount.findAll").getResultList();
			for (BankAccount bankAccount : bankAccounts) {
				xbriage.addContent(createBankAccountElement(bankAccount));
			}

			List<Budget> budgets = em.createNamedQuery("Budget.findAll").getResultList();
			for (Budget budget : budgets) {
				xbriage.addContent(createBudgetElement(budget));
			}


			Document request = new Document(xbriage);
			serializer.output(request, out);
		} catch (IOException ex) {
			Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			out.close();
		}

	}

	private void createBankProperties(Element xbriage) {
		Map<String, String> settings = SetupContext.getInstance().getSettings();
		String routing = settings.get("Bank.routingNumber");
		String org = settings.get("Bank.organization");
		String sLogin = settings.get("Bank.login");
		String sPass = settings.get("Bank.password");
		String sDate = settings.get("Security.expireDate");

		Element bankProperties = new Element("BankProperties", namespace);

		if (routing != null) {
			Element routingNumber = new Element("routingNumber", namespace);
			bankProperties.addContent(routingNumber);
			routingNumber.setText(routing);
		}

		if (org != null) {
			Element organization = new Element("organization", namespace);
			bankProperties.addContent(organization);
			organization.setText(org);
		}

		if (sLogin != null) {
			Element login = new Element("login", namespace);
			bankProperties.addContent(login);
			login.setText(sLogin);
		}

		if (sPass != null) {
			Element password = new Element("password", namespace);
			bankProperties.addContent(password);
			password.setText(sPass);
		}

		if (sDate != null) {
			Element date = new Element("date", namespace);
			bankProperties.addContent(date);
			date.setText(sDate);
		}

		xbriage.addContent(bankProperties);
	}

	private Element createBudgetElement(Budget b) {
		Element budget = new Element("Budget", namespace);
		AccountDao dao = Service.load(AccountDao.class);

		if (b.getAccount() != null) {
			Element account = new Element("account", namespace);
			budget.addContent(account);
			account.setText(dao.concatName(b.getAccount()));
		}

		if (b.getAmount() != null) {
			Element amount = new Element("amount", namespace);
			budget.addContent(amount);
			amount.setText(b.getAmount().toString());
		}

		if (b.getStart() != null) {
			Element start = new Element("start", namespace);
			budget.addContent(start);
			start.setText(GeneralUtil.dateToString(b.getStart()));
		}

		if (b.getEnd() != null) {
			Element end = new Element("end", namespace);
			budget.addContent(end);
			end.setText(GeneralUtil.dateToString(b.getEnd()));
		}

		return budget;
	}

	private Element createBankAccountElement(BankAccount bAccount) {
		Element bankAccount = new Element("BankAccount", namespace);
		AccountDao dao = Service.load(AccountDao.class);

		if (bAccount.getUserAccount() != null) {
			Element userAccount = new Element("userAccount", namespace);
			bankAccount.addContent(userAccount);
			userAccount.setText(dao.concatName(bAccount.getUserAccount()));
		}

		Element type = new Element("type", namespace);
		bankAccount.addContent(type);
		type.setText(bAccount.getType());

		Element bankId = new Element("bankId", namespace);
		bankAccount.addContent(bankId);
		bankId.setText(bAccount.getBankId());

		return bankAccount;
	}

	public Element createJournalEntryElement(JournalEntry entry) {
		Element journalEntry = new Element("JournalEntry", namespace);

		// fitId
		Element fitId = new Element("fitId", namespace);
		journalEntry.addContent(fitId);
		fitId.setText(entry.getFitId());

		// date
		Element date = new Element("date", namespace);
		journalEntry.addContent(date);
		Date dDate = entry.getDate();
		String sDate = GeneralUtil.dateToString(dDate);
		date.setText(sDate);

		// payee
		Element payee = new Element("payee", namespace);
		journalEntry.addContent(payee);
		String sPayee = entry.getPayee().getName();
		payee.setText(sPayee);

		// memo 
		Element memo = new Element("memo", namespace);
		journalEntry.addContent(memo);
		String sMemo = entry.getMemo();
		if (sMemo != null) {
			memo.setText(sMemo);
		} else {
			memo.setText("");
		}

		// rows
		Element rows = new Element("rows", namespace);
		for (JournalEntryRow row : entry.getRows()) {
			rows.addContent(createJournalEntryRowElement(row));
		}
		journalEntry.addContent(rows);

		// type
		Element type = new Element("type", namespace);
		journalEntry.addContent(type);
		String sType = GeneralUtil.getType(entry.getType());
		if (sType != null) {
			type.setText(sType);
		} else {
			type.setText("Deposit");
		}

		// checkNo
		Element checkNo = new Element("checkNo", namespace);
		journalEntry.addContent(checkNo);
		String sCheckNo = new Integer(entry.getCheckNo()).toString();
		if (sCheckNo != null) {
			checkNo.setText(sCheckNo);
		} else {
			checkNo.setText("0");
		}

		return journalEntry;
	}

	private Element createJournalEntryRowElement(JournalEntryRow row) {
		Element journalEntryRow = new Element("JournalEntryRow", namespace);

		// amount
		Element amount = new Element("amount", namespace);
		journalEntryRow.addContent(amount);
		String sAmount = row.getAmount().toString();
		amount.setText(sAmount);

		// account
		Element account = new Element("account", namespace);
		journalEntryRow.addContent(account);
		AccountDao dao = Service.load(AccountDao.class);
		String sAccount = dao.concatName(row.getAccount());
		account.setText(sAccount);

		// debitEntry
		Element debitEntry = new Element("debitEntry", namespace);
		journalEntryRow.addContent(debitEntry);
		String sDebitEntry = (row.isDebitEntry() == 1) ? "true" : "false";
		debitEntry.setText(sDebitEntry);

		// memo
		Element memo = new Element("memo", namespace);
		journalEntryRow.addContent(memo);
		String sMemo = row.getMemo();
		sMemo = (sMemo == null) ? "" : sMemo;
		memo.setText(sMemo);

		return journalEntryRow;
	}

	public Element createAccountElement(Account aAccount) {
		Element account = new Element("Account", namespace);
		AccountDao dao = Service.load(AccountDao.class);

		// name
		Element name = new Element("name", namespace);
		account.addContent(name);
		String sName = dao.concatName(aAccount);
		name.setText(sName);

		// userAccount
		Element userAccount = new Element("userAccount", namespace);
		account.addContent(userAccount);
		String sUserAccount = (aAccount.isUserAccount().equals(1)) ? "true" : "false";
		userAccount.setText(sUserAccount);

		// typeName
		Element typeName = new Element("typeName", namespace);
		account.addContent(typeName);
		String sTypeName = aAccount.getTypeName();
		typeName.setText(sTypeName);

		// default
		Element _default = new Element("default", namespace);
		account.addContent(_default);
		String sDefault = (aAccount.isDefault()) ? "true" : "false";
		_default.setText(sDefault);

		return account;
	}
}
