/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.util;

import com.xbriage.templates.TemplatePanel;
import java.awt.Component;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import javax.swing.JEditorPane;
import javax.swing.text.html.HTMLEditorKit;

/**
 *
 * @author shughes
 */
public class ViewEditorPane extends JEditorPane implements DropTargetListener {

	private DropTarget dropTarget;

	public ViewEditorPane(String type, String text) {
		super(type, text);
		setup();
	}

	public ViewEditorPane() {
		super();
		setup();
	}

	private void setup() {
		dropTarget = new DropTarget(this, this);
		setDropTarget(dropTarget);
	}

	public void dragEnter(DropTargetDragEvent dtde) {

	}

	public void dragOver(DropTargetDragEvent dtde) {

	}

	public void dropActionChanged(DropTargetDragEvent dtde) {

	}

	public void dragExit(DropTargetEvent dte) {

	}

	public void drop(DropTargetDropEvent dtde) {
		Component c = this.getParent();
		while (true) {
			if (c instanceof TemplatePanel) {
				break;
			} else if (c == null) {
				break;
			} else {
				c = c.getParent();
			}
		}
		TemplatePanel panel = (TemplatePanel) c;
		panel.drop(dtde);
	}
	}
