/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.util;

import com.xbriage.services.Service;
import com.xbriage.context.EmfContext;
import com.xbriage.services.AccountDao;
import com.xbriage.domain.Account;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

/**
 *
 * @author shughes
 */
public class AccountsComboBox extends JComboBox implements AccountsListener {

	private String query;

	public AccountsComboBox(String query) {
		super();
		this.query = query;
		updateAccounts(null, null, AccountsListener.UPDATE);
	}

	public void updateAccounts(String oldName, String newName, int action) {
		EntityManager em = EmfContext.create();
		List<Account> result = em.createNamedQuery(query).getResultList();
		List<String> names = new ArrayList<String>();
		AccountDao dao = Service.load(AccountDao.class);
		for (Account acct : result) {
			String name = dao.concatName(acct);
			names.add(name);
		}
		Collections.sort(names);
		setModel(new DefaultComboBoxModel(names.toArray()));
	}
}
