/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.util;

import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

/**
 *
 * @author shughes
 */
public class OfxParser {

	public static final String CHECKING = "CHECKING";
	public static final String CREDITCARD = "CREDIT CARD";
	public static final String SAVINGS = "SAVINGS";
	
	private String xmlString;
	private Document document;
	private List<OfxTrnNode> transactions;

	public OfxParser(String file) {
		FileReader reader = null;
		try {
			reader = new FileReader(file);
			Ofx2Xml ofx2Xml = new Ofx2Xml(reader);
			xmlString = ofx2Xml.getXmlString();

			SAXBuilder builder = new SAXBuilder();
			document = builder.build(new StringReader(xmlString));

		} catch (Exception ex) {
			Logger.getLogger(OfxParser.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			try {
				reader.close();
			} catch (IOException ex) {
				Logger.getLogger(OfxParser.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	public String getAccountType() {
		Element ofx = document.getRootElement();
		Element bankMsg = ofx.getChild("BANKMSGSRSV1");
		if (bankMsg != null) {
			return CHECKING;
		}
		Element ccMsg = ofx.getChild("CREDITCARDMSGSRSV1");
		if (ccMsg != null) {
			return CREDITCARD;
		}
		return null;
	}

	private String status(Element trns) {
		Element status = trns.getChild("STATUS");
		Element code = status.getChild("CODE");
		String sCode = code.getTextTrim();
		return sCode;
	}

	public List<OfxTrnNode> readCCTransactions() throws Exception {
		Element ofx = document.getRootElement();
		Element ccMsg = ofx.getChild("CREDITCARDMSGSRSV1");
		Element ccTrns = ccMsg.getChild("CCSTMTTRNRS");

		String status = status(ccTrns);
		if (status.equals("0")) {
			Element ccStmt = ccTrns.getChild("CCSTMTRS");
			Element bankList = ccStmt.getChild("BANKTRANLIST");
			Element from = ccStmt.getChild("CCACCTFROM");
			Element acctId = from.getChild("ACCTID");

			List<Element> stmts = bankList.getChildren("STMTTRN");
			readTransactions(stmts);
		} else {
			throw new Exception(friendlyStatus(status));
		}

		return transactions;
	}

	private void readTransactions(List<Element> stmtList) {
		transactions = new ArrayList<OfxTrnNode>();

		for (Element stmt : stmtList) {
			Element fitId = stmt.getChild("FITID");
			Element type = stmt.getChild("TRNTYPE");
			Element date = stmt.getChild("DTPOSTED");
			Element amount = stmt.getChild("TRNAMT");
			Element name = stmt.getChild("NAME");
			Element payee = stmt.getChild("PAYEE");
			Element memo = stmt.getChild("MEMO");
			Element checkNo = stmt.getChild("CHECKNUM");

			OfxTrnNode node = new OfxTrnNode();

			if (fitId != null) {
				node.setFitId(fitId.getTextTrim());
			}

			if (type != null) {
				node.setType(type.getTextTrim());
			}

			if (date != null) {
				node.setDate(date.getTextTrim());
			}

			if (amount != null) {
				node.setAmount(amount.getTextTrim());
			}

			if (name != null) {
				node.setName(name.getTextTrim());
			} else if (payee != null) {
				node.setName(payee.getTextTrim());
			}

			if (memo != null) {
				node.setMemo(memo.getTextTrim());
			}

			if (checkNo != null) {
				node.setCheckNo(checkNo.getTextTrim());
			}

			transactions.add(node);
		}
	}

	public String friendlyStatus(String status) {
		if (status.equals("2001")) {
			return "Invalid account";
		} else if (status.equals("2002")) {
			return "General account error";
		} else if (status.equals("2003")) {
			return "Account not found";
		} else if (status.equals("2004")) {
			return "Account closed";
		} else if (status.equals("2005")) {
			return "Account not authorized";
		} else if (status.equals("2019")) {
			return "Duplicate request";
		} else if (status.equals("2020")) {
			return "Invalid date";
		} else if (status.equals("2027")) {
			return "Invalid date range";
		} else if (status.equals("15500")) {
			return "An incorrect username/password combination has been entered";
		}
		return null;
	}

	public List<OfxTrnNode> readBankTransactions() throws Exception {
		Element ofx = document.getRootElement();
		Element bankMsg = ofx.getChild("BANKMSGSRSV1");
		Element stmtTrn = bankMsg.getChild("STMTTRNRS");
		String status = status(stmtTrn);
		if (status.equals("0")) {
			Element stmtRs = stmtTrn.getChild("STMTRS");
			Element acctFrom = stmtRs.getChild("BANKACCTFROM");
			Element acctId = acctFrom.getChild("ACCTID");
			Element bankTrans = stmtRs.getChild("BANKTRANLIST");

			List<Element> stmtList = bankTrans.getChildren("STMTTRN");
			readTransactions(stmtList);
		} else {
			throw new Exception(friendlyStatus(status));
		}
		return transactions;
	}

	public List<OfxAccountInfo> readAccountInfo() throws Exception {
		Element ofx = document.getRootElement();
		Element signUp = ofx.getChild("SIGNUPMSGSRSV1");
		Element infoTrns = signUp.getChild("ACCTINFOTRNRS");
		Element infoRs = infoTrns.getChild("ACCTINFORS");
		List<Element> acctInfoList = infoRs.getChildren("ACCTINFO");
		List<OfxAccountInfo> list = new ArrayList<OfxAccountInfo>();
		if (acctInfoList != null) {
			for (Element acctInfo : acctInfoList) {
				String sAccountId = null;
				String sBankId = null;
				String sAccountType = null;
				String name = null;

				Element desc = acctInfo.getChild("DESC");
				if (desc != null) {
					name = desc.getTextTrim();
				}

				Element bankInfo = acctInfo.getChild("BANKACCTINFO");
				if (bankInfo != null) {
					Element bankFrom = bankInfo.getChild("BANKACCTFROM");
					Element bankId = bankFrom.getChild("BANKID");
					sBankId = bankId.getTextTrim();
					Element acctId = bankFrom.getChild("ACCTID");
					sAccountId = acctId.getTextTrim();
					Element acctType = bankFrom.getChild("ACCTTYPE");
					sAccountType = acctType.getTextTrim();
				}

				Element ccInfo = acctInfo.getChild("CCACCTINFO");
				if (ccInfo != null) {
					Element ccFrom = ccInfo.getChild("CCACCTFROM");
					Element acctId = ccFrom.getChild("ACCTID");
					sAccountId = acctId.getTextTrim();
					sAccountType = CREDITCARD;
				}
				
				if(name == null) {
					name = sAccountType;
				}

				OfxAccountInfo node = new OfxAccountInfo(name, sBankId, sAccountId, sAccountType);
				list.add(node);
			}
		} else {
			throw new Exception("Account info is null");
		}
		return list;
	}

	public static void main(String[] args) {
		try {
			OfxParser parser = new OfxParser("/Users/shughes/Desktop/test.xml");
			List<OfxAccountInfo> infoList = parser.readAccountInfo();
			for (OfxAccountInfo info : infoList) {
				System.out.println("name: " + info.getName());
				if (info.getBankId() != null) {
					System.out.println("bank id: " + info.getBankId());
				}
			}

//		List<OfxTrnNode> transactions = parser.readBankTransactions();
//		for (OfxTrnNode node : transactions) {
//			System.out.println(node.getName());
//		}
//		Reader reader = null;
//		try {
//			reader = new FileReader("/Users/shughes/AppData/Local/Temp/EXPORT.ofx");
//			Ofx2Xml xml = new Ofx2Xml(reader);
//
//			BufferedWriter writer = new BufferedWriter(new FileWriter("/Users/shughes/Desktop/test2.txt"));
//			writer.write(xml.getXmlString());
//
//		} catch (Exception ex) {
//			Logger.getLogger(OfxParser.class.getName()).log(Level.SEVERE, null, ex);
//		} finally {
//			try {
//				reader.close();
//			} catch (IOException ex) {
//				Logger.getLogger(OfxParser.class.getName()).log(Level.SEVERE, null, ex);
//			}
//		}
		} catch (Exception ex) {
			Logger.getLogger(OfxParser.class.getName()).log(Level.SEVERE, null, ex);
		}

//		List<OfxTrnNode> transactions = parser.readBankTransactions();
//		for (OfxTrnNode node : transactions) {
//			System.out.println(node.getName());
//		}

//		Reader reader = null;
//		try {
//			reader = new FileReader("/Users/shughes/AppData/Local/Temp/EXPORT.ofx");
//			Ofx2Xml xml = new Ofx2Xml(reader);
//			
//			BufferedWriter writer = new BufferedWriter(new FileWriter("/Users/shughes/Desktop/test2.txt"));
//			writer.write(xml.getXmlString());
//			
//		} catch (Exception ex) {
//			Logger.getLogger(OfxParser.class.getName()).log(Level.SEVERE, null, ex);
//		} finally {
//			try {
//				reader.close();
//			} catch (IOException ex) {
//				Logger.getLogger(OfxParser.class.getName()).log(Level.SEVERE, null, ex);
//			}
//		}

	}
}
