/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 *
 * @author shughes
 */
public class DesEncrypter {

	private Cipher ecipher;
	private Cipher dcipher;
	private int iterationCount = 19;
	private byte[] salt = {
		(byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32,
		(byte) 0x56, (byte) 0x35, (byte) 0xE3, (byte) 0x03
	};
	private static DesEncrypter instance = new DesEncrypter("lkdjdslfjafjasdklasadl");

	public static DesEncrypter getInstance() {
		return instance;
	}

	protected DesEncrypter(String passPhrase) {
		try {
			KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(), salt, iterationCount);
			SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);
			ecipher = Cipher.getInstance(key.getAlgorithm());
			dcipher = Cipher.getInstance(key.getAlgorithm());

			AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);

			ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
			dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);

		} catch (InvalidKeyException ex) {
			Logger.getLogger(DesEncrypter.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InvalidAlgorithmParameterException ex) {
			Logger.getLogger(DesEncrypter.class.getName()).log(Level.SEVERE, null, ex);
		} catch (NoSuchPaddingException ex) {
			Logger.getLogger(DesEncrypter.class.getName()).log(Level.SEVERE, null, ex);
		} catch (NoSuchAlgorithmException ex) {
			Logger.getLogger(DesEncrypter.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InvalidKeySpecException ex) {
			Logger.getLogger(DesEncrypter.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public String encrypt(String str) {
		if (str != null) {
			try {
				byte[] utf8 = str.getBytes("UTF8");
				// Encrypt
				byte[] enc = ecipher.doFinal(utf8);
				return new BASE64Encoder().encode(enc);

			} catch (IllegalBlockSizeException ex) {
				Logger.getLogger(DesEncrypter.class.getName()).log(Level.SEVERE, null, ex);
			} catch (BadPaddingException ex) {
				Logger.getLogger(DesEncrypter.class.getName()).log(Level.SEVERE, null, ex);
			} catch (UnsupportedEncodingException ex) {
				Logger.getLogger(DesEncrypter.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return null;
	}

	public String decrypt(String str) {
		if (str != null) {
			try {

				byte[] dec = new BASE64Decoder().decodeBuffer(str);
				// Decrypt
				byte[] utf8 = dcipher.doFinal(dec);
				// Decode using utf-8
				return new String(utf8, "UTF8");

			} catch (IllegalBlockSizeException ex) {
				Logger.getLogger(DesEncrypter.class.getName()).log(Level.SEVERE, null, ex);
			} catch (BadPaddingException ex) {
				Logger.getLogger(DesEncrypter.class.getName()).log(Level.SEVERE, null, ex);
			} catch (IOException ex) {
				Logger.getLogger(DesEncrypter.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return null;
	}

	public static void main(String[] args) throws NoSuchAlgorithmException {
		String passPhrase = "my secret pass phrase";
		DesEncrypter encrypter = new DesEncrypter(passPhrase);
		String encrypted = encrypter.encrypt("dog");
		String decrypted = encrypter.decrypt(encrypted);
		System.out.println("encrypted: " + encrypted);
		System.out.println("decrypted: " + decrypted);
	}
}
