/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author shughes
 */
public class HtmlTable {
	StringBuffer html = new StringBuffer();
	private List<List<String>> data = new ArrayList<List<String>>();
	private Map<Integer, String> colAlign = new HashMap<Integer, String>();
	private int tableWidth = 500;
	
	public HtmlTable() {
		
	}
	
	@Override
	public String toString() {
		buildTable();
		return html.toString();
	}
	
	private void buildTable() {
		html = html.append("<table width=\""+tableWidth+"\">");
		for(int i = 0; i < data.size(); i++) {
			html = html.append("<tr>");
			List<String> cols = data.get(i);
			for(int j = 0; j < cols.size(); j++) {
				String value = cols.get(j);
				String align = "center";
				if(colAlign.get(j) != null) {
					align = colAlign.get(j);
				}
				html = html.append("<td align=\""+align+"\">");
				html = html.append(value);
				html = html.append("</td>");
			}
			html = html.append("</tr>");
		}
		html = html.append("</table>");
	}

	public List<List<String>> getData() {
		return data;
	}

	public void setData(List<List<String>> data) {
		this.data = data;
	}

	public Map<Integer, String> getColAlign() {
		return colAlign;
	}

	public void setColAlign(Map<Integer, String> columnAlign) {
		this.colAlign = columnAlign;
	}

	public int getTableWidth() {
		return tableWidth;
	}

	public void setTableWidth(int tableWidth) {
		this.tableWidth = tableWidth;
	}

}
