/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.util;

import com.xbriage.app.WorkbooksApp;
import com.xbriage.modules.journal.*;
import com.xbriage.domain.Account;
import com.xbriage.domain.JournalEntry;
import com.xbriage.domain.JournalEntryRow;
import com.xbriage.domain.JournalEntryRow;
import com.xbriage.domain.JournalEntryRow;
import com.xbriage.app.WorkbooksView;
import com.xbriage.config.Config;
import com.xbriage.services.Module;
import java.awt.Color;
import java.awt.Component;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import org.jdesktop.application.LocalStorage;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

/**
 *
 * @author shughes
 */
public class GeneralUtil {

	private static String[] entryTypes = null;
	private static String[] accountTypes = null;
	
	public static int compareVersions(String v1, String v2) {
		String[] arr = v1.split("\\.");
		String num = "";
		for (String s : arr) {
			num = num + s;
		}
		Integer iv1 = new Integer(num);

		num = "";
		arr = v2.split("\\.");
		for (String s : arr) {
			num = num + s;
		}
		Integer iv2 = new Integer(num);

		return iv1.compareTo(iv2);
	}
	
	public static int compareVersions(Module mod1, Module mod2) {
		String v1 = mod1.getVersion();
		String v2 = mod2.getVersion();
		return compareVersions(v1, v2);
	}
	
	public static List<String> getDates() {
		GregorianCalendar cal = new GregorianCalendar();
		Date date = new Date();
		cal.setTime(date);
		cal.set(Calendar.DATE, 1);
		if(cal.get(Calendar.MONTH) == 11) {
			cal.roll(Calendar.YEAR, true);
		}
		cal.roll(Calendar.MONTH, true);
		List<String> days = new ArrayList<String>();
		for(int i = 0; i < 48; i++) {
			Date d = cal.getTime();
			String sDate = GeneralUtil.dateToString(d);
			days.add(sDate);
			if(cal.get(Calendar.MONTH) == 0) {
				cal.roll(Calendar.YEAR, false);
			}
			cal.roll(Calendar.MONTH, false);
		}
		return days;
	}
	
	public static List<String> getBudgetDates() {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(new Date());
		cal.set(Calendar.DATE, 1);
		for(int i = 0; i < 2; i++) {
			cal.roll(Calendar.YEAR, true);
		}
		List<String> days = new ArrayList<String>();
		for(int i = 0; i < 48; i++) {
			Date d = cal.getTime();
			String sDate = GeneralUtil.dateToString(d);
			days.add(sDate);
			if(cal.get(Calendar.MONTH) == 0) {
				cal.roll(Calendar.YEAR, false);
			}
			cal.roll(Calendar.MONTH, false);
		}
		return days;
	}

	public static String getDbFile() {
		LocalStorage ls = WorkbooksApp.getApplication().getContext().getLocalStorage();
		String dbFile = ls.getDirectory().getPath() + File.separator + "db" + File.separator + "xbriage";
		return dbFile;
	}

	public static String getLogFile() {
		LocalStorage ls = WorkbooksApp.getApplication().getContext().getLocalStorage();
		String file = ls.getDirectory().getAbsolutePath() + File.separator + "xbriage.log";
		return file;
	}

	public static boolean fileExists(String file) {
		File f = new File(file);
		if (!f.exists() || f.isDirectory()) {
			return false;
		} else {
			return true;
		}
	}

	public static void paintComponent(Graphics g, Component p) {
		Graphics2D g2 = (Graphics2D) g;
		Color c = WorkbooksApp.getInstance().getContext().getResourceMap().getColor("Application.headerColor");
		Color c2 = WorkbooksApp.getInstance().getContext().getResourceMap().getColor("Application.headerColor2");
		GradientPaint paint = new GradientPaint(0, 0, c2, 0, 20, c);
		g2.setPaint(paint);
		g2.fillRect(0, 0, p.getWidth(), 20);

	}

	public static void moveRow(JTable table, int row) {
		table.removeEditor();
		if (row < 0) {
			System.out.println("bad row: " + row);
			row = 0;
		}
		table.changeSelection(row, 0, false, false);
		table.editCellAt(row, 0);
	}

	public static String formatRedString(String str) {
		return "<html><font color=red><b>" + str + "</b></font></html>";
	}

	public static String formatGreenString(String str) {
		return "<html><font color=green><b>" + str + "</b></font></html>";
	}

	public static String formatGrayString(String str) {
		return "<html><font color=gray>" + str + "</font></html>";
	}

	public static String formatStringNumber(String num) {
		return formatNumber(num, "####0.00");
	}

	public static String formatNumber(String num, String pattern) {
		DecimalFormat df = new DecimalFormat();
		df.applyPattern(pattern);
		BigDecimal bigDecimal = new BigDecimal(num);
		String sBigDecimal = df.format(bigDecimal);
		return sBigDecimal;
	}

	public static String formatStringNumberWithCommas(String num) {
		return formatNumber(num, "##,##0.00");
	}
	
	public static String removeCommas(String num) {
		String[] arr = num.split(",");
		StringBuffer buf = new StringBuffer();
		for(String str : arr) {
			buf.append(str);
		}
		return buf.toString();
	}

	public static Date ofxDatetoDate(String ofx) {
		String year = ofx.substring(0, 4);
		String month = ofx.substring(4, 6);
		String day = ofx.substring(6, 8);
		return stringToDate(year + "-" + month + "-" + day);
	}

	public static String formatOfxDate(String year, String month, String day) {
		String sDate = "";
		String sYear = year;
		String sMonth = formatNum(new Integer(month));
		String sDay = formatNum(new Integer(day));
		String sHour = "00";
		String sMin = "00";
		String sSec = "00";
		sDate = sYear + sMonth + sDay + sHour + sMin + sSec;
		return sDate;
	}

	private static String formatNum(int num) {
		String sNum = new Integer(num).toString();
		sNum = (num < 10) ? "0" + sNum : sNum;
		return sNum;
	}

	public static BigDecimal stringToNumber(String str) {
		boolean isNull = false;
		if (str == null) {
			isNull = true;
			str = "";
		}
		str = str.trim();
		if (str.equals("")) {
			isNull = true;
		}
		if (isNull) {
			str = "0";
		}
		str = str.trim();
		String[] arr = str.split(",");
		String sNum = "";
		for (String part : arr) {
			sNum = sNum + part;
		}
		BigDecimal num = new BigDecimal(sNum);
		return num;
	}

	public static Date stringToDate(String sDate) {
		Calendar calendar = Calendar.getInstance();
		String[] dateArr = sDate.split("-");
		if (dateArr.length == 3) {
			boolean useToday = false;
			Pattern p = Pattern.compile("\\d+");

			Matcher m = p.matcher(dateArr[0]);
			useToday = (!m.matches()) ? true : false;

			m = p.matcher(dateArr[1]);
			useToday = (!m.matches()) ? true : false;

			m = p.matcher(dateArr[2]);
			useToday = (!m.matches()) ? true : false;

			if (!useToday) {
				Integer year = new Integer(dateArr[0]);
				Integer month = new Integer(dateArr[1]);
				month = month - 1; // 0-based index
				Integer day = new Integer(dateArr[2]);
				calendar.set(year, month, day);
			} else {
				calendar.setTime(new Date());
			}
		}
		return calendar.getTime();
	}

	public static String dateToString(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		Integer month = calendar.get(Calendar.MONTH) + 1;
		Integer day = calendar.get(Calendar.DAY_OF_MONTH);
		Integer year = calendar.get(Calendar.YEAR);
		String sDate = year.toString() + "-" + month.toString() + "-" + day.toString();
		return sDate;
	}

	public static String[] getEntryTypes() {
		if (entryTypes == null) {
			try {
				InputStream input = Config.class.getResourceAsStream("EntryTypes.xml");
				SAXBuilder builder = new SAXBuilder();
				Document doc = builder.build(input);
				Element _entryTypes = doc.getRootElement();
				List<Element> typeList = _entryTypes.getChildren("type");
				entryTypes = new String[typeList.size()];
				for (Element type : typeList) {
					Attribute id = type.getAttribute("id");
					int index = id.getIntValue();
					entryTypes[index] = type.getTextTrim();
				}
				input.close();
			} catch (JDOMException ex) {
				Logger.getLogger(GeneralUtil.class.getName()).log(Level.SEVERE, null, ex);
			} catch (IOException ex) {
				Logger.getLogger(GeneralUtil.class.getName()).log(Level.SEVERE, null, ex);
			}
		}

		return entryTypes;
	}

	public static String getType(int key) {
		if (key < getEntryTypes().length) {
			return getEntryTypes()[key];
		} else {
			return getEntryTypes()[0];
		}
	}

	public static int getType(String key) {
		for (int i = 0; i < getEntryTypes().length; i++) {
			if (key.equals(getEntryTypes()[i])) {
				return i;
			}
		}
		return 0;
	}

	public static String[] getAccountTypes() {
		if (accountTypes == null) {
			try {
				InputStream input = Config.class.getResourceAsStream("AccountTypes.xml");
				SAXBuilder builder = new SAXBuilder();
				Document doc = builder.build(input);
				Element types = doc.getRootElement();
				List<Element> typeList = types.getChildren("type");
				accountTypes = new String[typeList.size()];
				for (Element type : typeList) {
					Attribute id = type.getAttribute("id");
					accountTypes[id.getIntValue()] = type.getTextTrim();
				}
				input.close();
			} catch (JDOMException ex) {
				Logger.getLogger(GeneralUtil.class.getName()).log(Level.SEVERE, null, ex);
			} catch (IOException ex) {
				Logger.getLogger(GeneralUtil.class.getName()).log(Level.SEVERE, null, ex);
			}

		}

		return accountTypes;
	}

	public static boolean showWarningAlert(String message) {
		boolean confirm = false;
		int n = JOptionPane.showConfirmDialog(WorkbooksView.getInstance().getComponent(),
				message,
				"Warning", JOptionPane.YES_NO_OPTION);
		if (n == 0) { // yes
			confirm = true;
		} else { // no
			confirm = false;
		}
		return confirm;
	}

	public static void showWarningAlert2(String message) {
		JOptionPane.showMessageDialog(WorkbooksView.getInstance().getComponent(),
				message, "Alert", JOptionPane.INFORMATION_MESSAGE);
	}

	public static JournalEntryRow getSelectedAccountEntryRow(JournalEntry entry, Account account) {
		JournalEntryRow theRow = null;
		for (JournalEntryRow row : entry.getRows()) {
			if (row.getAccount().equals(account)) {
				theRow = row;
				break;
			}
		}
		return theRow;
	}

	public static void main(String[] args) {
		System.out.println(GeneralUtil.removeCommas("2,738,038.00"));
	}
}
