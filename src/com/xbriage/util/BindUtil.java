/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.util;

import com.xbriage.services.Responder;
import com.xbriage.util.MethodResponderImpl;
import com.xbriage.util.MethodResponder;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;
import org.jdesktop.swingx.JXTreeTable;

/**
 *
 * @author shughes
 */
public class BindUtil {

	public final static int ACTION = 0;
	public final static int KEY_RELEASE = 1;
	public final static int KEY_PRESSED = 2;
	public final static int KEY_TYPED = 3;
	public final static int TABLE_MODEL_CHANGED = 4;
	public final static int LIST_SELECTION_CHANGED = 5;
	public final static int MOUSE_PRESSED = 6;
	public final static int MOUSE_WHEEL = 7;
	public final static int TREE_NODE_CHANGED = 8;
	public final static int COMPONENT_MOVED = 9;
	public final static int COMPONENT_RESIZED = 10;
	public final static int PROPERTY_CHANGED = 11;
	public final static int FOCUS_LOST = 12;
	public final static int FOCUS_GAINED = 13;
	public final static int XTREE_SELECTION_CHANGED = 14;
	public final static int MOUSE_RELEASED = 15;

	public static void propertyBind(Object object, String methodName, Component component, String changeName) {
		propertyBind(new Object[]{object}, new String[]{methodName}, component, changeName);
	}

	public static void propertyBind(Object object, String[] methodNames, Component component, String changeName) {
		Object[] objects = new Object[methodNames.length];
		for (int i = 0; i < methodNames.length; i++) {
			objects[i] = object;
		}
		propertyBind(objects, methodNames, component, changeName);
	}

	public static void propertyBind(final Object[] objects, final String[] methodNames, Component component, String changeName) {
		component.addPropertyChangeListener(changeName, new PropertyChangeListener() {

			public void propertyChange(PropertyChangeEvent evt) {
				for (int i = 0; i < objects.length; i++) {
					final Object object = objects[i];
					final String methodName = methodNames[i];
					try {
						Class clazz = object.getClass();
						final Method m = clazz.getMethod(methodName, new Class[]{PropertyChangeEvent.class});
						m.invoke(object, new Object[]{evt});
					} catch (IllegalAccessException ex) {
						Logger.getLogger(BindUtil.class.getName()).log(Level.SEVERE, null, ex);
					} catch (IllegalArgumentException ex) {
						Logger.getLogger(BindUtil.class.getName()).log(Level.SEVERE, null, ex);
					} catch (InvocationTargetException ex) {
						Logger.getLogger(BindUtil.class.getName()).log(Level.SEVERE, null, ex);
					} catch (NoSuchMethodException ex) {
						Logger.getLogger(BindUtil.class.getName()).log(Level.SEVERE, null, ex);
					} catch (SecurityException ex) {
						Logger.getLogger(BindUtil.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			}
			});
	}

	public static void bind(Object object, String methodName, Object component, int type) {
		bind(new Object[]{object}, new String[]{methodName}, component, type);
	}

	public static void bind(Object object, String[] methodNames, Object component, int type) {
		Object[] objects = new Object[methodNames.length];
		for (int i = 0; i < methodNames.length; i++) {
			objects[i] = object;
		}
		bind(objects, methodNames, component, type);
	}

	public static void bind(Object[] objects, String[] methodNames, Object component, int type) {
		List<Responder> responders = new ArrayList<Responder>();
		for (int i = 0; i < objects.length; i++) {
			final MethodResponder responder = new MethodResponderImpl();
			responder.setObject(objects[i]);
			responder.setMethodName(methodNames[i]);
			responder.setArgTypes(new Class[]{Map.class});
			responders.add(responder);
		}
		classBind(component, responders, type, new HashMap());
	}

	public static void classBind(Object component, final Responder response, int type) {
		List<Responder> responders = new ArrayList<Responder>();
		responders.add(response);
		classBind(component, responders, type, new HashMap());
	}

	public static void classBind(Object component, final Responder[] responders, int type) {
		List<Responder> newResponders = new ArrayList<Responder>();
		for (Responder responder : responders) {
			newResponders.add(responder);
		}
		classBind(component, newResponders, type, new HashMap());
	}

	public static void classBind(Object object, final List<Responder> responders, final int type, final Map params) {
		try {
			Class clazz = object.getClass();
			if (type == ACTION) {
				Method m = clazz.getMethod("addActionListener", new Class[]{ActionListener.class});
				ActionListener listener = new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						params.put(ActionEvent.class, e);
						for (Responder response : responders) {
							response.perform(params);
						}
					}
				};
				m.invoke(object, new Object[]{listener});
			} else if ((type == KEY_RELEASE) || (type == KEY_PRESSED) || (type == KEY_TYPED)) {
				Method m = clazz.getMethod("addKeyListener", new Class[]{KeyListener.class});
				KeyListener listener = new KeyListener() {

					public void keyTyped(final KeyEvent e) {
						if (type == KEY_TYPED) {
							params.put(KeyEvent.class, e);
							for (Responder response : responders) {
								response.perform(params);
							}
						}
					}

					public void keyPressed(final KeyEvent e) {
						if (type == KEY_PRESSED) {
							params.put(KeyEvent.class, e);
							for (Responder response : responders) {
								response.perform(params);
							}
						}
					}

					public void keyReleased(final KeyEvent e) {
						if (type == KEY_RELEASE) {
							params.put(KeyEvent.class, e);
							for (Responder response : responders) {
								response.perform(params);
							}
						}
					}
				};
				m.invoke(object, new Object[]{listener});
			} else if (type == TABLE_MODEL_CHANGED) {
				Method m = clazz.getMethod("addTableModelListener", new Class[]{TableModelListener.class});
				TableModelListener listener = new TableModelListener() {

					public void tableChanged(final TableModelEvent e) {
						params.put(TableModelEvent.class, e);
						for (Responder response : responders) {
							response.perform(params);
						}
					}
				};
				m.invoke(object, new Object[]{listener});
			} else if (type == LIST_SELECTION_CHANGED) {
				Method m = clazz.getMethod("addListSelectionListener", new Class[]{ListSelectionListener.class});
				ListSelectionListener listener = new ListSelectionListener() {

					public void valueChanged(final ListSelectionEvent e) {
						params.put(ListSelectionEvent.class, e);
						for (Responder response : responders) {
							response.perform(params);
						}
					}
				};
				m.invoke(object, new Object[]{listener});
			} else if (type == MOUSE_PRESSED || type == MOUSE_RELEASED) {
				Method m = clazz.getMethod("addMouseListener", new Class[]{MouseListener.class});
				MouseListener listener = new MouseListener() {

					public void mouseClicked(MouseEvent e) {
					}

					public void mousePressed(final MouseEvent e) {
						if (type == MOUSE_PRESSED) {
							params.put(MouseEvent.class, e);
							for (Responder responder : responders) {
								responder.perform(params);
							}
						}
					}

					public void mouseReleased(MouseEvent e) {
						if (type == MOUSE_RELEASED) {
							params.put(MouseEvent.class, e);
							for (Responder responder : responders) {
								responder.perform(params);
							}
						}
					}

					public void mouseEntered(MouseEvent e) {
					}

					public void mouseExited(MouseEvent e) {
					}
				};
				m.invoke(object, new Object[]{listener});
			} else if (type == MOUSE_WHEEL) {
				Method m = clazz.getMethod("addMouseWheelListener", new Class[]{MouseWheelListener.class});
				MouseWheelListener listener = new MouseWheelListener() {

					public void mouseWheelMoved(final MouseWheelEvent e) {
						params.put(MouseWheelEvent.class, e);
						for (Responder responder : responders) {
							responder.perform(params);
						}
					}
				};
				m.invoke(object, new Object[]{listener});
			} else if (type == COMPONENT_MOVED || type == COMPONENT_RESIZED) {
				Method m = clazz.getMethod("addComponentListener", new Class[]{ComponentListener.class});
				ComponentListener listener = new ComponentListener() {

					public void componentResized(final ComponentEvent e) {
						if (type == COMPONENT_RESIZED) {
							params.put(ComponentEvent.class, e);
							for (Responder response : responders) {
								response.perform(params);
							}
						}
					}

					public void componentMoved(final ComponentEvent e) {
						if (type == COMPONENT_MOVED) {
							params.put(ComponentEvent.class, e);
							for (Responder response : responders) {
								response.perform(params);
							}
						}
					}

					public void componentShown(ComponentEvent e) {
					}

					public void componentHidden(ComponentEvent e) {
					}
				};
				m.invoke(object, new Object[]{listener});
			} else if (type == TREE_NODE_CHANGED) {
				Method m = clazz.getMethod("addTreeModelListener", new Class[]{TreeModelListener.class});
				TreeModelListener listener = new TreeModelListener() {

					public void treeNodesChanged(final TreeModelEvent e) {
						if (type == TREE_NODE_CHANGED) {
							params.put(TreeModelEvent.class, e);
							for (Responder response : responders) {
								response.perform(params);
							}
						}
					}

					public void treeNodesInserted(TreeModelEvent e) {
					}

					public void treeNodesRemoved(TreeModelEvent e) {
					}

					public void treeStructureChanged(TreeModelEvent e) {
					}
				};
				m.invoke(object, new Object[]{listener});
			} else if (type == PROPERTY_CHANGED) {
				Method m = clazz.getMethod("addPropertyChangeSupport", new Class[]{PropertyChangeListener.class});
				PropertyChangeListener listener = new PropertyChangeListener() {

					public void propertyChange(final PropertyChangeEvent evt) {
						params.put(PropertyChangeEvent.class, evt);
						for (Responder responder : responders) {
							responder.perform(params);
						}
					}
				};
				m.invoke(object, new Object[]{listener});
			} else if (type == FOCUS_LOST || type == FOCUS_GAINED) {
				Method m = clazz.getMethod("addFocusListener", new Class[]{FocusListener.class});
				FocusListener listener = new FocusListener() {

					public void focusGained(final FocusEvent e) {
						if (type == FOCUS_GAINED) {
							params.put(FocusEvent.class, e);
							for (Responder responder : responders) {
								responder.perform(params);
							}
						}
					}

					public void focusLost(final FocusEvent e) {
						if (type == FOCUS_LOST) {
							params.put(FocusEvent.class, e);
							for (Responder responder : responders) {
								responder.perform(params);
							}
						}
					}
				};
				m.invoke(object, new Object[]{listener});
			} else if (type == XTREE_SELECTION_CHANGED) {
				Method m = clazz.getMethod("addTreeSelectionListener", new Class[]{TreeSelectionListener.class});
				TreeSelectionListener listener = new TreeSelectionListener() {

					public void valueChanged(final TreeSelectionEvent e) {
						params.put(TreeSelectionEvent.class, e);
						for (Responder response : responders) {
							response.perform(params);
						}
					}
				};
				m.invoke(object, new Object[]{listener});
			}
		} catch (IllegalAccessException ex) {
			Logger.getLogger(BindUtil.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalArgumentException ex) {
			Logger.getLogger(BindUtil.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InvocationTargetException ex) {
			Logger.getLogger(BindUtil.class.getName()).log(Level.SEVERE, null, ex);
		} catch (NoSuchMethodException ex) {
			Logger.getLogger(BindUtil.class.getName()).log(Level.SEVERE, null, ex);
		} catch (SecurityException ex) {
			Logger.getLogger(BindUtil.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	// @todo
	public static void selectEmptyRow(final JTable table) {
		table.addMouseListener(new MouseListener() {

			public void mouseClicked(MouseEvent e) {
			}

			public void mousePressed(MouseEvent e) {
				int index = table.rowAtPoint(e.getPoint());
				if (index == -1) {
					table.clearSelection();
				}
			}

			public void mouseReleased(MouseEvent e) {
			}

			public void mouseEntered(MouseEvent e) {
			}

			public void mouseExited(MouseEvent e) {
			}
		});
	}

	public static void selectEmptyRow(final JTree tree) {
		tree.addMouseListener(new MouseListener() {

			public void mouseClicked(MouseEvent e) {

			}

			public void mousePressed(MouseEvent e) {
				TreePath path = tree.getPathForLocation(e.getX(), e.getY());
				if (path == null) {
					tree.clearSelection();
				//TreeSelectionModel model = tree.getSelectionModel();
				//model.clearSelection();
				}
			}

			public void mouseReleased(MouseEvent e) {

			}

			public void mouseEntered(MouseEvent e) {

			}

			public void mouseExited(MouseEvent e) {

			}
		});
	}

	public static void selectEmptyRow(final JXTreeTable tree) {
		tree.addMouseListener(new MouseListener() {

			public void mouseClicked(MouseEvent e) {

			}

			public void mousePressed(MouseEvent e) {
				TreePath path = tree.getPathForLocation(e.getX(), e.getY());
				if (path == null) {
					ListSelectionModel model = tree.getSelectionModel();
					model.clearSelection();
				}
			}

			public void mouseReleased(MouseEvent e) {

			}

			public void mouseEntered(MouseEvent e) {

			}

			public void mouseExited(MouseEvent e) {

			}
		});
	}

	public static void selectAllBind(final JTextField field) {
		field.addFocusListener(new FocusListener() {

			public void focusGained(FocusEvent e) {
				field.selectAll();
			}

			public void focusLost(FocusEvent e) {
			}
		});
	}
}
