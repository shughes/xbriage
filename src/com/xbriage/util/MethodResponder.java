/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.util;

import com.xbriage.services.Responder;
import com.xbriage.app.*;

/**
 *
 * @author shughes
 */
public interface MethodResponder extends Responder {
	public void setObject(Object obj);
	public Object getObject();
	public void setMethodName(String name);
	public String getMethodName();
	public void setArgTypes(Class[] types);
	public Class[] getArgTypes();
}
