/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.services;

import com.xbriage.util.*;
import java.util.List;

/**
 *
 * @author shughes
 */
public interface OfxBanks {
	public List<OfxBank> getBanks();
}
