/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.services;

import com.xbriage.util.OfxTrnNode;
import java.util.List;

/**
 *
 * @author shughes
 */
public interface OfxData {
	public void setTransactions(List<OfxTrnNode> nodes);
	public List<OfxTrnNode> getTransactions();
	public void save();
}
