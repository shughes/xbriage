/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.services.impl;

import com.xbriage.services.*;
import com.xbriage.context.EmfContext;
import com.xbriage.domain.Property;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author shughes
 */
public class SettingsDb implements Settings {
	
	Map<String, String> settings = null;

	public Map<String, String> get() {
		if(settings == null) {
			settings = new HashMap<String, String>();
 			EntityManager em = EmfContext.create();
			List<com.xbriage.domain.Property> all = em.createNamedQuery("Property.findAll").getResultList();
			for(com.xbriage.domain.Property map : all) {
				settings.put(map.getKey(), map.getValue());
			}
		}
		return settings;
	}

	public void save() {
		
		EntityManager em = EmfContext.create();
		List<Property> all = em.createNamedQuery("Property.findAll").getResultList();
		em.getTransaction().begin();
		for(Property prop : all) {
			String value = settings.get(prop.getKey());
			if(value == null) {
				em.remove(prop);
			}
		}
		em.getTransaction().commit();
		
		for(String key : settings.keySet()) {
			String value = settings.get(key);
			put(key, value);
		}
	}
	
	private String get(String key) {
		EntityManager em = EmfContext.create();
		Query q = em.createQuery("select m from Property m where m.key = :key");
		q.setParameter("key", key);
		List<Property> result = q.getResultList();
		if (result.size() == 0) {
			return null;
		}
		return result.get(0).getValue();
	}
	
	private void put(String key, String value) {
		EntityManager em = EmfContext.create();
		em.getTransaction().begin();
		Property map = null;
		String v = get(key);
		if (v == null) {
			map = new Property(key, value);
		} else {
			Query q = em.createQuery("select m from Property m where m.key = :key");
			q.setParameter("key", key);
			map = (Property) q.getSingleResult();
			map.setValue(value);
		}
		em.persist(map);
		em.getTransaction().commit();
		em.close();
	}

}
