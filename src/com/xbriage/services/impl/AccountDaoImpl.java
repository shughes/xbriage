/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.services.impl;

import com.xbriage.services.AccountDao;
import com.xbriage.domain.Account;
import com.xbriage.domain.JournalEntryRow;
import com.xbriage.context.EmfContext;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author shughes
 */
public class AccountDaoImpl implements AccountDao {

	private EntityManagerFactory emf = EmfContext.get();

	public static void main(String[] args) {
		System.out.println("colon: " + Character.getNumericValue(':'));
		System.out.println("a: " + Character.getNumericValue('a'));
	}

	public Account breakName(String oldName) {
		boolean toDefault = false;
		boolean userAccount = false;
		
		if (oldName == null) {
			toDefault = true;
		} else if (oldName.equals("")) {
			toDefault = true;
		}
		
		EntityManager em = EmfContext.create(); 
		
		if (toDefault) {
			Account _default = (Account) em.createNamedQuery("Account.findDefault").getSingleResult();
			oldName = _default.getName();
		}
		
		oldName = oldName.trim();
		if (oldName.charAt(0) == '[') {
			oldName = oldName.replace('[', ' ');
			oldName = oldName.replace(']', ' ');
			userAccount = true;
		}
		oldName = oldName.trim();
		String[] arr = oldName.split(":");
		
		Account parent = (Account) em.createNamedQuery("Account.findRoot").getSingleResult();
		Account account = null;
		
		for (int i = 0; i < arr.length; i++) {
			String name = arr[i];
			String sQ = "select a from Account a where a.parent = :parent and a.name = :name and a.userAccount = :userAccount";
			Query q = em.createQuery(sQ);
			q.setParameter("parent", parent);
			q.setParameter("name", name);
			
			if(userAccount) {
				q.setParameter("userAccount", 1);
			} else {
				q.setParameter("userAccount", 0);
			}
			
			List<Account> accL = q.getResultList();
			Account acc = null;
			if (accL.size() > 0) {
				acc = accL.get(0);
			}
			if (acc == null) {
				break;
			}
			if (i == arr.length - 1) {
				account = acc;
				break;
			}
			parent = acc;
		}
		
		if (account == null) {
			System.out.println("account is null");
		}
		
		return account;
	}

	public String concatName(Account account) {
		if (account != null) {
			String name = account.getName();
			boolean userAccount = false;
			if (account.isUserAccount().equals(1)) {
				userAccount = true;
			}
			account = account.getParent();
			if (account != null) {
				if (userAccount) {
					name = "[" + name;
				}
				while (!account.isRoot().equals(1)) {
					name = account.getName() + ":" + name;
					account = account.getParent();
				}
				if (userAccount) {
					name = name + "]";
				}
			}
			return name;
		} else {
			return null;
		}
	}
	
	public BigDecimal getBalance(Account account, Date start, Date end, boolean debitAdds) {
		EntityManager em = EmfContext.create();
		account = em.find(Account.class, account.getId());
		Query q = em.createQuery("select row from JournalEntryRow row where row.account = :account and row.journalEntry.date >= :date1 and row.journalEntry.date < :date2");
		q.setParameter("account", account);
		q.setParameter("date1", start);
		q.setParameter("date2", end);
		List<JournalEntryRow> entryRows = q.getResultList();
		BigDecimal balance = new BigDecimal(0);
		for (JournalEntryRow entryRow : entryRows) {
			if (entryRow.isDebitEntry().equals(1) && debitAdds) {
				balance = balance.add(entryRow.getAmount());
			} else if (entryRow.isDebitEntry().equals(1) && !debitAdds) {
				balance = balance.subtract(entryRow.getAmount());
			} else if (entryRow.isDebitEntry().equals(0) && debitAdds) {
				balance = balance.subtract(entryRow.getAmount());
			} else if (entryRow.isDebitEntry().equals(0) && !debitAdds) {
				balance = balance.add(entryRow.getAmount());
			}
		}
		return balance;
	}

	public BigDecimal getBalance(Account account) {
		return getBalance(account, true);
	}

	public BigDecimal getBalance(Account account, boolean debitAdds) {
		EntityManager em = emf.createEntityManager();
		account = em.find(Account.class, account.getId());
		List<JournalEntryRow> entryRows = em.createQuery("select j from JournalEntryRow j where j.account = :account").setParameter("account", account).getResultList();
		BigDecimal balance = new BigDecimal(0);
		for (JournalEntryRow entryRow : entryRows) {
			if (entryRow.isDebitEntry().equals(1) && debitAdds) {
				balance = balance.add(entryRow.getAmount());
			} else if (entryRow.isDebitEntry().equals(1) && !debitAdds) {
				balance = balance.subtract(entryRow.getAmount());
			} else if (entryRow.isDebitEntry().equals(0) && debitAdds) {
				balance = balance.subtract(entryRow.getAmount());
			} else if (entryRow.isDebitEntry().equals(0) && !debitAdds) {
				balance = balance.add(entryRow.getAmount());
			}
		}
		return balance;
	}
	
}
