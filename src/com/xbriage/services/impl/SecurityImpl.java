/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.services.impl;

import com.xbriage.services.*;
import com.xbriage.security.*;
import com.xbriage.services.Security;
import com.xbriage.app.Splash;
import com.xbriage.app.WorkbooksApp;
import com.xbriage.context.EmfContext;
import com.xbriage.context.SetupContext;
import com.xbriage.domain.JournalEntry;
import com.xbriage.domain.Workview;
import com.xbriage.util.DesEncrypter;
import com.xbriage.util.GeneralUtil;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;

/**
 *
 * @author shughes
 */
public class SecurityImpl implements Security {

	private String snKey = "Security.serialNumber";
	private String emailKey = "Security.email";

	public void verify() {
		String betaLabel = "<html><b>Xbriage Financial Software</b></html>";
		final Map<String, String> settings = SetupContext.getInstance().getSettings();
		boolean perform = false;

		if (settings.get(snKey) == null) {
			perform = true;
		} else {
			perform = true;
			boolean verified = GeneralSecurity.verified(settings.get(snKey), settings.get(emailKey));
			perform = (verified == true) ? false : true;
		}

		if (perform) {

			EntityManager em = EmfContext.create();
			List<JournalEntry> entries = em.createNamedQuery("JournalEntry.findAll").getResultList();
			List<Workview> workviews = em.createNamedQuery("Workview.findAll").getResultList();

			if (entries.size() == 0 && workviews.size() == 1) {
				GeneralSecurity.resetDate();
			}

			String key = settings.get("Security.expireDate");
			if (key == null) {
				key = "";
			}
			String sDate = DesEncrypter.getInstance().decrypt(key);
			String d = "";

			if (sDate.startsWith("date:")) {
				d = sDate.substring(5, sDate.length());
				Date date = GeneralUtil.stringToDate(d);
				if (date != null) {
					d = GeneralUtil.dateToString(date);
				} else {
					d = GeneralUtil.dateToString(new Date());
				}
			} else {
				d = GeneralUtil.dateToString(new Date());
			}

			Date end = GeneralUtil.stringToDate(d);
			Date start = new Date();
			long daysBetween = daysBetween(start, end);

			betaLabel = "<html><b>Xbriage Financial Software</b><br/>Trial ends in <b>" + daysBetween + "</b> days. Press OK to continue using trial. To register Xbriage, click the File menu then \"Registration...\" inside Xbriage, or click \"Register Online...\" below.</html>";

			SecurityDialog dialog = new SecurityDialog(Splash.getInstance(), true);
			dialog.getBetaLabel().setText(betaLabel);
			dialog.setVisible(true);

			int status = dialog.getReturnStatus();

			if (SecurityDialog.RET_CANCEL == status) {
				WorkbooksApp.getApplication().exit();
			} else {
				String sn = dialog.getOne().getText().trim();
				sn = sn + "-" + dialog.getTwo().getText().trim();
				sn = sn + "-" + dialog.getThree().getText().trim();
				sn = sn + "-" + dialog.getFour().getText().trim();
				sn = sn + "-" + dialog.getFive().getText().trim();
				sn = sn + "-" + dialog.getSix().getText().trim();
				String email = dialog.getEmailField().getText().trim();

				if (sn.equals("-----")) {
					sn = "1-----";
				}

				boolean verified = GeneralSecurity.verified(sn, email);

				if (!verified) {
					boolean showVerify = GeneralSecurity.dateExpired(key);
					if (showVerify) {
						verify();
					}
				} else {
					settings.put(snKey, sn);
					settings.put(emailKey, email);
				}
			}
		}
	}

	public long daysBetween(Date d1, Date d2) {
		long oneHour = 60 * 60 * 1000L;
		return ((d2.getTime() - d1.getTime() + oneHour) /
				(oneHour * 24));
	}
}
