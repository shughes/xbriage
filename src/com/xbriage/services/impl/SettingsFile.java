/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.services.impl;

import com.xbriage.services.*;
import com.xbriage.app.WorkbooksApp;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author shughes
 */
public class SettingsFile implements Settings {

	private Map<String, String> settings = null;
	private String settingsFile = "settingsFile.xml";
	private WorkbooksApp app = WorkbooksApp.getApplication();

	public Map<String, String> get() {
		if (settings == null) {
			try {
				settings = (Map<String, String>) app.getContext().getLocalStorage().load(settingsFile);
			} catch (IOException ex) {
				Logger.getLogger(SettingsFile.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return settings;
	}

	public void save() {
		try {
			app.getContext().getLocalStorage().save(settings, settingsFile);
		} catch (IOException ex) {
			Logger.getLogger(SettingsFile.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
