/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.services.impl;

import com.xbriage.app.TemplateData;
import com.xbriage.app.WorkbooksApp;
import com.xbriage.app.WorkbooksView;
import com.xbriage.context.EmfContext;
import com.xbriage.context.TemplateContext;
import com.xbriage.domain.ModuleIndex;
import com.xbriage.services.Module;
import com.xbriage.modules.ModuleIcon;
import com.xbriage.menus.MainMenu;
import com.xbriage.icons.Icons;
import com.xbriage.services.ModuleLoader;
import com.xbriage.services.Service;
import com.xbriage.services.Template;
import com.xbriage.templates.TemplatePanel;
import com.xbriage.util.GeneralUtil;
import com.xbriage.util.JarClassLoader;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

/**
 *
 * @author shughes
 */
public class ModuleLoaderImpl implements ModuleLoader {

	private Map<String, Module> map;

	public ModuleLoaderImpl() {
	}

	public void setup() {
		init();
	}

	private class ModuleComparator implements Comparator {

		public int compare(Object arg0, Object arg1) {
			Node arr1 = (Node) arg0;
			Node arr2 = (Node) arg1;
			String label1 = arr1.label;
			String label2 = arr2.label;
			return label1.compareTo(label2);
		}
	}

	private class Node {

		public String id;
		public String label;

		public Node(String id, String label) {
			this.id = id;
			this.label = label;
		}
	}

	protected void init() {
		map = new HashMap<String, Module>();

		List<Module> modules = Service.loadAll(Module.class);
		modules = new ArrayList<Module>(modules);

		loadModules(modules);

		List<Node> keys = new ArrayList<Node>();
		int count = 0;

		for (Module module : modules) {
			if (map.get(module.getId()) != null) {
				int i = GeneralUtil.compareVersions(module, map.get(module.getId()));
				if (i < 0) {
					module = map.get(module.getId());
				}
			} else {
				if (module.isDisplayed()) {
					count++;
				}
				Node node2 = new Node(module.getId(), module.getLabel());
				keys.add(node2);
			}
			map.put(module.getId(), module);
		}

		MainMenu mainMenu = MainMenu.getInstance();
		mainMenu.getWorkbooksPanel().removeAll();
		int rows = count / 2;
		if (count % 2 == 1) {
			rows++;
		}
		mainMenu.getWorkbooksPanel().setLayout(new GridLayout(rows, 4));

		Collections.sort(keys, new ModuleComparator());

		JMenu menu = WorkbooksView.getInstance().getWorkbooksMenu();
		menu.removeAll();

		for (Node node : keys) {
			String key = node.id;
			final Module module = map.get(key);

			if (module.isDisplayed()) {
				ModuleIcon moduleIcon = new ModuleIcon(module);

				Icon icon = null;
				if (module.getIconPath() != null) {
					icon = new ImageIcon(module.getIconPath());
				} else {
					icon = new ImageIcon(Icons.class.getResource("import2-32x32.png"));
				}

				moduleIcon.setToolTipText(module.getDescription());
				moduleIcon.setIcon(icon);
				moduleIcon.setPreferredSize(new Dimension(moduleIcon.getWidth(), 40));

				JMenuItem item = new JMenuItem(module.getLabel());
				menu.add(item);
				item.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						showModule(module);
					}
				});

				mainMenu.getWorkbooksPanel().add(moduleIcon);
				mainMenu.getWorkbooksPanel().add(new JLabel(module.getLabel()));
			}
		}

		mainMenu.getWorkbooksPanel().repaint();
	}

	public void showModule(Module module) {
		Template t = TemplateContext.get("templateThree");
		MainMenu mainMenu = MainMenu.getInstance();
		mainMenu.setTemplate(t);
		Map<String, JPanel> workviewMap = TemplateData.getInstance().getWorkviewMap();
		TemplatePanel templatePane = (TemplatePanel) workviewMap.get("0:0");
		templatePane.setModule(module);
		templatePane.removeAll();
		templatePane.add(module.getComponent(), 0);
		templatePane.validate();
		templatePane.repaint();
		templatePane.requestFocusInWindow();
	}

	private void loadExtra(List<Module> modules) {
		EntityManager em = EmfContext.create();
		List<ModuleIndex> moduleList = em.createNamedQuery("ModuleIndex.findAll").getResultList();
		for (ModuleIndex index : moduleList) {
			if (index.isEnabled()) {
				try {
					URL url = new URL(index.getUrl());
					String className = index.getClassName();
					JarClassLoader loader = new JarClassLoader(url);
					Class mClass = loader.loadClass(className);
					Module m = (Module) mClass.newInstance();
					modules.add(m);

				} catch (InstantiationException ex) {
					Logger.getLogger(ModuleLoaderImpl.class.getName()).log(Level.SEVERE, null, ex);
				} catch (IllegalAccessException ex) {
					Logger.getLogger(ModuleLoaderImpl.class.getName()).log(Level.SEVERE, null, ex);
				} catch (ClassNotFoundException ex) {
					Logger.getLogger(ModuleLoaderImpl.class.getName()).log(Level.SEVERE, null, ex);
				} catch (MalformedURLException ex) {
					Logger.getLogger(ModuleLoaderImpl.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
	}

	private void loadDefault(File f, List<Module> modules) {
		for (File file : f.listFiles()) {
			try {
				URL url = file.toURI().toURL();

				Module m = null;
				String className = null;
				try {
					URL url2 = new URL("jar:" + url.toString() + "!/");

					URL moduleFile = new URL(url2.toString() + "META-INF/module.xml");
					SAXBuilder parser = new SAXBuilder();
					Document doc = parser.build(moduleFile);
					Element module = doc.getRootElement();

					if (module != null) {
						Element _class = module.getChild("class");
						if (_class != null) {
							className = _class.getTextTrim();
						}
					}

					JarClassLoader test = new JarClassLoader(url2);
					Class mClass = test.loadClass(className);
					m = (Module) mClass.newInstance();

				} catch (InstantiationException ex) {
					Logger.getLogger(ModuleLoaderImpl.class.getName()).log(Level.SEVERE, null, ex);
				} catch (IllegalAccessException ex) {
					Logger.getLogger(ModuleLoaderImpl.class.getName()).log(Level.SEVERE, null, ex);
				} catch (ClassNotFoundException ex) {
					Logger.getLogger(ModuleLoaderImpl.class.getName()).log(Level.SEVERE, null, ex);
				} catch (JDOMException ex) {
					Logger.getLogger(ModuleLoaderImpl.class.getName()).log(Level.SEVERE, null, ex);
				} catch (IOException ex) {
					Logger.getLogger(ModuleLoaderImpl.class.getName()).log(Level.SEVERE, null, ex);
				}

				modules.add(m);

			} catch (MalformedURLException ex) {
				Logger.getLogger(ModuleLoaderImpl.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	private void loadModules(List<Module> modules) {
		File f = new File("modules");
		loadDefault(f, modules);

		f = WorkbooksApp.getApplication().getContext().getLocalStorage().getDirectory();
		String path = f.getAbsolutePath() + File.separator + "modules";
		f = new File(path);
		if (!f.exists()) {
			f.mkdir();
		}
		loadExtra(modules);
	}

	public void fireStartup() {
		for (String key : map.keySet()) {
			Module m = map.get(key);
			m.startup();
		}
	}

	public void fireShutdown() {
		if (map != null) {
			for (String key : map.keySet()) {
				Module m = map.get(key);
				m.shutdown();
			}
		}
	}

	public void fireRefresh(String k) {
		for (String key : map.keySet()) {
			Module m = map.get(key);
			m.refresh(k);
		}
	}

	public <T> T get(String key) {
		if (map == null) {
			throw new NullPointerException("moduleMap cannot be null");
		}
		return (T) map.get(key);
	}

	public Map<String, Module> getMap() {
		if (map == null) {
			throw new NullPointerException("module map cannot be null");
		}
		return map;
	}
}
