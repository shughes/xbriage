/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.services.impl;

import com.xbriage.services.*;
import com.xbriage.services.Service;
import com.xbriage.modules.journal.JournalModule;
import com.xbriage.modules.journal.actions.AccountActions;
import com.xbriage.modules.journal.actions.JournalActions;
import com.xbriage.modules.journal.proxy.ProxyContext;
import com.xbriage.modules.ofx.OfxActions;
import com.xbriage.modules.payees.PayeeActions;
import com.xbriage.modules.settings.SettingsActions;

/**
 *
 * @author shughes
 */
public class RefreshImpl implements Refresh {

	public void refresh(String key) {
		Service.load(ModuleLoader.class).fireRefresh(key);
		
		if (key.equals("RowActions.remove")) {
		} else if (key.equals("JournalActions.saveTableRow")) {
			PayeeActions.getInstance().refresh();
		} else if (key.equals("CategoryActions.dialogOk")) {
			JournalActions.getInstance().refreshAndSetRow();
			AccountActions.getInstance().refresh();
			SettingsActions.getInstance().refreshUserAccounts();
			OfxActions.getInstance().refreshNodes();
			OfxActions.getInstance().refreshCategoriesComboBox();
		} else if (key.equals("CategoryActions.removeCategory")) {
			JournalActions.getInstance().refreshAndSetRow();
			OfxActions.getInstance().refreshNodes();
			OfxActions.getInstance().refreshCategoriesComboBox();
		} else if (key.equals("OfxActions.afterConfirm")) {
			JournalActions.getInstance().setForceBalance(true);
			JournalActions.getInstance().refreshAndSetRow();
			PayeeActions.getInstance().refresh();
			JournalModule mod = Service.load(ModuleLoader.class).get("Journal.module");
			mod.getSearchField().setText("");
		} else if (key.equals("RestoreActions.restore")) {
			OfxActions.getInstance().refreshCategoriesComboBox();
			JournalActions.getInstance().clearAccountSelection();
			PayeeActions.getInstance().refresh();
			ProxyContext.getInstance().setup();
			AccountActions.getInstance().refresh();
			SettingsActions.getInstance().refreshUserAccounts();
			SettingsActions.getInstance().refreshBank();
		} else if (key.equals("ImportActions.doImport")) {
			OfxActions.getInstance().refreshCategoriesComboBox();
			JournalActions.getInstance().clearAccountSelection();
			PayeeActions.getInstance().refresh();
			ProxyContext.getInstance().setup();
			AccountActions.getInstance().refresh();
			SettingsActions.getInstance().refreshUserAccounts();
		} else if (key.equals("UndoableJournalEntry.refresh")) {
			JournalActions.getInstance().refreshAndSetRow();
		} else if (key.equals("PayeesModel.setValueAt")) {
			JournalActions.getInstance().refreshAndSetRow();
		} else if (key.equals("AccountActions.add")) {
			AccountActions.getInstance().refresh();
			SettingsActions.getInstance().refreshUserAccounts();
		} else if (key.equals("AccountActions.remove")) {
			AccountActions.getInstance().refresh();
			JournalActions.getInstance().clearAccountSelection();
			SettingsActions.getInstance().refreshUserAccounts();
			OfxActions.getInstance().refreshCategoriesComboBox();
		}
	}
}
