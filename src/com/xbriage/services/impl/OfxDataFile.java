/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.services.impl;

import com.xbriage.app.WorkbooksApp;
import com.xbriage.services.OfxData;
import com.xbriage.util.OfxTrnNode;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdesktop.application.LocalStorage;

/**
 *
 * @author shughes
 */
public class OfxDataFile implements OfxData {

	private List<OfxTrnNode> transactions;
	private String trnFile = "transactions.xml";

	public void setTransactions(List<OfxTrnNode> nodes) {
		this.transactions = nodes;
	}

	public List<OfxTrnNode> getTransactions() {
		if (transactions == null) {
			try {
				LocalStorage ls = WorkbooksApp.getApplication().getContext().getLocalStorage();
				String path = ls.getDirectory().getAbsolutePath() + File.separator + trnFile;
				File file = new File(path);
				if (file.isFile()) {
					transactions = (List<OfxTrnNode>) ls.load(trnFile);
				}
			} catch (IOException ex) {
				Logger.getLogger(OfxDataFile.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		if (transactions == null) {
			System.out.println("instantiating transactions");
			transactions = new ArrayList<OfxTrnNode>();
		}
		return transactions;
	}

	public void save() {
		LocalStorage ls = WorkbooksApp.getApplication().getContext().getLocalStorage();
		if (transactions != null) {
			try {
				ls.save(transactions, trnFile);
			} catch (IOException ex) {
				Logger.getLogger(OfxDataFile.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}
}
