/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.services.impl;

import com.xbriage.services.*;
import com.xbriage.context.EmfContext;
import com.xbriage.util.GeneralUtil;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hsqldb.Server;

/**
 *
 * @author shughes
 */
public class DbNetServer implements DbServer {

	private static DbNetServer instance = new DbNetServer();
	private Connection connection;
	private String serverProps;
	private String url = null;
	private String user = "sa";
	private String password = "";
	private Server server;

	public DbNetServer() {

	}

	public static DbNetServer getInstance() {
		return instance;
	}

	public void start() {
		url = "jdbc:hsqldb:hsql://localhost/xbriage.xdb";
		EmfContext.getInstance().setUrl(url);
		String dbFile = GeneralUtil.getDbFile();

		serverProps = "database.0=file:" + dbFile + ";dbname.0=xbriage.xdb";
		server = new Server();
		server.putPropertiesFromString(serverProps);
		server.setLogWriter(null);
		server.setErrWriter(null);
		server.start();

		try {
			Class.forName("org.hsqldb.jdbcDriver");
			connection = DriverManager.getConnection(url, user, password);
		} catch (SQLException ex) {
			Logger.getLogger(DbNetServer.class.getName()).log(Level.SEVERE, null, ex);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(this + ".setUp() error: " + e.getMessage());
		}
		
		System.out.println("net server");
	}

	public void stop() {
		try {
			connection.prepareStatement("SHUTDOWN").execute();
			server.stop();
			server = null;
		} catch (SQLException ex) {
			Logger.getLogger(DbNetServer.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
