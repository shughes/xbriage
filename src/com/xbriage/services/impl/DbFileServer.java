/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.services.impl;

import com.xbriage.services.*;
import com.xbriage.context.EmfContext;
import com.xbriage.util.GeneralUtil;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author shughes
 */
public class DbFileServer implements DbServer {

	private Connection connection;
	private String user = "sa";
	private String password = "";

	public void start() {
		String url = "jdbc:hsqldb:file:"+GeneralUtil.getDbFile();
		EmfContext.getInstance().setUrl(url);
		
		try {
			Class.forName("org.hsqldb.jdbcDriver");
			connection = DriverManager.getConnection(url, user, password);
		} catch (SQLException ex) {
			Logger.getLogger(DbNetServer.class.getName()).log(Level.SEVERE, null, ex);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(this + ".setUp() error: " + e.getMessage());
		}
		
		System.out.println("file server");
	}

	public void stop() {
		try {
			connection.prepareStatement("SHUTDOWN").execute();
		} catch (SQLException ex) {
			Logger.getLogger(DbFileServer.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
