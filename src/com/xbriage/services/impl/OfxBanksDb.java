/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.services.impl;

import com.xbriage.services.*;
import com.xbriage.util.*;
import com.xbriage.config.Config;
import com.xbriage.context.EmfContext;
import com.xbriage.domain.Bank;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

/**
 *
 * @author shughes
 */
public class OfxBanksDb implements OfxBanks {

	private List<OfxBank> banks = new ArrayList<OfxBank>();

	public OfxBanksDb() {
		execute();
	}

	private void execute() {
		System.out.println("get banks");

		EntityManager em = EmfContext.create();
		Map<String, Boolean> tracker = new HashMap<String, Boolean>();

		try {
			URL url1 = new URL("http://config.xbriage.com/Banks.xml");
			InputStream input = null;
			try {
				input = url1.openStream();
			} catch (IOException ex) {
				Logger.getLogger(OfxClient.class.getName()).log(Level.SEVERE, null, ex);
			}
			if (input == null) {
				input = Config.class.getResourceAsStream("Banks.xml");
			}

			BufferedReader reader = new BufferedReader(new InputStreamReader(input));
			SAXBuilder sax = new SAXBuilder();
			Document response = sax.build(reader);
			Element root = response.getRootElement();
			List<Element> bankList = root.getChildren("bank");

			em.getTransaction().begin();
			for (Element bankElement : bankList) {
				Element id = bankElement.getChild("id");
				Element caps = bankElement.getChild("caps");
				Element fid = bankElement.getChild("fid");
				Element fiorg = bankElement.getChild("fiorg");
				Element url = bankElement.getChild("url");

				OfxBank bank = new OfxBank();
				bank.setCaps(caps.getTextTrim());
				bank.setFid(fid.getTextTrim());
				bank.setFiorg(fiorg.getTextTrim());
				bank.setUrl(url.getTextTrim());
				bank.setName(id.getTextTrim());

				tracker.put(bank.getFid(), true);

				Query q = em.createNamedQuery("Bank.findByFid").setParameter("fid", bank.getFid());
				List<Bank> result = q.getResultList();
				if (result.size() == 0) {
					Bank dbBank = new Bank(bank.getName(), bank.getFiorg(), bank.getCaps(), bank.getFid(), bank.getUrl());
					em.persist(dbBank);
				}
			}
			em.getTransaction().commit();

		} catch (JDOMException ex) {
			Logger.getLogger(OfxClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(OfxClient.class.getName()).log(Level.SEVERE, null, ex);
		}

		List<Bank> dbBanks = em.createNamedQuery("Bank.findAll").getResultList();
		for (Bank dbBank : dbBanks) {
			if (tracker.get(dbBank.getFid()) == null) {
				em.remove(dbBank);
				em.getTransaction().begin();
				em.getTransaction().commit();
			} else {
				OfxBank bank = new OfxBank();
				bank.setCaps(dbBank.getCaps());
				bank.setFid(dbBank.getFid());
				bank.setFiorg(dbBank.getFiorg());
				bank.setName(dbBank.getName());
				bank.setUrl(dbBank.getUrl());
				banks.add(bank);
			}
		}
		
		System.out.println("end get banks");
	}

	public List<OfxBank> getBanks() {
		return banks;
	}
}
