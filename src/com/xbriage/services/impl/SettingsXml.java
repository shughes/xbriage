/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.services.impl;

import com.xbriage.services.*;
import com.xbriage.app.WorkbooksApp;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

/**
 *
 * @author shughes
 */
public class SettingsXml implements Settings {

	Map<String, String> settingsMap = null;
	String file = null;

	public SettingsXml() {
		file = WorkbooksApp.getApplication().getContext().getLocalStorage().getDirectory().getAbsolutePath();
		file = file + File.separator + "settings.xml";
	}

	public Map<String, String> get() {
		if (settingsMap == null) {
			settingsMap = new HashMap<String, String>();
			File f = new File(file);
			if (f.exists()) {
				BufferedReader reader = null;
				try {
					reader = new BufferedReader(new FileReader(file));
					SAXBuilder parser = new SAXBuilder();
					Document response = parser.build(reader);
					Element settings = response.getRootElement();
					if (settings != null) {
						List<Element> propertyList = settings.getChildren("property");
						if (propertyList != null) {
							for (Element property : propertyList) {
								Element key = property.getChild("key");
								if (key != null) {
									Element value = property.getChild("value");
									if (value != null) {
										settingsMap.put(key.getTextTrim(), value.getTextTrim());
									}
								}
							}
						}
					}

				} catch (JDOMException ex) {
					Logger.getLogger(SettingsXml.class.getName()).log(Level.SEVERE, null, ex);
				} catch (IOException ex) {
					Logger.getLogger(SettingsXml.class.getName()).log(Level.SEVERE, null, ex);
				} finally {
					try {
						reader.close();
					} catch (IOException ex) {
						Logger.getLogger(SettingsXml.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			}
		}
		return settingsMap;
	}

	public void save() {
		PrintWriter out = null;
		try {
			XMLOutputter serializer = setupOutputter();
			out = new PrintWriter(new FileWriter(file));
			Element settings = new Element("settings");

			for (String k : settingsMap.keySet()) {
				Element property = new Element("property");
				settings.addContent(property);

				Element key = new Element("key");
				property.addContent(key);
				key.setText(k);

				String v = settingsMap.get(k);
				Element value = new Element("value");
				property.addContent(value);
				value.setText(v);
			}

			Document request = new Document(settings);
			serializer.output(request, out);
		} catch (IOException ex) {
			Logger.getLogger(SettingsXml.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			out.close();
		}
	}

	private XMLOutputter setupOutputter() {
		XMLOutputter serializer = new XMLOutputter();
		Format format = Format.getPrettyFormat();
		serializer.setFormat(format);
		return serializer;
	}
}
