/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.services;

import javax.swing.JPanel;

/**
 *
 * @author shughes
 */
public interface Template {
	public void setup();
	public String getId();
	public JPanel getPanel();
}
