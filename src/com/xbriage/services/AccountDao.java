/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.services;

import com.xbriage.domain.Account;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author shughes
 */
public interface AccountDao {
	public String concatName(Account account);
	public BigDecimal getBalance(Account account, Date start, Date end, boolean debitAdds);
	public BigDecimal getBalance(Account account);
	public BigDecimal getBalance(Account account, boolean debitAdds);
	public Account breakName(String oldName);
}
