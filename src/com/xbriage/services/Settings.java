/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.services;

import java.util.Map;

/**
 *
 * @author shughes
 */
public interface Settings {
	
	public Map<String, String> get();
	public void save();

}
