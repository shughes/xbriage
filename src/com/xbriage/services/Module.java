/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.services;

import java.awt.Component;
import java.net.URL;

/**
 *
 * @author shughes
 */
public interface Module {
	public Component getComponent();
	public String getId();
	public String getVersion();
	public String getLabel();
	public String getDescription();
	public URL getIconPath();
	public URL getHelpDoc();
	public boolean isDisplayed();
	public void afterDrop();
	public void shutdown();
	public void refresh(String key);
	public void startup();
}
