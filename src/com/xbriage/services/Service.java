/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.services;

import com.xbriage.config.Config;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

/**
 *
 * @author shughes
 */
public class Service {

	private Element services;
	private Map<String, Object> map = new HashMap<String, Object>();
	private Map<String, List<Object>> listMap = new HashMap<String, List<Object>>();
	private static final Service instance = new Service();

	protected Service() {
		try {
			InputStream input = Config.class.getResourceAsStream("Services.xml");
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(input);
			services = doc.getRootElement();

		} catch (JDOMException ex) {
			Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public static <T> T load(Class<T> c) {
		return instance._load(c);
	}

	private <T> T _load(Class<T> c) {
		String name = c.getName();
		Object obj = null;

		if (map.get(name) != null) {
			obj = map.get(name);
		} else {
			List<Element> serviceList = services.getChildren("service");
			for (Element service : serviceList) {
				Element interfaceE = service.getChild("interface");
				if (interfaceE.getTextTrim().equals(name)) {
					try {
						Element classE = service.getChild("class");
						Class c2 = Class.forName(classE.getTextTrim());
						obj = c2.newInstance();
						map.put(name, obj);
					} catch (InstantiationException ex) {
						Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
					} catch (IllegalAccessException ex) {
						Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
					} catch (ClassNotFoundException ex) {
						Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			}
		}

		return (T) obj;
	}

	public static List loadAll(Class c) {
		return instance._loadAll(c);
	}

	private List<Object> _loadAll(Class c) {
		String name = c.getName();
		List<Object> list = null;

		if (listMap.get(name) != null) {
			list = listMap.get(name);
		} else {
			List<Element> serviceList = services.getChildren("serviceList");
			for (Element sList : serviceList) {
				Element interfaceE = sList.getChild("interface");
				if (interfaceE.getTextTrim().equals(name)) {
					Element classes = sList.getChild("classes");
					List<Element> classList = classes.getChildren("class");
					list = new ArrayList<Object>();
					for (Element classE : classList) {
						try {
							Class c2 = Class.forName(classE.getTextTrim());
							Object obj = c2.newInstance();
							list.add(obj);
						} catch (InstantiationException ex) {
							Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
						} catch (IllegalAccessException ex) {
							Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
						} catch (ClassNotFoundException ex) {
							Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
						}
					}
					listMap.put(name, list);
				}
			}
		}

		return list;
	}
}
