/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.services;

import java.util.Map;

/**
 *
 * @author shughes
 */
public interface ModuleLoader {
	public void setup();
	public void fireStartup();
	public void fireShutdown();
	public void fireRefresh(String key);
	public void showModule(Module m);
	public <T> T get(String key);
	public Map<String, Module> getMap();
}
