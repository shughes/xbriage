/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author shughes
 */
@Entity
@Table(name = "Book")
@NamedQueries({
	@NamedQuery(name = "Book.findById", query = "SELECT b FROM Book b WHERE b.id = :id"), 
	@NamedQuery(name = "Book.findByModuleName", query = "SELECT b FROM Book b WHERE b.moduleName = :module"), 
	@NamedQuery(name = "Book.findByRow", query = "SELECT b FROM Book b WHERE b.row = :row"), 
	@NamedQuery(name = "Book.findByColumn", query = "SELECT b FROM Book b WHERE b.column = :column") 
})
public class Book implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "moduleName", nullable = false)
	private String moduleName;
	@Column(name = "_row", nullable = false)
	private int row;
	@Column(name = "_column", nullable = false)
	private int column;
	@ManyToOne
	@JoinColumn(name = "workviewId")
	private Workview workview;
	@Column(name = "width", nullable = false)
	private double width;
	@Column(name = "height", nullable = false)
	private double height;
	
	public Book() {
	}

	public Book(Integer id) {
		this.id = id;
	}

	public Book(Integer id, String module, int row, int column, Integer workviewId) {
		this.id = id;
		this.moduleName = module;
		this.row = row;
		this.column = column;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String objectName) {
		this.moduleName = objectName;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Book)) {
			return false;
		}
		Book other = (Book) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.xbriage.domain.Book[id=" + id + "]";
	}

	public Workview getWorkview() {
		return workview;
	}

	public void setWorkview(Workview workview) {
		this.workview = workview;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}


}
