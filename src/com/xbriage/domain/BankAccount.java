/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author shughes
 */
@Entity
@Table(name = "BankAccount")
@NamedQueries({
@NamedQuery(name = "BankAccount.findAll", query = "select a from BankAccount a")
})
public class BankAccount implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "bankId")
	private String bankId;
	@Column(name = "type")
	private String type;
	@ManyToOne
	@JoinColumn(name = "accountId")
	private Account userAccount;
	
	public BankAccount() {
		
	}
	
	public BankAccount(String bankId, String type, Account userAccount) {
		this.bankId = bankId;
		this.type = type;
		this.userAccount = userAccount;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof BankAccount)) {
			return false;
		}
		BankAccount other = (BankAccount) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.xbriage.domain.BankAccount[id=" + id + "]";
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Account getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(Account userAccount) {
		this.userAccount = userAccount;
	}

}
