/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author shughes
 */
@Entity
@Table(name = "Bank")
@NamedQueries ({
	@NamedQuery(name = "Bank.findByFid", query = "select b from Bank b where b.fid = :fid"),
	@NamedQuery(name = "Bank.findAll", query = "select b from Bank b")
})
public class Bank implements Serializable {
	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "fiorg", nullable = false)
	private String fiorg;
	@Column(name = "caps", nullable = false)
	private String caps;
	@Column(name = "fid", nullable = false)
	private String fid;
	@Column(name = "name", nullable = false)
	private String name;
	@Column(name = "url", nullable = false)
	private String url;
	
	public Bank() {
		
	}
	
	public Bank(String name, String fiorg, String caps, String fid, String url) {
		this.name = name;
		this.fiorg = fiorg;
		this.caps = caps;
		this.fid = fid;
		this.url = url;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFiorg() {
		return fiorg;
	}

	public void setFiorg(String fiorg) {
		this.fiorg = fiorg;
	}

	public String getCaps() {
		return caps;
	}

	public void setCaps(String caps) {
		this.caps = caps;
	}

	public String getFid() {
		return fid;
	}

	public void setFid(String fid) {
		this.fid = fid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
