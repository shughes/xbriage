/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author shughes
 */
@Entity
@Table(name = "Workview")
@NamedQueries({
	@NamedQuery(name = "Workview.findRoot", query = "SELECT w FROM Workview w WHERE w.isRoot = 1"),
	@NamedQuery(name = "Workview.findAll", query = "SELECT w FROM Workview w"),
	@NamedQuery(name = "Workview.findAllButRoot", query = "select w from Workview w where w.isRoot != 1"),
	@NamedQuery(name = "Workview.findById", query = "SELECT w FROM Workview w WHERE w.id = :id"), 
	@NamedQuery(name = "Workview.findByName", query = "SELECT w FROM Workview w WHERE w.name = :name")
})
public class Workview implements Serializable {
	public static final String NO_TEMPLATE = "empty";
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "name", nullable = false)
	private String name;
	@Column(name = "isRoot", nullable = false)
	private Integer isRoot;
	@OneToMany(mappedBy = "parent", cascade = {CascadeType.REMOVE})
	private List<Workview> children;
	@ManyToOne
	@JoinColumn(name = "parentId")
	private Workview parent;
	@OneToMany(mappedBy = "workview", cascade = {CascadeType.REMOVE})
	private List<Book> books;
	@Column(name = "template", nullable = false)
	private String template;
	@Column(name = "width")
	private int width;
	@Column(name = "height")
	private int height;
	
	public Workview() {
	}

	public Workview(Integer id) {
		this.id = id;
	}

	public Workview(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Workview)) {
			return false;
		}
		Workview other = (Workview) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.xbriage.domain.Workview[id=" + id + "]";
	}

	public Integer getIsRoot() {
		return isRoot;
	}

	public void setIsRoot(Integer isRoot) {
		this.isRoot = isRoot;
	}

	public List<Workview> getChildren() {
		return children;
	}

	public void setChildren(List<Workview> children) {
		this.children = children;
	}

	public Workview getParent() {
		return parent;
	}

	public void setParent(Workview parent) {
		this.parent = parent;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

}
