/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author shughes
 */
@Entity
@Table(name = "property")
@NamedQueries({
@NamedQuery(name = "Property.findAll", query = "select a from Property a")
})
public class Property implements java.io.Serializable {
	@Id
	@Column(name = "id")
	@GeneratedValue
	private Integer id;
	@Column(name = "_key")
	private String key;
	@Column(name = "_value")
	private String value;

	public Property() {
	}
	
	public Property(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
