/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author shughes
 */
@Entity
@Table(name = "Budget")
@NamedQueries({
@NamedQuery(name = "Budget.findAll", query = "select b from Budget b order by id")
})
public class Budget implements Serializable {

	public static final int INCOME = 0;
	public static final int EXPENSE = 1;
	public static final int MONTHLY = 0;
	public static final int TWICE_A_MONTH = 1;
	public static final int EVERY_OTHER_WEEK = 2;
	public static final int WEEKLY = 3;
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer id;
	@ManyToOne
	@JoinColumn(name = "accountId")
	private Account account;
	@Column(name = "amount")
	private BigDecimal amount;
	@Column(name = "startDate")
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date start;
	@Column(name = "endDate")
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date end;

	public void setAccount(Account account) {
		this.account = account;
	}

	public Account getAccount() {
		return account;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Budget)) {
			return false;
		}
		Budget other = (Budget) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.xbriage.domain.Budget[id=" + id + "]";
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}
}
