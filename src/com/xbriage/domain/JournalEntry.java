/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author shughes
 */
@Entity
@Table(name = "JournalEntry")
@NamedQueries({
	@NamedQuery(name = "JournalEntry.findById", query = "SELECT j FROM JournalEntry j WHERE j.id = :id"),
	@NamedQuery(name = "JournalEntry.findAll", query = "select j from JournalEntry j")
})
public class JournalEntry implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "fitId")
	private String fitId;
	@OneToMany(mappedBy = "journalEntry", cascade = {CascadeType.REMOVE, CascadeType.MERGE, CascadeType.PERSIST})
	private List<JournalEntryRow> rows;
	@ManyToOne
	@JoinColumn(name = "payeeId")
	private Payee payee;
	@Column(name = "memo")
	private String memo;
	@Column(name= "_date")
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date date;
	@Column(name = "entryType")
	private int type;
	@Column(name = "checkNo")
	private int checkNo;
	
	public JournalEntry() {
	}

	public JournalEntry(Integer id) {
		this.id = id;
	}
	
	public JournalEntry(JournalEntry entry) {
		this.type = entry.getType();
		this.date = entry.getDate();
		this.payee = entry.getPayee();
		this.memo = entry.getMemo();
		this.checkNo = entry.getCheckNo();
		this.fitId = entry.getFitId();
		List<JournalEntryRow> tmp = new ArrayList<JournalEntryRow>();
		for(JournalEntryRow otherRow : entry.getRows()) {
			JournalEntryRow row = new JournalEntryRow(otherRow);
			tmp.add(row);
			row.setJournalEntry(this);
		}
		this.rows = tmp;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof JournalEntry)) {
			return false;
		}
		JournalEntry other = (JournalEntry) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.xbriage.domain.JournalEntry[id=" + id + "]";
	}

	public List<JournalEntryRow> getRows() {
		return rows;
	}

	public void setRows(List<JournalEntryRow> rows) {
		this.rows = rows;
	}

	public Payee getPayee() {
		return payee;
	}

	public void setPayee(Payee payee) {
		this.payee = payee;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getCheckNo() {
		return checkNo;
	}

	public void setCheckNo(int checkNo) {
		this.checkNo = checkNo;
	}

	public String getFitId() {
		return fitId;
	}

	public void setFitId(String fitId) {
		this.fitId = fitId;
	}
	
}
