/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author shughes
 */
@Entity
@Table(name = "JournalEntryRow")
@NamedQueries({
@NamedQuery(name = "JournalEntryRow.findAll", query = "select r from JournalEntryRow r"),
@NamedQuery(name = "JournalEntryRow.findById", query = "SELECT j FROM JournalEntryRow j WHERE j.id = :id"),
@NamedQuery(name = "JournalEntryRow.findByAmount", query = "SELECT j FROM JournalEntryRow j WHERE j.amount = :amount")
})
public class JournalEntryRow implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "amount", nullable = false)
	private BigDecimal amount;
	@ManyToOne
	@JoinColumn(name = "journalEntryId")
	private JournalEntry journalEntry;
	@ManyToOne
	@JoinColumn(name = "accountId")
	private Account account;
	@Column(name = "isDebitEntry", nullable = false)
	private Integer debitEntry;
	@Column(name = "memo")
	private String memo;

	public JournalEntryRow() {
	}

	public JournalEntryRow(Integer id) {
		this.id = id;
	}

	public JournalEntryRow(Integer id, BigDecimal amount) {
		this.id = id;
		this.amount = amount;
	}
	
	public JournalEntryRow(JournalEntryRow row) {
		this.amount = row.getAmount();
		this.debitEntry = row.isDebitEntry();
		this.account = row.getAccount();
		this.memo = row.getMemo();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof JournalEntryRow)) {
			return false;
		}
		JournalEntryRow other = (JournalEntryRow) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.xbriage.domain.JournalEntryRow[id=" + id + "]";
	}

	public JournalEntry getJournalEntry() {
		return journalEntry;
	}

	public void setJournalEntry(JournalEntry journalEntry) {
		this.journalEntry = journalEntry;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Integer isDebitEntry() {
		return debitEntry;
	}

	public void setDebitEntry(Integer isDebitEntry) {
		this.debitEntry = isDebitEntry;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}
}
