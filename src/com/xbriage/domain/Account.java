/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author shughes
 */
@Entity
@Table(name = "Account")
@NamedQueries({
@NamedQuery(name = "Account.findDefault", query = "select a from Account a where a._default = true"),
@NamedQuery(name = "Account.findAllButRoot", query = "select a from Account a where a.root != 1"),
@NamedQuery(name = "Account.findAll", query = "select a from Account a"),
@NamedQuery(name = "Account.findAllButRootAndAccount", query = "select a from Account a where a.root != 1 and a != :account"),
@NamedQuery(name = "Account.findRoot", query = "select a from Account a where a.root = 1"),
@NamedQuery(name = "Account.findById", query = "SELECT a FROM Account a WHERE a.id = :id"),
@NamedQuery(name = "Account.findById2", query = "select a from Account a where a.id = ?0"),
@NamedQuery(name = "Account.findAllButRootAndUserAccounts", query = "select a from Account a where a.root != 1 and a.userAccount != 1"),
@NamedQuery(name = "Account.findUserAccounts", query = "SELECT a FROM Account a WHERE a.userAccount = 1 order by name"),
@NamedQuery(name = "Account.findByUserAccount", query = "SELECT a FROM Account a WHERE a.userAccount = :userAccount"),
@NamedQuery(name = "Account.findUserAccountByName", query = "select a from Account a where a.name = :name and a.userAccount = 1"),
@NamedQuery(name = "Account.findByName", query = "SELECT a FROM Account a WHERE a.name = :name"),
@NamedQuery(name = "Account.findByTypeName", query = "SELECT a FROM Account a WHERE a.typeName = :typeName"),
@NamedQuery(name = "Account.findByNameAndParent", query = "select a from Account a where a.name = ?0 and a.parent = ?1")
})
public class Account implements Serializable {

	public static final String EXPENSE = "Expense";
	public static final String FIND_ALL = "Account.findAll";
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "name", nullable = false)
	private String name;
	@Column(name = "typeName", nullable = false)
	private String typeName;
	@Column(name = "isUserAccount", nullable = false)
	private Integer userAccount;
	@Lob
	@Column(name = "memo")
	private String memo;
	@OneToMany(mappedBy = "account", cascade = {CascadeType.REMOVE})
	private List<JournalEntryRow> journalEntryRows;
	@Column(name = "isRoot", nullable = false)
	private Integer root;
	@OneToMany(mappedBy = "parent")
	private List<Account> children;
	@ManyToOne
	@JoinColumn(name = "parentId")
	private Account parent;
	@Column(name = "_default", nullable = false)
	private boolean _default = false;
	@OneToMany(mappedBy = "userAccount", cascade = {CascadeType.REMOVE})
	private List<BankAccount> bankAccounts;
	@OneToMany(mappedBy = "account", cascade = {CascadeType.REMOVE})
	private List<Budget> budgets;

	public Account() {
	}

	public Account(Integer id) {
		this.id = id;
	}

	public Account(String name, String typeName, Integer userAccount, Integer root, Account parent) {
		this.name = name;
		this.typeName = typeName;
		this.userAccount = userAccount;
		this.root = root;
		this.parent = parent;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Account)) {
			return false;
		}
		Account other = (Account) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return name;
	}

	public List<JournalEntryRow> getJournalEntryRows() {
		return journalEntryRows;
	}

	public void setJournalEntryRows(List<JournalEntryRow> journalEntryRows) {
		this.journalEntryRows = journalEntryRows;
	}

	public Integer isUserAccount() {
		return userAccount;
	}

	public void setUserAccount(Integer isUserAccount) {
		this.userAccount = isUserAccount;
	}

	public Integer isRoot() {
		return root;
	}

	public void setRoot(Integer isRoot) {
		this.root = isRoot;
	}

	public List<Account> getChildren() {
		return children;
	}

	public void setChildren(List<Account> children) {
		this.children = children;
	}

	public Account getParent() {
		return parent;
	}

	public void setParent(Account parent) {
		this.parent = parent;
	}
	
	public boolean isDefault() {
		return _default;
	}
	
	public void setDefault(boolean _default) {
		this._default = _default;
	}

	public List<BankAccount> getBankAccounts() {
		return bankAccounts;
	}

	public void setBankAccounts(List<BankAccount> bankAccounts) {
		this.bankAccounts = bankAccounts;
	}

	public List<Budget> getBudgets() {
		return budgets;
	}
	
	public void setBudgets(List<Budget> budgets) {
		this.budgets = budgets;
	}
	
}
