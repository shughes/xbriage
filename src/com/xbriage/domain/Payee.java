/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author shughes
 */
@Entity
@Table(name = "payee")
@NamedQueries({
	@NamedQuery(name = "Payee.findDefault", query = "select p from Payee p where p._default = true"),
	@NamedQuery(name = "Payee.findAll", query = "select p from Payee p order by name"),
	@NamedQuery(name = "Payee.findById", query = "SELECT p FROM Payee p WHERE p.id = :id"), 
	@NamedQuery(name = "Payee.findByName", query = "SELECT p FROM Payee p WHERE p.name = :name"),
	@NamedQuery(name = "Payee.findByName2", query = "SELECT p FROM Payee p WHERE p.name = ?0")
})
public class Payee implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "name", nullable = false)
	private String name;
	@OneToMany(mappedBy = "payee")
	private List<JournalEntry> journalEntries;
	@Column(name = "_default", nullable = false)
	private boolean _default;

	public Payee() {
	}
	
	public Payee(Payee oldPayee) {
		this.name = oldPayee.getName();
		this._default = oldPayee.isDefault();
		List<JournalEntry> newEntries = new ArrayList<JournalEntry>(oldPayee.getJournalEntries());
		this.journalEntries = newEntries;
	}
	
	public Payee(String name, boolean _default) {
		this.name = name;
		this._default = _default;
	}

	public Payee(Integer id) {
		this.id = id;
	}

	public Payee(Integer id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public Payee(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Payee)) {
			return false;
		}
		Payee other = (Payee) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.xbriage.domain.Payee[id=" + id + "]";
	}

	public List<JournalEntry> getJournalEntries() {
		return journalEntries;
	}

	public void setJournalEntries(List<JournalEntry> journalEntries) {
		this.journalEntries = journalEntries;
	}

	public boolean isDefault() {
		return _default;
	}

	public void setDefault(boolean _default) {
		this._default = _default;
	}

}
