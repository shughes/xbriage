/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xbriage.security;

import com.kagi.krm.KrmException;
import com.kagi.krm.KrmInitStringMaker;
import com.kagi.krm.KrmLoader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author shughes
 */
public class Register {
	public Register() {
		try {
			KrmInitStringMaker stringMaker = new KrmInitStringMaker();
			stringMaker.addPrice("USD", "9.95");
			stringMaker.addProductDisplayName("en", "Xbriage");
			stringMaker.setVendorID("6FGJV");
			stringMaker.setProductDbName("xbriage");
			String string = stringMaker.getInitString();
			
			RegListener listener = new RegListener();
			KrmLoader loader = new KrmLoader(string, listener);
			listener.setLoader(loader);
			loader.invokeUserInterface();
			
		} catch (KrmException ex) {
			Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
		}
		
	}
	
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			Register reg = new Register();
			
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

}
