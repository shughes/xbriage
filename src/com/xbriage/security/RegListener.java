/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.security;

import com.kagi.krm.KrmException;
import com.kagi.krm.KrmListener;
import com.kagi.krm.KrmLoader;
import com.xbriage.context.SetupContext;
import java.util.Map;

/**
 *
 * @author shughes
 */
public class RegListener implements KrmListener {

	private KrmLoader loader;

	public void krmDataEntryWindowIsUp() {
	}

	public void krmTerminated(int arg0, KrmException arg1) {
		String sn = loader.getRegCode();
		String email = loader.getUserName();

		if (sn != null && email != null) {
			Map<String, String> settings = SetupContext.getInstance().getSettings();
			settings.put("Security.serialNumber", sn);
			settings.put("Security.email", email);
		}
	}

	public KrmLoader getLoader() {
		return loader;
	}

	public void setLoader(KrmLoader loader) {
		this.loader = loader;
	}
}
