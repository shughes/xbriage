/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xbriage.security;

import com.xbriage.services.Settings;
import com.xbriage.util.DesEncrypter;
import com.xbriage.util.GeneralUtil;
import com.xbriage.services.Service;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;

/**
 *
 * @author shughes
 */
public class GeneralSecurity {

	public static boolean dateExpired(String key) {
		String sDate = DesEncrypter.getInstance().decrypt(key);
		boolean showVerify = true;

		if (sDate.startsWith("date:")) {
			sDate = sDate.substring(5, sDate.length());
			Date date = GeneralUtil.stringToDate(sDate);
			if (date == null) {
				date = new Date();
			}
			Date today = new Date();
			if (date.after(today)) {
				showVerify = false;
			}
		}

		return showVerify;
	}
	
	public static boolean verified(String entry, String email) {
		return DecodeRegCode.verify(entry, email);
	}
	
	public static void resetToday() {
		Map<String, String> settings = Service.load(Settings.class).get();
		Date today = new Date();
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(today);
		String end = GeneralUtil.dateToString(cal.getTime());
		end = "date:" + end;
		settings.put("Security.expireDate", DesEncrypter.getInstance().encrypt(end));
	}

	public static void resetDate() {
		Map<String, String> settings = Service.load(Settings.class).get();
		Date today = new Date();
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(today);
		int month = cal.get(Calendar.MONTH);
		if (month == 11) {
			cal.roll(Calendar.YEAR, true);
		}
		cal.roll(Calendar.MONTH, true);

		String end = GeneralUtil.dateToString(cal.getTime());
		end = "date:" + end;
		settings.put("Security.expireDate", DesEncrypter.getInstance().encrypt(end));
	}
}
